# GPU Computing Questions

### Differenze e analogie tra i modelli SIMD e SIMT, fare un esempio di codice SIMD

Nel modello SIMD sono presenti diverse unità di computazione con accesso ad una memoria privata per i dati e un accesso a memoria globale unica per le istruzioni. Viene eseguita la stessa istruzione da tutte le unità di computazione, ad ogni passo un processore centrale invia un’istruzione alle unità che leggono dati privati. Applicazioni con alto grado di parallelismo ne traggono grande beneficio (multimedia, computer graphics, simulazioni, etc.).

Nel modello SIMT (Single Instruction Multiple Threads) viene implementato il multithreading: in sistemi operativi multitasking rappresenta un diffuso modello di programmazione ed esecuzione che consente a thread multipli di coesistere all’interno del contesto di un processo in esecuzione. Questo modello è usato in parallel computing dove si
combina il modello SIMD con il multithreading. Attraverso una condivisione di risorse ma esecuzioni indipendenti si fornisce al programmatore una utile astrazione del concetto di esecuzione concorrente.

Il modello SIMT prevede tre caratteristiche chiave che SIMD non contempla:

1. Ogni thread ha il proprio instruction address counter;
2. Ogni thread ha il proprio register state e in generale un register set;
3. Ogni thread può avere un execution path indipendente.

In architettura SIMT, una differenza chiave è che SIMD espone l’intero set di dati (vettore) all’istruzione che deve essere eseguita, mentre SIMT specifica il comportamento di esecuzione e branching di ogni singolo thread.

```pseudocode
// SSE
__m128 a = _mm_set_ps (4, 3, 2, 1);
__m128 b = _mm_set_ps (8, 7, 6, 5);
__m128 c = _mm_add_ps (a, b);

// Multithread
float a[4] = {1, 2, 3, 4};
float b[4] = {5, 6, 7, 8}, c[4];
{
	int id = ... ; // my thread ID
	c[id] = a[id] + b[id];
}
```



### Cos'è la Pinned Memory

La memoria host allocata è per default paginabile (soggetta a page fault per effetto della virtual memory gestita dal sistema operativo e non nota al device). La virtual memory offre l’illusione di avere molta più memoria di quella  fisicamente disponibile come nel caso della cache L1 (memoria però on-chip). La GPU non può avere accesso sicuro a
dati in memoria virtuale, per cui il driver CUDA prima di trasferire alloca temporaneamente memoria page-locked o pinned e poi effettua il trasferimento sicuro al device.

La pinned memory può essere acceduta direttamente dal device, con modalità di accesso asincrona, e può essere letta e scritta con più alta bandwidth rispetto alla memoria paginabile. Eccessi di allocazione di pinned memory potrebbero far degradare le prestazioni dell’host (ridurre la memoria paginabile inficia l’uso della virtual memory).



### Cosa sono gli Stream e a cosa servono, fare un esempio di utilizzo degli Stream

Uno stream CUDA è riferito a sequenze di operazioni CUDA asincrone che vengono eseguite sul device nell’ordine stabilito dal codice host. L’esecuzione di operazioni in uno stream è sempre asincrona rispetto all’host e le operazioni appartenenti a stream distinti non hanno restrizioni vicendevoli sull’ordine di esecuzione, sono indipendenti.

Esistono 2 tipi di stream:

- Dichiarato implicitamente (NULL stream o default stream);
- Dichiarato esplicitamente (non-NULL stream).

Il default stream interviene quando non si usa esplicitamente uno stream, mentre i non-NULL stream sono usati per  sovrapporre articolate computazioni host e device, trasferimento di dati o computazioni concorrenti.

```c
__global__ void kernel(float *x, int n) {
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    for (int i = tid; i < n; i += blockDim.x * gridDim.x) {
        x[i] = sqrt(pow(2,i));
    }
}
. . .
cudaStream_t streams[num_streams];
float *data[num_streams];
for (int i = 0; i < num_streams; i++) {
    cudaStreamCreate(&streams[i]);
    cudaMalloc(&data[i], N * sizeof(float));
	kernel<<<1, 64, 0, streams[i]>>>(data[i], N);
}
```



### Spiegare sincronizzazione globale e a livello di blocco. Cosa è un deadlock?

Quando si hanno diversi flussi di esecuzione che cooperano tra loro è importante stabilire un certo grado di sincronizzazione per garantire consistenza e un corretto risultato delle istruzioni. Sincronizzare a livello globale, significa sincronizzare a livello host le esecuzioni kernel su GPU, ovvero l'host aspetta che i thread GPU lanciati terminino la loro esecuzione prima di proseguire. Sincronizzando a livello di blocco, invece, significa scendere di un livello e mettere tale sincronizzazione interna ad un blocco, tra i thread presenti in esso, nel caso in cui essi debbano attingere ad una shared memoy ed eseguano letture e scritture di dati reciproche, Un deadlock rappresenta una situazione di stallo all'interno del sistema, ovvero una situazione in cui ogni thread è fermo e non può lavorare perchè sta aspettando la fine di un altro thread.



### Cosa è la Shared Memory e come è organizzata?

La shared memory è una memoria programmabile con bandwidth molto più alta e minor latenza della local o global memory. E’ suddivisa in moduli della stessa ampiezza, chiamati bank, in grado di soddisfare simultaneamente, in un singolo ciclo di clock, una richiesta di accesso fatta di n indirizzi che riguardino n distinti bank.

Ogni SM ha una quantità limitata di shared memory che viene ripartita tra i blocchi di thread con cui condivide anche lifetime e scope.

La shared memory serve come base per la comunicazione inter-thread, i thread in un block possono cooperare
scambiandosi dati memorizzati in shared memory.

La SMEM dell’architettura Fermi sono suddivisi in blocchi da 4 byte (chiamate word) o da 32 bit (64 da Kepler in poi), ogni word può contenere 1 int, 1 float, 2 shoth, 4 char, 2 half float, ... Quando viene richiesto un singolo byte viene comunque letta la word per intero cui il byte appartiene. Dati 32 bank, ogni word è memorizzata in bank distinti a gruppi di 32.



### Qual è lo schema di utilizzo di una libreria CUDA?

Lo schema di utilizzo di una libreria CUDA consiste in una serie di operazioni particolari:

- Creare un handle per la libreria;
- Allocare la memoria richiesta per l'operazione;
- Inizializzare tale memoria;
- Configurare le computazioni
- Eseguire l'operazione di libreria;
- Recuperare il risultato dell'operazione;
- Convertire, se necessario, i dati in un formato coerente;
- Rilasciare le risorse allocate precedentemente.



### Fare un esempio di programmazione multi-GPU

```c
int ngpus = 2;

float **d_src = (float **)malloc(sizeof(float) * ngpus);
float **d_rcv = (float **)malloc(sizeof(float) * ngpus);
float **h_src = (float **)malloc(sizeof(float) * ngpus);

cudaStream_t *stream = (cudaStream_t *)malloc(sizeof(cudaStream_t)*ngpus);

for (int i = 0; i < ngpus; i++) {
    CHECK(cudaSetDevice(i));
    CHECK(cudaMalloc(&d_src[i], iBytes));
    CHECK(cudaMalloc(&d_rcv[i], iBytes));
    CHECK(cudaMallocHost((void **) &h_src[i], iBytes));
    CHECK(cudaStreamCreate(&stream[i]));
}

// ping pong memcpy
for (int i = 0; i < 100; i++) {
	cudaMemcpy(d_src[1], d_src[0], iBytes, cudaMemcpyDeviceToDevice);
	cudaMemcpy(d_src[0], d_src[1], iBytes, cudaMemcpyDeviceToDevice);
}
```



### Come mai il Warp è importante?

L’architettura GPU è costruita attorno a un array scalabile di Streaming Multiprocessors (SM), ed ogni SM in una GPU è progettato per supportare l’esecuzione concorrente di centinaia di thread, tuttavia molteplici SM per GPU NVIDIA  seguono thread in gruppi di 32 chiamati warp.

Tutti i thread in un warp eseguono la stessa istruzione allo stesso tempo. 

32 è un numero magico di thread (consecutivi), idealmente tutti i thread in un warp eseguono in parallelo allo stesso tempo (modello SIMD) e ogni thread ha il suo program counter e register state, ed esegue l’istruzione corrente su dati assegnati.

Ogni thread può seguire cammini distinti di esecuzione delle istruzioni (parallelismo a livello thread) e i thread che compongono un warp iniziano assieme allo stesso indirizzo del programma, ma ognuno è libero di seguire path propri indipendentemente da altri thread, seguendo flussi distinti.



###  Spiegare la struttura di una bitonic merging network

Una bitonic merging network nasce come soluzione ad un problema di ordinamento nativamente parallelo basato su sequenze bitoniche di numeri, ovvero sequenze divise in una parte strettamente crescente e una strettamente decrescente. Una bitonic merging network in un contesto di questo tipo risulta essere una rete di comparatori con lo scopo di ritornare una sequenza crescente (+) o decrescente (-) in base al tipo richiesto.

Si parte da comparatori di tipo BM[2] per poi gradualmente salire di grado combinandoli in comparatori BM[4] i quali faranno poi nuovamente affidamento su una rete BM[2] e così via fino al raggiungimento del corretto ordinamento.

Per l'ordinamento bitonico si procede in due passi: si costruisce una sequenza bitonica e poi la si ordina con una bitonic merging network.



### Spiegare la struttura dell'algoritmo Boruvka per MST

L'algoritmo di Borukva nasce con lo scopo di trovare l'albero di peso minore presente in un grafo pesato.

Si comincia considerando il grafo come una foresta fatta di alberi composti da un solo vertice e si analizza per ognuno di essi ognuno di essi gli archi di peso minore uscenti per ogni nodo dell'albero, in seguito vengono uniti tali archi per collegare differenti alberi minimi, in seguito viene reiterato questo processo fino a quando la foresta sarà composta da un solo albero, il MST.



### Spiegare lo schema parallel prefix sum work efficient

L’operazione parallel scan (chiamata anche prefix-sum) considera un operatore binario associativo ⨁ e un array di n elementi [a_0 , a_1 , ... , a_n-1 ] e restituisce l’array b = [a_0, (a_0 ⨁ a_1 ), ... , (a_0 ⨁ a_1 ⨁ ⋯ ⨁ a_n-1 )]

```c
__global__ void scan_GPU(float *x, float *y, unsigned int N) {
    __shared__ float smem[BLOCKSIZE];
    unsigned int tid = threadIdx.x + blockIdx.x * blockDim.x;
    
    if (tid < N)
        smem[threadIdx.x] = x[tid];
	
    for (unsigned int stride = 1; stride <= threadIdx.x; stride *= 2) {
        __syncthreads();
        smem[threadIdx.x] += smem[threadIdx.x - stride];
    }
    y[tid] = smem[threadIdx.x];
}
```

In una versione parallela su un solo blocco in shared tutti i thread iterano al più fino a log N, con N = BLOCKSIZE e ad ogni passo il numero di thread che non effettua operazione è pari a stride.

Complessità:

- Step complexity: Θ log N
- Work efficiency: Θ N log N
- Peggiore del caso sequenziale ... poco efficiente!



### Colorazione di grafi, MIS e algoritmi visti a lezione





### Prefix sum e algoritmi visti a lezione





### Warp, divergence, pattern di accesso a gmem e smem



















