# Lesson 5

Excursus sulla gerarchia di memoria cuda

Shared memory, dal punto di vista algoritmico e come l'hardware strututra questa memoria, pattern di accesso che rendono ottimale l'uso della memoria shared.

## La Memoria

Elementi chiave nel disegno architetturale dei computer moderni. Utilizzare la memoria e in gerarchia, non si può avere tutto. Spostamento di grandi dati dalla memoria centrale alla memoria specifica. Tutto quello che l'hardware scatena è uso di gerarchia con memorie piccole e veloci rispetto a memorie grandi e lenti, dove i cicli di clock necessari per aggiungere dati sono più lunghi.

località e gerarchie di memoria:

- Località spaziale: i dati che sono stati acceduti a un certo istante è facile che dati limitrofi vengano acceduti di nuovo con una certa frequenza, correlazione tra il dato e i suoi vicini.
- Località temporale: boh

Costo, capacità latenza frequenza di accesso al processore sono elementi da combinare assieme con la gerarchia di memoria per ottenere un risultato complessivo ottimale rispetto alla necessità di elaborare grosse moli di dati. Parametri messi nel giusto ordine, ridurre i cammini di accesso indipendentemente dalla loro dimensione.

Il modello di memoria cuda prevede:

- memoria programmabile, ad uso del programmatore, su cui serve conoscere aspetti importanti tipo accessi lettura e scrittura di dati
- memoria non-programmabile, 

## Registri

Le memorie più veloci in assoluto, ripartite tra i warp. I registri sono in numero finito, ricavabili dalle informazioni della scheda e sono in numero limitato per ogni thread e dipende dalla architettura quindi questo è un aspetto che limita pesantemente il loro utilizzo, rischiando di limitare il numero di warp attivi.

Da usare con parsimonia.

Quando si ecceedono le dimensioni del numero di registri attivi per thread può succedere che arriva il restister spilling, i dati non posson oessere contenuti e si riversano in un altro spazio di indirizzamento, local memory.

Non si possono allocare quindi grandi array locali automatici, un elenco sterminato di variabili automatiche altrimenti inficiano sull'efficienza e inoltre possono non essere allocati come registri.

Si può definire il numero massimo di registri disponibili a tempo di compilazione specificando un `-maxrregcount N` 

Esiste una primitiva per ridurre e definire manualmente su un certo kernel il numero di thread per blocco e gestire così il problema dello spilling. Meglio gestirli a priori, avendo un idea di quanti registri saranno utilizzati.

## Local Memory

La memoria locale è lenta come la global memort (è parte di essa) una parte di memoria che viene utilizzata per dati non contenuti nei registri, i quali hanno dei limiti, il resto finisce qua. Si tratta di una memoria locale ai thread.

...

## Constant Memory

Risiede nella device memory. Ha una cache dedicata.

Non ha grandi dimensione e deve essere caricata a livello host, che fa un trasferimento di dati dalla memoria CPU alla memoria GPU allocato.

Constant rimanda alle costante utilizzate nelle formule, quindi se uno sta applicando una formula e tutti i thread la usano, è un tipico uso di constant memory dove carichi tutto li e sono pochi dati immutabili utilizzati comune da svariati thread.

Vantaggio di una cache dedicata.

## Texture Memory

Usata per il rendering di immagini, per elaborare immagini. Non ha grandi efficienze intrinseche se non per questo.

Usata anche per compressione video e interpolazione con sottocampionamento sulle informazioni crometiche di una immagine o di un video, il codec necessitano questi supporti, per i quali è bene considerare di utilizzare texture memory

## Shared memoy

Memoria programmabile on-chip, con bandwith molto più alta. Una memoria polto pratica da usare esplicitamente. Se un dato deve essere utilizzato piu volte da un thread dello stesso blocco si pensa preventivamente di trasferire i dati con principio di località.

Tanti trhread elaborano più volte gli stessi dati, allora conviene usare memoria a bassa latenza per risparmiare computazione. 

Non si può pensare di caricare troppa roba in shared memory per non inficiare il lavoro dei warp

La cache L1 e la shared memory usano la stessa memoria on-chip da 64kb che è staticamente partizionata ma che puo essere dinamicamente configurata a runtime usando la call, fatto al ivello di programmazione.

## Global Memory

Quella che utilizziamo di solito

Possibile fare una dichiarazione statica (`__device__ int a`) o dinamica (`cudaMalloc` e `cudaFree`)

...

## Cache su GPU

le cache in gioco sono quelle organizzate in 

- L1
- L2
- read only constant
- read only texture

## Uso shared memory

Canale di comunicazione per thread intra-block, gestita da noi e aperta a tutti i dati, utilizzabile dal programamtore in tutti i modi possibili.

Il programmatore la deve dichiarare e poi popolarla.

## Organizzazione

Thread distribuiti man mano, la cosa più natural e da pensare in un wap è accendere 32 bit per sfruttare al masismo la banda senza dover fare più volte la stessa operazione.

SMEM a runtime ripartita tra tutti i blocchi in un SM. Maggiore la shared memory e minore il numero di blocchi attivi concorrenti.

## HW memory banks

Per ridurre la latenza e sfruttare ampiezza di banda c'è un mapping uno a uno tra thread del warp e altro. 

Il problema è che potrebbero ssere richieste maggiore di uno e degenerare fino a richiedere 32 accessi per rilevare e trasferire dati nella memoria shared, e altri casi con conflitti in cui dieversi indirizzi devono essere acceduti da thread distinti sullo stesso banco, con lthread distinti.

Limitare i conflitti. Accesso non rigidissimo, potrebbero nascere conflitti.

Mapping non pi+ uno a uno e misti, ma con un solo indirizzo per banco da parte di un qualsiasi thread, senza che questo abbia svantaggi particolari, ma se ci sono troppi tread allo stesso banco vi sono forti rallentamenti, da evitare.

...

## Allocazione SMEM

### Statica

Di solito fatta verso l'inizio del codice, svolta con una dimensione dichiarata e definita a livello di compilazione.

Si dichiarano direttamente variabili condivise `__shared__ int i;` e queste variabili possono essere condivise in modo shared.

### Dinamica

Se la dimensione non è nota si usa la keyword `extern`

Si possono allocare dinamicamente solo array 1D `extern __shared__ int array[]`

Possibile un allocazione di una chiamata esplicitando la chiamata `kernel <<<grid, block, N*sizeof(int)>>>(...)` 

Per allocazioni multiple dinamiche occorre usare esplicitamente gli offset all'interno di una singola allocazione globale per tutti i dati

...



































