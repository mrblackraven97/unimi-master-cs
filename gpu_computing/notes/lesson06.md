# Global Memory

Meccanismi che rilassano aspetti di sincronizzazione verso la non sincronizzazione, trasferimenti asincroni, direttamente controllati dallo sviluppatore

Zero-copy, memoria che consente al device di putnare direttamente alla memoria dell'host

Unified memory dove si ha un solo puntatore per lo stesso dato sia in memoria host che in memoria device, sempre copiato in mem device, ma semplifica la scrittura del codice nonostante alcuni aspetti critici

## Allocazione dinamica

Una malloc che restituisce un puntatore non è un'allocazione di uno spazio il primo che trova, ogni allocazione di blocchi avviene secondo una logica.

## INizializzazione

cudamemset serve per inizializzare la memoria fissando una certa quantità di byte

questa operazione è un operazione che mette al riparo da valori casuali nei blocchi appena allocati, queste due modalità vengono utilizzate per avere dati conosciuti.

la memoria allocata è allineata anche del tipo base del chunk di dati allocato che entra in gioco come parametro/elemento per ottenere tale allineamento

## Variabili globali

qualificatore `__device__` allocate staticamente, ma anche dinamicamente con cudamalloc dove si specifica la size a runtime

a differenza della dinamica che viene costruita dinamicamente di dimensione variabile la statica la si conosce a priori

## Uso memoria globale

siccome non abbiamo una allocazione di ritorno come nel dinamico, occorre utilizzare delle primitive API del cuda a runtime che permettono di settare i dati attraverso una tabella di simboli cudamemcptyosymbol e cudamemcpytfromsimbol

è possibile acquisire anche l'indirizzo della variabile attraverso la cuda api cudagetsymboladdress

...

la pinned memory rende più efficiente il trasferimento dati

[esempi di codice che lo dimostrano]

## Zero-copy Memory

Sempre a partire da memorie pinned, si ha la possibilità di mappare un blocco di memoria host nello spazio di indirizzamento del device, questo consente di far si che la GPU non abbia necessità di fare meccanismi di trasferimento classici ma i può direttamente accedere alla memoria post.

Fatto attraverso la flag che ne specifica il comportamento

Capire se il device lo consente, indagando attraverso le proprietà della gpu

quando il device ha poca memoria è possibile estendere la dimensionalità virtuale della memoria device. evita il trasferimento esplicito riducendo il tasso di trasferimento usando il bus pci express

## UVA

caratteristica introdotta come uso e gestione degli indirizzi, che si ha dalle prime architetture

host memory e device memory condibidono un singolo virtual address space, la cpu con tutte le gpu hanno uno spazio di indirizzamento unificato, possibile avere un puntatore che va direttamente nello spazio indifferentemente tra gpu e cpu

modello non direttamente utilizzabile dall'utente ma che sta alla base dell'indirizzamento della memoria unified

## Unified memory

Modello di programmazione più semplice che elimina l'uso di cudamemcpy

singolo puntatore da gestire a seconda che sia in host o device, attenzioni da prestare per mantenere la correttezza glogale e non avere scritture inconsistenti in modo asincrono. 

Vantaggio dal fatto che è tutto trasparente, sempre migrazione di dati ma gestita direttamente da pci express e non fatta esplicitamente dal programmatore

per questa cosa serve usare la nuova keyword `__managed__` globale

la gestione si fa definendo la memoria managed attraverso la primitiva cudamallocmanaged e si può fare dinamicamente

[esempio mutua esclusione]

l'esempio mostra la memoria statica in cui vengono definite 2 variabili e un kernel che si occupa di elaborare i dati e a livello di compilazione viene gestita la mutua esclusione, gpu e cpu possono creare delle funcion e intervenire sulla stessa memoria, ma questo deve essere escluso quindi entra in gioco la sicronizzazione

## Constant memory

legata alla memoria in generale non solo device, ci consente di collocare dati costanti di sola lettura direttamente const e risiete anche essa nella global memory, con la caratteristica particolare di avere ampia visibilità a tutti i trhead che stanno eseguendo nel kernel

memoria in sola lettura dichiarata con qualificatore `__constant__` e allocata attraverso alla tabella dei simboli con una cache dedicata su chip

## Memory Bandwidth

Prestazioni del kernel:

- memory latency il tempo necessario a soddisfare una richiesta dati in memoria
- memory bandwidth tasso col quale la device memory viene acceduta da un sm misurata in bytes per unita di tempo

## Prestazioni

MIgliorare i warp e come essi accedono alla memoria, devono farlo nel modo migliore possibile, i 32 trhread eseguono spesso la stessa istruzione simd e fanno fetching di 32 indirizzi di memoria simultaneamente

minimizzare il numero di transizione da warp in attesa a warp attivi, un dato che puo peggioare arbitrariamente al punto da avere una transazione per ogni thread con uno spreco enorme

le applicazioni gpu tendono ad essere limitate dalla bandwidth

## Schema Cache

...

## Accessi allineati e coalescenti

...

























