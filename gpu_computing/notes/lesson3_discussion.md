# Lezione 3

Nozione di warp, raggruppamento di thread in un blocco di 32 che lavorano in modo simd, per esporre il massimo parallelismo alla gpu sottostante. Per dare una risposta al miglior utilizzo della memoria per superare determinati colli di bottiglia.

Sincronizzazione prevista da CUDA a livello di blocco. Impone una sincronizzazione in attesa che tutti svolgano un compito parziale prima di procedere, applicato ad un problema parallel reduction, basato su operazioni binarie, sftuttando al meglio l'architettura.

## Panoramica

Host e device in rapporto master-slave, dove host coordina le esecuzioni di kernel svolte dai device, recuperando dopo le sincronizzazioni opportune i risultati.

Eseguire un kernel significa eseguire dei thread. Core in numero molto elevato e forniscono unità di computazione per varie cose ad esempio numeri floating point e dotati di chip di memoria interna per allocazioni di variabili dinamiche o tutto quello che serve per svolgere chiamate di funzione.

Le risorse sono limitate e devono essere ripartite attraverso i thread di un blocco, forte limitazioni di concorrenza, unite alle barriere di sincronizzazione in cui i blocchi incorrono. Per la comunicazione tra i thread è importante la shared memory.

## Streaming Multiprocessor

Il cuore di ogni architettura GPU è costituita dallo streaming multiprocessor, un processore che contiene diverse unita funzionali.

Ogni architettura è un arrei scalabile di SM, con composizioni diverse di quelle elencate. Questo realizza il parallelismo hardware vero e proprio, semplificando l'approccio computazionale quando si conosce il meccanismo secondo il quale vengono allocati i vari thread secondo SM.

Ogni blocco di thread viene associato ad un singolo SM e gli shceduler smondano ogni blocco e cominciano ad allocare ai singoli core l'esecuzione dei thread secondo SIMD.

## Mapping Logico

Organizzazione logica utilizzata per lo sviluppo di algoritmi paralleli. Se un thread esegue su un cuda core, un blocco esegue su un SM. Una grid può essere sparsa per tutto il device e i blocchi applicati a tutti gli SM disponibili.

La gerarchia dei thread vengono mappati sugli array di SM

La gpu si trova ad eseguire uno o più kernel

Diversi processi su diversi core dell'host possono lanciare kernel, indipendenti tra loro ma concorrono sulla stessa GPU (o piu connesse)

SM prende in carico un numero di blocchi e li gestisce internamente, per poi eseguirli in maneire il più simultanea possibile, con prestazioni che dipendono dal design del kernel e dalla necessità di accesso alle risorse richiesto.

I core e altre unità delle SM eseguono le istruzioni dei thread a seconda del percorso che ciascuno di essi seguirà.

All'interno di ogni SM vi è una politica di scheduling secondo warp, una suddivisione ottimale dei core

## Warp

Il numero massimo di thread contenuti in un warp è dato da 32 thread consecutivi. 

Metafora della tessitura, fili di un telaio e della trama che li connette tutti. I thread hanno un comportamento seguendo un modello di parallelismo SIMD, stessa istruzione su dati diversi, anche se l'architettura consente introduce il modello in cui ogni thread è dotato di risorse per poter eseguire indipendentemente, ciascuto ottiene risorse distribuite che gli consentono le esecutioni indipendenti.

## HW dei Warp

La prospettiva logica è di blocchi con 1,2,3 dimensioni suddivise in warp, il n di warp legato alla dimensione di blocco e generati con indice progressivo a seconda del numero di thread all'interno del blocco. 

L'allocazione su SM i thread entrareanno in esecuzione secondo le politiche di scheduling dei warp

Hardware vede una sequenza di warp in lista eseguiti in modo sequenziale, vengono distribuiti in successione agli SM disponibili e alla configurazioen hw sottostante entrando fisicamente i thread composti da ciascun warp. Con un blocco da 128 thread verrà suddiviso in 4 blocchi warp da 32 thread

## Scheduling dei Blocchi

immaginando 1000 blocchi da 128 thread.

Ogni glocco ha un SM, quando un blocco viene allocato viene decomposto nei suoi warp, 8 - 12 blocchi sullo stesso SM con un totale di 48 warp sullo stesso SM. Lo scheduler decide quale sara il prossimo warp da eseguire.

Quello per cui lo scheduler lavora è la massimizzazione delle prestazioni avendo sempre qulacosa da eseguire.

Il compito di chi produce software è importante avere un numero elevato di warp schedulati in esecuzioni per poterli fornire pensando che molti di essi possano andare in attesa.

## Thread & Warp status

Per mantenere il piu elevato possibile il tasso di attivita dei singoli core, è bene che un elevato numero di blocchi sia attivo, un blocco è attivo quando ha assegnate le risorse previste. Poichè esse hanno una quantità finita à bene minimizzare cio.

QUando un blocco è attivo anche i warp sono attivi, e l'attivita effettiva è legata ai warp

3 stati:

- selezionato, in esecuzione  su un dato path condizionale. i distinti path vengono eseguiti sequenzialmente.
- bloccato, in condizioni di non poter eseguire per qualche motivo, attesa di dati.
- candidato, quando vi sono32 core pari al n di thread che sono liberi e tutte le istruzioni sul path in esecuzione sono disponibili, dati caricati e fetching effettuato

un thread è

- attivo, quando esegue un istruzione del warp, partecipa a un path
- inattivo, non è nel path corrende, multibranghing, o in una situazione in cui ha finito prima e aspetta, ultimo di blocchi non multipli di 32

IL numero di warp per thread block è: [vedi espressione slike]

## Ripartizione risorse

Importante incide nel tempo

Il numero di registri viene ripartito tra i thread, per rendere massimo il numero di blocchi e thread di un SM bisogna ridurre la shared memory.

Prendendo architettura kepler/fermi per aumentare il n di thread operativi/attivi si riducono i registri e la shared memory.

## Latency hiding

La latenza è nemica dell'efficienza di un algoritmo. Gli SM usano il parallelismo thread per ottimizzare le unità funzionali.

Grado elevato di parallelismo garantito dal n di warp attivi nel tempo, ci consente di abbattere i n di cicli di clock necessari al completamento operazioni, throughput massimo.

Scheduler sempre avere istruzioni pronte per essere eseguite - latency hinding

Gpu gestire un grande numero di thread leggeri, cpu gestire piccolo numero di thread pesanti.

Le istruzioni che producono latenza sono le classiche istruzioni di calcolo e accesso a memoria, la quale possono essere anche molto pesanti richiedendo molti più clock di un calcolo

## CUDA occupancy calculator

Foglio di calcolo fornito da CUDA utilizzato per vedere le effettive occupazioni di memoria, fornendo dati di hardware e input.

Strumento utile per aumentare occupancy dell'applicazione, attraverso tentativi di modificare i blocchi si puo provare a vedere come migliorare l'uso delle risorse.

Andando da blocchi molto piccoli a molto grandi si incorre in inefficienze: blocchi piccoli portano ad un sottoutilizzo di risorse, d'altro canto blocchi grandi portano inefficienze perchè troppe poche risorse per i singoli thread

## Warp divergence 

branch prediction uso di cpu per prevedere salti condizionali, effettuando operazioni in avanti prima di effettivamente controllare la condizione. guadagno di efficienza. se non fosse stato indovinato il branch si ha una leggera perdita di tempo, ma nel complesso ne vale la pena.

Meccanismi di branch prediction non son opresenti nelle gpu, tuttavia i branch sono importanti perchè sono fonte primaria di inefficienza. L'idea è che tutti e 32 i thread di un warp eseguono la stessa istruzione, massima efficienza, quando accade che all'interno di un warp un gruppo di thread di fronte ad un salto prendono strade diverse abbiamo la warp divergence, il principale motivo di inefficienza computazionale, i thread seguono un flusso o un altro a seconda dell'indice che li caratterizza.

```c
__global__ void kernel(int* x, int* y) {
int i = threadIdx.x + blockDim.x * blockIdx.x;
int t;
bool b = f(x[i]);
if( b ) {
// evaluate g(x)
t = g(x[i]);
}
else {
// evaluate h(x)
t = h(x[i]));
}
y[i] = t;
}

```

proseguono tutti in parallelo eseguendo le istruzioni fino a che non arrivano al branch condizionale, che valuta b e seguono strade diverse i rami, avviene una serializzazione dei due branch di esecuzione distinti, con aumento di tempi di esecuzione. alla fine del branch si riparte in sincrono

## Ottimizzazione nvcc x divergenza

Problemi di divergenza ottimizzate dal compilatore nvcc. Come in passato si introducono predicati valutati da tutti i thread. le istruzioni connesse vengono eseguite solo se il predicato è vero. 

i thread eseguono le istruzioni senza introdurre divergenze.

tutti i thread calcolano il predicato logico e le due istruzioni del predicato

In situazione di branching clomplessi il compilatore usa tecniche avanzate warp voting per verificare se tutti i warp usano la stessa strada, altrimenti con meccanismi estesi basati su predicati si evita l'inefficienza

## Sincronizzazione block level

Un costrutto molto usato è la barriera di sincronizzazione. una chiamata ad essa è usata per trattenere un thread in attesa che tutti i thread raggiungano tale barriera. 

implementata con variabili mutex/condizionali, in cuda si collocano barriere di sincronizzazione solo tra i thread che partecipano ad uno stesso blocco, perchè spesso si trovano in condizione di dover scambiare dati. in quel caso è meglio evitare condizioni di accessi non ordinati (race condition) dopo ogni lettura  e tutti devono scrivere è bene che si consenta di avere dei meccanismi di sincronizzazione e di ripartenza da parte di tutti i thread dopo aver superato la brrievaa e avere dati condivisi in memoria.

rischio deadlock, tutti tithread del blocco non raggiungono la barriera, solo ultimamente sono stati incluse sincronizzazione tra blocchi

## DIvergenza e deadlock

In caso di divergenza che produce deadlock, dove si blocca la computazione, è stata introdotta una bariera su cui arrivano solo i thread di un solo ramo if e non quelli del ramo else, in questo caso il sistema è bloccato per sempre

## Reduction

un operazione comune è la parallel reduction, il caso in cui si ha un array di grande dimensione da cui estrearre elementi particolari e la somma di tutti gli elementi di un array

- calcolo media
- calcolo prodotto interno tra vettori in algebra
- operazioni di calcolo min max

le proprietà richieste da un op di reduction:

- commutativa
- associativa

ordinare e ricombinare elementi come voglio per ottenere un risultato invariante e piu efficiente

### Parallelizzazione della reduction

l'approccio sequenziale è molto semplice, risolto con un ciclo dove risolvo uno alla volta

, l'approccio parallelo è complesso: con un vettore di grande dimenzioni è difficile coordinare i thread, meglio pensare di suddividere il mega vettore in mini vettori e attivare un gruppo di thread che simultaneamente producono somme parziali per poi sommare sequenzialmente i risultati parziali ottenuti.

effettuare somme a coppie di elementi iterativamente. indipendentemente dalle coppie scelte, ci sono due approcci distinti, e in entrambi i casi si vedono le potenzialità del parallelismo:

- elementi equidistanti
- elementi contigui

processo che via via dimezza le somme fino alla finale. vedere che il numero di passi necessari è log nella dimensione del blocco di dati che si sta sommando.

## Implementazioni Cuda

...

## Prodotto tra Matrici

il prodotto tra matrici è il tipico esempio con parallelismo intrinseco, ripetere il prodotto interno.

come definire la griglia? qual'è il lavoro da atribuire ad ogni thread che passa dal lavoro di indicizzazione di matrici A B C

prodotti e somme si occuperà ogni singolo thread

suddivisione in sottotask si un task generale, granularità dei task.

Passare da una granularità fine, per cui ogni thread si occupa al più di un prodotto, nxmxk questa granularità porta ad un overhead sulla gestione dei task, specie per matrici elevate

forniamo ad ogni task di occuparsi di risultati di prodotti di ogni riga, prodotto interno tra due vettori. questo puo far pensare ad una sequenzializzazione forzata, ma il num di prodotti interni può essere elevato per avere dei vantaggi. un ragionevole algoritmo, che consente di avere buona occupazione dei core. 

quello che conta è avere un numero elevato di warp schedulati con num elevato di block attivi. con m e n grandi il  num di prodotti interni è elevato e di conseguenza il num dithread simultaneamente in operazione sulla scheda.























