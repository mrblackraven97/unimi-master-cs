# Course Introduction

In time, computation power gradually shifted from the CPU to the GPU, but how this happened and why?

## From CPU to GPU

Originally, **the primary source for computation was the CPU**: it was powerful enough to run general purpose application as well as the OS on which they were run onto. Their computational power was always ruled by Moore's law, which said that the number of transistors in a dense integrated circuit, thus the power, doubles about every two years. 

However this law is coming to an end, because of 

- Physical limitations of the circuit's materials: They can't be shrunk down to fit any more of them;
- Clock limitation: Since the dynamic power dissipated grows with the cube of the clock frequency, we can't surpass the 4 GHz clock limits without the risk of overheating.

Some adaptations have been made, such the implementation of multi-core, a primitive **parallel computing technique which then evolved completely into the GPU architecture**.

## General Purpose Computing on GPU

GPU originally was born, as its name suggests, for graphical processing, since handling multiple pixels for the screen with almost the same operations is a heavily parallel workload. Compared to the CPU, which only has 2, 4 or 8 cores, it has thousands of cores, extremely specialized in highly parallelizable problems. The introduction of the GPU in other problems, external to graphics processing, gave origin to the General Purpose GPU paradigm.

We said general purpose and we mean it! We have countless of uses for GP-GPUs:

- "Big data", data-mining;
- Deep learning for imaging, object detection and segmentation;
- Web search engines and web-based business services;
- Medical imaging and diagnosis;
- Financial and economic modeling;
- Pharmaceutical design;
- Oil exploration;
- and many more...

The motivation behind this is that GPU ease the heavy calculation problems treating them as a single parallel problem: what appeared to be a great number of calculations are instead the same calculation!

Some example of heavy calculations are:

- Matrix product:

   ```pseudocode
    for all entry C(i,j):
    	for all k = 1:num_col(A)
    		calculate s = s + A(i,k)*B(k,j)
    	store s in C(i,j) 
   ```
   
- We have to do many calculations for each row and column, however the operation applied is the same: doing it "the parallel way" reduces the time of computation;
  
- Ray tracing:

   ![](img/ray.png)
   ```pseudocode
   for all pixels (i,j):
   	Calculate ray point and direction in 3d space
   		if ray intersects object:
   			calculate lighting at closest object
   		store color of (i,j) 
   ```

   - Here we have to calculate many ray vectors to determine the color of the objects in our scene: we could parallelize the calculations for each pixel.

## Heterogeneous Architectures

In 2007, NVIDIA saw the opportunity to introduce GPU and the parallelism it offers in general purpose programming, defining an easy-to-use interface through CUDA (Compute Unified Device Architecture). 

An heterogeneous model has emerged, which makes the most of CPU and GPU capabilities and supporting co-processing between these two components:

- The sequential part of the application is run by the CPU;
- The parallel part of the application (the one most heavy computationally) is computed and accelerated by the GPU.

Through this, applications appear faster and more responsive to the user, but with added energy consumption: our goal is to maximize the computing power while minimizing the consumed energy.

## Hybrid Model

![](img/hetmodel.png)

In our heterogeneous model we have two complex components: the host (CPU) and the device (GPU). This is because the CPU orchestrates the synchronization and sends the computation loads to the GPU through the PCI-Express bus with direct memory transfers.

![](img/orchest.png)

As we said, different task requires different capabilities:

- Sequential tasks with unpredictable control flow can be easily handled by few but optimized cores in the CPU;
- Parallel tasks, numerous but simple, can be delegated to the thousands of cores in the GPU, which cooperate in an efficient way to reach the goal.

## Hybrid Applications

![](img/hetapp.png)

Heterogeneous applications that take advantage this hybrid model are characterized by:

- A section of **host code** (for the sequential tasks): This code is executed on the CPU and is responsible for:
  - Handling the environment;
  - Handling I/O;
  - Handling data to be transferred to the device.
- A section of **device code** (for parallel tasks): This is the code executed on the GPU and is responsible for executing parallel workloads.

## Instruction Parallelism

We talked about parallel workloads, but how are they structured?

![](img/par1.png)

Let's suppose we have a problem, made of a discrete number N of instructions to solve it: usually, each instruction T<sub>i</sub> is executed sequentially on a single processor. Only one instruction is executed at a time.

However, if our problem has some parts that can be executed concurrently, we can split it into **sub-problems**. 

![](img/par2.png)

This way, each sub-problem is then made of a series of instruction that can be executed simultaneously on different processors. At the end, we need a control and coordination mechanism to gather the partial results of the sub-problems and combine them into the final result.

We have to distinguish, however, between two kind of parallelism:

- **Task parallelism**, in which many tasks can operate independently in parallel (different functions distributed on many cores, as we just saw);
- **Data parallelism**, in which we operate on many data values at the same time (distributed data between multiple threads).

## Parallel Architectures

### Bit Parallelism

Originally, the first parallel architecture was on a bit level: word size would be larger and larger (from 4 to 63 bits) to support greater accuracy in floating point operations and guarantee broader address spaces (2<sup>64</sup>). 

Every computer supports this, from the older mainframes to the modern single-core computers.

### Pipeline Parallelism

The main idea was to overlap the execution of many instructions through a pipeline: there were a set of execution stages (fetch, decode, execute, write back).

This way many stages of different instruction could be executed in parallel, however the data dependency (the need of data outputted by a previous instruction) limited a lot its parallel capabilities.

### Multiple Functional Unit Parallelism

In this architecture, we have many independent functional units such as ALUs (arithmetic logic units), FPUs (floating point units), load/store units or branch units. The parallelism is achieved by executing different independent functions by their relative unit. 

The super-scalar processors solved the data-dependency problem: they could determine the dependency at run-time, scheduling the instructions accordingly and dynamically.

### Process / Thread Parallelism

Instead of having on one control flow of sequential instructions, we have different (one per core or one per thread). This way we can make parallel programming real, letting us coordinate thread flows, multiple access to a shared memory, synchronization and concurrency.

### Flynn Taxonomy

Flynn's taxonomy is a specific **classification** of parallel computer architectures that are based on the number of concurrent instruction (single or multiple) and data streams (single or multiple) available in the architecture. 

We have four different combinations of instruction/data arrangement:

- **SISD** (Single Instruction Single Data):

  ![](img/sisd.png)

  - This is the simplest model, in which one computation unit accesses both program and data;

  - There is **no parallelism in this model**: operations are executed sequentially on one value at a time (classic Von Neumann architecture).

    

- **SIMD** (Single Instruction Multiple Data):

  ![](img/simd.png)

  - We have multiple computation units which operate by executing the **same set of instructions on different data**;

  - Each unit has its own private data to work on but share the same instruction with the others: high grade parallelism applications take advantage of this (computer graphics, multimedia, ...);

    

- **MISD** (Multiple Instruction Single Data):

  ![](img/misd.png)

  - We have multiple computation units which operate by executing **different instructions on the same set of data**;

  - Each unit has its own private instructions but work on the same shared set of data: there is no known commercial development yet;

  - For example, a potential application is applying different filters on the same image to see how they alter the image.

    

- **MIMD** (Multiple Instruction Multiple Data):

  ![](img/mimd.png)
  - We have **multiple computation units with separate access to instructions and separate access to data** on a shared or distributed memory;
  - Each unit loads its own instruction and it executes it on its own data, independently and asynchronously from the others;
  - Examples are multi-core processors, distributed systems and data centers.

## Other Than Flynn : The SIMT Model

Except from the standard Flynn taxonomy, we have a model abstraction **similar to the SIMD**: the SIMT (Single Instruction Multiple Threads) instead of only having the data to work on, each thread has:

- His own instruction address counter;
- His own register state and a register set;
- An independent execution path.

![](img/simt2.png)

Here we can see the difference between SSE instructions (SIMD) versus multi-thread instructions (SIMT):

- In **SIMD** we have **two sets of data to which we apply an instruction**: in this case we are applying a sum in bulk to both vectors a and b;
- In **SIMT** we have **two sets of data and a set of threads**, which will single-handedly perform the sum operation on only a digit of both sets of data: In the example, each thread i sums only a digit per time (`c[i] = a[i] + b[i]`), instead of performing it on the whole vector. This way threads can perform calculations independently and concurrently.

This way we can implement multitasking with multiple threads working together in the same process context: this enhances the multi-core architecture, splitting the threads of a process on all cores to speed up execution and throughput.

![](img/simt1.png)

# NVIDIA GPUs

Architectures for GPUs aren't all the same: depending on the technology they're based on and which use they're targeted for, they have different structure and optimizations. 

Usually, the main families for GPUs are:

- **Embedded**
  - More apt for embedded systems and mobile devices (like smartphones and such);
  - Tegra Family.
- **Consumer desktop/laptop**
  - Designed for the typical desktop/laptop use for graphical applications and gaming;
  - GeForce Family.
- **Professional workstation**
  - Suitable for professional computer graphics and visualization;
  - Quadro Family.
- **Data center**
  - Programmed for datacenter and parallel computing in bulk;
  - Tesla Family.

However, all of these families share the same backbone architecture, which every 2 or so years, gets better, similarly to the Moore's Law. Since 2010, different architectures have emerged, each with more computation power and new features, as the market and research demanded:

- **Fermi** (2010), for high performance computing (HPC) applications, such as seismic data elaboration, biochemical simulations, data analysis and so on;
- **Kepler** (2012), enhances Fermi's computation power for HPCs, while optimizing parallel workload and using less power;
- **Maxwell** (2014), extends Kepler programming model by enhancing memory transfer throughput and bandwidth while maintaining compatibility with older code;
- **Pascal** (2016), enhances HPC workloads and deep learning for neural networks and artificial intelligence, reducing the time spent for their training;
- **Volta** (2018), adds tensor-based computation for AI.

Each architecture is rated by its compute capability (CC rate): higher is better, ranging from Maxwell with CC 5.2 to Volta with CC 7.0.

## Base Architecture Overview

The GPU architecture is based on a scalable array of **Streaming Multiprocessors** (SM). Stream processors are simple, but flexible, **calculation unit**. In fact, they can calculate stuff differently from each other and work in parallel.

Each SM in a GPU is designed to support the concurrent execution of hundreds of threads: these are executed in groups of 32 called **warps**. Each thread in a warp execute the same instruction at the same time.

![](img/archow.png)

The **CUDA core** is the basic building block of the SMs: it is a vector processing unit, which works on a single operation.

![](img/cudacore.png)

# CUDA Zone

The CUDA SDK was introduced to effectively enable the **general-purpose programming model** on NVIDIA GPUs. It enables explicit GPU memory management and as we saw before the GPU is viewed as a compute device, which means:

- It's a co-processor to the CPU (or host);
- Has its own DRAM (global memory in CUDA slang);
- Runs many threads in parallel.

CUDA offers two kinds of APIs for GPU and thread management:

![](img/cudapi.png)

- **Driver APIs** - They are low-level APIs and, as such, they're hard to use for programs, however they give more freedom and a finer GPU management;
- **Runtime APIs** - They are high-level APIs based on driver APIs, in which each function includes and enriches the basic driver functions. 

## A CUDA Program

Every CUDA program is made by two parts:

- The **host code**, executed by the CPU;
- The **device code**, executed by the GPU.

The nvcc CUDA compiler split the two types of code during the compilation process.

<img src="img/nvccsplit.png" style="zoom:80%;" />

## A Simple Problem...

Computing the sum between two vectors: `C[*] <- A[*] + B[*]`

From the CPU side:

```c
float *A = malloc(N * sizeof(float));
float *B = malloc(N * sizeof(float));
float *C = malloc(N * sizeof(float));
for (int i = 0; i < N; i++)
	C[i] = A[i] + B[i];
```

This is inherently sequential.

Possible upgrades to make the problem not sequentially pure:

- Creates a number of threads equal to the number of cores in the process (2, 4, 8, ...);

- Select the portion of data of A, B, C  to assign to each thread;

- For each thread:

  ```c
  for all i <nella regione relativa al thread>
  	C[i] <- A[i] + B[i] 
  /* Lot of time to wait for operations because of read and write operation required in memory */
  ```

- Synchronize threads (with `wait()` and other stuff);

- Slightly faster with a speedup from 2x to 8x.

## ...A Simple Solution

Computing the sum between two vectors: `C[*] <- A[*] + B[*]`

From GPU side:

- Allocate memory for A, B, C on GPU;

- Look for a "kernel" (each thread executes one or more addictions);

- Specify the following kernel operation:

  ```c
  Specify the following kernel operation:
  for all i <assegnati a questo specifico thred>
  C[i] <- A[i] + B[i]
   // poca attesa per le op. read/write in memoria...
  Attiva ~20000 (!) thread
  ```

- Activate like 20000 threads;

- Synchronize threads (with `wait()` and other stuff).

GPU solution:

```c
/*
* kernel: somma di vettori
*/
__global__ void vector_sum (int *A, int *B, int *C) {
	int idx = blockDim.x * blockIdx.x + threadIdx.x; // Thread ID
	if (idx < N)
		C[idx] = A[idx] + B[idx];
}
```

# My First CUDA Program

Connect to Lagrange lab:

- `$ ssh grossi@cuda.lagrange.di.unimi.it`

Check if there is the **CUDA compiler** with command:

- `$ whereis nvcc`

Check if CUDA card are mounted on the system with command:

- ```shell
  $ ls -l /dev/nv*
  crw-rw-rw-. 1 root 195,   0 2 mar 11.29 /dev/nvidia0
  crw-rw-rw-. 1 root 195,   1 2 mar 11.29 /dev/nvidia1
  crw-rw-rw-. 1 root 195,   2 2 mar 11.29 /dev/nvidia2
  crw-rw-rw-. 1 root 195, 255 2 mar 11.29 /dev/nvidiactl
  crw-rw-rw-. 1 root 245,   0 2 mar 11.29 /dev/nvidia-uvm
  ```

Now let's create a simple "Hello World" program like the following:

```c
/* helloworld.cu */
#include <stdio.h>

// __global__ is the qualifier that tells the compiler that 
// this code has to be run by the GPU, instead of the CPU
__global__ void helloFromGPU (void) {
	printf("Hello World from GPU!\n");
}

int main(void) {
	// hello from CPU
	printf("Hello World from CPU!\n");
    
    // hello from GPU (ten times)
	helloFromGPU <<<1, 10>>>();
    
    // this function destroys and clears all allocated resources 
    // current device in the current process (not always needed)
	cudaDeviceReset();
	return 0;
}
```

Let's analyze the code:

- The `__global__` qualifier indicates to the compiler that the function is called by CPU but executed in GPU;
- The kernel invocation is `helloFromGPU <<< 1, 10 >>>()`, where the triple angular parenthesis denotes a call from the code host to the device host, activating its execution;
- A kernel gets executed by an array of threads and all those threads execute the same code;
- Parameters inside the triple angular parenthesis are configuration parameters that specify how many threads will be used to execute the kernel;
- In this example we have 10 GPU threads;
- Function `cudaDeviceReset()` destroys and cleans all resources associated with the current device in the current process (not always needed);
- Compilation requires the switch `-arch sm_20` to generate code that can be executed on a Fermi architecture of capability 2.0.

## Summary

CUDA program structure:

- Allocate GPU memory;
- Copy data from CPU to GPU;
- Invoke CUDA kernel;
- Copy data from GPU to CPU;
- Clean GPU memory.

CUDA development tools:

- IDE NVIDIA Nsignt(TM);
- Debugger from command line CUDA-GDB;
- Graphic profiler or from command line to check performances;
- Memory analyzer CODA-MEMCHECK;
- Tool to manage the GPU device.