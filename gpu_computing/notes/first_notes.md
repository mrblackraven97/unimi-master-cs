# FINALMENTE LEZIONE

# IL MODELLO DI PROGRAMMAZIONE CUDA

Il motto per la programmazione parallela è quello di avere tante computazioni della stessa cosa.

Nel corso del tempo il clock si è stabilizzato abbastanza, faticando ad incrementare, di conseguenza si è arrivati ad aumentare la potenza di calcolo non più basandosi sulla potenza del singolo processore, ma di un insieme di processori, nasce così la necessità del calcolo parallelo.

Sono quindi state aumentate il numero delle unità di computazione, introducendo il multi-core, secondo diversi modelli di astrazioni, che espongono il parallelismo ai progettisti di algoritmi:

- Modello di macchina, molto base, per chi lavora in assembly.
- Modelli architetturali, tassonomia di flynn, distinguendo i modelli in base alla capacità di esprimere in parallelo istruzioni e dati, in particolare SIMD e MIMD.
- Modello computazionale
- Modello di programamzione parallela, aspetti dal punto di vista utente, a che fare con tutti gli aspetti del linguaggio

## SIMD

Modello singola istruzione su multiplo dato, usato dalle alu per eseguire la stessa computazione su molteplici dati. Si tratta di dati indipendenti parallelizzati

Simd usa operazioni vettoriali, il vantaggio è la parallelizzazione delle somme.

## MIMD

I modelli sono privati, si attivano meccanismi che non passano dal processore scambi di memorie con processore applicando funzionalità di dma che si occupa di trasferimento peer to peer che passa attraverso necessità collo di bottiglia costruire reti di calcolatori con topologie diverse per distribuire il calcolo su un numero eleveto limitato di calcolatori

Scambio di dati avviene in modo piu efficiente da un processore all'altro dove ci poniamo per studiare il modello Cuda

## PRAM

Centrale il concetto di memoria condivisa, modello che dal singolo processore contiene dati che verranno condivisi.

Meccanismi di controllo accesso per evitare race condition. Sincronizzazione per uso univoco di variabili condivise 

il calcolo procede per passi in condizioni simd (più probabile) su dati distinti e con meccanismi di controllo per lo scambio variabili chiamati shared.

In linea di principio ogno algoritmo puo essere parallelizzato ma non ha sempre senso perchè ci sono operazioni intrinsecamente non parallelizzabili e altre che si prestano bene a questo meccanismo

Situazione in cui la situzaione è mista, ci sono passi o sequenze che devono restare sequenziali e task parallelizzabili, ci troviamo in questa condizione molto frequentemente. Alla gpu verranno demandati solo operazioni fortemente parallelizaabili il resto resta alla cpu. QUesto richiede un grado elevato di sincronizzazione, richiesto calcolo consistente finale

Compito importante logico di disegnare tank adeguati, partizionando il proprio problema in un sottoinsieme di task top down per adattarlo ad una soluzione parallelistica. Capire quali task sono indipendenti tra loro e quindi parallelizzabili, la granularità a carico del programmatore

Si cerca di ragionare pensando che si andra in contro ad inefficiente ---> necessita di bilanciare il carico 

## Processi e Thread

Avere processi figli, generare più thread da un processo di base.

Rispetto ad un processo qual'è il numero di thread migliore? deve collidere con il numero di thread del sistema? 

importante gestione dello switch di contesto tra i processi per il parallelismo. processi gestiti con illusione di parallelismo anche se spesso sono eseguiti da una singola unità computazionale

Gestione del processo di vita e morte dei thread, i processi son pi usemplici da comprendere rispetto ai thread, sono flussi di istruzioni con delle risorse private indipendenti. Il processo è più complesso e va diviso in thread

due thread vicini tra di loro non è detto che facciano la stessa computazione, spesso son simili ma mai uguali

IL thread una volta creato riceve risorse dal contesto, entra in eseguzione e inizia a seguire il proprio flusso per fare fetching di istruzione nel suo ciclo di vita sfruttando al meglio la gerarchia di memoria sotto mano e così via

nemici della programmazione parallela sono colli di bottiglia su cui far fronte

Un thread appena ha le risorse necessarie inizia a processare finche non incontra un problema tipicamente un evento che mette in attesa, di solito lock di accesso ai dati o i/o o semplice sincronizzazione

QUesto panorama sottintende un grande nemico: la latenza, l'evento di mettersi in attesa per sincronizzazione, questi eventi fan si che la computazione si arresti. Necessario sopperire a questo problema grazie al fatto dell'elevato numero di thread, e se uno va in blocco un altro entra in esecuzione al suo posto.

## THread unix

Ogni processo ben identificato entra in politiche di scheduling dallo stack ogni thread in un processo entra in circolo e gli consente la libertà di far simultaneamente le attività e diciamo questo è cio che si presenta per dare un idea di quello che accade nelle architetture standard

L'interno di un core di cpu si vedono alcuni elementi importanti, tra memorie l1 e l2 cache fondamentali per istruzionI. Livello l3 di cache che mette in comunte diversi core. Presente un chipset per comunicare con l'esterno, molta parte dedicata alla memoria. 

## Esempio cani

Trasformazioni su immagini sono perazioni parallele in quanto basate su matrici, flipping di pixel ogni singolo flipp è indipendente e quindi parallelizzo easy. Fattibile nel mondo cuda con pthread, partendo da una bitmap bmp

IL compito è quello di fare uno swap dove per ogni riga dobbiamo scambiare e riflettere rispetto al centro le righe equidistanti rispetto al centro, la colonna zeresima con la colonna ultima e così via

Ogni thread fara uno o più swap in base al nostro algoritmo, grande parallelizzazione di operazioni

## Inizializzazione thread

Dopo aver definito tipologia e comportamento bisogna assegnare una fascia

un thread avra una propria funzione da eseguire

si posson definire funzioni che quando il thread parte mette in esecuzione il suo codice legato ad essa con parametri che specificano che cosa dovrà fare cambiando i dati per ogni funzione prendo una porzione di dati distinti prendere righe distinte e fare lo swap

--seguono dettagli tecnici da codice [...]--

## Prestazioni

hypertreading in queste architetture è una coppia di core che se anche hanno risorse molto condivise son sempre due thread separati

## Grid e Block

I thread vengono organizzati in gruppi di grid e block

I blocchi contengono tutti lo stesso numero di thread, fatti con lo stampino, le grid sono fatte da diversi block

Blocchi e grid possono essere 1D 2D 3D combinati a piacere

I thread possono cooperare in un blocco attraverso sincronizzazione e memoria condivisa. Trhead di blocchi distinti non possono in alcun modo comunicare

Tutti i thread però condividono una memoria globale, uno spazio di indirizzamento che dereferenziano

I thread vengono identificati tramite due indici, parole chiave dell inguaggio:

- blockIdx, indice del blocco nella grid
- threadIdx, indice di thread nel blocco

combinando questi due indici identifico tutti i thread di questa gerarchia

Queste informazioni sono contenute nel kernel e vengono utilizzate per elaborare i dati.

Variabili di default per dimensioni:

- blockDim, dimensione blocco
- gridDim, dimensione grid

Come eseguo sequenzialmente i thread in queste strutture?

- nel caso lineare scorro da 0 a n
- nel caso nxm su piu livelli superare Dx quante volte y aggiungendo x 

...