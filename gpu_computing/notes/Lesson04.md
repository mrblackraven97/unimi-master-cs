# Lesson 4 - Architettura della GPU

Portare attenzione su cioò che c'è all'interno di una gpu, non solo i core e la gestione astratta ma anche gli elementi architetturali e le innovazioni hardware introdotti nel corso del tempo.

Per parlare di evoluzione di GPU è bene non separarle dalle CPU, una delle architetture più in voga al momento di Intel è la I7 Nehalem, i cui elementi che aiutano a comprendere come GPU e CPU interagiscono tramite un bus PCI-E, una collezione di GPU possono essere inserite in diversi slot di questo bus.

Obiettivo di aumentare il tasso di trasferimento di dadi da un dispositivo ad un altro tramite l'evoluzione hardware del PCI Express.

## Compute Capability

Le varie versioni uscite aumentano la capacità computazionale della scheda, un'etichettatura che caratterizza le varie architetture con un numero principale, poi vi sono delle microarchitetture minor number.

Caratterizzata da tutti gli aspetti hardware che aumentano la potenza della scheda, a cui segue una diretta evoluzione software di cuda, nuove API in grado di sfruttare i nuovi elementi introdotti.

...

Trasferimento dati dalla catena delle cache fino alle unita di computazione alu.

Scheduler che si occupa di collocare i blocchi allinterno degli SM con alcune politiche semplici tra cui l'host interface

## HOst interface

Controller che si interfaccia al pci e si occupa del trasferimento, analogo a quello sulla cpu, parte di hardware a cui viene dispensato il trasfermiento senza intaccare i core e la parte di computazione. Vi sono poi le memorie cache con un controller per la memoria glogale che gestisce lo scabio di dati scaricandoli in cache (lost level cache)

## Giga thread scheduler

quando un kernel viene lanciato si scatena l'allocazione di tutti i blocchi all'interno degli streaming multiprocessor i thread possono essere tanti e gestiti dallo scheduler, in particolare questo è l'evoluzione del classico scheduler che si occupa anche della gestione dei blocchi di thread, importante collocare e dare attività a tutti gli sm per tenere occupato il piu possibile ciascun core.

Scheduler tiene conto della dimensione della griglia. Questa è una evoluzione di cosa succede all'interno di una stessa famiglia di architetture.

Ad esempio nella fermi gf110 mostra che l'array di sm si arrichisce da 6 a 16 sm aumentata anche la cache l2 e introdotte elementi di floating point. 

Cache shere memory ripartito tra le due, aspetto lasciato all'utente dipende dai casi attraverso API. Nella fermi ad esempio la gestione dello scheduling dei kernel si passa da una versione che considerava la gestione dei kernel come uno alla volta, in sequenza, sono stati introdotti elementi per aumentare le prestazioni della scheda, se diversi kernel lanciati simultaneamente la gestione concorrente dei kernel è stata possibile gia da questa architettura. 

Il discorso di kernel simultaneo concorrente ha senso se sono piccoli, deciso a runtime dal device, se sono grandi e richiedono risorse non si può pensare alla concorrenza ma si ritorna al sequenziale. Code di gestione dei kernel per aumentare al massimo il parallelismo e sfruttare il dispositivo.

## Kepler

SMX diverso, scheduler giga thread engine, cambiato da prima con molti piu blocchi operativi simultaneamente

La doppia precisione è onerosa a livello hardware, per questo viene usata poco e la gpu aiuta molto. double significa allocare piu chip dei core stessi, i moltiplicatori hardware a 64 bit sono elementi di computazione, circuiti molto pesanti. quando si opta pe introdurre double bisogna fare tradeoff tra unifa funzionali e feature della scheda.

6 volte il numero di core di Fermi dove ogni smx ha 192 core.

numero di warp scheduler e dispacther in aumento, si traduce in un aumento di blocchi attivi simultaneamente sui core, piu efficienza.

---confronti tra fermi e kepler

## Maxwell

Include 6 graphic processing lcusters

## Pascal

Introdotto novità, potenziando tutti gli aspetti di memoria e di sm. qui gli sm e l'idea di questa scheda è di arrivare a numeri elevati di memoria fino a 32 gb di memoria residenti sulla scheda. 

Il fatto di avere un supporto fortissimo per il trasferimento di dati rispetto alle precedenti, tecnologia hbm2 cosi come il bus nvlink piu veloce di pcie 3. Questi elementi e il potenziamento dei core e doppia precisione potenziata e tutto quello che ne consegue porta questa architettura ad essere uno dei modelli di calcolo scheduler piu indicati per il calcolo scientifico.

---dati sulla gp100

## Transparent Scalability

Quello che non cambia molto la vita ne lprodurre applicazioni è che di fatto hanno mantenuto caratteristiche uguali per tutte le schede permettendo di far girare il nostro software su quasi tutte le architetture. scalabilità trasparente

facilità di utilizzo del framework di sviluppo su cuda.

## Double precision unit DPU

Si vede che le implementazioni rispettano gli standard con anche la mezza precisione, quando non si ha bisogno di troppa precisione (16 bit) per rendere le cose piu veloci.

I picchi che si hanno sono legati a prestazioni teoriche o quando si hanno calcoli di questo tipo e il picco di gigaflops è legato alla potenza della gpu espresso in cuda core e frequenza di clock.

Schede con architetture come pascal e kepler hanno un elevato tasso di doppia precisione.

---confronti vari

...

## Hyper q

COnsente di avere maggiore prestazioni dal punto di vista dell'uso dell'hardware sottostante, il salto da fermi a kepler è stato il primo vero salto, da una coda vera e propria a 32 code per gestire i kernel, se la logica di controllo aumenta pe gestire i vari kernel quindi il passare da una sola in fermi ad un numero piu vasto di code con scheduldr specifici e dedicati delle varie attività e richieste di operazioni aumenta la concorrenza e le prestazioni generali della scheda, massimizzando l'uso e quindi il tasso di uso della scheda stessa.

l'utente percepisce maggior potenza ma non ha idea di tutto questo

## parallelismo dinamico

Parallelismo dinamico, quella possibilità/capacità che un architettura ha di gestire gerarchie di griglie all'interno della gpu

un adattamento dinamico alle necessità che emergono dei dati, i dati trattati hanno una dimensione che non è possibile fissare a priori sempre, spesso si ha necessita di fare in modo adattativo

Ogni kernel puo sincronizzarsi autonomamente come la cpu e la gpu un kernel lancia un altro kernel una griglia padre lancia una griglia figlio per sincronizzazione e dipendenze e terminazione e scope dei parametri dei dati no race condition tutte operazioni viste in presenza del binomio gpu-cpu e ora sono tutte all'interno della gpu con appositi controlli API e tutto quello che serve

## Esempio

cuda kernel figlio e cuda kernel padre

bilanciamento del lavoro

## Granularità

Come nello studio di fluidodinamica ci si mette in condizioni di lavorare un immagine con una determinata griglia e poi si decide quali blocchi richiedono calcolo e blocchi di diversa densità di core che ci devono lavorare

Elenco di limitazioni che ogni architettura si pone legato al numero di blocchi attivo, num di warp eccetera

Introdurre griglie diverse con diverso livello di dettaglio in base alla necessità di calcolo per le specifiche zone.

Punti di sincronizzazione che richiede un meccanismo che impedisca di avere situazioni inconsistenti nella memoria.

...

## Sincronizazione Implicita

Parte la sincronizzazione tra thread perchè la griglia padre completi la sua esecuzione tutte le griglie figlio devono essere terminate prima. Una sincronizzazione operazionale della gpu e del codice, viene fatta così per una gestione implicita

## Sincronizzazione esplicita

quando piu padri e piu figli collidono nell'utilizzo degli stessi dati hanno obbligo di sincronizzare l'accesso a quei dati.

La barriera di sicnronizzazione tra thread dello stesso blocco, thread padre, utilizzo di synch thread, significa che tutti i thread del blocco arrivano alla stessa barriera di sinc e poi riprendono quando tutti ci sono arrivati.

Tutti i thread hanno eseguito un istruzione, ed è importante, ne potrebbero beneficiare uno o piu figli.

garantisce he il thread padre e figlio arrivino alla stessa sincronizzazione con cudadevicesynchronize()























