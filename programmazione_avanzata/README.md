# Credentials for the study materials

These are the credentials to log into Cazzola's webpage to download the study materials:

- Website Link: <https://cazzola.di.unimi.it/>
- ID: `pa`
- Password: `PA+#2009#`
