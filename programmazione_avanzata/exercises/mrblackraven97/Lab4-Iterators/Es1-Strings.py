import re
import itertools

class MyString(str):
   
   def isPalindrome(self, l = 0, r = None):
      cleanStr = re.sub("[\"\',?!;.*-+\s]*","", self).lower()
      if(r is None):
         r = len(cleanStr)-1
      return self._isPalindromeRecursion(cleanStr, l, r)

   def _isPalindromeRecursion(self, cleanStr, l, r):
      return (l >= r and True) or \
         (self._isPalindromeRecursion(cleanStr, l+1, r-1) and cleanStr[l] == cleanStr[r])

   def subtract(self, anotherStr):
      return MyString("".join([x for x in self if x not in anotherStr]))

   def isAnagram(self, wordsDict: dict):
      return len([x for x in self if x in [k[0].lower() for k in wordsDict.keys()]]) == len(self)

if __name__ == "__main__":
   mystr = MyString("abvba")
   print(mystr.isPalindrome())
   print(mystr.subtract("abcwxyz"))
   print(mystr.isAnagram({"Atomic":"d", "Baboon":"s", "Constructive": "d"}))