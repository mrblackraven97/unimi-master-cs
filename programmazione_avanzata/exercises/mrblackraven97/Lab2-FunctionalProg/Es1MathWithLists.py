import functools, math

#sys.setrecursionlimit(10000)

# 1) Sum all the natural numbers below one thousand that are multiples of 3 or 5.

# 1.1) Soluzione con comprehensions e sum() o reduce()
def mult35compr():
   return sum([x for x in range(1,1000) if x % 3 == 0 or x % 5 == 0])


# oppure functools.reduce(lambda x,y:x+y,multOf35)
# con multOf35 [x for x in range(1,1000) if x % 3 == 0 or x % 5 == 0]

# 1.2) Soluzione con filter e reduce

#     natNum = [x for x in range(1,1000)]
#     mmultof35 = list(filter(lambda x: x % 3 == 0 or x % 5 == 0, natNum))
#     functools.reduce(lambda x,y:x+y,multOf35)


# 2) Calculate the smallest number divisible by each of the numbers 1 to 20.
def lcmN(arr, s = 0):
   return ((s == len(arr) - 1) and arr[s]) or lcm(arr[s], lcmN(arr, s+1))

def gcdN(arr, s = 0):
   return ((s == len(arr) - 1) and arr[s]) or gcd(arr[s], gcdN(arr, s+1))

def lcm(x,y):
   return int(abs(x*y)/gcd(x,y))

def gcd(x,y):
   return (y == 0 and x) or gcd(y, x%y)

# 3) Calculate the sum of the figures of 2^1000
# Il trucco è trasformare il numero in una stringa di caratteri e usare le comprehension per creare una lista di interi
def sumFigures(n):
   return sum([int(x) for x in str(n)])

# 4) Calculate the first term in the Fibonacci sequence to contain 1000 digits.
g_ratio = (1 + 5**0.5) / 2

def fib(n):
    return int((g_ratio**n - (-g_ratio)**-n) / math.sqrt(5))

def firstNumWith1000digits(start):
   return (len(str(fib(start))) == 1000 and start) or \
          (firstNumWith1000digits(start+1))

if __name__ == "__main__":
   print("1) Nat nums under 1000 mult of 3 or 5: ", mult35compr())
   print("2) Smallest num div by each of the nums from 1 to 20: ", lcmN([n for n in range(1,21)]))
   print("3) Sum figures of 2**1000: ",sumFigures(2**1000))
   #print("4) First fib term with 1000 digits", firstNumWith1000digits(0))
   




