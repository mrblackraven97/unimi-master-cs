from inspect import signature

class monoid:
    """ Basic implementation of a monoid"""
    def __init__(self, monoidSet, monoidOperation, monoidIdentity):
        assert(self.is_a_set(monoidSet) and self.is_binary_op(monoidOperation) and (monoidIdentity in monoidSet))
        self.__set = monoidSet		
        self.__op = monoidOperation
        self.__id = monoidIdentity	

    def is_a_set(self, elemList):
        return len(set(elemList)) == len(elemList) 

    def is_binary_op(self, operation):
        return callable(operation) and len(signature(operation).parameters) == 2
        
    def applyOp(self, *args):
        return self.__op(*args)
        
    def __str__(self):
        return "I’m a Monoid! My set is: {0}\nMy identity is {1}".\
            format(self.__set,  self.__id)


if __name__ == "__main__":
    binarySet = [True, False]
    orOperator = lambda x,y: x or y
    orMonoid = monoid(binarySet, orOperator, False)

    numberSet = [0, 1, 2, 3, 4]
    addOperator = lambda x,y: (x + y)
    addMonoid = monoid(numberSet, addOperator, 0)

    
    print(orMonoid.applyOp(True,True))
