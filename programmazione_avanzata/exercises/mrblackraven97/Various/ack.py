import sys
from functools import lru_cache

sys.setrecursionlimit(40000)

cache = {}

def ackermann(m, n):
    if m == 0:
        return (n + 1, 1)

    if (m, n) in cache:
        return cache[(m, n)]

    if n == 0:
        r = cache[(m, n)] = ackermann(m - 1, 1)
        return r

    r1, e1 = ackermann(m, n - 1)
    r2, e2 = ackermann(m - 1, r1)
    r = cache[(m, n)] = (r2, e1 + e2)
    return r

@lru_cache(None)
def ack(m, n):
   if m == 0: ans = n + 1
   elif n == 0: ans = ack(m - 1, 1)
   else: ans = ack(m - 1, ack(m, n - 1))
   return ans

if __name__ == "__main__":
   for i in range(0,5):
      for j in range(0,5): #with j=165534 he can actually calculate ack(3,15)
         print("ack({},{}) = {}".format(i, j, ackermann(i, j)))
         if i == 3 and j == 15: break
	
