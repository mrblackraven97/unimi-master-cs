import time


def isPrime(n):
   for x in range(2, n):
      if (n % x == 0):
         return False
   return True

def primesBeforeSund(n):
   nNew = int((n - 2) / 2)
   
   marked = [0] * (nNew + 1)
   
   for i in range(1, nNew + 1): 
      j = i
      while((i + j + 2 * i * j) <= nNew):
         marked[i + j + 2 * i * j] = 1
         j += 1
         
   return [2*i+1 for i in range(1, nNew+1) if marked[i] == 0]

def primesBeforeErat(n):
   nums = [x for x in range(0,n)]
   marks = [0] * n
   for i in range(2,n):
      j = i
      if(not marks[i]):
         while(j < len(nums) and i*nums[j] < n):
            marks[i*nums[j]] = 1
            j += 1
   
   return [((not m) and x) for x,m in zip(nums, marks) if ((not m) and x) > 2]

def primesBefore(n):
   return [x for x in range(1,n) if isPrime(x) and x > 2]

def erat_sieve(bound):
    if bound < 2:
        return []
    max_ndx = (bound - 1) // 2
    sieve = [True] * (max_ndx + 1)
    #loop up to square root
    for ndx in range(int(bound ** 0.5) // 2):
        # check for prime
        if sieve[ndx]:
            # unmark all odd multiples of the prime
            num = ndx * 2 + 3
            sieve[ndx+num:max_ndx:num] = [False] * ((max_ndx-ndx-num-1)//num + 1)
    # translate into numbers
    return [2] + [ndx * 2 + 3 for ndx in range(max_ndx) if sieve[ndx]]

def goldbachWorst(n):
   if(n % 2 != 0):
      return []
   primes = primesBeforeErat(n)
   return [(x,y) for x in primes for y in primes if (x+y) == n]

def goldbachBest(n):
   if(n % 2 != 0):
      return []
   primes = primesBeforeSund(n)
   result = []
   i = 0
   while (primes[i] <= n // 2):
      diff = n - primes[i] 
      if diff in primes:
         result.append((primes[i],diff))
      i += 1
   return result

def goldbach_list(n, m):
   return {x:goldbachWorst(x) for x in range(n, m)}

if __name__ == "__main__":
   '''start = time.time()
   erat_sieve(1000000)
   end = time.time()
   print(end - start)

   start = time.time()
   primesBeforeErat(1000000)
   end = time.time()
   print(end - start)'''
   goldbach_list(10,2000)