#!/usr/bin/env python3

from datetime import date

class Person():
   def __init__(self, name : str, lastname : str, birthday : date):
      self.name = name
      self.lastname = lastname
      self.birthday = birthday
   
   def __repr__(self):
      return self.__str__(self)

   def __str__(self):
      return "Name: {} {} B-Day: {}".format(self.name, self.lastname, self.birthday)


class averageCalcDescriptor:
   def __get__(self, instance, owner):
      marks = list(instance.lectures.values())
      return sum(marks) / len(marks)

class Student(Person):
   def __init__(self, name : str, lastname : str, birthday : date, lectures = {}):
      super().__init__(name, lastname, birthday)
      self.lectures = lectures

   def addLecture(self, name : str, mark : int):
      self.lectures[name] = mark
   
   def __str__(self):
      return "Name: {} {} B-Day: {} \nLectures: {} Avg. mark: {}".format( \
         self.name, self.lastname, self.birthday, self.lectures, self.grade_average)
   
   grade_average = averageCalcDescriptor()

class dayGainCalcDescriptor:
   def __get__(self, instance, owner):
      return instance.pay_per_hour*8

class weekGainCalcDescriptor:
   def __get__(self, instance, owner):
      return instance.day_salary*5

class monthGainCalcDescriptor:
   def __get__(self, instance, owner):
      return instance.week_salary*4

class yearGainCalcDescriptor:
   def __get__(self, instance, owner):
      return instance.month_salary*12

class Worker(Person):
   def __init__(self, name : str, lastname : str, birthday : date, pay_per_hour : float):
      super().__init__(name, lastname, birthday)
      self.pay_per_hour = pay_per_hour

   def __str__(self):
      return "Name: {} {} B-Day: {} \nSalary: {} x H, {} x D, {} x W, {} x M, {} x Y".format( \
         self.name, self.lastname, self.birthday, self.pay_per_hour, self.day_salary, \
            self.week_salary, self.month_salary, self.year_salary)

   day_salary = dayGainCalcDescriptor()
   week_salary = weekGainCalcDescriptor()
   month_salary = monthGainCalcDescriptor()
   year_salary = yearGainCalcDescriptor()

class wizardryDateManipulationDesc:
   def __get__(self, instance, owner):
      return (date.today()-instance.birthday).days
   
   def __set__(self, instance, newDate):
      instance.birthday = newDate

class Wizard(Person):
   age = wizardryDateManipulationDesc()

if __name__ == "__main__":
   p = Person("Andrea", "Rossi", date(1998,2,1))
   print(p, "\n")

   s = Student("Marco", "Antoniotti", date(1968,12,31), {"English" : 22, "Science" : 25})
   s.addLecture("Math", 18)
   print(s, "\n")

   w = Worker("Walter", "Cazzotti", date(1959, 4, 23), 15.5)
   print(w, "\n")

   m = Wizard("Mago", "Merlino", date(2000, 1, 25))
   print(m)
   print(m.age)
   m.age = date.today()
   print(m.age)
   print(m)

