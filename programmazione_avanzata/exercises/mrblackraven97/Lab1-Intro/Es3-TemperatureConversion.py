
# TODO finisci  le lambda

fromKelvin = lambda k: [k, k-273.15, k*9/5-459.67, k*9/5, (373.15-k)*3/2, (k-273.15)*0.33, (k-273.15)*4/5, (k-275.15)*21/40+7.5]
fromCelsius = lambda c: [c]
fromFahrenheit = lambda f: [f]
fromRankine = lambda r: [r]
fromDelisle = lambda d: [d]
fromNewton = lambda n: [n]
fromReaumur = lambda re: [re]
fromRomer = lambda ro: [ro]

scales = {"Kelvin" : fromKelvin, \
          "Celsius" : fromCelsius, \
          "Fahrenheit" : fromFahrenheit, \
          "Rankine" : fromRankine, \
          "Delisle": fromDelisle, \
          "Newton": fromNewton, \
          "Réaumur": fromReaumur, \
          "Rømer": fromRomer}


def toAll(temp, scale):
   return [round(x,2) for x in scales[scale](temp)]
        
def table(val):
   
   tab = [toAll(val,scale) for scale in scales.keys()]

   string = "[" + " ".join(scales) + "]" + "\n"

   for row in tab:
      string += str(row) + "\n"
   return string

if __name__== "__main__":
   print(table(0))

