#!/usr/bin/env python3

import string


def freqs(filename, number):

    file = open(filename, "r", encoding="utf-8-sig")
    data = stringequalizer(file.read())

    wordcount = {}
    for word in data.split():
        if word not in wordcount:
            wordcount[word] = 1
        else:
            wordcount[word] += 1
    file.close()

    def minfreq(x):
        return (x[1] >= number)

    return list(filter(minfreq, wordcount.items()))


def stringequalizer(text):
    exclude = set(string.punctuation)
    text = ''.join(c for c in text if c not in exclude)
    return text.lower()


def prettyprint(lst):
    for elem in lst:
        print("{:<20}\t{:^3}".format(*elem))


if __name__ == '__main__':
    prettyprint(freqs("textfile.txt", 5))
