#!/usr/bin/env python3

import math


def gen_factorial():
    x = 1
    n = 1
    while True:
        yield x
        x = x * n
        n = n + 1


def gen_odd():
    x = 1
    while True:
        yield x
        x = x + 2


def sin(x, n):
    ''' Calculate the value of sin(x) by using Taylor'
    series with n terms of approximation'''
    fact = gen_factorial()
    odd = gen_odd()

    def sin_help(x, n, i=0):
        ''' Helper method '''
        next(fact)
        if i == n:
            return 0
        else:
            a = (x**(next(odd)))/next(fact)
            c = (-1)**(i+1)
            b = sin_help(x, n, i+1)
            r = a + c*b
            return r
    return sin_help(x, n, 0)


if __name__ == "__main__":
    for i in range(1, 10):
        print("My sin: ", sin(math.pi/2, i))
    print("Math sin: ", math.sin(math.pi/2))
