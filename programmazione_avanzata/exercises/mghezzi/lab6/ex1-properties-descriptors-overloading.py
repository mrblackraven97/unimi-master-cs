#!/usr/bin/env python3

import datetime


class Person:
    def __init__(self, name, lastname, birthday):
        self.name = name
        self.lastname = lastname
        self.birthday = birthday

    def get_name(self):
        return self.name

    def get_lastname(self):
        return self.lastname

    def get_birthday(self):
        return self.birthday

    def __repr__(self):
        st = "[" + self.name + ", " + self.lastname + \
                ", " + self.birthday + "]"
        return st


class StudentP(Person):
    def __init__(self, name, lastname, birthday, lectures):
        self.lectures = lectures
        super().__init__(name, lastname, birthday)

    def get_grade_average(self):
        grades = [k for k in self.lectures.values()]

        res = 0
        for i in range(len(grades)):
            res += grades[i]

        return res

    grade_average = property(get_grade_average, None, None,
                             "calculating grade average")


class day_descriptor:
    def __get__(self, instance, owner):
        return instance.pay_per_hour * instance.WORKING_HOURS

    def __set__(self, instance, amount):
        assert amount > 0, "Only positive salary"
        instance.pay_per_hour = amount / instance.WORKING_HOURS


class weeks_descriptor:
    def __get__(self, instance, owner):
        return instance.day_salary * instance.WORKING_DAYS

    def __set__(self, instance, amount):
        assert amount > 0, "Only positive salary"
        instance.day_salary = amount / instance.WORKING_DAYS


class WorkerD(Person):
    WORKING_HOURS = 8
    WORKING_DAYS = 5
    WORKING_WEEKS = 4
    WORKING_MONTHS = 12

    def __init__(self, name, lastname, birthday, pay_per_hour):
        self.pay_per_hour = pay_per_hour
        super().__init__(name, lastname, birthday)

    day_salary = day_descriptor()

    week_salary = weeks_descriptor()

    # month_salary

    # year_salary


class WizardO(Person):
    def __init__(self, name, lastname, birthday):
        super().__init__(name, lastname, birthday)

    def getbirth(self):
        return self.birthday


class Overloaded_Wizzard(WizardO):
    def __setattr__(self, attr, age):
        if attr == 'age':
            print(self.__dict__)
            self.__dict__[attr] = age

    def __getattr__(self, attr):
        if attr == 'age':
            return self.__dict__[attr]


if __name__ == '__main__':
    name = "marco"
    lastname = "ghezzi"
    birthday = "29/08/1996"

    lectures = {'pa': 30,
                'tsp': 10}
    std = StudentP(name, lastname, birthday, lectures)
    print("Student average with property: ", std.grade_average)

    salary = 10
    wrk = WorkerD(name, lastname, birthday, salary)
    print("Worker day salary with descriptors: ", wrk.day_salary)
    wrk.week_salary = 1000
    print("Worker modified day salary: ", wrk.day_salary)

    wiz = Overloaded_Wizzard(name, lastname, birthday)
    wiz.age = 10
    print(wiz.age)
