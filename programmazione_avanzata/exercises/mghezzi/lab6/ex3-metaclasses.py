#!/usr/bin/env python3


class Counter(type):
    counter = 0

    def __new__(meta, classname, supers, classdict):
        myfun = classdict["__init__"]
        classdict["__init__"] = meta.decCounter(meta, myfun)
        return type.__new__(meta, classname, supers, classdict)

    def decCounter(cls, F):
        def wrapper(*args):
            cls.counter += 1
            return F(*args)
        return wrapper


class Spell(type):
    def __call__(cls, *args, **kwargs):
        return super(Spell, Worker).__call__(*args, pay_per_hour=0, **kwargs)


class MultiTriggeredMethod(type):

    def call_counter(func):
        def helper(*args, **kwargs):
            helper.calls += 1
            if helper.calls > 1:
                return func(*args, **kwargs)
        helper.calls = 0
        helper.__name__ = func.__name__
        return helper

    def __new__(cls, clsname, superclasses, attributedict):
        for attr in attributedict:
            if callable(attributedict[attr]) and not attr.startswith("__"):
                attributedict[attr] = cls.call_counter(attributedict[attr])
        return type.__new__(cls, clsname, superclasses, attributedict)


class Person:
    def __init__(self, name, lastname, birthday):
        self.name = name
        self.lastname = lastname
        self.birthday = birthday


class Worker(Person, metaclass=MultiTriggeredMethod):
    def __init__(self, name, lastname, birthday, pay_per_hour=0):
        self.pay_per_hour = pay_per_hour
        super().__init__(name, lastname, birthday)

    def get_salary(self):
        return self.pay_per_hour


if __name__ == "__main__":
    name = "marco"
    lastname = "ghezzi"
    birthday = "29/08/1996"
    # for i in range(5):
    p = Worker(name, lastname, birthday)
    print("First call", p.get_salary(), p.get_salary.calls)
    print("Second call", p.get_salary(), p.get_salary.calls)
