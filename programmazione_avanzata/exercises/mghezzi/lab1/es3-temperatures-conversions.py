#!/bin/python3

fromC = {'C': lambda x: x,
         'F': lambda x: x*(9/5)+32,
         'K': lambda x: x+273.15,
         'R': lambda x: (x+273.15)*(9/5),
         'De': lambda x: (100-x)*(3/5),
         'N': lambda x: x*(33/100),
         'Ré': lambda x: x*(4/5),
         'Rø': lambda x: x*(21/40)+7.5
         }
toC = {'C': lambda x: x,
       'F': lambda x: (x-32)*(5/9),
       'K': lambda x: x-273.15,
       'R': lambda x: (x-419.67)*(5/9),
       'De': lambda x: 100-x*(2/3),
       'N': lambda x: x*(100/33),
       'Ré': lambda x: x*(5/4),
       'Rø': lambda x: (x-7.5)*(40/21)
       }
TEMPS = ['C', 'F', 'K', 'R', 'De', 'N', 'Ré', 'Rø']


def table(temp):
    tot = [[y(v(temp)) for (x, y) in fromC.items()] for (k, v) in toC.items()]
    formattedres = "    "+(8*"{:^8}")+"\n"+(8*("{:^4}"+(8*"{:^8.1f}")+"\n"))
    tmp = totwithtemps = TEMPS
    for i in range(len(TEMPS)):
        totwithtemps += [tmp[i]] + tot[i]
    return formattedres.format(*totwithtemps)


def toAll(val, temp):
    cval = toC[temp](val)
    tot = [v(cval) for (k, v) in fromC.items() if k != temp]
    format_tot = "{:>5.1f}"
    format_temp = "{:<3}"
    NEW_TEMPS = list(fromC.keys())
    NEW_TEMPS.remove(temp)
    res = ""
    for i in range(len(NEW_TEMPS)):
        res += format_tot.format(tot[i]) + "°" + format_temp.format(NEW_TEMPS[i])
    return res


if __name__ == '__main__':
    print('1) Table conversion for 25°C:\n', table(25))
    print("2) 25◦F are : ", toAll(25, 'F'))
