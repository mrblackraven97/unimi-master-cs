#!/bin/python3


def evaluate(x, y):
    return (x == y and 1) or 0


def identity(size):
    return [[evaluate(x, y) for y in range(size)] for x in range(size)]


def square(size):
    return [[1+x+y*size for y in range(size)] for x in range(size)]


def transpose(matrix):
    rows = len(matrix)
    columns = len(matrix[0])
    return [[matrix[c][r] for c in range(columns)] for r in range(rows)]


elem = lambda A, B, i, j: sum([A[i][k]*B[k][j] for k in range(len(A[0]))])


def multiply(A, B):
    return [
        [elem(A, B, i, j) for i in range(len(A))]
        for j in range(len(B[0]))
    ]


def prettymatrix(matrix):
    pmatrix = ""
    for row in matrix:
        pmatrix += str(row) + "\n "
    return pmatrix


if __name__ == '__main__':
    print("1) Identity matrix: \n", prettymatrix(identity(5)))
    print("2) Square matrix: \n", prettymatrix(square(5)))
    print("3) Transpose matrix: \n", prettymatrix(transpose(square(5))))
    A = [[1, 2, 3], [4, 5, 6]]
    B = [[7, 8], [9, 10], [11, 12]]
    print("4) Multriply two matrix: \n", prettymatrix(multiply(A, B)))
