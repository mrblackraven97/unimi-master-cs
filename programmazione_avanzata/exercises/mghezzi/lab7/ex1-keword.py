#!/usr/bin/env python3


class KeWord():

    def __init__(self):
        self.store = []
        self.read_data()

    def read_data(self):
        f = open("ex1-input.txt", "r")
        titles = f.readlines()
        [self.store.append(title.split()) for title in titles]

    def format_data(self):
        data = self.kw_calculate()
        formattedres = "{:>5}" + " {:>33}" + " {} " + " {:<40} " + "\n"
        res = ""
        for i in range(len(data)):
            line = self.string_line(data[i])
            res += formattedres.format(*line)
        return res

    def string_line(self, line):
        myline = []
        myline.append(line[0])
        myline.append(str(line[1]))
        myline.append(line[2])
        myline.append(str(line[3]))
        return myline

    def kw_calculate(self):
        data = self.store
        indexed = []
        for i in range(len(data)):
            for j in range(len(data[i])):
                if len(data[i][j]) < 2 or not(data[i][j] == 'The'):
                    myline = []
                    myline.append(i+1)
                    myline.append(data[i][:j])
                    myline.append(data[i][j])
                    myline.append(data[i][j+1:])
                    indexed.append(myline)
        indexed = sorted(indexed, key=lambda a_entry: a_entry[2])
        return indexed


if __name__ == '__main__':
    kw = KeWord()
    # for i in kw.format_data():
    #    print(i)
    print(kw.format_data())
