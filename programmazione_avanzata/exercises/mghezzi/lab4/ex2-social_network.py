#!/usr/bin/env python3


class Node:
    def __init__(self, name, surname, age, gender):
        self.name = name
        self.surname = surname
        self.age = age
        self.gender = gender
        self.points_to = []

    def get_key(self, key):
        if key == "name":
            return self.name
        if key == "surname":
            return self.surname
        if key == "age":
            return self.age
        if key == "gender":
            return self.gender
        raise Exception("key " + key + " is not valid!")

    def add_friend(self, friend):
        self.points_to.append(friend)

    def get_neighbours(self):
        return [i for i in self.points_to]

    def __str__(self):
        s = ""
        s += self.name + " " + self.surname + " "
        s += str(self.age) + " " + self.gender
        return s


class SocialNetwork:
    def __init__(self):
        self.nodes = {}

    def add_node(self, node):
        self.nodes[node] = node

    def get_nodes(self):
        return self.nodes

    def get_node(self, key):
        return self.nodes[key]

    def add_link(self, src_node, dest_node):
        self.nodes[src_node].add_friend(dest_node)
        self.nodes[dest_node].add_friend(src_node)

    def __str__(self):
        res = ""
        for n in self.nodes:
            neigh_list = n.get_neighbours()
            res += str(n) + " --> "
            for i in range(len(neigh_list)):
                res += str(neigh_list[i]) + ", "
            res += "\n"
        return res


if __name__ == "__main__":
    fb = SocialNetwork()

    n1 = Node("Marco", "Ghezzi", 23, "M")
    n2 = Node("Gianmarco", "Carraglia", 24, "M")
    n3 = Node("Nicolò", "Talignani", 23, "M")
    n4 = Node("Daniel", "Deleo", 23, "M")

    fb.add_node(n1)
    fb.add_node(n2)
    fb.add_node(n3)
    fb.add_node(n4)

    fb.add_link(n1, n2)
    fb.add_link(n1, n3)
    fb.add_link(n2, n4)
    fb.add_link(n3, n4)

    print(fb)
