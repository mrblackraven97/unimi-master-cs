#!/usr/bin/env python3


class pascal:
    def __init__(self):
        self.triangle = []

    def __iter__(self):
        return self

    def __next__(self):
        self.addLine()
        return self.triangle

    def prev(self):
        pass

    def addLine(self):
        ptriangle = self.triangle
        newline = []
        if len(ptriangle) <= 0:
            newline.append(1)
        else:
            lastpos = len(ptriangle)-1
            lastline = ptriangle[lastpos]
            for i in range(len(lastline)+1):
                if i == 0 or i == len(lastline):
                    newline.append(1)
                if i > 0 and i < len(lastline):
                    newline.append(lastline[i-1] + lastline[i])
        self.triangle.append(newline)
        return True


if __name__ == "__main__":
    pt = pascal()
    for i in range(5):
        print(next(pt))
