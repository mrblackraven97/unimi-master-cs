#!/usr/bin/env python3

import math


class shape:
    pass


class etriangle(shape):
    def __init__(self, base, height):
        self.__base = base
        self.__height = height

    def __lt__(self, other):
        return self.calculate_area() < other.calculate_area()

    def __str__(self):
        return "Triangle wiith area: " + str(self.calculate_area())

    def calculate_area(self):
        return (self.__base * self.__height) / 2

    def calculate_perimeter(self):
        return self.__base * 3


class circle(shape):
    def __init__(self, radius):
        self.__radius = radius

    def __lt__(self, other):
        return self.calculate_area() < other.calculate_area()

    def __str__(self):
        return "Circle wiith area: " + str(self.calculate_area())

    def calculate_area(self):
        return math.pi*(self.__radius**2)

    def calculate_perimeter(self):
        return 2*math.pi*self.__radius


class rectangle(shape):
    def __init__(self, base, height):
        self.__base = base
        self.__height = height

    def __lt__(self, other):
        return self.calculate_area() < other.calculate_area()

    def __str__(self):
        return "Rectangle wiith area: " + str(self.calculate_area())

    def calculate_area(self):
        return self.__base * self.__height

    def calculate_perimeter(self):
        return (self.__base+self.__height)*2


class square(shape):
    def __init__(self, side):
        self.__side = side

    def __lt__(self, other):
        return self.calculate_area() < other.calculate_area()

    def __str__(self):
        return "Square wiith area: " + str(self.calculate_area())

    def calculate_area(self):
        return self.__side*self.__side

    def calculate_perimeter(self):
        return self.__side*4


class pentagon(shape):
    def __init__(self, side):
        self.__side = side
        self.__apotema = self.__side * 0.688

    def __lt__(self, other):
        return self.calculate_area() < other.calculate_area()

    def __str_(self):
        return "Pentagon with area: " + str(self.calculate_area())

    def calculate_area(self):
        return (self.calculate_perimeter() * self.__apotema)/2

    def calculate_perimeter(self):
        return self.__side*5


class shapeiterator:
    def __init__(self, lst):
        lst.sort()
        self.__lst = lst
        self.__pos = 0

    def __iter__(self):
        return self

    def __next__(self):
        pos = self.__pos
        self.__pos += 1
        return self.__lst[pos]


if __name__ == '__main__':
    shapes = [square(7), circle(3.14), rectangle(6, 7), square(5), circle(7),
              rectangle(7, 2), square(2)]

    shapes.sort()
    print("3) List sorted by area: ")
    for i in range(len(shapes)):
        print(" ", shapes[i])

    print("5) Added an iterator: ")
    it = shapeiterator(shapes)
    print(" ", next(it))
    print(" ", next(it))
