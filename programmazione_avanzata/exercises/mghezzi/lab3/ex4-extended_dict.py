#!/usr/bin/env python3

import collections


class dictplus(dict):
    def __init__(self, d):
        self.dictionary = d
        self.sorting()

    def sorting(self):
        od = collections.OrderedDict(sorted(self.dictionary.items()))
        newd = {}
        for k, v in od.items():
            newd[k] = v
        self.dictionary = newd

    def add(self, key, value):
        self.dictionary[key] = value
        self.sorting()

    def __str__(self):
        self.sorting()
        return str(self.dictionary)


if __name__ == "__main__":
    mydict = {2: "a",
              1: "b",
              3: "c",
              5: "d",
              4: "e"
              }
    D = dictplus(mydict)
    D.add(0, "v")
    print(D)
