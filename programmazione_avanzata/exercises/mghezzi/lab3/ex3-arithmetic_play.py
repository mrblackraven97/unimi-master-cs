#!/usr/bin/env python3


class PolishCalculator:
    def __init__(self, txt):
        self.operations = ['**', 'not', '+', '-', '*', '/', 'or', 'and']
        self.txt = txt

    # evaluate the expression contained in the string 'str'
    # written in polish notation
    def eval(self):
        txt = self.txt
        stack = []
        op = []
        for i in txt[::-1].split():
            stack.append(i)
        while len(stack) > 0:
            # print(stack, op)
            i = stack.pop()
            if i in self.operations:
                if len(op) == 1:
                    stack.append(eval(str(i + str(op.pop()))))
                elif len(op) == 2:
                    stack.append(eval(str(str(op.pop()) + i + str(op.pop()))))
            else:
                op.append(i)
            if len(stack) == 1 and len(op) == 0:
                return stack.pop()

    # format the expression in the corresponding infix notation
    def __str__(self):
        stack = []
        num = []
        res = ""
        for i in self.txt[::-1].split():
            stack.append(i)
        while len(stack) > 0:
            i = stack.pop()
            if i in self.operations:
                if len(num) == 1:
                    res += i + str(num.pop())
                if len(num) == 2:
                    res += str(num.pop()) + i + str(num.pop())
            else:
                num.append(i)
        return res


if __name__ == '__main__':
    C = PolishCalculator("3 4 + 5 *")
    print(C, C.eval())
    C = PolishCalculator("3 - 5 +")
    print(C, C.eval())
