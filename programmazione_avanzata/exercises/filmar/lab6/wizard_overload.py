#!/usr/bin/env python3
from datetime import *

input1 = ["pierangelo", "arturo", "5/8/1987"]

class Person:
    def __init__(self, name, lastname, birthday):
        self.name = name
        self.lastname = lastname
        self.birthday = birthday
    
    def __getattr__(self, name):
        return self.__dict__[name]
    
    def __setattr__(self, name, value):
        self.__dict__[name] = value

class Wizard(Person):
    def __getattr__(self, name):
        if (name != "age"): return super().__getattr__(name)
        else:
            now = datetime.now().year
            miy = self.birthday.split("/")[2]
            return now - int(miy)
    
    def __setattr__(self, name, value):
        if (name != "age"): super().__setattr__(name, value)
        else:
            now = datetime.now().year
            newy = now - value
            temp = self.birthday.split("/")
            self.birthday = temp[0] + "/" + temp[1] + "/" + str(newy)


if (__name__=="__main__"):
    a = Person(*input1)
    print(a.name)
    a.name = "melanzana"
    print(a.name)

    b = Wizard(*input1)
    print(b.age)
    b.age = 200
    print(b.age)
    print(b.birthday)