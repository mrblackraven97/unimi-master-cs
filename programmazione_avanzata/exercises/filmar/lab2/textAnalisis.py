#!/usr/bin/env python3

input1 = ("testfile", "r+")

#input personalizzato
def customInput(filename, power):
    try:
        return open(filename, power)
    except:
        exit("file not found")

 #classe analizzatore di file 
class FileAnalizer:

    def __init__(self,name, power):
        self.file = customInput(name, power)

    def wordlist(self):
        text = self.file.read()
        result = []
        for i in  text.strip().split("\n"):
            result.append(i.strip().split(" "))
        return result
#contatore di parole, ritorna solo quelle maggiori di n (di default n è settato a 1)
def wordCounter(wordList, n=1):
    counter = {}
    result = {}
    for i in wordList:
        if (i == ['']): continue
        for j in i:
            if (j not in counter.keys()): 
                counter[j] = 1
            else:
                counter[j] += 1
                if(counter[j] >= n): result[j] = counter[j]
    return result
#ordina le parole in ordine di frequenza inversa
def wordSort(wordList : dict):
    tempList = wordList.items()
    return dict(sorted(tempList, key = lambda x : x[1], reverse=True))




if (__name__=="__main__"):
    fAnalizer = FileAnalizer(*input1)
    print(wordSort(wordCounter(fAnalizer.wordlist(), 3)))
