#!/usr/bin/env python3
import math

input1 = (math.pi/2, 31)
input2 = (3*math.pi/2, 30)

def customInput():
    node = input("insert value of corner: ")
    precision = input("insert the level of precision: ")
    return (node, precision)

def taylorSin(x,n):
    result = x
    for i in range(1,n+1):
        result += ((-1)**i)*((x**(2*i+1))/math.factorial(2*i+1))
    return round(result,15)

if (__name__=="__main__"):
    print (taylorSin(*input1), " original sin : ", round(math.sin(input1[0]),10))
    print (taylorSin(*input2), "original sin : ", round(math.sin(input2[0]),10))


