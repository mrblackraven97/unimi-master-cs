#!/usr/bin/env python3
from math import *

class EqTriangle:
    def __init__(self, side):
        self._side = side
        self._height = sqrt(3)/2*side

    def calcArea(self):
        return (self._side*self._height)/2
    
    def calcPerimeter(self):
        return 3*self._side
    
class Circle:
    def __init__(self, radius):
        self._radius = radius
    
    def calcArea(self):
        return pi*(2*self._radius/2)**2
    
    def calcPerimeter(self):
        return pi*(2*self._radius)
    
class rettangle:
    def __init__(self, basis, height=None):
        self._basis = basis
        if (height == None): self._height = basis
        else: self._height = height
    
    def calcArea(self):
        return self._basis*self._height
    
    def calcPerimeter(self):
        return (2*self._basis)+(2*self._height)

class regularPoligon:
    def __init__(self, side, apothem, sideN):
        self._side = side
        self._apothem = apothem
        self._sideN = sideN
    
    def calcArea(self):
        return (self.calcPerimeter()*self._apothem)/2
    
    def calcPerimeter(self):
        return self._side*self._sideN

class pentagons(regularPoligon):
    def __init__(self, side, apothem):
        super().__init__(side,apothem,5)

def areaSort(figures, reverse=False):
    return sorted(figures,reverse=reverse, key=lambda x:x.calcArea())

def perimeterSort(figures, reverse=False):
    return sorted(figures,reverse=reverse, key=lambda x:x.calcPerimeter())


input1 = [EqTriangle(5), Circle(5), rettangle(5,10), rettangle(5), pentagons(5,4), regularPoligon(5,4,6)]
   

if (__name__=="__main__"):
    af = areaSort(input1)
    for i in af: print(i.calcArea())
    print("\n\n")
    at = areaSort(input1, True)
    for i in at: print(i.calcArea())
    print("\n\n")
    pf = perimeterSort(input1)
    for i in pf: print(i.calcPerimeter())
    print("\n\n")
    pt = perimeterSort(input1, True)
    for i in pt: print(i.calcPerimeter())
    print("\n\n")




 
