# Advanced Programming

## Introduction

- **Professor**: Walter Cazzola
- **Login for study materials**: <http://cazzola.di.unimi.it/pa.html>
- **Username** : `pa`
- **Password** : `PA+#2009#`

### Timetable

| Day |    Time    | Classroom |        Type       |
| --- | ---------- | --------- | ----------------- |
| Mon |13.30-15.30 | 211       | Theory            |
| Tue |11.30-13.30 | 110/Ω     | Theory/Laboratory |
| ??? |11.30-13.30 | ???       | Office hours      |

### Suggested readings

| Author           | Title                                | Release year | Editor    |
| ---------------- | ------------------------------------ | ------------ | --------- |
| Mark Lutz        | Learning Python 4th edition          | 2009         | O'Reilly  |
| Mark Pilgrim     | Dive into Python 3                   | 2009         |  Apress   |
| Mark Summerfield | Programming in Python 3 a complete introduction to the python language | 2009 | Addison-Wesley |

### Contents of the course

- Intro to Python
- Data types
    - lists, tuples, dictionaries...
- Recursion
- Object-oriented programming 
    - duck typing, inheritance…
- Functional programming in a nutshell
    - list comprehension, map/filter...
- Closures and generators
- Iterators
- Reflection and meta-classes

### Exam mode

1 written exam, done in the laboratory (on a PC)

# Python in a nutshell

Python is a fairly old language, about 10 years older than Java. It is not totally object-oriented, it belongs to the *scripting languages* paradigm. 

Python is a general-purpose high-level programming language with high readability and productivity (low production time), and so it usually results very easily maintainable.

Main characteristics:

- Interpreted
- Dynamically typed
- Object based
- Open Source

Python supports different programming paradigms, but none of them 100%:

- Imperative
  - Functions
  - State
- Object based/oriented
  - Objects
  - Methods
  - Inheritance
  - No polymorphism
- Functional
  - Lambda abstractions
  - Generators
  - Dynamic typing

In this course we will use Python 3 because at the end of 2019 support for Python 2.7 (deprecated but still widely used) will end at the end of 2019.

# How To Python

A Python program can be written instruction by instruction in Python's shell. If you have Python installed in your system, you can get a Python 3 shell by just using the `python3` command in your terminal.

```bash
$ python3
Python 3.7.4 (default, Oct  4 2019, 06:57:26) 
[GCC 9.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> print("Hello World!")
Hello World!
```

Alternatively scripts can be run in the form of simple text files like so:

```bash
$ cat helloworld.py
print("Hello World!")
$ python3 helloworld.py
Hello World!!!
```

First Python program: *humanize.py*

```python
SUFFIXES = {1000: ['KB', 'MB', 'GB', 'TB', 'PB'],
            1024: ['KiB', 'MiB', 'GiB', 'TiB', 'PiB']} # è un dizionario

def approximate_size(size, a_kilobyte_is_1024_bytes = True):
   ''' Converte un file size in una forma leggibile per un umano ''' 
   # ^ a comment that works as a description for a documentation
   if size < 0:
      raise ValueError('number must be non-negative')

   # inline if operator
   multiple = 1024 if a_kilobyte_is_1024_bytes else 1000 
                                                    
   for suffix in SUFFIXES[multiple]:
      size /= multiple
      if size < multiple:
         return '{0:.1f} {1}'.format(size, suffix)
      raise ValueError('number too large')

# if there is no main, this code will be executed. 
# prevents fake calls on this file invocation
if __name__ == '__main__': 
   print(approximate_size(100000000, False)) 
   print(approximate_size(100000000))
   # no strict order needed for parameters
   print(approximate_size(False, 100000))
```

In Python, variable type is not necessary, that information is connected to the object himself and not held by the variable (label) name.

## Calling Functions

Functions in Python has no header files and (C/C++) no interface/implementation (Java). They also have no return type, but a value is always returned (*none* as default). They have also no parameter type, because the interpreter figures it out on his own.

Functions can have a default parameter:

```python
def approximate_size(size, a_kilobyte_is_1024_bytes = True):
    ...
    
if __name__ == '__main__': 
   print(approximate_size(100000000, False)) 
   print(approximate_size(100000000))
```

On the first call the parameter `a_kilobyte_is_1024_bytes` is set to `False`; on the second call it keeps the default value `True`. 

It is possible to pass values by name:

```python
approximate_size(a_kilobyte_is_1024_bytes=False, size=1000000000000)
```

## Everything Is An Object

The keyword `import` can be used to load python programs, they are threated as objects. To gain access to a specific public functionality or attribute of the imported module the **dot-notation** is used:

```python
>>> import humanize
>>> print(humanize.approximate_size(4096))
4.0 KiB
>>> print(humanize.approximate_size.__doc__)
Convert a file size to human-readable form.
```

By the `__doc__` attribute, it is possible to access to the documentation string of a function (`humanize.approximate_size.__doc__`).

In python everything is a **first-class object**, everything can be assigned to a variable or passed as an argument.

## Indenting Code

There are no explicit block delimiters in Python, the only delimiter is a column ':', the only way to indent code is by spaces:

- Code blocks (functions, statements, loops, ...) are defined by their indentation;

- White spaces and tabs are relevant: **use them consistently**;

- Indentation is checked by the compiler;

## Exception

Python exceptions are anomaly situations, the language encourages the use of exceptions with you handles.

To raise an exception the `raise` statement is used:

```python
raise ValueError('number must be non-negative')
```

There is no need to list the exceptions in the function declaration.

To handle exception a `try ... except` block is used:

```python
try:
	from lxml import etree
except ImportError:
	import xml.etree.ElementTree as etree
```

## Running Scripts

Modules are objects, they have a built-in attribute `__name__` whose value depends on how you call it:

- if imported, it contains the name of the file without path and extension;
- if run as stand-alone program, it contains the *"main"* string;

```python
>>> import humanize
>>> humanize.__name__
’humanize’
```

