# Functional Programming

The Functional Programming is a programming paradigm that treats computation as the evaluation of mathematical functions and avoids changing-state and mutable data and has an extensive use of recursion as primary control structure.

In Python, functions are first class objects and reated as data, so **data and functions are equivalent**.

All these characteristics make for more rapidly developed, shorter and less bug-prone code, and it's also a lot easier to prove formal properties of functional languages programs than of imperative languages and programs.

Functional programming tends to remove state, to eliminate side-effects coming from unexpected values of variables (one of the most common mistakes). 

 Python has functional capability since its first release, and his basic elements are:

- `map()`: to apply a function to a sequence
	
  ``` python
  >>> import math, functools
  >>> print(list(map(math.sqrt, [x**2 for x in range(1,11)])))
  [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0]
  ```
  
- `filter()`: to extract from a list those elements which verify the passed function
  
    ``` python
    >>> def odd(x): return (x%2 != 0)
    >>> print(list(filter(odd, range(1,30))))
    [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29]
    ```
- `reduce()`: to reduce a list to a single element according to the passed function
  
    ``` python
    >>> def sum(x,y): return x+y
    >>> print(functools.reduce(sum, range(1000)))
    499500
    ```

**N.B.** `map()` and `filter()` return an iterator.

## Eliminating the IF Statement

In the functional programming paradigm, control structures of selection and iteration does not exist, so we need to find a way to reproduce them.

**Short-Circuit conditional** calls used instead of `if`:

``` python
def cond(x):
# with the short-circuit if first condition returns true than the second part is  	  # true as well, so it is returned
    return (x==1 and 'one') or (x==2 and 'two') or 'other' 

if __name__ == "__main__":
    for i in range(3):
        print("cond({0}) :- {1}".format(i, cond(i)))
```

Using abstractions, it is possible to create functions `cond()` using **lambda expressions**, they are anonymous functions that can be executed as normal ones, but without having a specific name.

``` python
block = lambda s: s

cond = \
	lambda x: (x==1 and block("one")) or (x==2 and block("two")) or (block("other"))

if __name__ == "__main__":
	print("cond({0}) :- {1}".format(3,cond(3)))
```

## Lambda Functions

The name lambda comes from *λ-calculus* which uses the Greek letter *λ* to represent a similar concept.

Lambda is a term used to refer to **anonymous functions**, that is a block of code which can be executed as if it were a function, but without a name.

Lambdas can be defined anywhere a legal expression can occur.

Lambda syntax: `lambda "args" : "an_expression_on_the_args"`

```python
>>> add = lambda i,j: i+j
>>> print(functools.reduce(add, range(1000)))
499500
```

### Example: Evolving the Factorial

Traditional implementation:

```python
def fact(n):
    return 1 if n<=1 else n*fact(n-1)
```

Short-circuit implementation

```python
def factSC(n):
    return (n<=1 and 1) or n*factSC(n-1)
```

`reduce()`-based implementation

```python
from functools import reduce
def factR(p):
    return reduce(lambda n,m : n*m, range(1,p+1)) 
# Generating a set of n numbers, multiplying them using the lambda n*n, and using the reduce to store and return the result of the computation.
```

## Eliminating the Sequence Statements

Sequential program flow is typical of imperative programming, it relies on side-effect (variables assignments). This is basically in contrast with the functional approach.

In a list processing style we have:

```python
# let’s create an execution utility function
do_it = lambda f: f()
# let f1, f2, f3 (etc) be functions that perform actions
map(do_it, [f1,f2,f3])		# map()-based action sequence
```

- Single statements of the sequence are replaced by functions;
- The sequence is realized by mapping an activation function to all the function objects that should compose the sequence

Combining techniques used before, it is possible to avoid using iterative control statement like `while`

### Example : Echo function

A echo function is a function that repeats what is written on the terminal.

*Statement-based implementation*

```python
def echo_IMP():
	while True:
		x = input("FP -- ")
		if x == ’quit’: 
            break
	else: 
        print(x)
        
if __name__ == "__main__": 
    echo_IMP()
```

*Functional implementation*

```python
# Utility function for identity whit sideffect (a monad)
def monadic_print(x): 
    print(x)
    return x
    
# Lambda recursive function without parameters that keeps printing what is passed via standard input, untill the "quit" string arrives.
echo_FP = lambda : monadic_print(input("FP -- ")) == 'quit' or echo_FP()    
    
if __name__ == "__main__": 
    echo_FP()
```

## Whys

Functional programming helps eliminating many side-effects. Mostly all errors depend on variables that obtain unexpected values. 

Functional programs bypass the issue by not assigning values to variables at all.
