# Metaclasses

**Metaclasses** are a mechanism to gain a **high-level of control** over how a set of classes works, they:

- Intercept and augment class creation;
- Provide an API to insert extra-logic at the end of `class` statement;
- Provide a general protocol to manage class objects in a program.

This new added logic does not rebind the class name to a decorator callable, but rather routes creation of the class itself to specialized logic.

Metaclasses add code to be run at class creation time and not at instance creation time.

Classes are instances of `type`. Metaclasses are subclasses of `type`.

The type `type`:

- Is a class that generates user-defined classes;
- Metaclasses are subclasses of the `type` class;
- Class objects are instances of the `type` class, or a subclass thereof;
- Instance objects are generated from a class.

At the end of class statement, after filling `__dict__`, Python calls '`class = type(classname, superclasses, attributedict)`' to create the `class` object.

The `type` object defines a `__call__` operator that calls `__new__` to create class objects and `__init__` to create instance objects when `type` object is called.

## Metaclass in Python

To create a class with a custom metaclass you have just to list the desired metaclass as a keyword argument in the `class` header: `class Spam(metaclass=Meta)`.

Metaclasses are subtype of `type`, and to code one the operators `__new__`, `__init__` and `__call__` must be overridden.

```python
class MetaOne(type):
	def __new__(meta, classname, supers, classdict):
		print(’In MetaOne new: ’, classname, supers, classdict, sep=’\n...’)
		return type.__new__(meta, classname, supers, classdict)
	def __init__(Class, classname, supers, classdict):
		print(’In MetaOne init:’, classname, supers, classdict, sep=’\n...’)
		print(’...init class object:’, list(Class.__dict__.keys()))

class Eggs:
    pass

print('making class')

class Spam(Eggs, metaclass=MetaOne):
	data = 1
	def meth(self, arg): 
        pass

print(’making instance’)
X = Spam()
print(’data:’, X.data)
```

```python
>>> python3 metaone.py
making class
In MetaOne new:
...Spam
...(<class ’__main__.Eggs’>,)
...{’__module__’: ’__main__’, ’data’: 1, ’meth’: <function meth at 0xb79d99ac>}
In MetaOne init:
...Spam
...(<class ’__main__.Eggs’>,)
...{’__module__’: ’__main__’, ’data’: 1, ’meth’: <function meth at 0xb79d99ac>}
...init class object: [’__module__’, ’data’, ’meth’, ’__doc__’]
making instance
data: 1
```

In spite of the syntax, metaclasses and superclasses are quite different:

- Metaclasses inherit from the `type` class;
- Metaclass declarations are inherited by subclasses;
- Metaclass attributes are not inherited by class instances.
