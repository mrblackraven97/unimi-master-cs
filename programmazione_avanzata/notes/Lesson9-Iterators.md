# Iterators

Iterators are special container objects in Python that respect the **iterator protocol**, implementing:

- A method **\_\_iter\_\_,** to build the iterator structure;
- A method **\_\_next\_\_**, to get the next element in the container;
- An exception **StopIteration**. to notify when data in container are finished.

**N.B.** Generators are a special case of iterators.

```python
class Fib:
'''iterator that yields numbers in the Fibonacci sequence'''
def __init__(self, max):
	self.max = max
def __iter__(self):
	self.a = 0
	self.b = 1
	return self
def __next__(self):
	fib = self.a
	if fib > self.max: 
        raise StopIteration
	self.a, self.b = self.b, self.a + self.b
	return fib

if __name__ == "__main__":
	f = Fib(1000)
	for i in f: 
        print(i)
```

## Cryptarithms

A **cryptarithms** is a riddle in the form:

​															`HAWAII + IDAHO + IOWA + OHIO == STATES`

- The letters spell out actual words and a meaningful sentence;

- Each letter can be translated to a digit (0-9), but no initial can be a 0;

- To the same letter corresponds the same digit along the whole sentence and no digit can be associated to two different letters;

- The resulting arithmetics equation represents a valid and correct equation.

  ​												 `HAWAII + IDAHO + IOWA + OHIO == STATES`

  ​											     `510199 + 98150 + 9301 + 3593 == 621246`

### First solution: Brute force

First step consists of organizing the data:

- Find the words that need to be translated;
- Determine which characters compose such a sentence;
- Determine which characters are at the beginning of the words

Then, look for the solution, if it exists, by:

- Generating every possible permutation of ten digits (0-9);
- Skimming those permutations with 0 associated to an initial;
- Trying if the remaining permutations represent a valid solution.

```python
import re, itertools, sys

def solve(puzzle):
	words = re.findall(’[A-Z]+’, puzzle.upper())
	unique_characters = set(’’.join(words))
	assert len(unique_characters) <= 10, ’Too many letters’
	first_letters = {word[0] for word in words}
	n = len(first_letters)
	sorted_characters = ’’.join(first_letters) + ’’.join(unique_characters-									first_letters)
	characters = tuple(ord(c) for c in sorted_characters) # generator expression
	digits = tuple(ord(c) for c in ’0123456789’)
	zero = digits[0]
	for guess in itertools.permutations(digits, len(characters)):
		if zero not in guess[:n]:
			equation = puzzle.translate(dict(zip(characters, guess)))
		if eval(equation): 
            return equation

if __name__ == ’__main__’:
	for puzzle in sys.argv[1:]:
		print(puzzle)
		solution = solve(puzzle)
		if solution: 
            print(solution)
```

## Itertools Module

The Python module `Itertools` gives come cool methods to work with iterators:

- Combinatoric generators: `permutations()`, `combinations()`, and more;

  ```python
  >>> list(itertools.combinations(’ABCD’,2))
  [(’A’, ’B’), (’A’, ’C’), (’A’, ’D’), (’B’, ’C’), (’B’, ’D’), (’C’, ’D’)
  ```

- Infinite iterators: `count()`,  `cycle()` and `repeat()`;

  ```python
  >>> list(itertools.repeat(’ABCDF’,3))
  [’ABCDF’, ’ABCDF’, ’ABCDF’]
  ```

- Predefined iterators: `zip_longest()`, `groupby()`, `islice()`;

  ```python
  >>> list(itertools.starmap(lambda,y:x+y,itertools.zip_longest(’a’*7,’1234567’)))
  [’a1’, ’a2’, ’a3’, ’a4’, ’a5’, ’a6’, ’a7’]
  >>> names = [’alpha’, ’beta’, ’gamma’, ’delta’, ’epsilon’, ’zeta’, ’eta’,
  	’theta’, ’iota’, ’kappa’, ’lambda’, ’nu’, ’mu’, ’xi’, ’omicron’, ’pi’,
  	’rho’, ’sigma’, ’tau’, ’upsilon’, ’phi’, ’chi’, ’psi’, ’omega’]
  >>> groups = itertools.groupby(sorted(names, key=len), len)
  >>> for g, itr in groups: print(list(itr), end=’ ’)
  [’nu’, ’mu’, ’xi’, ’pi’] [’eta’, ’rho’, ’tau’, ’phi’, ’chi’, ’psi’]
  [’beta’, ’zeta’, ’iota’] [’alpha’, ’gamma’, ’delta’, ’theta’, ’kappa’, ’sigma’,
  ’omega’] [’lambda’] [’epsilon’, ’omicron’, ’upsilon’]
  ```