# Managed Attributes

Let us consider the classic implementation for the `account` class:

```python
class account:
	def __init__(self, initial_amount):
		self.amount = initial_amount
	def balance(self):
		return self.amount
	def withdraw(self, amount):
		self.amount -= amount
	def deposit(self, amount):
		self.amount += amount

if __name__ == "__main__":
	a = account(1000)
	print("The current balance is {0}".format(a.balance()))
	a.withdraw(100)
	a.deposit(750)
	print("The current balance is {0}".format(a.balance()))
	a.withdraw(3000)
	print("The current balance is {0}".format(a.balance()))
```

We need to add some functionalities to maintain the class in a consistent state (`amount` should never have negative value).

A clean approach is to automatically execute extra code when an attribute is accessed.

Python provides three approaches:

- Properties;
- Descriptor protocol;
- Operation overloading.

## Properties Approach

Implementing the property extending the class account and implementing an `assert`:

```python
import account

class safe_account(account.account):
	def __init__(self, initial_amount):
		self._amount = initial_amount
	def save_get(self):
		return self._amount
	def save_set(self, amount):
		assert amount > 0, ’Not admitted operation: the final balance ({0}) MUST be 				positive’.format(amount)
		self._amount=amount
		amount = property(save_get, save_set, None, "Managed balance against 						excessive withdrawals")
```

This approach will generate an assertion error when we try to take more that it is available.

```python
>>> a.withdraw(3000)
Traceback (most recent call last):
	File "account+property.py", line 19, in <module>
		a.withdraw(3000)
	File "/home/cazzola/esercizi-pa/managed/account.py", line 7, in withdraw
		self.amount -= amount
	File "account+property.py", line 9, in save_set
		assert amount > 0, ’Not admitted operation: the final balance ({0}) MUST be 			positive’.format(amount)
AssertionError: Not admitted operation: the final balance (-1350) MUST be positive
```

But with the use of `property` it is possible do dynamically calculate the balance

```python
class account_with_calculated_balance:
	def __init__(self, initial_amount):
		self._deposits = initial_amount
		self._withdrawals = 0
	def deposit(self, amount):
		self._deposits += amount
	def withdraw(self, amount):
		self._withdrawals += amount
	def calculated_balance(self):
		return self._deposits-self._withdrawals
	def zeroing_balance(self):
		self._deposits = 0
		self._withdrawals = 0
	balance = property(calculated_balance, None, zeroing_balance, "Calculate 				Balance")
```

## Descriptor Protocol Approach

The descriptor protocol consists in having a descriptor class to add in out main class that helps avoiding red balances.

```python
import account

class safe_descriptor:
	"""Managed balance against excessive withdrawals"""
	def __get__(self, instance, owner):
		return instance._amount
	def __set__(self, instance, amount):
		assert amount > 0, ’Not admitted operation: the final balance ({0}) MUST be 				positive’.format(amount)
		instance._amount=amount

class safe_account(account.account):
	def __init__(self, initial_amount):
		self._amount = initial_amount
	amount = safe_descriptor()

if __name__ == "__main__":
	a = safe_account(1000)
	print("The current balance is {0}".format(a.balance()))
	a.withdraw(100)
	a.deposit(750)
	print("The current balance is {0}".format(a.balance()))
	a.withdraw(3000)
	print("The current balance is 0".format(a.balance()))
```

```python
The current balance is 1000
The current balance is 1650
Traceback (most recent call last):
	File "account+descriptors.py", line 22, in <module>
		a.withdraw(3000)
	File "/home/cazzola/esercizi-pa/managed/account.py", line 7, in withdraw
		self.amount -= amount
	File "account+descriptors.py", line 8, in __set__
		assert amount > 0, ’Not admitted operation: the final balance ({0}) MUST be 			positive’.format(amount)
AssertionError: Not admitted operation: the final balance (-1350) MUST be positive
```

## Operation Overloading Protocol Approach

This protocol make uses of overloading some of those methods:

- `__getattr__`, run for fetches on undefined attributes;
- `__getattribute__`, is run for fetches on every attribute, so when using it you must be cautious to avoid recursive loops by passing attribute accesses to a superclass;
- `__setattr__`, run for setting value on attributes;
- `__delattr__`, is run for deletion on every attribute.

```python
import account
class safe_account(account.account):
	def __setattr__(self, attr, amount):
		assert amount > 0, ’Not admitted operation: the final balance ({0}) MUST be 				positive’.format(amount)
		self.__dict__[attr] = amount
        
if __name__ == "__main__":
	a = safe_account(1000)
	print("The current balance is {0}".format(a.balance()))
	a.withdraw(100)
	a.deposit(750)
	print("The current balance is {0}".format(a.balance()))
	a.withdraw(3000)
	print("The current balance is 0".format(a.balance()))
```

```python
The current balance is 1000
The current balance is 1650
Traceback (most recent call last):
	File "account+overloading.py", line 16, in <module>
		a.withdraw(3000)
	File "/home/cazzola/esercizi-pa/managed/account.py", line 7, in withdraw
		self.amount -= amount
	File "account+overloading.py", line 7, in __setattr__
		assert amount > 0, ’Not admitted operation: the final balance ({0}) MUST be 				positive’.format(amount)
AssertionError: Not admitted operation: the final balance (-1350) MUST be positive
```

### Use of \_\_getattr\_\_ vs \_\_getattribute\_\_

The `__getattr__` is invoked only when we try to get an attribute that is not defined:

```python
class GetAttr:
	attr1 = 1
	def __init__(self):
		self.attr2 = 2
	def __getattr__(self, attr):
		print(’get: ’ + attr)
		return 3
```

```python
>>> from GetAttr import GetAttr
>>> X=GetAttr()
>>> print(X.attr1)
1
>>> print(X.attr2)
2
>>> print(X.attr3)
get: attr3
3
```

The `__getattribute__` is always called when we look for an attribute:

```python
class GetAttribute(object):
	attr1 = 1
	def __init__(self):
		self.attr2 = 2
	def __getattribute__(self, attr):
		print(’get: ’ + attr)
		if attr == ’attr3’:
			return 3
		else:
		return object.__getattribute__(self, attr)
```

```python
>>> from GetAttribute import GetAttribute
>>> X = GetAttribute()
>>> print(X.attr1)
get: attr1
1
>>> print(X.attr2)
get: attr2
2
>>> print(X.attr3)
get: attr3
3
```
