# Object-Oriented Programming - Part 2

## Attributes

It's possible to add attributes to objects dynamically, making a difference between class attributes and instance attributes:

```python
class C:
	def __init__(self):
		self.class_attribute="a value"
	def __str__(self):
		return self.class_attribute
```

```python
[15:18]cazzola@hymir:~/oop>python3
>>> from C import C
>>> c = C()
>>> print(c)
a value
>>> c.class_attribute
’a value’
>>> c1 = C()
>>> c1.instance_attribute = "another value"
>>> c1.instance_attribute
’another value’
>>> c.instance_attribute
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
AttributeError: ’C’ object has no attribute ’instance_attribute’
>>> C.another_class_attribute = 42
>>> c1.another_class_attribute, c.another_class_attribute
(42, 42)
```

An alternative way to access attributes is with the `__dict__` default class attribute. It is a dictionary that contains the user-provided attributes and permits to introspect them.

```python
>>> def introspect(self):
...		result=""
...		for k,v in self.__dict__.items():
...			result += k + ": " + v + "\n"
...		return result
...
>>> C.__str__ = introspect
>>> print(c)
class_attribute: the answer

>>> print(c1)
class_attribute: a value
instance_attribute: another value
```

## Methods

Functions are not accessed through the dictionary of the class, they must be bound to an instance.

A bound method is a callable object that calls a function passing an instance as the first argument.

Python does not support multiple inheritance: if in languages like C++ it was possible to manually choose which implementation obscure with keywords like `virtual`, here we have the **Method Resolution Order**, a mechanism that automatically finds superclasses of classes on witch a method is getting used and then calls the relative method on the parent too and so on until reaching the root method.

## The Diamond Problem

![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Diamond_inheritance.svg/220px-Diamond_inheritance.svg.png)

The Diamond Problem is an ambiguity that happens when two classes (B, C) inherit from the same class (A), and a new class (D) inherits those classes (B, C): if there is a method in A that B and C have reimplemented and gets called by D, we don't know which implementation refer to!

***Class A (Root)***


``` python
class A(object):
	def do_stuff(self):
    	# does stuff for a
        return
```

***Class B (Inner node)***


``` python
class B(A):
	def do_stuff(self):
    	A.do_stuff(self)
        # does stuff for b
        return
```

**Class C (Inner node)**


``` python
class C(A):
	def do_stuff(self):
    	A.do_stuff(self)
        # does stuff for c
        return
```

***Class D (Leaf)***


``` python
	class D(B,C):
        def do_stuff(self):
            B.do_stuff(self)
            C.do_stuff(self)
            # does stuff for d
            return
```

With two copies of A:

- if `do_stuff()` is called once, `B` or `C` is incomplete;
- if called twice, it could have undesired side-effects.

The Method Resolution in Python helps to dynamically determine which `do_stuff()` to call every time by linearizing the problem.

To linearize the problem, it is created a list of calls to use for `D` (`D.next_class_list=[D,B,C,A]`) according to his parents, and calling the relative methods in order.

```python
B.next_class_list = [B,A]
C.next_class_list = [C,A]
D.next_class_list = [D,B,C,A]

class B(A):
	def do_your_stuff(self):
		next_class = self.find_out_whos_next(B)
		next_class.do_your_stuff(self)
		# do stuff with self for B
	def find_out_whos_next(self, clazz):
		l = self.next_class_list # l depends on the actual instance
		mypos = l.index(clazz)	 # Find this class in the list
		return l[mypos+1] 		 # Return the next one
```

The Method Resolution Order (MRO) also give two tools:

- `__mro__` keeps the list of the superclasses without duplicates in a predictable order;
- `super` is used in place of the `find_out_whos_next()`.

Computing with the MRO:

- if `A` is a superclass of `B`, then `B > A;`
- if `C` precedes `D` in the list of bases in a class statement, then `C > D`;
- if `E > F` in one scenario, then `E > F` must hold in all scenarios.

## Special Methods

There are special methods that can be defined to grant a correct behavior of the class (similar to Java standard methods like `equals()`, `hash()` e `toString()`):

- `__len__()`, to declare the "length" attribute of your class;
- `__str__()`, the '`toString()`' of Python;
- `__lt__()`, comparator method, to define that `this` class is "less than" another;
- `__add__()`, that overrides the "+" operator

Those special methods are class methods, and can't be changed through an instance and can be called statically on the class (like `C.__len__()`).

To be more flexible, those methods must be forwarded to a method that can be overridden in the instance.

Also built-in types, as `lists` and `tuples` can be subclassed: unfortunately the subtype of `list` allows the adding of attributes (due to the presence of `__dict__`).

The presence of `__slots__` in a class definition inhibits the introduction of `__dicts__`, disallowing any user-defined attributes, and this can't change structure/scheme (like a Java class can't be dynamically changed).

``` python
class MyList(list):
	''' List that converts added items to ints '''
	def append(self, item):
		list.append(self, int(item))
	def __setitem__(self, key, item):
		list.__setitem__(self,key,int(item))

class MyList2(list):
    ''' List with no attributes '''
    __slots__ = []
   
class MyList3(list):
    ''' List that only contains an attribute called "color" '''
    __slots__ = ['color']
    
class IntList(list):
    ''' List that contains only ints '''
    def __init__(self, itr):
        list.__init__(self, [int(x) for x in itr])
    def append(self, item):
        list.append(self, int(item))
    def __setitem__(self, key, item):
        list.__setitem__(self, key, int(item))
```

```python
[10:45]cazzola@hymir:~/esercizi-pa>python3
>>> l = MyList()
>>> l.append(1.3)
>>> l.append(444)
>>> l
[1, 444]
>>> len(l)
2
>>> l[1] = 3.14
>>> l
[1, 3]

>> m2 = MyList2()
>>> m2.color = ’red’
Traceback (most recent call last):
	File "<stdin>", line 1, in <module>
AttributeError:
	’MyList2’ object has no attribute ’color’

>>> m3 = MyList3()
>>> m3.color = ’red’
>>> m3.weight = 50
Traceback (most recent call last):
	File "<stdin>", line 1, in <module>
AttributeError:
	’MyList3’ object has no attribute ’weight’
```

