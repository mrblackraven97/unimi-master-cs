# Dynamic typing

Python is dynamically typed, there is no need to explicit type name on a variable.

``` python
a = 42 # Object 42 knows to be an integer, there's no nedd to write 'int a = 42'
```

On such an assignment, thee separate concepts happens:

- **Variable creation**, python works with the name, not the content;
- **Variable types**, no type associated to the variable name, type live with the object;
- **Variable use**, the name is replaced by the object when used in an expression.

The compiler just creates an object that represents the value 42, then creates the label `a` if not already used, then links the label to the object.

``` python
a = 42 	  	# a is an integer
a = 'spam'	# a becomes a string
a = 3.14	# a becomes a floating point
```

Coming from typed languages (like Java or C++) it looks like `a` is changing type, but in Python **names have no type**.

The variable reference is just changed to a different object: **objects know what type they have**, each one has an header field that tags it with its type, and if objects know their type, variables don't have to.

When the variable is reassigned, the space held by the referenced object is reclaimed by the **garbage collector**, unless another variable is referring to it.

``` python
>>> a = [1,2,3]
>>> b = a			# b e a sono due etichette e puntano entrambe alla lista [1,2,3]
>>> b[1] = 'spam'	# cambio la posizione 1 della lista puntata da b: cambierà anche da a?
>>> b
[1, 'spam', 3]
>>> a
[1, 'spam', 3]		# Sì, il cambio è avvenuto: quindi a e b sono dei meri puntatori alla lista, quasi come delle variabili reference in java
```

## References and Equality

For each object there are two ways to check equality:

- To check if two labels points to equal but different objects (same content) use `==`;
- To check if two labels point to the same object use `is`.

``` python
>>> a = [1,2,3]
>>> b = [1,2,3]
>>> c = a
>>> a == b	# a and b have the same value (equal lists)
True
>>> a == c	# a and c points to the same object
True
>>> a is b	# a are b are different objects (different but equals)
False
>>> a is c	# a and c points to the same object
True
```

This mechanism doesn't apply with primitive types, because they are considered as singletons. This happens because Python preistantiates objects that are likely to be use (like the first 256 integers), and those objects are chached to improve performance.

## Passing Arguments

By default, arguments are passed to function by **value** (`fake_mutable(X, L)`) and by **reference** (`fake_mutable(X, L[:])`)

*Example: arguments passed by value*

``` python
X = 42
L = [1,2,3]
def fake_mutable(i,l):
    i = i*2
    l[1] = '?!?!'
    l = {1,3,5,7}
```

``` python
>>> print("X :- {0} \t L :- {1}".format(X,L))
X :- 42 	L :- [1, 2, 3]  # original X and L values
>>> fake_mutable(X,L)
>>> print("X :- {0} \t L :- {1}".format(X,L))
X :- 42 	L :- [1, '?!?!', 3]	# values of X and L modified by fake_mutable()
```

As we can see, the variable X has been passed by value, so his original value outside of the function has not been modified, but variable L has been altered by the instruction `l[1] = '?!?!'`. The list is not altered, but the inner values can be!!!

Global values are immutable, they can only be changed using `global`

``` python
def mutable():
    global X, L
    X = X*2
    L[1] = '?!?!'
    L = {1,3,5,7}
```

``` python
>>> print("X :- {0} \t L :- {1}".format(X,L))
X :- 42 	L :- [1, 2, 3]
>>> mutable()					 # modifying X, L as global values
>>> print("X :- {0} \t L :- {1}".format(X,L))
X :- 84 	L :- {1, 3, 5, 7}	 # X and L have both been changed
```

## Currying: Closures in Action

**Currying** is a technique that tends to reduce function's parameters, binding one of the values and creating a new function with one less parameter, until reaching a function with only one parameter.

f(x,y) = y+x	==>	g(y) = f(2,y) = y + 2	==>	g(3) = 3+2

``` python
def make_currying(f,a):
	def fc(*args):
        return f(a,*args)
	return fc

def f2(x, y):
    return x+y

def f3(x, y, z):
    return x+y+z

if __name__ == "__main__":
    a = make_currying(f2, 3)
	b = make_currying(f3, 4)
	c = make_currying(b, 7)
	print("(cf2 3)({0}) :- {1}, (cf3 4)({2},{3}) :- {4}".format(1,a(1),2,3,b(2,3)))
	print("((cf3 4) 7)({0}) :- {1}".format(5,c(5)))
    
```

**N.B.** Look at `partial` in `functools`