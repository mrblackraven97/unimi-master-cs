# Valutazione della stima di verosimiglianza

## Recap

Abbiamo parlato l'altra volta di come vogliamo **stimare la vera uscita** dal nostro modello $z_i$ e come in realtà l'uscita che misuriamo è affetta da un certo errore $v_i$. E' stato dimostrato che questo **errore** può essere può essere modellato da una variabile statistica, in particolare una **gaussiana**, avente un certo valor medio $\mu$ e una certa varianza $\sigma^2$.

Siamo infine arrivati a dedurre che ogni uscita **dipende condizionalmente** dai parametri del modello $w$ e dagli ingressi $u$, pertanto considerando le misurazioni come eventi indipendenti avremo che la probabilità di ottenere una certa serie di misurazioni pre-definita è pari al **prodotto delle probabilità congiunte**, ovvero
$$
p(z_{1m},\ ...\ ,\ z_{Nm}) = p(z_{1m}) \cdot \ ...\ \cdot\ p(z_{Nm})=\prod_i{p(z_{im})}
$$
Alla quale abbiamo poi assegnato la funzione di **Likelihood** $L$, che indica quanto più i valori misurati si accostano a quelli reali. L'idea era di **massimizzare** queste probabilità tramite il fine tuning dei parametri $w$ o degli ingressi $u$ con un problema appunto di massimizzazione: tuttavia massimizzare una produttoria può essere complesso, pertanto vogliamo cercare una strada alternativa.

## Stima a massima verosimiglianza

Siccome la funzione logaritmo è monotona, quindi non altera il massimo o il minimo della funzione al suo interno, segue che possiamo utilizzarla nella nostra produttoria. Per questioni di ottimizzazione, anzichè trovare il massimo del logaritmo, calcoleremo il suo inverso ne troveremo il minimo. 

Ricordandoci inoltre delle proprietà dei logaritmi, il prodotto fra gli argomenti di un logaritmo equivale alla somma del logaritmo degli argomenti (nella stessa base). Pertanto possiamo tradurre la produttoria in una più semplice sommatoria:
$$
-ln\ \prod_i{p(z_{im})}\ = - \sum_i ln(p(z_{im}))
$$
e infine, siccome abbiamo detto che $z_{im}$ è affetta da un rumore gaussiano, le sue probabilità saranno calcolate dalla relativa formula, pertanto avremo
$$
- \sum_i ln(p(z_{im})) = - \sum_i ln(\frac{1}{(\sqrt{2\pi})\sigma}*e^{-\frac12(\frac{z_{im}-(mu_i+q)}{\sigma})^2})
$$
Questa formula si basa sul modello lineare di cui abbiamo parlato l'altra volta: vogliamo trovare la retta di equazione $mu+q$ con distanza minima da tutti i punti affetti da errore per una stima dei veri valori di uscita.

Pertanto se vogliamo trovare il minimo di questa funzione, ovvero la retta che massimizza la Likelihood $L$, dovremo calcolarne la derivata per trovare i punti in cui vale zero: ci rimane solo scegliere su quale parametro derivare fra $m$, $u$ o $q$.

### Stima a massima verosimiglianza di $m$ 

Svolgendo la derivata prima della sopracitata funzione rispetto a $m$ otterremmo che
![](img/derm.png)

il quale finalizza in
$$
\frac{1}{\sigma^2} \sum_i^n (z_{im}-(mu_i+q))(-u_i) = 0 \rightarrow \sum_i^n (z_{im}u_i-mu_i^2-qu_i) = 0
$$
Semplifichiamo la frazione con sigma di fronte alla sommatoria poiché dall'altro lato dell'equazione c'è zero e moltiplichiamo $u_i$ con l'altro termine. Siccome vogliamo ora riscrivere il tutto cercando di evidenziare i parametri per $m$ e $q$, la moltiplicazione ci consente di spezzare la sommatoria nei diversi termini
$$
\bigg[\sum_i^n (z_{im}u_i)\bigg]  - m \cdot \bigg[\sum_i^n u_i^2\bigg] - q \cdot \bigg[\sum_i^n u_i\bigg] = 0
$$
e a questo punto, riscriverla come una retta in forma esplicita:
$$
m \cdot \bigg[\sum_i^n u_i^2\bigg] + q \cdot \bigg[\sum_i^n u_i \bigg]= \bigg[\sum_i^n (z_{im}u_i) \bigg]
$$

### Stima a massima verosimiglianza di $q$

In questo caso, il calcolo della derivata è analogo

![](img/derq.png)

Il primo termine si elide poichè tutti i termini noti mentre continuamo a derivare il secondo 

![](img/derq2.png)

Otteniamo quindi, spezzando la sommatoria
$$
\bigg[\sum_i^n (z_{im})\bigg]  - m \cdot \bigg[\sum_i^n u_i\bigg] - q \cdot \bigg[\sum_i^n 1\bigg] = 0
$$
e riformulandola in forma esplicita
$$
m \cdot \bigg[\sum_i^n u_i\bigg] + q \cdot \bigg[\sum_i^n 1\bigg] = \bigg[\sum_i^n (z_{im})\bigg]
$$
ma la sommatoria del parametro q $ \sum_i^n 1$ è pari a $n$ pertanto lo riscriveremo come
$$
m \cdot \bigg[\sum_i^n u_i\bigg] + q \cdot n = \bigg[\sum_i^n (z_{im})\bigg]
$$

### Massimizzare la verosimiglianza della retta

Ora che abbiamo calcolato queste due equazioni che massimizzano rispettivamente $m$ e $q$, le vogliamo porre a sistema, in modo da ottenere 2 equazioni lineari in 2 incognite.
$$
\begin{cases} 
m \cdot \bigg[\sum_i^n u_i^2\bigg] + q \cdot \bigg[\sum_i^n u_i \bigg]= \bigg[\sum_i^n (z_{im}u_i) \bigg] \\ 
m \cdot \bigg[\sum_i^n u_i\bigg] + q \cdot n = \bigg[\sum_i^n (z_{im})\bigg] \\
\end{cases}
$$
In questo modo possiamo risolverle secondo il metodo matriciale $Ax = b$ come accennato nella scorsa lezione con
$$
A = 
\begin{bmatrix} 
\bigg[\sum_i^n u_i^2\bigg] & \bigg[\sum_i^n u_i\bigg] \\
\bigg[\sum_i^n u_i\bigg] & n
\end{bmatrix}
$$

$$
x = \begin{bmatrix} m & q\end{bmatrix}
$$

$$
b = \begin{bmatrix} 
\bigg[\sum_i^n (z_{im}u_i) \bigg] \\ 
\bigg[\sum_i^n (z_{im}) \bigg]
\end{bmatrix}
$$

Facciamo un esempio:

![](img/essom.png)

Come possiamo vedere, avendo solo due misure affette da errore non possiamo fare di meglio per stimare la linea rossa (uscite reali). Aumentando il numero di misure a disposizione sicuramente otterremmo una retta più precisa e conforme all'originale. Nelle prossime lezioni vedremo come risolvere questo problema.

Siamo arrivati quindi, tramite massimizzazione della funzione Likelihood, a trovare una retta di parametri mq che tenta di rappresentare il modello originale.

Ma vediamolo dal punto di vista di problema lineare: abbiamo sempre un problema di M equazioni con N (2 in questo caso) incognite. Pertanto la nostra equazione $z_{im} = m \cdot u_i+q+v_i$ sarebbe trascrivibile in forma matriciale come $Ax = b + v$ ovvero
$$
\begin{bmatrix} 
u_1 & 1 \\
... & ... \\
u_M & 1
\end{bmatrix} 
\cdot
\begin{bmatrix} 
m \\
q \\
\end{bmatrix} 
=
\begin{bmatrix} 
b_1\\
...\\
b_M
\end{bmatrix} 
+
\begin{bmatrix} 
v_1\\
...\\
v_M
\end{bmatrix}
= 
\begin{bmatrix} 
z_1\\
...\\
z_M
\end{bmatrix}
$$
Proviamo a risolverlo come problema matriciale dunque:

- Calcoliamo $A^TA$ 
  $$
  A^TA = 
  \begin{bmatrix} 
  u_1 & ... & u_M \\
  1 & ... & 1
  \end{bmatrix} 
  \cdot
  \begin{bmatrix} 
  u_1 & 1 \\
  ... & ... \\
  u_M & 1
  \end{bmatrix} 
  =
  \begin{bmatrix} 
  \bigg[\sum_i^n u_i^2\bigg] & \bigg[\sum_i^n u_i\bigg] \\
  \bigg[\sum_i^n u_i\bigg] & n
  \end{bmatrix}
  $$
  
- Calcoliamo $A^Tb$
  $$
  A^TA = 
  \begin{bmatrix} 
  u_1 & ... & u_M \\
  1 & ... & 1
  \end{bmatrix} 
  \cdot
  \begin{bmatrix} 
  z_1\\
  ...\\
  z_M
  \end{bmatrix}
  =
  \begin{bmatrix} 
  \bigg[\sum_i^n (z_{im}u_i) \bigg] \\ 
  \bigg[\sum_i^n (z_{im}) \bigg]
  \end{bmatrix}
  $$

Come è possibile notare, svolgendo il semplice prodotto fra matrici, abbiamo ottenuto esattamente gli stessi risultati che avevamo calcolato prima dalla massima verosimiglianza!

Infatti quello che noi avevamo in $Ax=b$ prima ora diventano, secondo un equazione normale in $A^TA x = A^Tb$

Pertanto possiamo concludere che calcolare la **massima verosimiglianza**, seguendo il modello di rumore ***gaussiano a media nulla*** in cui le misure sono ***indipendenti***, equivale a risolvere il sistema di equazioni lineare dei minimi quadrati: per quanto diversi possano essere questi modelli di calcolo, essi arrivano alla medesima soluzione.

Questa soluzione è quella che minimizza lo scarto quadratico medio dei residui, ovvero minimizza la varianza. Questo perché una minore varianza è ciò che ci rende più sicuri dei parametri ottenuti in quanto più accurati e possibilmente in linea col modello originale che vogliamo stimare.

### Valutazione della bontà della stima

Ora che sappiamo come calcolare una approssimazione del modello originale vogliamo calcolarne la bontà, ovvero quanto si discosta dai valori originali basandosi sui possibili errori di misura commessi.

Supponendo che $A^TA x = A^Tb$ diventi $x = (A^TA)^{-1}A^T b$ isolando la $x$, ovvero la nostra soluzione, calcoliamo il possibile $\Delta x$ ottenuto introducendo nei calcoli l'errore di misura $\Delta v$.

Per semplicità, riscriviamo la matrice di covarianza come $C = (A^TA)^{-1}$, così da semplificare la nostra equazione in $x = C A^Tb$

Ora introduciamo i $\Delta$ che abbiamo visto: 
$$
(x + \Delta x) = C A^T(b + \Delta v)
$$
Supponendo che l'errore sia a media nulla e gaussianamente distribuito ($E[\Delta v] = E[\Delta x] = 0$) otteniamo che
$$
x = CA^Tb\\
\Delta x = C A^T \cdot \Delta v
$$
Siccome abbiamo M parametri, vogliamo individuare le correlazioni fra due parametri qualunque $i$ e $j$, ovvero $<\Delta x_i, \Delta x_j>$ 

Per farlo però dobbiamo prima costruire la matrice di covarianza fra parametri 

 ![](img/covmt.png)

Per farlo dobbiamo calcolare il vettore trasposto $\Delta x^T$ di $\Delta x$, ovvero $ \Delta x^T = C^TA \cdot \Delta V^T$

Quindi la matrice di covarianza di $\Delta x$ sarà:
$$
\Delta x \cdot \Delta x^T = CA^T\Delta v \cdot C^TA \Delta v^T
\\ \rightarrow \\
<\Delta x, \Delta x^T> = CA^T<\Delta v, \Delta v^T> A C^T
$$
Dato che i $v$ sono i residui, sono indipendenti fra loro, e tutti i punti di controllo hanno lo stesso tipo di errore di misura, si avra che $<\Delta v, \Delta v^T> = I\sigma_0^2$, ovvero una matrice diagonale in cui su essa ci sono i valori $\sigma_0^2$

Pertanto avremo
$$
<\Delta x, \Delta x^T> = CA^T  I\sigma_0^2 A C^T
$$
Risostituendo C, è facile osservare come possiamo semplificare il tutto elidendo $A$ e $A^T$ con i loro inversi
$$
<\Delta x, \Delta x^T> = (A^TA)^{-1}A^T  I\sigma_0^2 A C^T = C^T \sigma_0^2
$$
Da qui segue che la trasposta della matrice di covarianza $<\Delta x, \Delta x^T>$ sarà
$$
<\Delta x^T, \Delta x> = C\sigma_0^2
$$
Questo mi rappresenta la varianza della stima sul parametro, dunque la correlata incertezza su $z$.

Grazie alla matrice di covarianza C possiamo calcolare le varie correlazioni fra i parametri: per esempio

![](img/cov.png)

con gli elementi della matrice $c_{ij}$ possiamo calcolare la correlazione fra il parametro i e il parametro j:

- Se si avvicina a **-1** si ha una correlazione **inversa** fra i parametri
- Se si avvicina a **1** si ha una correlazione **diretta** fra i parametri
- Se si avvicina allo **0** si ha una assenza di **correlazione** fra i parametri

Pertanto come possiamo vedere nei prossimi esempi di stima della retta, man mano che aumentano i punti su cui fare affidamento, **diminuisce la covarianza** fra i parametri e conseguentemente **aumenta la confidenza** sui parametri $m$ e $q$ stimati della retta.

![](img/ret1.png)

![](img/ret2.png)

![](img/ret3.png)

## Linearizzazione di sistemi non-lineari

E' possibile, come vedremo brevemente, trattare sistemi non-lineari allo stesso modo di quelli lineari (usando quindi le stesse tecniche) tramite un meccanismo di linearizzazione.

La linearizzazione consiste nel calcolare un differenziale, ovvero una retta tangente alla curva di una funzione non-lineare in un certo punto di essa

![](img/lin1.png)

Questa retta $dz$ equivale quindi a
$$
dz = z_0 + \frac{\delta f(u)}{du}
$$
ovvero partendo da $z_0$ vado a sommare la derivata della funzione sull'incognita $u$ in un intorno di $u_0$ o, in termini più semplici, calcolo lo spostamento da $z_0$ lungo una direzione lineare dettata dalla retta tangente in quel punto.

### Metodo di Gauss-Newton

L'idea di base è sfruttare la linearizzazione e riconvertirci al problema lineare che abbiamo osservato prima:

- Inizializzazione

  - Inizializzo i parametri ad un valore iniziale

- Iterazioni

  1. Linearizzo le equazioni sul valore corrente dei parametri

  2. Stima dell'aggiornamento dei parametri nel modello linearizzato ai minimi quadrati (ottengo quindi la soluzione ottimale, il minimo del problema linearizzato)

  3. Correggo i parametri analizzandone la covarianza

Il calcolo può risultare computazionalmente pesante poichè richiede l'inversione della matrice di covarianza, pertanto spesso si preferisce utilizzare metodi di ottimizzazione di primo ordine (come il metodo del gradiente).