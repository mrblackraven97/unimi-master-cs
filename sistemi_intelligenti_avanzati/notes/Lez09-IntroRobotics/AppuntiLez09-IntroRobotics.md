

Vedremo ora una overview sul mondo della robotica autonoma, dei principali problemi affrontati nel tempo e delle loro soluzioni che, come vedremo nella prossima lezione, verranno standardizzate nel ROS, ovvero il Robot Operating System.

Un robot mobile autonomo (AMR) prende innumerevoli forme e avere diverse funzionalità

![](img/rob.png)

## Caratteristiche principali

Iniziamo a capire cosa definisce effettivamente un AMR:

- **Architettura**
  - Definisce come effettivamente è composto un robot, quali siano i suoi ***mezzi di movimento e di interazione*** con l'ambiente esterno
    - Dotato di ruote o di gambe
    - Antropomorfo o zoomorfo
    - Volante - UAVs, ala fissa
    - Acquatico -  ASVs, sottomarini
- **Ambiente**
  - Definisce l'ambiente in cui è stato ideato il suo utilizzo
    - Indoor (casa, ufficio, logistica, ospedale)
    - Outdoor (terra, mare, aria, spazio)
- **Funzionalità**
  - Definisce i principali compiti del robot: generalmente sono attività ripetitive, noiose, costose o pericolose
    - Assistivo / collaborativo (robot informativi, di pulizie ...)
    - Sorveglianza / monitoraggio (robot sicurezza, raccoglimento dati)
    - Ricerca e salvataggio
- **Interazione umana**
  - Grado di interazione con l'essere umano e di autonomia
    - Funzionamento in autonomia completa
    - Funzionamento semi-automatico
- **Multi-robot**
  - Sistemi composti da robot che interagiscono collaborativamente al raggiungimento di uno scopo

Passando oltre il lato prettamente fisico e hardware, dal nostro punto di vista di sistema intelligente, un AMR è un **agente** che si muove autonomamente in un determinato **ambiente** per svolgere una determinata **task**. Per farlo, utilizza i suoi **sensori** per analizzare l'ambiente circostante e agire di conseguenza tramite i suoi **attuatori**.

![](img/agent.png)

Scendendo un pochino nel dettaglio, possiamo raffinare questo schema come segue

![](img/agref.png)

## Ruoli

Attualmente, i robot di cui siamo in possesso oggi permettono di svolgere semplici e ripetitive attività in ambienti controllati, come ad esempio i robot da catena di montaggio e altri robot industriali.

Il nostro obiettivo è quello di ideare il classico robot fantascientifico dotato di intelligenza artificiale che riesca a interagire con noi in modo paritario e che sia in grado di adattarsi a nuove sfide e compiti.

Vediamo una possibile evoluzione dei robot per come li conosciamo e per come sono i nostri obiettivi

- **Industriale**
  - ![](img/man.png)
  - I manipolatori sono i classici bracci robotici utilizzati nell'industria. Sono generalmente ***pre-programmati*** (e difficilmente ri-programmabili). Molto efficaci nello svolgere ***azioni prestabilite e ripetitive***.
- **Domestico**
  - ![](img/dom.png)
  - I robot domestici stanno pian piano entrando nelle nostre case, ma tutt'ora sono prettamente **assistivi** e possono svolgere solo ***semplici mansioni*** (aspirapolvere, tagliaerba...).
- **Guida**
  - ![](img/drv.png)
  - La guida autonoma è quasi realtà, tuttavia se guidare in strada è un problema facilmente modellabile, avere una guida autonoma sicura e affidabile è ancora un problema irrisolto. 

![](img/evo.png)

Siamo ancora fermi alla guida autonoma e, pertanto, i robot dall'intelligenza umana rimangono ancora fantascienza: ma quali sono le ragioni?

## Limitazioni

Le principali limitazioni dei robot moderni sono date dal fatto che un robot necessita di compiere determinate **decisioni** nell'adattare il loro comportamento all'ambiente che li circonda mentre tentano di svolgere il proprio compito.

Questa difficoltà di adattamento e di decisione da cosa è dovuta?

- ***Embodiment***: è collegata alle limitazioni hardware o fisiche?
- ***Cognition***: è collegata alle capacità cognitive del software?

Nonostante abbiamo notevoli limitazioni dal punto di vista dell'attuazione del compito (tramite ruote, braccia o pinze), della raccolta di informazione (sensori) e della capacità computazionale, la maggiore limitazione è collegata principalmente al loro **livello cognitivo** e di **decision-making**.

Se dobbiamo scegliere una dei problemi più limitanti è quelli della corretta **percezione** e **interpretazione** dei dati osservati: siccome la **mobilità** si basa molto su questi aspetti percettivi, essa sarà negativamente influenzata anch'essa.

![](img/cute.png)

Dato che un robot autonomo deve svolgere tutti questi compiti contemporaneamente, è necessario riorganizzarli secondo uno schema logico, semplificato e possibilmente sequenziale per essere facilmente computabile dal robot, anche durante eventi imprevisti, garantendo adattabilità e robustezza nel comportamento.

Una cosa importante è raggruppare questi compiti in macro-aree così da capire quali attivare in quali casi, ad esempio:

![](img/mov.png)

![](img/task.png)

La robotica è ancora un campo di studi giovane e involve numerose disciplice data la complessità dei suoi scopi: il nostro focus è la parte di planning, ovvero la parte cognitiva in cui l'agente decide cosa fare dati gli input ricevuti dall'ambiente.

![](img/plan.png)