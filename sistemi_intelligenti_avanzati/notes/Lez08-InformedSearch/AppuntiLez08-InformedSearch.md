

# Informed search

## Conoscenza ed euristiche

Come abbiamo visto, ogni algoritmo di ricerca decide dove cercare la soluzione utilizzando le **conoscenze** a loro disposizione.

Abbiamo gli algoritmi di **non-informed search**, come BFS o DFS, i quali utilizzano solo la ***conoscenza*** data al momento della ***definizione del problema***: pertanto conoscono solo i nodi di inizio e di goal, i nodi vicini per ogni nodo e il costo degli archi per raggiungerli.

Gli algoritmi di **informed search**, invece, conoscono informazioni aggiuntive a esse, in modo da poter usarle per trovare una migliore soluzione o una soluzione ottimale approssimata riducendo i tempi di calcolo. Queste informazioni aggiuntive vengono dette ***euristiche***.

Tornando all'esempio della città, potremmo dire che una possibile euristica per raggiungere la metro è la distanza in linea d'aria di ogni nodo del grafo verso il nodo della metro.

Queste stime del costo della distanza verso il nodo goal a partire da un qualsiasi nodo $v \in V$ del grafo $G=(V,E)$ vengono indicate come una funzione $h(v)$.

![](img/meth.png)

Proviamo quindi ora a utilizzare come base l'algoritmo non-informed UCS ed aggiungere questa informazione come rilevante nella scelta del percorso da esplorare: invece di considerare il costo collegato all'arco, consideriamo il valore $h(n)$ di distanza in linea d'aria.

Utilizzeremo un semplice approccio greedy, cercando di minimizzare ogni scelta locale di distanza.

Torniamo al nostro classico problema e applichiamo l'algoritmo UCS ora informed con l'euristica $h(v)$

![](img/gbfs.png)

A primo impatto possiamo vedere come questo algoritmo, nei vari step, sia molto efficiente e conduca alla soluzione ottima in fretta. Tuttavia, come tutti gli algoritmi greedy, si corre il rischio di incappare in un minimo locale facendo delle scelte sub-ottime: in altre parole, potrei fare una scelta che mi sembra vantaggiosa ora ma che poi potrebbe vincolarmi a un percorso più costoso in totale rispetto ad aver scelto l'altro percorso.

Pertanto questo algoritmo, pur essendo completo e abile a trovare la soluzione, non sempre restituisce una soluzione ottima. Complessità spazio-tempo : $O(b^m)$

## Algoritmo A*

L'algoritmo A*, ideato per la navigazione dei robot già nel 1968, va a espandere l'idea dell'uso di euristiche per scelte greedy ma utilizzando un approccio diverso, semplice e intelligente.

![](img/rob.png)

A partire sempre dall'UCS, invece di considerare solamente la distanza $h(n)$ vogliamo considerare una funzione $f(n)$ che consideri anche i costi accumulati lungo il percorso sugli archi $g(n)$

 ![](img/fn.png)

### Ammissibilità

Condizione importante per garantire che la ricerca sia sound e completa è il fatto che l'euristica sia **ammissibile**: vuol dire che essa deve essere una stima ottimistica, per esempio, nel nostro caso, che $h(n)$ sia minore o uguale al costo del percorso minimo dal nodo $n \in V$ fino al goal
$$
h(n) \leq cost\ of\ minimum\ path\ from\ n\ to\ goal
$$
Nel nostro esempio della città la distanza in linea d'aria è una euristica ammissibile poiché è sicuramente vero che la distanza che devo percorrere, anche a piedi, sia maggiore a essa per via di ostacoli che mi costringono a deviare. Nel peggiore dei casi, se non ci sono ostacoli, l'euristica è comunque uguale al percorso minimo, mai minore.

Questo è necessario perché se l'euristica non fosse ammissibile, potremmo scartare dei percorsi di ricerca che in realtà sarebbero potuti essere ottimali in futuro.

Vediamo quindi come si comporta l'algoritmo A* sul nostro classico grafo d'esempio:

![](img/astar.png)

Sembra tutto funzionare liscio, abbiamo trovato la soluzione ottima senza i problemi che avevamo con l'altro algoritmo greedy: eppure ci può essere un problema.

Cosa succederebbe se dessimo dei valori a caso e non per nulla rappresentativi per la nostra euristica? Facciamo un esempio

![](img/asp.png)

Effettivamente, possiamo vedere come F in linea d'aria sembri distare 100 mentre A dista 10 (topologicamente dovrebbero distare di più di F) e B, D, G distano 0 (come fanno a distare 0 se sono posti diversi?)

Detto ciò, vediamo l'A* come si comporta in questo caso bizzarro

![](img/asp2.png)

Otteniamo una soluzione che però in realtà non ottimizza il percorso minimo! ABDGE vale 112, ma per "colpa" della euristica sbagliata mi sono perso il percorso AFGE che vale 111 e quindi il migliore.

### Consistenza

Avere delle euristiche può facilitarci il lavoro ma se usate impropriamente può rendercelo un inferno: pertanto, oltre alla ammissibilità, è necessario introdurre il concetto di **consistenza**.

![](img/const.png)

La consistenza è una proprietà ancora più forte della ammissibilità in quanto afferma che, per ogni due nodi connessi $u$ e $v$
$$
h(v) \leq c(v,u) + h(u)
$$
Questo vuol dire che l'euristica per un certo nodo $v$ deve essere necessariamente minore o uguale al costo dell'arco $(v,u)$ sommato all'euristica di $h(u)$

Quindi se $f(v) = g(v) + h(v)$ e $f(u) = g(u) + h(u)$ allora vogliamo che
$$
f(u) = g(v)+c(v,u)+h(u) \\
sia \\
f(u)\geq g(v) + h(v)
$$
Tutto ciò per fare in modo che la funzione $f$ basata sulla somma di costo accumulato ed euristica sia una funzione **non-decrescente** lungo tutto il percorso di ricerca.

### Ottimalità

Una volta che ci siamo accertati della consistenza dell'euristica, possiamo verificare l'ottimalità del nostro algoritmo A*

Ipotizziamo che

- A* selezioni un nodo di frontiera $G$ generato tramite un percorso $p$
- $p$ non è il percorso ottimale per arrivare al nodo $G$

![](img/front.png)

Supponiamo quindi che esista un altro path che porta al nodo $X$ e un altro che da $X$ porta a $G$ che sia ottimale. Allora in questo caso, siccome $f$ deve essere non-decreasing per consistenza allora $f(G) \geq f(X)$.

Tuttavia noi sappiamo che A* di principio seleziona il nodo per cui $f(v)$ è minore, pertanto se, date le ipotesi, ha selezionato il nodo $G$ allora vuol dire necessariamente che $f(G) < f(x)$.

Siccome queste due condizioni non possono valere allo stesso tempo, non è possibile che A* abbia selezionato un path non ottimale. L'ottimalità è dimostrata. 

### Building good heuristics

Buone euristiche ci aiutano a limitare il numero di stati esplorati prima di trovare la soluzione: le euristiche sono complesse da modellare poiché devono basarsi sulla struttura intrinseca del problema, oltreché soddisfare i requisiti di ammissibilità e consistenza.

Fortunatamente, esiste un modo per computare le euristiche per un problema e valutare per vedere quella più conforme ed adatta.

![](img/eveur.png)

Una euristica semplificata potrebbe essere facile da modellare ma poco aderente al problema e quindi poco efficiente nel ridurre gli stati.

Quello che vogliamo è, quindi, costruire una euristica bilanciata tendente verso la vera soluzione: pertanto, un algoritmo A* seguirà ed espanderà tutti i nodi $v$ per i quali
$$
f(v) < g^*(goal)\\
h(v) < g^*(goal) - g(v)
$$
Quindi, per esempio, per ogni nodo $v$ per cui vale
$$
h_1(v) \le h_2(v)
$$
avremo che $h_2(v)$ non espanderà più nodi di A*, pertanto è più efficiente e preferibile da usare in quanto permette di visitare meno nodi rispetto ad $h_1(v)$: come abbiamo detto, vogliamo una euristica il meno triviale possibile e che mi riduca il più possibile il numero di stati visitati.

In caso per un problema avessimo più euristiche, converrebbe quindi trovare il massimo fra tutte le euristiche
$$
h(v) = \max{\{h_1(v),\ ..., h_n(v)\}}
$$

#### Problemi rilassati

Ma come troviamo una euristica adatta al nostro problema? Una buona idea sarebbe ***rilassare i vincoli*** che stiamo fronteggiando per risolvere un problema più lasco e approssimato e stimare la mia euristica da qui.

Dato il nostro problema $P$, il corrispettivo **problema rilassato** $\hat{P}$ è una versione più semplice che non considera alcuni vincoli di $P$.

Per questo motivo avremo che i costi del rilassamento del problema devono essere necessariamente minori o uguali ai costi del problema originale, mai maggiori.

![](img/relax.png)

Usiamo questo a nostro vantaggio creando una nuova rotta fra $B$ ed $E$ dato che il costo da infinito diminuisce a un numero finito (7): abbiamo effettivamente rilassato il vincolo di non poter usare la strada $\overline{BE}$ del problema originale.

Bisogna tenere conto, però, che più tolgo vincoli al mio problema rilassato e meno accurata sarà l'euristica, e quindi meno efficiente.

#### Calcolo dell'euristica

Partendo da qui, l'idea è di applicare A* a ogni nodo del problema rilassato per calcolare l'euristica $\hat{h}^*(v)$ e utilizzarla nell'A* del problema originale, ponendo $h(v)= \hat{h}^*(v)$

![](img/idea.png)

E' facile creare un problema rilassato di $P$ in quanto è sufficiente rimuovere o ridurre qualche vincolo, ma siamo sicuri che la soundness e la completeness di A* vengano preservati in questo modo? 

Proviamo a verificare la ***consistenza*** della nuova euristica $\hat{h}^*(v)$. 

Nel problema rilassato abbiamo che, per consistenza, $\hat{h}^*(v)$ deve essere
$$
\hat{h}^*(v) \le \hat{g}(v,u) + \hat{h}^*(u)
$$
Dalla nostra idea, siccome abbiamo posto $h(v)= \hat{h}^*(v)$ 
$$
h(v) \le \hat{g}(v,u) + h(u)
$$
Dalla nostra idea di problema rilassato abbiamo posto che i costi del problema rilassato siano sempre minori o uguali a quelli del problema originale
$$
\hat{g}(v,u) \le g(v,u)
$$
Pertanto possiamo vedere come, sostituendo, la nostra euristica rimane consistente:
$$
h(v) \le g(v,u) + h(u)
$$

#### Esempio: heuristics for 8-puzzle

Sappiamo come visto la scorsa volta della complessità dei vincoli dell'8-puzzle: proviamo a ideare una versione più rilassata:

![](img/8pzu.png)

Originariamente abbiamo che una tile si può muovere da una cella A a una cella B se

- A è adiacente verticalmente o orizzontalmente a B 
- B è una cella blank (vuota)

Qui possiamo creare diversi problemi rilassati manipolando questi vincoli

1. Una tile si può muovere da A a B se A è adiacente a B (rimuovo secondo vincolo e non-obliqueness dal primo)
2. Una tile si può muovere da A a B se B è blank (rimuovo primo vincolo)
3. Una tile si può muovere da A a B liberamente (nessun vincolo)

Ovviamente ricordiamo che più vincoli sono rimossi e meno efficiente sarà l'euristica poichè aderisce meno alla struttura del problema (la terza euristica sarà meno precisa infatti!)

Possiamo creare euristiche anche derivandole dai sottoproblemi, per esempio rimuovendo alcune tile dal gioco e avendo più blanks a disposizione (facilitando il gioco quindi)

![](img/ez.png)

Altre alternative possibili possono essere:

- ***Imparare dall'esperienza***: giocare tantissimi 8-puzzles e imparare una buona strategia da cui trarne una euristica (**neural networks approach**)
- Usare conoscenza di dominio per estrarre ***feature*** (per esempio, numero di tile in posizione errata)

Da qui, possiamo tornare alle nostre versioni heuristic-enriched di BFS e DFS:

- **Hill climbing**
  - Versione del DFS che sceglie i nodi col minor valore $h(v)$ in situazioni di spareggio
- **Beam** (di raggio $w$)
  - Versione del BFS che invece di considerare *tutti* i possibili sotto-nodi nella ricerca ne utilizza solo un "raggio" di $w$ nodi che cresce in funzione di $h(v)$