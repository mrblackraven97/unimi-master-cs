# Reinforcement Learning dinamico

## Agenti e sistemi dinamici

Abbiamo visto come i nostri agenti intelligenti si comportano quando hanno a che fare con un problema statico, il quale stato non cambia nel tempo.

Tuttavia nel mondo reale, l'ambiente è può variare stato improvvisamente e in modo non per forza deterministico; è anche normale che quando l'agente agisce sull'ambiente può cambiarne lui stesso lo stato e quindi dover ri-affrontare quel problema ma in uno stato completamente diverso.

Questo vuol dire che l'azione che ha compiuto prima, che era allora ottimale, non è detto che rimanga ancora ottimale; inoltre, non è garantito che quella azione sia ottimale in tutti i possibili stati: facciamo un esempio con gli scacchi

![](img/acsc.png)

La stessa mossa, in due stati della scacchiera separati, dà luogo a due reward diversi e potenzialmente a due esiti diversi per la partita: 

- nel primo caso, la mossa è ottimale in quanto la regina pone in scacco il re, facendo vincere il bianco
- nel secondo caso, la mossa è catastrofica in quanto la regina verrebbe mangiata dalla torre, facendo perdere il bianco

### Stato

Dall'esempio precedente possiamo concludere come ogni azione del nostro agente intelligente non deve essere mossa solo dal reward o dalla value function, ma deve anche essere considerato lo **stato**, ovvero la ***situazione in cui si trova l'ambiente*** in cui si trova per fare la scelta ottimale.

Questa situazione è il risultato dell'azione dell'agente sull'ambiente e dalla dinamica (spesso non nota) dell'ambiente stesso. Da qui riesco a intravedere il fatto che ***le azioni passate influiscono sull'ottimalità delle azioni future***!

Lo stato deve essere considerato, quindi, nel raggiungimento del goal in modo ottimale e pertanto deve essere osservabile e misurabile dall'agente.

Come rappresento lo stato $s$? Memorizzo la sequenza temporale degli stimoli di interesse in una variabile che riassume al meglio la situazione attuale.

### Diagramma di un agente dinamico

![](img/dgr.png)

Come possiamo vedere, questo è il diagramma aggiornato del nostro agente che ora deve considerare anche lo stato $s$, oltre al reward $r$ per poter decidere quale azione $a$ eseguire: potremmo dire che l'azione in uscita $a$ è in funzione dello stato in ingresso $s$, ovvero $a_t = f(s_t)$ an ogni istante $t$.

Possiamo quindi dire che la funzione di scelta della azione $f$ è in realtà una policy, quindi la rinominiamo come 
$$
\pi : s \rightarrow a
$$
dato che la policy trasforma lo stato in ingresso in una azione di uscita.

### Environment Markoviano

La scelta dell'agente si basa, quindi, solo sullo stato del sistema attuale, ignorando quelli precedenti e come si è arrivato a quello corrente: questo concetto è alla base dell'environment markoviano.

Una variabile di stato (non funzione del tempo), che riassume le informazioni sulla storia del task, utili all’agente per agire, è detta **variabile markoviana**. 

Si definisce **processo stocastico markoviano**, un processo aleatorio in cui la probabilità di transizione che determina il passaggio a uno stato di sistema dipende solo dallo ***stato del sistema immediatamente precedente*** e non da come si è giunti a questo stato.

Proviamo a formalizzare: supponendo $s_t$, $r_t$ variabili discrete appartenenti a un insieme finito di valori avremo che in un processo la probabilità di transizione è data da

- Se ordinario, dipende da tutta la serie di azioni avvenuta nel processo $Pr\{s_{t+1} = s'|s_t,a_t;\ s_{t-1},a_{t-1};...;s_0,a_0\}$

- Se Markoviano, dipende solo dall'ultimo stato che viene memorizzato $Pr\{s_{t+1} = s'|s_t,a_t\}$

Allo stesso modo, anche i reward dipendono solo dall'ultimo stato infatti:

- Reward stocastico, dipende da tutta la serie di azioni avvenuta nel processo $Pr\{r_{t+1} = r'|s_t,a_t, s_{t+1};\ s_{t-1},a_{t-1},r_t;...;s_0,a_0,r_1\}$
- Reward stocastico markoviano  $Pr\{r_{t+1} = s'|s_t,a_t,s_{t+1}\}$
- Reward deterministico markoviano $r_{t+1} = r(s_t, a_t, s_{t+1})$

Siccome l'ambiente ha completamente proprietà Markoviane, dobbiamo basarci su queste per i nostri agenti.

### Decision process markoviano

![](img/mrkv.png)

## Value function e ricompensa a lungo termine

L’agente deve scoprire quale **azione** (quindi quale policy) fornisca la ricompensa massima provando le varie azioni (in stile trial-and-error) sull’ambiente con un criterio intelligente.

>“Learning is an adaptive change of behaviour and what is indeed the reason of its existence in animals and man” (K. Lorentz, 1977).

Il rinforzo che abbiamo osservato l'altra volta con la ciotola del cane e il metronomo è definito **condizionamento classico**: il rinforzo è puntuale, azione per azione, ed il segnale di rinforzo è sempre lo stesso per ogni coppia input-output (reward istantaneo). 

Quello che vogliamo introdurre noi è un **condizionamento operante**: vogliamo quello che si può definire un comportamento intelligente, una sequenza di azioni da svolgere affinchè si ***massimizzi il reward totale accumulato***, e non solamente quello azione per azione. Questo implica voler fare scelte meno ottimali volutamente per influenzare lo stato dell'ambiente in un certo modo che poi porti un migliore vantaggio.

Ci stiamo discostando sempre più dal comportamento greedy, miope, e avvicinandoci sempre più a un comportamento basato su una strategia a lungo termine.

Facciamo un altro esempio basato sugli scacchi:

![](img/acsc2.png)

Come possiamo vedere dall'immagine:

- Nel primo caso, la mossa compiuta dall'agente fornisce un reward istantaneo positivo (la regina bianca mangia l'alfiere nero), tuttavia nel prossimo turno la regina verrà mangiata dalla torre di fianco a lei: pessima strategia a lungo termine implica un pessimo total reward.
- Nel secondo caso, la mossa compiuta dall'agente non fornisce alcun reward istantaneo (non mangia nulla e non perde nulla), tuttavia quella mossa ha il potenziale di mettere in scacco il re nero e vincere la partita: nessun reward istantaneo ma un miglior total reward, quindi una migliore strategia!

Ma cosa intendiamo per total reward quindi? Ovviamente intendiamo la value function, ma in questo caso dobbiamo valutare la ricompensa futura in base alla policy che scegliamo per capirne la bontà.

Al tempo $t$, data una certa policy: $\pi(s, a)$, la ricompensa sarà una funzione dei reward negli istanti di tempo successivi a t, ad esempio:
$$
R^\pi_t = r_{t+1} + r_{t+2} + r_{t+3} + ... \\
R^\pi_t = \sum_{k=0}^{\infin}{r_t+k+1}
$$
Tuttavia, per limitare l'orizzonte di previsione, introduciamo la meccanica del **discount**: diamo maggior peso ai reward più vicini nel tempo e meno a quelli più remoti nel futuro.
$$
R^\pi_t = \gamma^0r_{t+1} + \gamma^1r_{t+2} + \gamma^2r_{t+3} + ... \\
R^\pi_t = \sum_{k=0}^{\infin}{\gamma^kr_t+k+1}
$$
dove $\gamma$ è un coefficiente detto **discount rate** che vive fra $0 \le \gamma \le 1$

Cosa succede se 

- $\gamma = 0$, in questo caso non terrei conto minimamente dei reward futuri ma solo dell'esatto prossimo
-  $\gamma = 1$, in questo caso considero in modo paritario (con medesimo peso) tutti i reward futuri nel calcolo 

Per limitare in modo finito l'orizzonte temporale per una certa policy è sufficiente definire un certo istante $T = t+k$, definito terminal state, dove viene calcolato l'ultimo possibile reward futuro, ovvero $r_T$

$R^\pi_t = r_{t+1} + r_{t+2} + r_{t+3} + ... + r_T $

Questa limitazione è adeguata in problemi ad orizzonte finito e in problemi stazionari.

### Confronto con problemi statici

Possiamo facilmente vedere un pattern simile a quello che abbiamo visto nella interazione dell'agente in un problema statico:

- Nell'interazione **statica**, guardo i ***reward passati*** per imparare a scegliere la prossima azione migliore
- Nell'interazione **dinamica**, guardo i ***reward futuri*** per cercare di scegliere la prossima azione che porti a un miglior total reward

![](img/rlz.png)

### Diagramma agente aggiornato

![](img/dgr2.png)

In seguito a ciò che abbiamo detto, abbiamo aggiunto una terza domanda che l'agente si deve porre: qual è il valore della mia azione?

## Elementi della RL

Per disambiguare e ricordare meglio i termini utilizzati, ecco un veloce recap

- **Environment**: Fornisce il reward (istantaneo), fornisce lo stato dell’ambiente. Reagisce alle azioni (output) dell’agente. 
- **Agent**: Ragiona sullo stato fornito dall’ambiente e sul reward istantaneo per produrre un’azione adeguata sull’ambiente. 
- **Goal**: Obbiettivo che deve raggiungere l’agente. Si può aggiungere che l’agente deve raggiungere l’obbiettivo con una policy ottima. 
- **Policy**: Descrive l’azione scelta dall’agente: mapping tra stato (output dell’ambiente) e azioni dell’agente. 
- **Funzione di controllo**: Le policy possono avere una componente stocastica. Viene utilizzata una modalità adeguata pe rappresentare il comportamento dell’agente (e.g. tabella, funzione continua parametrica…). 
- **Reward**: Ricompensa immediata. Associata all’azione intrapresa in un certo stato. Può essere data al raggiungimento di un goal (esempio: successo / fallimento). E’ uno scalare. Rinforzo primario, solitamente qualitativo. 
- **Value**: Ricompensa a lungo termine. Somma dei reward: costi associati alle azioni scelte istante per istante + costo associato allo stato finale. Orizzonte temporale ampio. Rinforzo secondario. Vogliamo realizzare agenti che ragionino in modo strategico. (cf. “Cost-togo” in ricerca operativa). 

### Reward meaning

Ricordiamo che il reward istantaneo è utile perchè ci permette di capire in che direzione stiamo andando dato che ci comunica se abbiamo successo in COSA vogliamo ottenere; non ci dice però nulla su COME ottenerla. A quello ci deve pensare l'agente formulando una strategia.

### Proprietà del rinforzo

L’ambiente o l’interazione può essere complessa. 

Le azioni dell'agente, oltre a influire sull'ambiente, influiscono intrinsecamente anche le proprie azioni future. Il rinforzo può avvenire dopo una sequenza più o meno lunga di interazioni (azione-> cambiamento di stato e reward istantaneo): delayed reward.

Questo vale poichè l'apprendimento non avviene per esempi, ma tramite trial-error, pertanto arriva dall'osservazione dell'impatto sull'ambiente che ha avuto il comportamento dell'agente.

Nell'esempio degli scacchi, dove l'agente ha come ambiente la scacchiera e l'avversario, è chiaro come alcune mosse possano precluderne di altre.

Alcuni problemi collegati proprio alla causalità delle proprie azioni passate sono

- Temporal credit assignment (quando ho sbagliato la mossa? in che turno?)
- Structural credi assignment (quale mossa era sbagliata?)

Come vedremo poi in un esempio, l'agente è un sistema interconnesso in cui l’azione in uno stato si ripercuote a valle, anche sugli stati futuri (e le azioni scelte da lì in poi). La scelta di un’azione o di un’altra, cambia la funzione Q per le coppie stato-azione future.

### Meccanica di apprendimento

**Inizializzazione**: se l’agente non agisce sull’ambiente non succede nulla. Occorre specifiare una policy iniziale. 

**Ciclo dell’agente** (le tre fasi sono sequenziali): 

1. Implemento una policy $\pi(s,a)$
2. Aggiorno la Value function $Q^\pi (s,a)$
3. Aggiorno la policy

## Esempio robot

...