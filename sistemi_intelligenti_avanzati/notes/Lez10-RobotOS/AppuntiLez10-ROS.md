# Robot Operating System (ROS)

Come accennato la scorsa volta, vedremo sommariamente come funziona il Robot Operating System, progetto open-source creato apposta come framework di base per costruire software per robot.

Di fatto è un meta-OS che provvede dei servizi essenziali come un classico sistema operativo, quali astrazione dall'hardware, controllo dei device a basso livello, message-passing nella comunicazione fra processi e packet management. Inoltre fornisce una serie di tool e librerie per buildare, compilare e runnare codice funzionante indipendentemente dalla macchina sul quale viene runnato (computer di sviluppo, robot, ...).

## Robot software architecture

![](img/rosa.png)

Andando nel dettaglio, abbiamo implementate di default:

- **Funzionalità sensioriali-attive** di basso-livello, che agiscono direttamente sull'hardware, ovverosia controllo real-time su sensori, sui dati raw forniti da essi, sugli attuatori e tutto ciò che involve la parte attiva/informativa (e.g. battery management, motor controllers ...)
- **Funzionalità di localizzazione e detection**, conseguentemente di mapping dell'ambiente e della navigazione in esso
- **Funzionalità di ragionamento e planning**, quindi di alto livello, che includono il pathing per trovare un percorso per raggiungere il proprio obiettivo, gestione delle task da svolgere e self-management (e.g. andare in risparmio energetico a fine lavoro / a batteria quasi scarica)

ROS ci fornisce questi moduli as-is ed è il nostro lavoro modificare quelli che a noi interessano per il nostro lavoro e customizzarli per raggiungere il nostro obiettivo: generalmente, in ambito di sistemi intelligenti, lasceremo intatti i primi due moduli e andremo ad agire sul meccanismo di reasoning ad alto livello.

Questo ci permette di mantenere una certa scalabilità e modularità nelle implementazioni di queste features nella progettazione di un robot, facilitando enormemente lo sviluppo.

### Sommario

ROS è quindi un meta-sistema operativo che fornisce

- **Gestione dei processi** del nostro robot (scheduling, loading, monitoring e error handling)
- **Layer di virtualizzazione** fra applicazioni 
- **Distribuzione delle risorse computazionali** fra applicazioni
- **Eseguibile su diversi OS**

Agisce quindi come un framework su cui costruire software prettamente modulare.

## ROS software architecture

ROS viene gestito come un framework distribuito di processi definiti ***nodes***. Ogni nodo è effettivamente un'unità atomica di computazione in cui dovrebbe essere incapsulato ogni processo.

Questo permette agli eseguibili di essere designati separatamente e di essere **loosely coupled** fra di loro a run-time, avendo quindi una interfaccia comune per comunicare e non conoscendo internamente il loro funzionamento (peraltro intercambiabile).

Inoltre ciò abilità l'utilizzo di questi nodi come in una architettura a microservizi, in cui i processi possono essere **condivisi** e **distribuiti** praticamente senza sforzo, favorendo lo **scaling**.

ROS, siccome deve essere eseguito su macchine con potenza computazionale limitata, è progettato per essere il più leggero possibile, seppur mantenendo una certa facilità di integrazione con altri framework e librerie.

![](img/com.png)

L'idea principale di ROS è quindi evidente: **riusabilità** e **modularità**.