# Lezione 1 - Introduzione ai sistemi "intelligenti"

## Introduzione 

La creazione e l'utilizzo di sistemi cosiddetti "intelligenti" avviene con lo scopo di aiutarci a risolvere problemi complessi, che possono andare da "giocare a scacchi" a "capire il proprio interlocutore (anche dal punto di vista emotivo)" ad ancora "saper guidare una macchina". 

Riassumendo, con "intelligenti" intendiamo capaci di:

- imparare da sole data l'esperienza ed esempi di situazioni passate e cumulate nel tempo (Reinforcement learning)
- analizzare le situazioni, raggrupparle e classificarle, definendo delle predizioni in base alla situazione analizzata (Machine learning)
- fornire una soluzione simile a quella che fornirebbe un umano (Fuzzy systems)
- essere un partner intelligente per l'uomo (Emotional intelligence, AI agents and assistants)

Negli anni sono stati individuati diversi tipi di intelligenza (linguistico-verbale, matematico-logica, naturalistica ...), ognuna con la propria sfumatura di specializzazione. Ma in assoluto cos'è l'intelligenza?

L'intelligenza è una **funzione attiva** che consente, a breve termine, di trovare soluzioni nuove a problemi nei domini di interazione con l'ambiente: l'intelligenza quindi emerge e si sviluppa di più in un ambiente **ricco di stimoli**, che genera problemi e necessità.

Questo si traduce in una maggiore capacità di destreggiarsi in situazioni nuove o insolite, rendendo il portatore di intelligenza abile a 

1. Analizzarle e cogliere razionalmente i significati e le relazioni fra oggetti
2. Quindi risolverle comparandole e associandole a esperienze passate simili

## Matematica e logica

Una delle questioni agli albori della informatica era quella di riuscire a determinare se delle formule di logica formale potessero essere soddisfatte o meno: l'obiettivo di Hilbert era cercare di creare uno strumento per svolgere delle dimostrazioni automatiche di esse.

Una prima risposta arrivò con l'enunciazione del Teorema di incompletezza di Godel, il quale enuncia

> Qualunque sistema coerente di logica formale deve comprendere enunciati veri di cui non è possibile dare una dimostrazione

Secondo Godel, ogni sistema di logica formale è incompleto, pertanto non esistono verità assolute comuni a tutti: è necessario sempre creare prima una base di assiomi presi per veri e da lì derivare delle ulteriori verità. Pertanto non può esistere un metodo universale che possa stabilire se un qualunque enunciato è vero o falso a priori.

### Computabilità

Questo enunciato ha quindi spostato l'attenzione dei logici dal concetto di verità al concetto di provabilità da cui è scaturita la possibilità di dimostrazione tramite il calcolo ricorsivo.

La tesi di Church dimostra che

> Ogni funzione che sia effettivamente calcolabile è ricorsivamente computabile

in cui

- **effettivamente** significa che esiste una procedura "meccanica" in grado di calcolare la funzione in un tempo finito dato un certo input
- **ricorsivamente** significa che esiste un insieme finito di operazioni elementari che, applicate all'ingresso e ai risultati parziali, conducono in tempo finito al valore della funzione in uscita

L'obiettivo è sfruttare queste funzioni per creare un metodo di computazione fatto di passi elementari (che chiameremo poi algoritmo), darlo in pasto ad una macchina astratta insieme all'enunciato per ricavarne una soluzione in modo automatico.

Soluzione → Computazione → Metodo di computazione (Algoritmo) → Macchina computazionale

### Macchina di Turing

Ogni metodo computazionale, composto da passi elementari automatici, è definito algoritmo: Turing nella sua tesi definisce questa "macchina computazionale astratta" in grado di implementare questi passi elementari

> Qualsiasi funzione ricorsivamente computabile può essere calcolata in un tempo finito da una macchina manipolatrice di simboli

Questa sarà quella che viene definita Macchina di Turing (MdT), antenata dei moderni elaboratori: ogni algoritmo eseguibile dalla macchina di Turing è definito esattamente computabile.

![](img/mdt.png)

Ogni macchina di Turing è composta da:

- Un nastro infinito di celle da cui leggere/scrivere
- Una testina di lettura/scrittura che può leggere una cella (un carattere) alla volta e che si può muovere a sinistra o destra di una cella alla volta $D = [sx , dx, stop]$
- Un alfabeto di caratteri $A = [a_0,  a_1, ..., a_n]$
- Un insieme possibili di stati $S = [s_0 , s_1, ..., s_m]$ 
- Un insieme di funzioni di transizione di stato, dipendenti dalla lettura di un carattere $a_x$ e dallo stato $s_x$ in quel momento che portano a un nuovo stato $s_k$, fanno scrivere il carattere $a_k$ sulla cella corrente e fanno muovere la testina lungo la direzione $d_k$ dopo aver scritto $\delta_i(s_x, a_x) \rightarrow [s_k, a_k, d_k]$

![](img/mdt2.png)

La macchina di Turing universale quindi consente di rappresentare un qualunque algoritmo e una qualunque funzione computabile: pertanto appare come soluzione operativa di un qualsiasi problema risolvibile. Siamo riusciti a creare una macchina che, seppur tramite operazioni elementari, esegue un ragionamento logico.

Ma che relazione ha con l'intelligenza? La macchina di Turing, seppur estremamente utile e potente, è intelligente come lo intendevamo?

Turing, per verificare questa ipotesi, idea l'omonimo test di Turing: in questo test un umano isolato inizia a comunicare telematicamente con un altro essere umano e contemporaneamente con una macchina. 

![](img/ttur.png)

La macchina è considerata intelligente se l'umano non riesce a distinguere se sta parlando con lei o con l'altro essere umano per più del 30% delle volte: per ora, nessuna macchina è riuscita a passare appieno questo test.

### Ipotesi debole dell'IA

Il problema che ci rimane tutt'ora è trovare quell'insieme di funzioni, indubbiamente complesse a piacere, che governano la struttura delle risposte umane all'ambiente e poi scrivere il programma: questo è il pensiero alla base dell'intelligenza artificiale classica e ciò viene definita come ipotesi dell'IA debole. 

La **IA debole** si fonda sul concetto del “come se”, ovvero agisce e pensa come se avesse un cervello. Il suo obiettivo non è quello di realizzare macchine che abbiano un’intelligenza umana o replichino il funzionamento della nostra mente, quanto invece **sistemi che possano agire con successo in alcune funzioni complesse umane**, come ad esempio la traduzione automatica di testi.

### Critiche all'ipotesi debole

Tuttavia dobbiamo considerare che alle macchine **manca il vasto cumulo di conoscenze** di base inarticolate che ogni persona possiede e costruisce durante la sua vita (come le capacità di "buon senso" e "intuito"): pensiero e intelligenza umani non sono riducibili alla mera manipolazione di simboli e applicazione di regole svolta dalla macchina di Turing, infatti

1. Una macchina non può originare nulla di nuovo all'infuori di ciò per cui è stata programmata
   - Sebbene una macchina rimane programmata in un certo modo, è possibile permettere alla macchina di imparare, come nell'essere umano, dalle esperienze passate: una macchina, in base al bagaglio di conoscenze che costruisce tramite l'interazione con l'ambiente, fa le migliori scelte possibili per risolvere il problema (Reinforcement learning)
2. Il comportamento intelligente non può essere completamente replicato da regole formali (disability)
   - Siamo ancora lontani dal creare macchine capaci di replicare emozioni, creatività, intuito e altri comportamenti prettamente umani (soprattutto quelli non razionali), tuttavia ci sono esempi notevoli di progresso in questo ambito composto dagli androidi (es. Actroid) e dall'interazione emotiva con l'utente (sentiment analysis)
3. Il comportamento intelligente non può essere completamente catturato da regole formali (informality)
   - Il comportamento umano è troppo complesso per essere codificato in regole formali, così come la conoscenza non è sempre codificabile in forma simbolica. Questo avviene perché l'intelligenza è in realtà una proprietà emergente dell'architettura neurale, avente una certa struttura, con certi collegamenti e un certo funzionamento (conoscenza **sub-simbolica**).
4. Il test di Turing non è sufficiente come dimostrazione di intelligenza, anche se venisse superato 
   - E' difficile pensare che una macchina che svolge meccanicamente operazioni elementari sia intelligente, al massimo potrebbe essere una replica di una intelligenza linguistica. Questo perchè una manipolazione di simboli tramite l'applicazione di regole non può produrre un'intelligenza cosciente, come vedremo nell'esempio della "Stanza Cinese" di J. Searle.

### La Stanza Cinese

![](img/chroom.png)

J. Searle mostra un semplice esempio, comparando la macchina di Turing a un signore inglese: questo signore riceve un insieme di simboli cinesi che manipola secondo regole a lui ignote e alla fine fornisce delle risposte. Il punto è che questo signore, anche se ha svolto un lavoro corretto, non conosce il cinese, quindi non capisce il significato di ciò che ha svolto, ne di ciò che ha usato per arrivare al risultato. 

Allo stesso modo, il calcolatore potrebbe dimostrare di essere intelligente per il test di Turing, senza però comprendere nulla di ciò che ha svolto.

Pertanto, con questo si vuole dimostrare che non si può generare una semantica dalla sintassi, quindi i programmi non sono condizione essenziale né sufficiente per la determinazione di una mente intelligente: gli elementi dell'intelligenza cosciente **devono** possedere un contenuto semantico reale.

Tuttavia c'è un contraddittorio: dato che ogni uomo è costituito da molecole, e le molecole non hanno coscienza, questo vuol dire che l'uomo non ha coscienza?

### La stanza di Maxwell

Per risolvere questa contraddizione basta pensare all'esempio de "La stanza di Maxwell". 

![](img/mxroom.png)

Un signore ruota un magnete con sopra montato un led nella sua stanza, sapendo che la rotazione di un campo elettromagnetico dovrebbe generare delle onde in grado di accendere il led: tuttavia questo non accade.

Saremmo quindi propensi a dire che la legge di Maxwell sull'elettromagnetismo sia scorretta perché l'esperimento non ha avuto successo: tuttavia il fatto che non si veda la luce, non vuol dire che non ci sia! 

Siccome il signore non ruota abbastanza velocemente il magnete non è possibile osservare alcuna luce perché sarebbe troppo fioca all'occhio umano, ma esiste comunque: se il magnete fosse ruotato molto più velocemente, allora sarebbe possibile osservarla.

Questo dimostra che quel contraddittorio in realtà non regge: nulla ci autorizza a dire che la coscienza umana non esiste solo perché non possiamo osservarla. La coscienza è infatti una **proprietà emergente** del nostro sistema di neuroni, ognuno propriamente disposto e attivo.

Su queste basi si pone il focus del machine learning: non una intelligenza programmata, ma una intelligenza emergente, proveniente da degli elementi relativamente semplici che interagiscono fra di loro e con l'ambiente come un sistema coeso, evolvendo nel tempo.

Passiamo quindi da una macchina homunculus, nella quale è il programmatore a dare la conoscenza pregressa, a una macchina più umana, in grado di imparare da sè anche in assenza (o quasi) di conoscenze alla creazione.

### Ipotesi forte dell'IA

Ricapitolando, nella **IA forte**, la macchina non è soltanto uno strumento funzionale. Se programmata in maniera opportuna, diventa essa stessa una mente, con una **capacità cognitiva non distinguibile da un cervello umano**.

La tecnologia alla base dell’Intelligenza Artificiale forte è quella dei **sistemi esperti**, cioè una serie di programmi che vogliono riprodurre, attraverso una macchina, le prestazioni e le conoscenze delle persone esperte in un determinato campo, basandosi su reti neurali, connessionismo, machine learning e scienze cognitive. 

### Recap ipotesi

- Weak AI: "Think rationally"
  - Il calcolatore è uno strumento costruito per agire come se fosse intelligente, in cui la conoscenza è direttamente fornita dal costruttore e la quale limita la macchina a svolgere ciò che è stato richiesto senza capire la semantica.
- Strong AI: "Think humanly"
  - Il calcolatore è uno strumento costruito per avere una mente intelligente, reale e conscia, per poter comprendere la semantica e avere stati cognitivi simili a quelli del cervello umano.

## Il modello umano

Abbiamo detto che l'intelligenza umana è una proprietà emergente proveniente dalla rete di neuroni che ci compone: ma cosa succederebbe se sostituissimo uno per uno tutti i neuroni nella corteccia con un dispositivo elettronico equivalente?

Questo è il cosiddetto **Brain Prosthetic Experiment**, formulato da Moravec, per evidenziare la fallacia nel pensiero che la nostra intelligenza provenga solo dalla rete di neuroni e niente più.

Da questa domanda, ci sono due risposte fra le più sostenute:

- **Risposta funzionalista**
  - Se il neurone artificiale funziona "allo stesso modo", allora vuol dire che anche se i costituenti sono diversi la mente e quindi la coscienza *permane*. Il problema è capire cosa si intende per "allo stesso modo"
- **Risposta strutturalista**
  - La coscienza deriva dalla struttura stessa dei neuroni e dei loro collegamenti, rimpiazzandoli la coscienza *svanirebbe*

Se mai dovessimo implementare un sistema simile al nostro sistema nervoso, dovremmo prima osservare come è composto: 

- Ha una struttura altamente densa e parallela ($10^{11}$ neuroni con $10^{14}$ connessioni)
- Il neurone è un elemento relativamente semplice, il suo stato è definibile come continuo, non discreto
- Possiede connessioni bidirezionali

A partire da queste proprietà si è iniziato a creare delle **reti neurali artificiali**, atte a emulare il funzionamento biologico. Questi modelli si sono rivelati più potenti e versatili rispetto a quelli parametrici: ma come definiamo l'apprendimento in una rete neurale? Lo vedremo nel corso delle prossime lezioni.

## Note e link utili

1. https://www.intelligenzaartificiale.it/intelligenza-artificiale-forte-e-debole/