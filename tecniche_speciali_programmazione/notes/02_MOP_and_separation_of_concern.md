# Meta-Object Protocol and Separation of Concerns

The idea of meta-object protocol was born from the need of simpler operation for reading and writing.

It's important that lower level of reflection are unaware of being observed. 

Therefore there is a *black box* use of the functionality of the base-level system, whose structure can be dynamically modified and the details of the implementation are open up to the meta-level system.

The **black box** approach give us:

- The accesses to the system functionality is limited to the mechanism provided by the adopted programming language.
- An attempt of using the system functionality can raise an "application mismatch" when a component is used in the wrong way.
- Flexibility is really limited.

But we want to have more information about the process, so we will use a **grey box** approach, that grant us:

- Open implementation.
- The component behavior can be adapted to our needs.
- We can bypass the mechanism provided by the programming language to access the system functionality.
- We can re-class the objects respecting their use and behavior.

There are (at least) three ways to open up the system details:

- **Introspection**, the system ability of observing his own state and structure.
- **Intercession**, the system ability of modifying his own behavior and structure.
- **Invoke**, the system ability of applying the system functionality.

The meta-object protocol is implemented in non-typed and interpreted programming language (Lisp, SmallTalk), in compiled programming languages (C, C++) and in typed and interpreted programming languages (Java).

Our focus will be on the use of those methods in Java, with the *java.lang.reflect* library, but reflection in java can also be found in *OpenJava* and *Javaassist* (which let us use assembly code without actually know it).

## Separation of Concerns

**COMPLETE APPLICATION = CORE FUNCTIONALITY (base-level) + NONFUNCTIONAL CONCERNS (meta-levels)**

**N.B.** Separation between functional and nonfunctional is not strictly defined.

The **separation of concerns** (SoC) of the code is a very important concept to take in consideration. Traditionally, the separation of concern is done at design stage only and the source code is a mix of all concerns (functional and nonfunctional).

The separation of concerns aims at enabling such a separation inside of the implementation code, and to do that it makes use of the reflection and the aspect-oriented programming.

Reflection allows the designer of separating the functional aspect from the nonfunctional ones. Therefore we get:

- An augmentation of the functionality reuse.
- More stability in the system.
- Independence between functional and nonfunctional aspects.
