# Dynamic Proxy

A **proxy** is a design pattern of an object providing a surrogate/placeholder for another object to control and manipulate the access to it. A proxy is set between the server and the client.

A **dynamic proxy class** is a class implementing a list of interfaces such that the invocation of a method belonging to one of these interfaces on one of its instances is seamlessly dispatched to another object implementing such interface and bound to the dynamic proxy object.

A dynamic proxy class can be used to create a type-safe proxy object for a set of objects determined by the implemented interfaces without both explicit coding and static pre-generation of the proxy class as happens with several compile-time tools.

A dynamic proxy receives some interfaces and creates **proxy objects** based on them.

Each proxy instance has an associated **invocation handler**: when a method is invoked on a proxy instance, the method invocation is dispatched to the `invoke()` method of its invocation handler.

## Proxy class in Java

The Java class **Proxy** provides static methods for creating dynamic proxy classes and instances and it also is the superclass of all dynamic proxy classes created by these methods. 

Basically, *Proxy* enables the meta-object approach in Java.

```java
public final class Proxy extends Object implements Serializable {
	protected InvocationHandler h;
	protected Proxy(InvocationHandler h) { ... }
	public static InvocationHandler getInvocationHandler(Object proxy) { ... }
	public static Class<?> getProxyClass(ClassLoader l, Class<?>... interfaces) {...}
	public static boolean isProxyClass(Class<?> cl) { ... }
	public static Object newProxyInstance(ClassLoader loader,
			Class<?>[] interfaces, InvocationHandler h ) { ... }
}
```

# Examples

## Tracing Methods Calls

```java
public class TraceHandler implements InvocationHandler {
	private Object baseObject;
	public TraceHandler(Object base) { 
        baseObject = base; 
    }
	public Object invoke(Object proxy, Method m, Object[] args) {
		try {
			System.out.println("before " + m.getName());
			Object result = m.invoke(baseObject, args);
			System.out.println("after " + m.getName());
			return result;
		} catch (Exception e) {e.printStackTrace(); return null;}
	}
	public String toString() { return "th :- "+this.baseObject; }
}
```



## Invariant Checking and Proxy Chaining

```java
public class InvariantHandler implements InvocationHandler {
	private Object target;
	private Method invariant;
	public InvariantHandler(Object target) {
		this.target = target;
		try {
			invariant = target.getClass().getMethod("invariant", new Class<?>[]{});
			if (!invariant.getReturnType().equals(boolean.class)) 
                invariant = null;
		} catch (NoSuchMethodException ex) { 
            invariant = null; 
        }
	}
	public Object invoke(Object proxy, Method method, Object[] args) 
        	throws Throwable {
		this.invokeInvariant(method);
		Object retvalue = method.invoke(this.target, args);
		this.invokeInvariant(method);
		return retvalue;
	}
	private void invokeInvariant(Method method) {
		if ((this.invariant == null) || (method.equals(this.invariant))) 
            return;
		try {
			Boolean passed = (Boolean)invariant.invoke(target, new Object[]{});
			if (!passed.booleanValue()) 
                throw new RuntimeException();
		} catch (Exception e) { 
            System.out.println("Failed invariant check!!!"); 
        }
	}
	public String toString() {
        return "ih :- "+this.target; 
    }
}
```



















































