# TSP CHEATSHEET: PART 2

A collection of the most important things of the second part of the course.

## The AOR Process

1. Introduce a refactoring aspect:
   1. Insert an empty aspect;
   2. Define a pointcut to capture the requested join points;
   3. Create a suitable advice for the pointcut (empty).
2. Introduce the crosscutting functionalities:
   1. Introduce compile-time warnings (optional);
   2. Add crosscutting functionality filling the advice;
   3. Remove method calls from the advised methods.

## Most Important Predicates

- `call(MethodPattern)`;
- `execution(MethodPattern)`;
- `get(FieldPattern)`;
- `set(FieldPattern)`;
- `target(Type/Id)`;
- `this(Type/Id)`;
- `within(TypePattern)`;
- `withincode(MethodPattern)`;
- `cflow(Pointcut)`;
- `cflowbelow(Pointcut)`;
- `args(Type/Id, ...)`;

## Most Important Advices

- `before`;
- `after`;
- `after returning`;
- `after throwing`;
- `around`;

## Wildcarding in Pointcuts

```java
target(Point)
target(graphics.geom.Point)
target(graphics.geom.*)				// any type in graphics geom
target(graphics..*)					// any type in any sub-package of graphics

call(void Point.setX(int))
call(public * Point.*(..))			// any public method on Point class
call(public * *(..))				// any puclic method on any type
call(void Point.getX())
call(void Point.getY())
call(void Point.get*())
call(void get*())					// any getter method
    
call(Point.new(int, int))
call(new(..))						// any constructor
```

