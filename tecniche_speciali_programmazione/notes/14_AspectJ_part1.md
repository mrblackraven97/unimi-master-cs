# AspectJ (Part 1)

AspectJ is an Overlay onto Java that manages where a crosscutting occurs in the computational flow, called **join points**, and it provides a mechanism tat manages the crosscutting concerns composition at the specified join points, called **weaver**.

AspectJ, as en extension to the Java language, introduces new definitions:

- **Pointcuts**: Predicates that pick out join points and values at those points;
- **Advice**: Additional action to take at join points in a pointcut;
- **Inter-class declaration**: Also known as introduction, they're like open classes;
- **Aspect**: A modular unit of crosscutting behavior.

### A Single Figure Package

![](https://www.eclipse.org/aspectj/doc/released/progguide/figureUML.gif)

The code to implement these classes could look like:

```java
class Line implements FigureElement{
	private Point p1;
    private Point p2;
	
    public Point getP1() { 
        return p1; 
    }
	public Point getP2() { 
        return p2; 
    }
	public void setP1(Point p1) { 
        this.p1 = p1;
    }
	public void setP2(Point p2) {
        this.p2 = p2;
    }
	public void moveBy(int dx, int dy) { 
        p1.moveBy(dx, dy); 
        p2.moveBy(dx, dy);
    }
}

class Point implements FigureElement {
	private int x = 0;
    private int y = 0;

    public int getX() { 
        return x;
    }
	public int getY() { 
        return y;
    }
	public void setX(int x) {
        this.x = x;
    }
	public void setY(int y) { 
        this.y = y;
    }
	public void moveBy(int dx, int dy) {
        x += dx;
        y += dy;
    }
}
```

The display is a sort of a collection of `FigureElements` that moves periodically.

The display must be periodically refreshed and must coordinate asynchronous events.

Several applications manifest a similar behavior:

- Session liveness;
- Value checking (in spreadsheets).

## Join Points

**Join Points** are key points in the dynamic call graph.

Let's suppose that the statement `l.moveBy(1,3)` moves the line `l`

![](https://i.ytimg.com/vi/5jeZjmwhIbQ/maxresdefault.jpg)

There are several kinds of join points:

- Method and constructor call;
- Method and constructor execution;
- Field get and set;
- Exception handler execution;
- Static and dynamic initialization.

## Pointcuts

A **pointcut** is a predicate on join points that can match (or not) any given join point. Optionally, it can pull out some of the values at that join point. For example:

```java
call(void Line.setP1(Point));
```

This pointcut matches those join points that:

- Represent a call to a method named `setP1`
- Have an argument of type `Point` and no return value (`void`);
- Comes from an object of type `Line`.

### Pointcut: Composition

Pointcuts compose like logic predicates, using '&&', '||' and '!'.

```java
call(void Line.setP1(Point)) || call(void Line.setP2(Point));
```

This pointcut matches those join points that:

- Represent a call to a method named `setP1`;
- Have an argument of type `Point` and no return value;
- Comes from an object of type `Line`

Or that:

- Represent a call to a method named `setP2`;
- Have an argument of type `Point` and no return value;
- Comes from an object of type `Line`

### User-Defined Pointcuts

A pointcut can be of two kinds:

- **Named pointcuts**: Primitive predicates;
- **User-defined pointcuts**: Custom predicates, used the same as primitive ones.

*Example: a custom pointcut*

```java
pointcut move():
	call(void Line.setP1(Point)) || call(void Line.setP2(Point));
```

### Primitive predicates

Primitive pointcut are:

- `call(MethodPattern)`: Picks out each method call join point whose signature matches *`MethodPattern`*.
- `execution(MethodPattern)`: Picks out each method execution join point whose signature matches *`MethodPattern`*.
- `get(FieldPattern)`: Picks out each field reference join point whose signature matches *`FieldPattern`*. [Note that references to constant fields (static final fields bound to a constant string object or primitive value) are not join points, since Java requires them to be in lined.]
- `set(FieldPattern)`: Picks out each field set join point whose signature matches *`FieldPattern`*. [Note that the initializations of constant fields (static final fields where the initializer is a constant string object or primitive value) are not join points, since Java requires their references to be in lined.]
- `call(ConstructorPattern)`: Picks out each constructor call join point whose signature matches *`ConstructorPattern`*.
- `execution(ConstructorPattern)`: Picks out each constructor execution join point whose signature matches *`ConstructorPattern`*
- `initialization(ConstructorPattern)`: Picks out each object initialization join point whose signature matches *`ConstructorPattern`*.
- `preinitialization(ConstructorPattern)`: Picks out each object preinitialization join point whose signature matches *`ConstructorPattern`*.
- `staticinitialization(TypePattern)`: Picks out each static initializer execution join point whose signature matches *`TypePattern`*.
- `handler(TypePattern)`: Picks out each exception handler join point whose signature matches *`TypePattern`*.
- `adviceexecution()`: Picks out all advice execution join points.
- `within(TypePattern)`: Picks out each join point where the executing code is defined in a type matched by *`TypePattern`*.
- `withincode(MethodPattern)`: Picks out each join point where the executing code is defined in a method whose signature matches *`MethodPattern`*.
- `withincode(ConstructorPattern)`: Picks out each join point where the executing code is defined in a constructor whose signature matches *`ConstructorPattern`*.
- `cflow(Pointcut)`: Picks out each join point in the control flow of any join point *`P`* picked out by *`Pointcut`*, including *`P`* itself.
- `cflowbelow(Pointcut)`: Picks out each join point in the control flow of any join point *`P`* picked out by *`Pointcut`*, but not *`P`* itself.
- `this(Type/Id)`: Picks out each join point where the currently executing object (the object bound to `this`) is an instance of *`Type`*, or of the type of the identifier *`Id`* (which must be bound in the enclosing advice or pointcut definition). Will not match any join points from static contexts.
- `target(Type/Id)`: Picks out each join point where the target object (the object on which a call or field operation is applied to) is an instance of *`Type`*, or of the type of the identifier *`Id`* (which must be bound in the enclosing advice or pointcut definition). Will not match any calls, gets, or sets of static members.
- `args(Type/Id, ...)`: Picks out each join point where the arguments are instances of the appropriate type (or type of the identifier if using that form). A `null` argument is matched if the static type of the argument (declared parameter type or field type) is the same as, or a subtype of, the specified `args` type.
- `PointcutId(TypePattern/Id, ...)`: Picks out each join point that is picked out by the user-defined pointcut designator named by *`PointcutId`*.
- `if(BooleanExpression)`: Picks out each join point where the boolean expression evaluates to `true`. The boolean expression used can only access static members, parameters exposed by the enclosing pointcut or advice, and `thisJoinPoint` forms. In particular, it cannot call non-static methods on the aspect or use return values or exceptions exposed by after advice.
- `!Pointcut`: Picks out each join point that is not picked out by *`Pointcut`*.
- `Pointcut0 && Pointcut1`: Picks out each join points that is picked out by both *`Pointcut0`* and *`Pointcut1`*.
- `Pointcut0 || Pointcut1`: Picks out each join point that is picked out by either pointcuts. *`Pointcut0`* or *`Pointcut1`*.
- `(Pointcut)`: Picks out each join points picked out by *`Pointcut`*.

## After Advice

The after advice `after()` specifies the action to take after the computation under join points is completed:

```java
pointcut move():
	call(void Line.setP1(Point)) || call(void Line.setP2(Point));

after() returning: move() {
	//«code here runs when the method move returns»
}
```

## Display Updating v 1.0

An aspect defines a special class that can crosscut other classes.

```java
aspect DisplayUpdating {
	pointcut move():
		call(void Line.setP1(Point)) || call(void Line.setP2(Point));
	after() returning: move() { 
        Display.update();
    }
}
```

Note that the `DisplayUpdating` is a complete running code.

Update calls are tangled through the code, but "what is going on" is less explicit.

Updating without AOP would look like this:

```java
public class Line extends FigureElement {
	private Point p1;
    private Point p2;
	
    public Point getP1() {
        return p1;
    }
	public Point getP2() { 
        return p2;
    }
	public void setP1(Point p1) {
		this.p1 = p1;
		Display.update();
	}
	public void setP2(Point p2) {
		this.p2 = p2;
		Display.update();
	}
	public void moveBy(int dx, int dy) {
		p1.moveBy(dx, dy);
        p2.moveBy(dx, dy);
		Display.update();
	}
}
```

Pointcuts can:

- Cut across multiple classes;
- Use interface signatures.

```java
pointcut move(FigureElement figElt):
	target(figElt) &&
		(call(void FigureElement.moveBy(int, int)) ||
		 call(void Line.setP1(Point)) || call(void Line.setP2(Point)) ||
		 call(void Point.setX(int)) || call(void Point.setY(int)));

after(FigureElement fe) returning: move(fe) {
    // fe is bount to the figure element
}
```

Pointcut can explicitly expose certain values to the advice.

Variables that are bound by user-defined pointcut declarations:

- Pointcut supplies value for variable;
- Value is available to all users of user-defined pointcut.

```java
pointcut move(Line l):
	target(l) &&
		(call(void Line.setP1(Point)) ||
		 call(void Line.setP2(Point))
		);

after(Line line) returning: move(line) {
	//«line is bound to the line»
}
```

Variables that are bound by advice declaration:

- Pointcut supplies value for variable;
- Value is available in advice body.

Values are "pulled":

- From right to left (right pointcuts are computed first);
- From pointcuts to user-defined pointcuts;
- From pointcuts to advice, and then advice body.

## Primitive Pointcut Designator: target()

​			`target(<type_name> | <formal_reference>)`

It does two things:

- Expose the target;
- Predicate on join points: any join point at which target object is an instance of type name (dynamic test).

`target()` can be used in polymorphic pointcut:

- Does not further restrict the join points;
- Does pick up the target object.

## Display Update v 3.0

Context and Multiple Classes.

```java
aspect DisplayUpdating {
	pointcut move(FigureElement figElt):
		target(figElt) &&
			(call(void FigureElement.moveBy(int, int)) 	||
			 call(void Line.setP1(Point)) 				||
			 call(void Line.setP2(Point)) 				||
			 call(void Point.setX(int))					||
			 call(void Point.setY(int))
			);

	after(FigureElement fe): move(fe) {
		Display.update(fe);
	}
}
```

### Without AspectJ

```java
public class Line {
	private Point p1;
    private Point p2;
    
	public Point getP1() { 
        return p1;
    }
	public Point getP2() { 
        return p2;
    }
	public void setP1(Point p1) {
        this.p1 = p1;
        Display.update(this);
    }
	public void setP2(Point p2) {
		this.p2 = p2;
        Display.update(this);
    }
}
public class Point {
	private int x = 0;
    private int y = 0;

    public int getX() { 
        return x;
    }
	public int getY() {
        return y;
    }
	public void setX(int x) { 
        this.x = x;
        Display.update(this);
    }
	public void setY(int y) { 
        this.y = y;
        Display.update(this);
    }
}
```

No locus of "display updating":

- Evolution is cumbersome;
- Changes in all classes;
- Have to track and change all callers.

### With AspectJ

```java
import it.unimi.di.adapt.figures.* ;
import it.unimi.di.adapt.display.* ;

public aspect DisplayUpdating {
	pointcut move(FigureElement figElt):
		target(figElt) && (
			call(void FigureElement.moveBy(int, int)) ||
			call(void Line.setP1(Point)) ||
			call(void Line.setP2(Point)) ||
			call(void Point.setX(int)) ||
			call(void Point.setY(int))
		);
    after(FigureElement fe) returning: move(fe) {
        Display.update(fe);
    }
}
```

```java
public class Line {
	private Point p1;
    private POint p2;

    public Point getP1() { 
        return p1;
    }
	public Point getP2() {
        return p2;
    }
	public void setP1(Point p1) {
		this.p1 = p1;
	}
	public void setP2(Point p2) {
		this.p2 = p2;
	}
}

public class Point {
	private int x = 0;
    private int y = 0;

    public int getX() { 
        return x;
    }
	public int getY() { 
        return y;
    }
	public void setX(int x) {
		this.x = x;
	}
	public void setY(int y) {
		this.y = y;
	}
}
```

Clear display updating module:

- All changes in single aspect;
- Evolution is modular.

The aspect modularity cuts across the class modularity.

## Advice

**Advice** is an additional action to take at join points:

- `before`: Before proceeding at join point;
- `after returning`: After returning a value to join point;
- `after throwing`: After throwing a throwable to join point;
- `after`: After returning to join point in either ways;
- `around`: On arrival at join point gets explicit control over when and if program proceed.

## Contract Checking: An Example with Advices

Contract conditions:

- **Preconditions** (`before` advice): check whether parameter is valid;

  ```java
  aspect PointBoundsPreCondition {
  	before(int newX): 
      	call(void Point.setX(int)) && args(newX) {
  			assert(newX >= MIN_X);
  			assert(newX <= MAX_X);
  	}
  	before(int newY):
  		call(void Point.setY(int)) && args(newY) {
  			assert(newY >= MIN_Y);
  			assert(newY <= MAX_Y);
  	}
      private void assert(boolean v) {
          if(!v) 
              throw new RuntimeException();
      }
  }
  ```

- **Postconditions** (`after` advice): check whether values where set;

  ```java
  aspect PointBoundsPostCondition {
      after(Point p, int newX) returning:
      	call(void Point.setX(int)) && target(p) && args(newX) {
              assert(p.getX() == newX);
      }
      after(Point p, int newY) returning:
      	call(void Point.setY(int)) && target(p) && args(newY) {
              assert(p.getY() == newY);
      }
      private void assert(boolean v) {
          if(!v)
              throw new RuntimeException();
      }
  }
  ```

- **Condition enforcement** (`around` advice): force parameters to be valid.

  ```java
  aspect PointBoundsEnforcement {
  	void around(int newX):
      	call(void Point.setX(int)) && args(newX) {
  		proceed(clip(newX, MIN_X, MAX_X));
  	}
  	void around(int newY):
  		call(void Point.setY(int)) && args(newY) {
  		proceed(clip(newY, MIN_Y, MAX_Y));
  	}
  	private int clip(int val, int min, int max) {
  		return Math.max(min, Math.min(max, val));
  	}
  }
  ```

For each around advice with the signature:

​										`<Tr> around(T1 arg1, T2 arg2, ..., Tn argn)`

There is a special method with the signature:

​										`<Tr> proceed(T1, T2, ..., Tn)`

Available only in an around advice. This means "run what would have run if this around advice had not been defined".







