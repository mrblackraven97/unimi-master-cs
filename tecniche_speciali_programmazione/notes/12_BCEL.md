# BCEL - Bytecode Engineering Library

**BCEL** (Bytecode Engineering Library) is a toolkit for the static analysis and dynamic creation or transformation of Java class files.

It is part of the Apache project.

BCEL approach is very low level: it tries to map one to one on the Java class file.

The BCEL API, formerly known as **JavaClass**, is used to **analyze Java classes without having the source files** at hand.

All the binary components and data structures declared in the JVM specification are mapped to classes.

This enables developers to implement the desired features on a high level of abstraction without handling all the internal details of the Java class file format.

BCEL can be logically divided into 2 parts:

- Static API: read-only access to class files;
- Dynamic API: read-write access to class files.

BCEL is "physically" divided into 4 parts:

- `org.apache.bcel.classfile`: Structure describing Java class file and class file parser;
- `org.apache.bcel.generic`: Code to modify class files and byte code instructions;
- `org.apache.bcel.util`: Generic utilities, like converter for class file to HTML, find instructions pattern via regular expressions, and so on;
- `org.apache.bcel.verifier` : Bytecode verifier (JustIce).

## JavaClass

To start inspecting existing class with BCEL, the starting point is looking at `org.apache.bcel.classfile.JavaClass`.

`JavaClass` enables the class reification as done by `java.lang.Class` when using the regular reflection.

All the `JavaClass` instances have:

- A constant pool, fields and methods;
- Symbolic references to the super class and to the implemented interfaces of the class.

At run-time, the `JavaClass` objects can be used as meta-objects describing the contents of a class.

In short, `JavaClass` object represents the parsed class file, by querying these objects we can get any information about the class.

`JavaClass` can be reified thanks to:

- The tools provided by the `org.apache.bcel.generic` package;
- The `ClassParser` that parses a given Java `.class` file and returns a `JavaClass` object.

*Example: Introspection with JavaClass*

```Java
jshell> import org.apache.bcel.Repository
jshell> import org.apache.bcel.classfile.JavaClass
jshell> JavaClass clazz = Repository.lookupClass("HelloWorld")
clazz ==> public class HelloWorld extends java.lang.Object
... void <init>()
	void say()
jshell> System.out.println(clazz)
public class HelloWorld extends java.lang.Object
filename				HelloWorld
compiled from			HelloWorld.java
compiler version		55.0
access flags			33
constant pool			32 entries
ACC SUPER flag			true
    
Attribute(s):
		SourceFile: HelloWorld.java

1 fields:
		private String ht

2 methods:
		public void <init>()
		void say()
            
jshell> clazz.getMethods()
$7 ==> Method[2] { public void <init>(), void say() }

jshell> var m = clazz.getMethods()[1]
m ==> void say()

jshell> m.getCode()
$10 ==> Code(max_stack = 2, max_locals = 1, code_length = 11)
0:		getstatic			java.lang.System.out Ljava/io/PrintStream; (4)
3:		aload_0
4:		getfield			HelloWorld.ht Ljava/lang/String; (3)
7:		invokevirtual		java.io.PrintStream.println (Ljava/lang/String;)V (5)
10:		return
    
Attribute(s) =
LineNumber(0,5), LineNumber(10,6)
```

Using the provided `Repository` class, reading class files into a `JavaClass` is quite simple:

```java
JavaClass clazz = Repository.lookupClass("java.lang.String");
```

The repository also contains methods providing the dynamic equivalent of the `instanceof` operation, and other useful functionalities:

```java
if (Repository.instanceOf(clazz, super_class) { ... }
```

Information within the Class file components may be accessed via intuitive set/get methods:

```java
import org.apache.bcel.Repository ;
import org.apache.bcel.classfile.* ;

public class IntrospectionBCEL {
    
	public static void printCode(Method[] methods) {
		for (var m:methods) {
			System.out.println(m);			// print method signature
			Code body = m.getCode();		// print bytecode of
			if (body != null)				// non-abstract, non native methods
				System.out.println(body) ;
		}
	}
    
	public static void main(String[] args) {
		try {
			JavaClass clazz = Repository.lookupClass("java.lang.String") ;
			printCode(clazz.getMethods());
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
```

## ClassGen

The `org.apache.bcel.generic` package defines classes whose instances deal with the construction of various class components and the dynamic transformation of class file.

The generic constant pool, for example, implemented by the class `ConstantPoolGen`, provides methods for introducing new constant types.

Accordingly, `ClassGen` provides with methods to add or delete other methods, fields and class attributes.

In short, `ClassGen` objects represent classes that can be edited.

### Type

The class `Type` permits to abstract from the details of type signatures (ex. when defining new methods).

BCEL provides some default constants for common types, like `BOOLEAN`, `CHAR`, `FLOAT`, `INT`.

*Example: Type signature of the `main()` method*

```java
jshel> import org.apache.bcel.generic.*

jshel> Type ret_type = Type.VOID;
ret_type ==> void
    
jshel> Type[] arg_types = new Type[] {new ArrayType(Type.STRING, 1)};
arg_types ==> Type[1] { java.lang.String[] }
```

### Fields and Methods

In BCEL, fields are represented by `FieldGen` objects. If they have the access right `static final` they may optionally have an initiating value.

Methods are represented by `MethodGen` objects. They have methods to:

- Add local variables;
- Throw exceptions;
- Catch and handle exceptions.

### Instruction Objects

Instructions are modeled as objects, this enables programmers to obtain a high level view upon control flow without handling details like concrete bytecode addresses.

Instruction objects basically consist of a tag: an opcode and the length in bytes.

The class `InstructionList` is a container for a list of `Instruction` where:

- `Instruction` can be appended, inserted, deleted, etc.
- Each `Instruction` is wrapped into a `InstructionHandles` object that play the role of an iterator.

A list is finally dumped to a byte code array with `getByteCode()`.

## Example: New Methods and Attributes

The goal is to "extend" and existing class by adding a new method and a new attribute with the bytecode engineering process provided by BCEL.

To do so, first we create a `ClassGen` object representing the class we want to upgrade

```java
ClassGen cg = new ClassGen("Test","java.lang.Object","Test.java",ACC_PUBLIC, null);
```

- `Test` = Class name;
- `java.lang.Object` = Super class name;
- `Test.java` = File name;
- `null` = implemented interfaces list.

Then we create and add a new field ``'field1'` of type `int` to the new class

```java
ConstantPoolGen cp = cg.getConstantPool();
FieldGen fg = new FieldGen(ACC_PUBLIC, org.apache.bcel.generic.Type.INT, "field1", 								cp);
cg.addField(fg.getField());
```

- `org.apache.bcel.generic.Type.INT` = field type;
- `"field1"` = field name;

Finally, we create and add a new method `'method1'` without arguments and return value, whose body simply returns

```java
InstructionList il = new InstructionList(); 
il.append(RETURN);
MethodGen mg = new MethodGen(ACC_PUBLIC, org.apache.bcel.generic.Type.VOID,
							 org.apache.bcel.generic.Type.NO_ARGS, null, 										 "method1","Test", il, cp);
cg.setMaxStack();
cg.addMethod(mg.getMethod());
```

- `org.apache.bcel.generic.Type.VOID` = Return type;
- `org.apache.bcel.generic.Type.NO_ARGS` and `null` = Arguments type and name;

## Bytecode Instrumentation at Load Time

Now we want to carry out bytecode instrumentation at load time.

To do so we must:

- Remove the annotations decorating a class or its methods;
- Introducing some specific bytecode in a place of the annotation.

This kind of instrumentation can be used to enrich the semantics of a programming language (ex. `Serializable` interface).

To instrument the code at load-time, we need an **external agent** that carries out the needed instrumentation.

An agent is launched by indicating the agent class and its agent options when the JVM is launched (through `-javaagent` option).

The agent class must implement a public static `premain()` method similar in principle to the main application entry point:

```java
public static void premain(String agentArgs, Instrumentation inst) {
    ...
}
```

After the JVM is initialized:

1. Each `premain()` method will be called in the specific order;

2. The real application `main()` method will be called.

Each agent receives its options via the `agentArgs` parameter.

Since Java 1.5, the `java.lang.instrument` package provides services that enables Java agents to instrument running programs.

In particular `java.lang.instrument` defines two interfaces:

- `ClassFileTransformer`: An agent provides an implementation of this interface to transform class files;
- `Instrumentation`: The class that implements this interface provides the agent with the services needed to instrument the Java code.

To register our `ClassFileTransformer` as agent, we need to define a `premain()` method:

```java
public class InstrumentorAdaptor implements ClassFileTransformer {
	private Instrumentor instrumentor;
	
    public InstrumentorAdaptor(Instrumentor instrumentor) {
		this.instrumentor = instrumentor;
	}

	public byte[] transform(ClassLoader cl, String className,
		Class classBeingRedefined, ProtectionDomain pd, byte[] classfileBuffer) { 
        ...
    }

	public static void premain(String className, Instrumentation i)
			throws ClassNotFoundException, InstantiationException, 									   IllegalAccessException {
		Class instClass = Class.forName(className);
		Instrumentor inst = (Instrumentor)instClass.newInstance();
		i.addTransformer(new InstrumentorAdaptor(inst));
	}
}
```

Our agent `InstrumentorAdaptor` is invoked as follows:

```java
>>> java -javaagent:InstrumentorAdaptor
```

The installed adaptor acts as a **bridge** between the `Instrumentor` interface and the Java's `ClassFileTransformer` interface.

`Instrumentation.addTransformer()` enrolls a transformer among the agents of the application and it will be called every calls definition and redefinition.

- `ClassLoader.defineClass()` requests a new class definition;
- `Instrumentation.redefineClasses()` requests a class redefinition.

The implementation of the `transform()` method provided by the agent will deal with the bytecode instrumentation:

```java
public byte[] transform (ClassLoader cl, String className,
		Class classBeingRedefined, ProtectionDomain pd, byte[] classfileBuffer) {
	try {
		ClassParser cp =
		new ClassParser(new ByteArrayInputStream(classfileBuffer), 											className+".java");
		JavaClass jc = cp.parse();
		ClassGen cg = new ClassGen(jc);
		for (Annotation an : getAnnotations(jc.getAttributes()))
			instrumentor.instrumentClass(cg, an);
		for (org.apache.bcel.classfile.Method m : cg.getMethods()) {
			for (Annotation an : getAnnotations(m.getAttributes())) {
				MethodGen mg = new MethodGen(m, className, cg.getConstantPool());
				instrumentor.instrumentMethod(cg, mg, an);
				mg.setMaxStack(); mg.setMaxLocals();
				cg.replaceMethod(m, mg.getMethod());
			}
		}
		return cg.getJavaClass().getBytes();
	} catch (Exception ex) {
		throw new RuntimeException("instrumenting " + className, ex);
	}
}
```

**`transform()` - Parameters**:

- `cl` = The defining loader of the class to be transformed;
- `className` = The fully qualified name of the class (ex. `java/util/List`);
- `classBeingRedefined` = If it has been called on a redefine, it contains the class being redefined, otherwise is `null`;
- `pd` = The protection domain of the class being defined or redefined;
- `classfileBuffer` = The input byte buffer in class file format, it must not be modified.

**`transform()` - Locals**:

- `ClassParser cp` = A wrapper class that parses a given Java `.class` file;
- `ClassGen cd` = For building up a Java class from an existing class;

## Instrumentor

```java
import org.apache.bcel.classfile.* ;
import org.apache.bcel.generic.* ;
import java.lang.annotation.* ;

public interface Instrumentor {
  public void instrumentClass(ClassGen classGen, Annotation a);
  public void instrumentMethod(ClassGen classGen, MethodGen methodGen, Annotation a);
}
```

```java
public class TestInstrumentor implements Instrumentor {
    private static TestInstrumentor instance = new TestInstrumentor();
    
    public static Instrumentor getInstance() {
        return instance;
    }
    
    public void instrumentClass(ClassGen classGen, Annotation a) {
        ...
    }
    
    public void instrumentMethod(ClassGen classgen, MethodGen methodGen, 
                                 Annotation	a) {
        ClassGen modClass = classGen;
        ConstantPoolGen cp = modClass.getConstantPool();
        InstructionFactory factory = new InstructionFactory(modClass, cp);
        InstructionList il = methodGen.getInstructionList();
        il.insert(factory.createPrintln("BCEL Bytecode Instrumentation!!!"));
    }
}
```

## Conclusion

Bytecode instrumentation is quite a complex job.

BCEL provides us with services to deal with the job. It's a powerful tool, you can do pretty much everything, but:

- You need a deep knowledge of the JVM and of its bytecode;
- Simple instrumentations are extremely complex as well as big instrumentations;
- The instrumentation process is error prone.

















