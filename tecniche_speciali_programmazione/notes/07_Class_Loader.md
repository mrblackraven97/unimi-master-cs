# Class Loading

A **class loader** is an object responsible of loading all the classes of a program. It's defined by the abstract class *ClassLoader* and has many subclasses (ex. *SecureClassLoader*, *URLClassLoader*, ...).

Compiled Java files are stored in *.class* files. The class loader is tasked to load those files in memory, allowing them to be available so that they can work on running application.

When using different Java tools, like Eclipse or Maven, it is important to note that the class loaders may be different to manage the environment differently from the JVM standard. It is also possible to create a specific user loader when we want to change the system libraries (ex. for changing the definition of *String* requires a specific loader).

Class loaders define **namespaces** with all classes loaded with the same loader. Every *Class* object contains a *getClassLoader()* method to get the referenced *ClassLoader* that defines it.

​			<Class A loaded by CL<sub>1</sub>> = <Class A loaded by CL<sub>2</sub>> <=> CL<sub>1</sub> = CL<sub>2</sub> 

 ## Example: A Broken Singleton

A class loader can break the singleton principle and make different singleton at the same time, by creating a new instance of the class in a new class loader (different namespace). The class gets rebuilt.

```java
public class Singleton {
	static private boolean runOnce = false;
	public Singleton() {
		if (runOnce)
			throw new IllegalStateException("[ERROR] re-instantiation of 							Singleton!!!");
		runOnce = true;
	}
}
```

```java
public class SingletonViolationTest {
	public static void main(String[] args) throws Exception {
		SimpleClassLoader CL1 = new SimpleClassLoader("testclasses");
		Class<?> c1 = CL1.loadClass("Singleton");
		println("Loaded class Singleton via the CL1 class loader");
		Field flag = c1.getDeclaredField("runOnce"); 
        flag.setAccessible(true);
		println("Let’s instatiate Singleton@CL1 runOnce :- " + flag.get(null));
		Object x = c1.getDeclaredConstructor().newInstance() ;
		println(" runOnce :- " + flag.get(null));
		try {
			println("Let’s re-instantiate Singleton@CL1 runOnce :- "+flag.get(null));
			Object y = c1.getDeclaredConstructor().newInstance() ;
			throw new RuntimeException("Test Fails!!!");
		} catch (Exception e) { 
            println(e.getCause().getMessage()); 
        }
        
		SimpleClassLoader CL2 = new SimpleClassLoader("testclasses");
		println("Loaded class Singleton via the CL2 class loader");
		Class<?> c2 = CL2.loadClass("Singleton");
		Field flag2 = c2.getDeclaredField("runOnce"); 
        flag2.setAccessible(true);
		println("Let’s instatiate Singleton@CL2 runOnce :- " + flag2.get(null));
		Object z = c2.getDeclaredConstructor().newInstance();
		println(" runOnce :- "+flag.get(null));
	}
}
```

## Creating a new Class Loader

Applications implement subclasses of *ClassLoader* in order to:

- Extend the manner in which the JVM dynamically loads classes;
- Obtain different protected namespaces;
- Transform Bytecode;

**Problem.** Write a method that outputs the inheritance hierarchy of an application.

**Subproblem**. Discover what classes belong to your application.

**Solution.** Impossible, the subproblem is extrinsic to Java meta-object protocol. A new class loader must be created, because it knows all classes that are loaded.

Working with a class loader requires the use of four main methods:

- *loadClass()*;
- *findLoadedClass()*;
- *findClass()*;
- *defineClass()*;

The *loadClass()* method calls *findLoadedClass()* to check if the class is already been loaded, if it is, it returns that class, otherwise it delegates the task to its parent (**Delegation Model**). If everything fails, the method *findClass()* is invoked to find the class reading the bytecode and creating the class object (*defineClass()*).

To create a specific new class loader a subclass of *ClassLoader* must be created:

- Create a default constructor and one linking to the parent;
- Override the *findClass()* method;
- Do **not** override the *loadClass()* method, its original implementation supports the delegation model;

```java
public class SimpleClassLoader extends ClassLoader {
	String[] directories;
	
    public SimpleClassLoader(String path) { 
        directories = path.split( ";" );
    }
	public SimpleClassLoader(String path, ClassLoader parent) {
		super(parent);
		directories = path.split(";"); 
    }
    
	public synchronized Class<?> findClass(String name) 
        	throws ClassNotFoundException {
		for (int i = 0; i < directories.length; i++) {
			byte[] buf = getClassData(directories[i], name);
			if (buf != null) 
                return defineClass(name,buf,0,buf.length);
		}
		throw new ClassNotFoundException();
	}
    protected byte[] getClassData(String directory, String fileName) {
		String classFile = directory + "/" + fileName.replace(’.’,’/’) + ".class";
		int classSize = (int)(new File(classFile)).length();
		byte[] buf = new byte[classSize];
		try {
			FileInputStream filein = new FileInputStream(classFile);
			classSize = filein.read(buf);
			filein.close();
		} catch(FileNotFoundException e) { 
            return null; 
        } catch(IOException e) { 
            return null; 
        }
		return buf;
    }
}
```

Instead of generating Java code, it works with bytecode:

```java
defineClass(String name, byte[] b, int off, int len)
```

The sub-array from `byte[off]` to `byte[off+len-1]` contains the bytecode for the class.

You can create from scratch the bytecode for a new class or modify the bytecode of an existent class (**bytecode injection**).

Extra notes for class loaders:

- At run-time, a class object is identified by the combination of the class name and the class loader object created by *ClassLoader.defineClass()*;
- All other classes referenced by a class are loaded by its defining loader;
- Class loaders delegate to other class loaders, especially the system class loader, which interprets the class path;

**N.B.** Launching an empty program loads more than 470 classes!

Types of class loaders:

- **Bootstrap Class Loader**: the built-in one, written in C and the base loader;
- **Platform Class Loader**: it extends *Bootstrap*, loading classes from jars in JRE;
- **System Class Loader**: it extends *Platform*, loading classes and jars specified by the *CLASSPATH*, can be customized.





























