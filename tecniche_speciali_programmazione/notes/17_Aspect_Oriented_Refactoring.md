# Aspect-Oriented Refactoring

**Refactoring** is the process and a set of techniques to reorganize code while preserving the external behavior.

AOP enables the encapsulation of crosscutting concerns by using a new unit of modularity (aspects).

Aspect-oriented refactoring (AOR)  synergistically combines these two techniques to refactor crosscutting elements:

- **Individually**, refactoring and AOP share the high-level goal of creating systems that are easier to understand and maintain without requiring huge up-front design effort;

- **Combined**, AOR helps in reorganizing code corresponding to crosscutting concerns to further improve modularization and get rid of the usual problem symptoms: *code-tangling* and *code-scattering*.

AOR goes beyond conventional refactoring techniques, offering substantial improvement in a variety of situations.

The conventional refactoring modularize code into a clean o-o implementation, the use of AOP squeezes out code that cannot be further refactored.

AOR grants implementation of the same functionality with fewer lines of code, the code is easy to understand, highly consistent and simple to change.

## Peculiarities of AOR

AOP principles still apply to AOR, but they acquire relevance or have a different perspective:

- **Approach towards crosscutting functionality**.
  - In AOP, the focus is on adding a new crosscutting functionality on existing applications;
  - In AOR, we design and prototype a conventional solution, then we can design and implement aspects to encapsulate such a functionality.
- **Applicability of aspect's crosscutting**.
  - In AOP, when implementing a crosscutting functionality using aspects, we restrict crosscutting to only selected parts of the system. Then we will increase the scope of the aspects;
  - In AOR, refactoring aspects deliberately limit crosscutting to typically just a few methods or a class, and sometimes a package.

- **Coupling consideration**.
  - In AOP, it is desirable to minimize coupling between aspects and classes;
  - In AOR, the coupling issue receives less emphasis. A refactoring aspect is *part of the target class' implementation* and therefore it may use intimate knowledge from the class.
- **Placement of aspects**.
  - In AOR, since the aspects may have to change with the implementation, it is desirable to put aspects closer to the target module.

## AOR Example: Extract Method

Let's consider the **extract method** from the refactoring point of view: it encapsulates (often duplicated) logic into a separate method, while leaving calls to the method in potentially multiple places.

Instead, with AOR technique of **extract method calls**, you can take an additional step and refactor out even those calls into a separate aspect and also refactor out any exception handling code into a separate aspect.

There are several concrete occasions for such refactoring, like class-level logging, security permission checks, persistence session management and so on.

**Problem.** The implementation has a duplicate piece of code in multiple places.

**Classical extract method refactoring**. 

- Encapsulates the duplicated logic in a new method;
- Replaces each original piece of code with a call to the new method.

**Problem.** Now the tangling code are the *duplicated calls*.

**Aspect-Oriented extract method refactoring**. 

- Encapsulates those method calls in an aspect;
- A pointcut captures all the join points where the method should be called;
- Weaves such join points with the calls to the refactored method.

*Example: Classical extract method refactoring*

```java
public class Account {
	private int _accountNumber;
	private float _balance;
	
    public Account(int accountNumber) {
        _accountNumber = accountNumber;
    }
	public int getAccountNumber() {
		AccessController.checkPermission(new BankingPermission("accountOperation"));
		return _accountNumber;
	}
	public void credit(float amount) {
		AccessController.checkPermission(new BankingPermission("accountOperation"));
		_balance = _balance + amount;
	}
	public void debit(float amount) throws InsufficientBalanceException {
		AccessController.checkPermission(new BankingPermission("accountOperation"));
		if (_balance < amount)
            throw new InsufficientBalanceException();
		else 
            _balance = _balance - amount;
	}
	public float getBalance() {
		AccessController.checkPermission(new BankingPermission("accountOperation"));
		return _balance;
	}
	public String toString() {
        return "Account: " + _accountNumber;
    }
}
```

We break this kind of refactoring process into **two required steps** followed by **two optional steps**. 

At the end of each step the behavior would *always* match that of the original code, and it will be impossible to run your unit tests to ensure that refactoring did not change any behavior.

### The AOR Process - Step 1

The first step **introduces a no-op refactoring aspect**. 

This step goal is to create the required infrastructure (static and dynamic crosscutting), but add no crosscutting functionality.

The first step can be broken in three sub-steps:

1. Insert an empty aspect;
2. Define a pointcut to capture join points that need the refactored functionality;
3. Create a no-op advice to the pointcut.

```java
private static aspect PermissionCheckAspect {
	private pointcut permissionCheckedExecution() :
		(execution(public int Account.getAccountNumber()) ||
		 execution(public void Account.credit(float)) ||
		 execution(public void Account.debit(float) throws 											   InsufficientBalanceException) ||
		 execution(public float Account.getBalance())) && within(Account);

    before() : permissionCheckedExecution() {}
}
```

### The AOR Process - Step 2

The second step **introduces the crosscutting functionality**.

We move code from the core classes into the aspect in three steps:

1. Introducing compile-time warnings (optional):

   ```java
   declare warning:
   	call(void AccessController.checkPermission(java.security.Permission))
   	&& within(Account) && !within(PermissionCheckAspect) :
   		"Do not call AccessController.checkPermission(..) from Account";
   ```

   This will issue warnings in case a programmer, unaware of the aspect, introduces the refactored method back into the class.

2. Add crosscutting functionality to advice:

   ```java
   before() : permissionCheckedExecution() {
   	AccessController.checkPermission(new BankingPermission("accountOperation"));
   }
   ```

3. Remove method calls from the advised methods.

*The new `Account` class*.

```java
public class Account {
	private int _accountNumber;
	private float _balance;

    public Account(int accountNumber) {
        _accountNumber = accountNumber;
    }
	public int getAccountNumber() {
		return _accountNumber;
	}
	public void credit(float amount) {
		_balance = _balance + amount;
	}
	public void debit(float amount) throws InsufficientBalanceException {
		if (_balance < amount) 
            throw new InsufficientBalanceException();
		else
            _balance = _balance - amount;
	}
	public float getBalance() {
		return _balance;
	}
	public String toString() {
        return "Account: " + _accountNumber;
    }
}
```

## The AOR Process - Step 3 (Optional)

The third step aims to **simplify pointcuts definition**.

We redefine the pointcut to make it shorter and make it semantically more meaningful.

The pointcut defined in step 1 will:

- Capture just the listed methods with the specified signatures;
- Not capture method with the same functionality but added later or with a different signature.

This step can't be automated, it requires:

- Comprehension of how functionality interact;
- Due diligence in the functionalities implementation.

```java
private pointcut permissionCheckedExecution() :
	(execution(public * Account.*(..)) &&
	 !execution(String Account.toString())) && within(Account);
```

## The AOR Process - Step 4 (Optional)

The forth step consists in **refactoring the refactoring aspect**.

We may refactor the refactoring aspect itself. Following the agile process wisdom, you will probably want to wait until you see some other class needing the same refactoring.

The process typically involves creating an abstract aspect and moving most of the functionality to it.

- The base aspect contains a few abstract pointcuts and methods;

- The concrete refactoring aspects for each refactored module extends this aspect and provides definition for pointcuts and implementation for the methods.

In a sense, such a refactoring is the AOP equivalent of *"extract superclass"* refactoring, called *"extract base aspect"*.

## The AOR Process- Final Result

```java
public class Account {
	private int _accountNumber;
	private float _balance;
	
    public Account(int accountNumber) {
        _accountNumber = accountNumber;
    }
	public int getAccountNumber() {
        return _accountNumber;
    }
	public void credit(float amount) {
        _balance = _balance + amount;
    }
	public void debit(float amount) throws InsufficientBalanceException {
		if (_balance < amount)
            throw new InsufficientBalanceException();
		else
            _balance = _balance - amount;
	}
	public float getBalance() {
        return _balance;
    }
	public String toString() {
        return "Account: " + _accountNumber;
    }
}
```

```java
private static aspect PermissionCheckAspect {
	declare warning:
		call(void AccessController.checkPermission(java.security.Permission)) &&
			within(Account) && !within(PermissionCheckAspect) :
				"Do not call AccessController.checkPermission(..) from Account";
	
    private pointcut permissionCheckedExecution() :
		(execution(public * Account.*(..)) &&
		 !execution(String Account.toString())) && within(Account);

    before() : permissionCheckedExecution() {
		AccessController.checkPermission(new BankingPermission("accountOperation"));
	}
}
```

## Extract Exception Handling

Exception handling is a crosscutting concern that affects most nontrivial classes.

Due to its structure (try/catch blocks), conventional refactoring cannot perform further extraction of common code.

Each class (or a set of classes) may have its own way to handle exceptions encountered during execution of its logic.

With AOR, you can extract exception handling code in a separate aspect. It doesn't exist a corresponding conventional refactoring technique.

*Example: Code with lots of exceptions*

```java
public class LibraryDelegate {
	private LibrarySession _session;
    
	public LibraryDelegate() throws LibraryException {
        init();
    }
	private void init() throws LibraryException {
		try {
			LibrarySessionHome home = 																(LibrarySessionHome)ServiceLocator.getInstance().
			getRemoteHome("Library", LibrarySessionHome.class);
			_session = home.create();
		} catch (ServiceLocatorException ex) {
            throw new LibraryException(ex);
        }
		catch (CreateException ex) {
            throw new LibraryException(ex);
        }
		catch (RemoteException ex) {
            throw new LibraryException(ex);
        }
	}
	public LibraryTO getLibraryDetails() throws LibraryException {
		try {
			return _session.getLibraryDetails();
		} catch (RemoteException ex) {
            throw new LibraryException(ex);
        }
	}
	public void setLibraryDetails(LibraryTO to) throws LibraryException {
		try {
            _session.setLibraryDetails(to);
		} catch (RemoteException ex) {
            throw new LibraryException(ex);
        }
	}
	public void addBook(BookTO book) throws LibraryException {
		try {
            _session.addBook(book);
		} catch (RemoteException ex) {
            throw new LibraryException(ex);
        }
	}
	public void removeBook(BookTO book) throws LibraryException {
		try {
            _session. removeBook(book);
		} catch (RemoteException ex) {
            throw new LibraryException(ex);
        }
	}
}
```

Let's create an aspect that refactors out all the exception handling logic:

```java
aspect LibraryExceptionHandling {
	declare soft : RemoteException :
		call(* *.*(..) throws RemoteException) && within(LibraryDelegate);
	declare soft : ServiceLocatorException :
		call(* *.*(..) throws ServiceLocatorException) && within(LibraryDelegate);
	declare soft : CreateException :
		call(* *.*(..) throws CreateException) && within(LibraryDelegate);

    after() throwing(SoftException ex) throws LibraryException :
		execution(* LibraryDelegate.*(..) throws LibraryException)
		&& within(LibraryDelegate) {
			throw new LibraryException(ex.getWrappedThrowable());
	}
}
```

Each **`declare soft`** statement causes any exception of the specified type thrown during a call matching the specified pointcut to be treated as a run-time exception. The advantage is that run-time exceptions do not require to be declared and trapped.

*Example: New code with extracted exceptions*

```java
public class LibraryDelegate {
	private LibrarySession _session;
	
    public LibraryDelegate() throws LibraryException {
        init();
    }
	private void init() throws LibraryException {
		LibrarySessionHome home
			= (LibrarySessionHome)ServiceLocator.getInstance().
		getRemoteHome("Library", LibrarySessionHome.class);
		_session = home.create();
	}
	public LibraryTO getLibraryDetails() throws LibraryException {
		return _session.getLibraryDetails();
	}
	public void setLibraryDetails(LibraryTO to) throws LibraryException {
		_session.setLibraryDetails(to);
	}
	public void addBook(BookTO book) throws LibraryException {
		_session.addBook(book);
	}
	public void removeBook(BookTO book) throws LibraryException {
		_session. removeBook(book);
	}
}
```

## Extract Concurrency Control

**Concurrency control** is often a critically important, but equally hard to implement, crosscutting concern.

- Concurrency control implementation is intrinsically scattered over several methods;
- There are a few concurrency control patterns available, even if the concepts are reusable, their implementation aren't;
- AOP offers reusable implementations of those patterns, thus taking some pain out of concurrency control implementation.

Let's consider a program that uses the read-write lock pattern:

- The concurrency pattern requires *managing two kinds of lock*: read lock and write lock;
- Upon entering each method that *only reads* data, the read lock is acquired and that lock is released before leaving the method;
- The *write lock* is acquired and released in a similar manner for each method that modifies data.

*Example: Code with concurrency control*

```java
public class Account {
	private ReadWriteLock _lock = new ReentrantWriterPreferenceReadWriteLock();
	
    // ... constructors etc.
	public void credit(float amount) {
		try {
			_lock.writeLock().acquire();
			// ... business logic for credit operation ...
		} catch (InterruptedException ex) {
            throw new InterruptedRuntimeException(ex);
		} finally {
            _lock.writeLock().release();
        }
	}
	public void debit(float amount) throws InsufficientBalanceException {
		try {
			_lock.writeLock().acquire();
			// ... business logic for debit operation ...
		} catch (InterruptedException ex) { 
            throw new InterruptedRuntimeException(ex);
		} finally {
            _lock.writeLock().release();
        }
	}
	public float getBalance() {
		try {
			_lock.readLock().acquire();
		}
		// ... business logic for getting the current balance ...
		} catch (InterruptedException ex) {
        	throw new InterruptedRuntimeException(ex);
		} finally {
        	_lock.readLock().release();
    }
	public void setBalance(float balance) {
		try {
			_lock.writeLock().acquire();
			// ... business logic for setting the current balance ...
		} catch (InterruptedException ex) {
            throw new InterruptedRuntimeException(ex);
		} finally {
            _lock.writeLock().release();
        }
	}
	public String toString() {
		try {
			_lock.readLock().acquire();
		// ... form the description string ...
        } catch (InterruptedException ex) {
            throw new InterruptedRuntimeException(ex);
		} finally { _lock.readLock().release();
 	}
}
```

The solution uses an abstract (and reusable) aspect that declares two abstract pointcuts `readOperations()` and `writeOperations()` which advices them to manage read and write lock.

```java
public abstract aspect ReadWriteLockSynchronizationAspect
		perthis(readOperations() || writeOperations()) {
	
    public abstract pointcut readOperations();
	public abstract pointcut writeOperations();

    private ReadWriteLock _lock = new ReentrantWriterPreferenceReadWriteLock();

    before() : readOperations() {
        _lock.readLock().acquire();
    }
	after() : readOperations() {
        _lock.readLock().release();
    }
	before() : writeOperations() {
        _lock.writeLock().acquire();
    }
	after() : writeOperations() {
        _lock.writeLock().release();
    }
	after() throwing(SoftException ex) throws InterruptedRuntimeException :
			readOperations() || writeOperations() {
		throw new InterruptedRuntimeException(ex);
	}
}

aspect SoftenInterruptedException {
	declare soft : InterruptedException :
		call(void Sync.acquire()) && within(ReadWriteLockSynchronizationAspect);
}
```

The concurrency control aspect is applied to the `Account` class in a concrete sub-aspect of `ReadWriteLockSynchronizationAspect`, that

- Provides a definition for the abstract pointcuts: `readOperations()` and `writeOperations()`;
- Defines the read operations as all methods with their name starting in `get` as well as the `toString()` method;
- Consider every other method as a write operation.

```java
static aspect ConcurrencyControlAspect extends ReadWriteLockSynchronizationAspect {
	public pointcut readOperations() :
		(execution(* Account.get*(..)) || execution(* Account.toString(..)))
		 && within(Account);

    public pointcut writeOperations() :
		(execution(* Account.*(..)) && !readOperations()) && within(Account);
}
```

*Example: Cleaned `Account` class*

```java
public abstract class Account {
	// ... no _lock variable here (compared to not refactored listing)
	// ... constructors etc.
	public void credit(float amount) {
		// ... business logic for credit operation ...
	}
	public void debit(float amount) throws InsufficientBalanceException {
		// ... business logic for debit operation ...
	}
	public float getBalance() {
		// ... business logic for setting the current balance ...
	}
	public void setBalance(float balance) {
		// ... business logic for getting the current balance ...
	}
} 
```

## Extract Interface Implementation

The refactoring technique of **extract interface** enables an improved decoupling of clients from implementations.

If more than one class implements that interface, you may end up duplicating code required to implement the interface.

In AOR, you can take the idea further and avoid any duplication.

Let's consider the `ServiceCenter` interface.

```java
public interface ServiceCenter {
	public String getId();
	public void setId(String id);
	public String getAddress();
	public void setAddress(String address);
}
```

The `ATM` class implements the `ServiceCenter` interface.

```java
public class ATM extends Teller implements ServiceCenter {
	private String _id;
	private String _address;
	
    public String getId() {
        return _id;
    }
	public void setId(String id) {
        _id = id;
    }
	public String getAddress() {
        return _address;
    }
	public void setAddress(String address) {
        _address = address;
    }
	//...
}
```

Other classes, as well as the `ATM` class, could have similar implementations of the `ServiceCenter` interface, ending up in repeating the code to implement the interface. 

Duplicated code could be avoided by creating the default implementation of the interface and make the data classes extend the default implementation, but we do not have multiple inheritance.

With AOR, we introduce the default implementation into the `ServiceCenter` interface using an aspect.

```java
static abstract aspect implementation {
	private String ServiceCenter._id;
	private String ServiceCenter._address;
	
    public String ServiceCenter.getId() {
        return _id;
    }
	public void ServiceCenter.setId(String id) {
        _id = id;
    }
	public String ServiceCenter.getAddress() {
        return _address;
    }
	public void ServiceCenter.setAddress(String address) {
        _address = address;
    }
}
```

The aspect introduces data members as well as methods with the default implementation of the `ServiceCenter` interface.

By weaving this aspect into the interface, any class that implements the interface automatically inherits the default implementation.

Benefits:

- The implementing classes no longer include boilerplate code to implement interfaces;
- The implementing classes can still override the default implementation provided by the aspects.

### Variations 

The implementing classes could choose whether to inherit the default implementation provided by the aspect or not by creating `ServiceCenterDefaultAspectImpl`, a sub-interface of the original interface and let the aspect introduce the default implementation to this interface.

The implementing classes will inherit the default implementation only if they declare themselves to implement such a sub-interface.

## Static Crosscutting to Flag Mismatched Join Points

We create a temporary pointcut that captures a larger sets of join points according to their semantics, we could capture all public methods of the `Account` class.

```java
pointcut tmp() : execution(public * Account.*(..)) && within(Account);
```

Now we will use a construct that produces errors if two pointcuts do not match exactly the same set of join points. Tis is feasible when both pointcuts are statically determinable.

```java
declare error:
	(!permissionCheckedExecution() && tmp()) || (permissionCheckedExecution() && 				!tmp()) :
		"Mismatch in join points captured";
```

Any error issued by the compiler implies that the two pointcuts aren't equivalent.

We will get an error since `tmp()` captures the `toString()` method, but `permissionCheckedExecution()` does not.

## Use of `around()` to Remove the Factored Calls

In a larger system, you might want to write an around advice to make the factored method calls a no-op and test the result before actually removing the code.

You can introduce the following advice to nullify calls to the `AccessController.checkPermission()` method from the `Account` class.

```java
void around() :
	call(void AccessController.checkPermission(java.security.Permission)) &&
			within(Account) && !within(PermissionCheckAspect) {
		// do NOT call proceed()
}
```

Once you successfully complete the testing, you may remove the advice.

## Conclusions

Many other refactoring techniques can benefit of aspect- orientation, like:

- **Replace/Override with advice**, when it is required to argument additional common behavior to many methods of a class.
  - Create a subclass and override methods to perform some additional logic;
  - Use an aspect to advise the needed method with additional logic.
- **Extract Lazy Initialization**, when expensive resources are often use, and we want to optimize their use.
  - Check for uninitialized resources in every place that uses those resources in every place that uses those resources and causes code-scattering and code-tangling;
  - Use of getter methods helps but do not prevent from direct access to the resource;
  - Advise read access to the resources using a `get()` pointcut to initialize it.































