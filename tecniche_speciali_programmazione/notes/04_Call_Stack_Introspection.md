# Call Stack Introspection

It is possible to use introspection not only on classes, but also on the application structure, in particular the execution state and the call stack, which allows us to control the execution flow of the program.

The call stack introspection allows a thread to examine its context (snapshot).

The key that let us do introspection are the exceptions: when an instance of *Throwable* is created, the call stack is saved as an array of *StackTraceElement*, so when we create an instance of *Throwable* we gain access to a representation of the call stack. The *getStackTrace()* method is used to return the current call stack (as an array of *StackTraceElement*).

Every **Frame** (snapshot) let us gain information about through the following methods:

- *getFileName()*
- *getLineNumber()*
- *getClassName()*
- *getMethodName()*

**N.B.** An exception needs only to be created, not raised.

## Logger

It is possible to add a logging facility to our applications to trace the program execution with the interface

```Java
public interface Logger {
	void logRecord(String msg, int type);
	void logProblem(Throwable prob);
}
```

Implemented as

```Java
public void logRecord(String message, int logRecordType) {
	StackTraceElement f = new Throwable().getStackTrace()[1];
	String callerClassName = f.getClassName();
	String callerMethodName = f.getMethodName();
	int callerLineNumber = f.getLineNumber();
	// write of log record goes here.
}
```

## Invariant Checking

An **invariant** is a property of the code that must hold for the whole instance's life cycle (contractual programming).

Example: we have a class *VisiblePoint*, instances of this class are legit only when their coordinates are within the display limits, and those limits and the way to check them are defined by a *Visible* interface.

```Java
public interface Visible {
	public final int XMIN = -1080;
	public final int XMAX = 1080;
	public final int YMIN = -1920;
	public final int YMAX = 1920;
	
    default boolean isvisiblex(int x) { 
        return (x>= XMIN && x <= XMAX);
    }
	default boolean isvisibley(int y) {
        return (y>= YMIN && y <= YMAX);
    }
}

public class VisiblePoint implements Visible {
	private int x,y;
    
	public VisiblePoint(int x, int y) {
		this.x = x;
		this.y = y;
		assert isvisiblex(this.x) && isvisibley(this.y):
			"x or y coordinates outside the display margin";
	}
	public int getX() { 
		return this.x; 
	}
	public int getY() { 
		return this.y; 
	}
	public void setX(int x) { 
		this.x=x; 
	}
	public void setY(int y) { 
		this.y=y; 
	}
}
```

The class to be checked must implement the interface **`InvariantSupporter`**, that gives the method **boolean `invariant()`** where the invariant check will be insert. Method `invariant()` must be invoked at the begin/end of each method, through `InvariantChecker.checkInvariant(this)`

```Java
public class VisiblePoint implements Visible, InvariantSupporter {
...
	public boolean invariant() { 
		return isvisiblex(getX()) && isvisibley(getY());
	}
	
	public int getX() {
		InvariantChecker.checkInvariant(this); 
		int result = this.x;
		InvariantChecker.checkInvariant(this);
		return result ;
	}

	public void setX(int x) {
		InvariantChecker.checkInvariant(this); 
		this.x=x;
		InvariantChecker.checkInvariant(this);
	}

...
```

**Problem**: `invariant()` uses a method of `VisiblePoint` that is checked as well. This creates an infinite loop of invariant checking.

**Solution**: Inspecting the call stack before invoking `invariant()` looking for a loop

```Java
public class InvariantChecker {
	public static void checkInvariant(InvariantSupporter obj) {
		StackTraceElement[] ste = new Throwable().getStackTrace();
		for (int i = 1; i < ste.length; i++) {
			if (ste[i].getClassName().equals("InvariantChecker") &&
				ste[i].getMethodName().equals("checkInvariant")) 
				return;
		}
		if (!obj.invariant()) {
			throw new IllegalStateException("invariant failure");
		}
}
```
**Problem**: Accessibility permissions are all allowed or negated.

**Solution**: Call stack introspection when the permissions should be enabled.


