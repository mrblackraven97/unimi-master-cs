# OpenJava

Up to now some hints about how to use Java's MOP have been provided:

- A set of functional extensions;
- A set of reusable class-to-class transformations;
- Examples of large grain use for adaptable applications.

Clearly reflection in Java is not as powerful as it could be, because it is missing the appropriate intercessional features.

Some restrictions could be overcame by code generation and dynamic compilation, but it is tricky and error-prone.

## Non Standard MOPs

Java can support different kind of MOPs not standard:

- **Compile Time**:
  - To reflect on language constructs;
  - Based on source to source transformations;
  - Per class basis changes;
  - Examples: **OpenJava**, Reflective Java.

- **Load Time**:
  - To reflect on the bytecode;
  - Based on the specialization of the class loader;
  - The source code is not necessary;
  - Per class basis changes;
  - Examples: **BCEL**, ASM, **Javassist**.

- **Run Time (VM-based)**:
  - Modification of the VM to intercept events;
  - Lose of portability;
  - Could be per object basis;
  - Examples: MetaXa, Iguana/J

- **Run Time (Proxy-based)**:
  - Transparent generation of interceptors;
  - Can be based on source or bytecode generation/modification;
  - Can be per object basis;
  - Examples: Dalang/Kava, mChaRM.

## What is OpenJava?

OpenJava is a reflective language derived from Java.

It enables the programmer to extend a program both semantically and syntactically and can be used to write both the base-level and the meta-level program.

The reflective activity occurs at compile-time.

**N.B.** If a meta-level program is not defined, an OpenJava program behaves exactly as the corresponding Java program.

When an OpenJava program is compiled, the extensions are directly passed to the compiler as meta-objects:

- The meta-objects say to the compiler how to translate the program's components;
- Before the program's bytecode is built, the compiler translates it into pure Java thanks to the meta-objects' directives.

Before compiling the source code, the compiler invokes the method `translate()` of the meta-object at the root of the program abstract syntax tree (AST). That method recursively walks the AST translating the code (type-driven visit).

Language extensions are implemented by redefining the `translate()` method in the corresponding class.

OpenJava works on a per-class basis, associating to every class another class (the meta-object) that governs the translation process.

The meta-objects represent the program in execution in the meta-level.

## Programming with OpenJava

Reflective programming in OpenJava mainly takes three steps:

1. Decide how the base-level program has to look after the translation;
2. Detect which part of the base-level code will be involved in the translation and decide which ancillary code is necessary during the translation;
3. Write a meta-object that will carry out the designed translation.

**N.B.** The translation is carried out at compile-time, the reflective API is very simple and looks like the one provided by `java.lang.reflect`.

### Example: Verbose Execution

```java
public class Hello instantiates VerboseClass {
	public static void main( String[] args ) {
		hello();
	}
	static void hello() {
		System.out.println( "Hello, world." );
	}
}
```

The base-level code is mainly pure Java code. The statement `instantiates VerboseClass` says to the compiler that the class `Hello` has a meta-object instance of the class `VerboseClass`.

The class `VerboseClass` describes how the class `Hello` has to be translated.

In the example, we want to print a message at every method call, so we can imagine that the code, after the translation, should look like this:

```java
public class Hello {
	public static void main( String[] args ) {
		System.out.println( "main is called." );
		hello();
	}
	static void hello() {
		System.out.println( "hello is called." );
		System.out.println( "Hello, world." );
	}
}
```

Figured out how the code should look, we have to write a meta-object that can do that, our `VerboseClass`:

```java
import openjava.mop.*;
import openjava.ptree.*;

public class VerboseClass instantiates Metaclass extends OJClass {
    @Override
	public void translateDefinition() throws MOPException {
		OJMethod[] methods = getDeclaredMethods();
		for (int i = 0; i < methods.length; ++i) {
			Statement printer = makeStatement(
                "System.out.println( " + methods[i] + "\" is called.\" );"
			);
			methods[i].getBody().insertElementAt( printer, 0 );
		}
	}
}
```

To enable such a translation, the class `VerboseClass` has to:

- Extend the class `openjava.mop.OJClass`;
- Override the method `translateDefinition()` that translates the body of the methods;
- Retrieve the methods declared in the base-level class with a call to the `getDeclaredMethods()`;
- Allow to build a statement from a string on-the-fly with the method `makeStatement()`;
- Return a list of statements representing the body of the method through `getBody()`. 

