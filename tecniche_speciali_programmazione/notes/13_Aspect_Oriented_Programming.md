# Aspect Oriented Programming

Programs with good modularity have codes relevant to a specific task confined into a single specific class.

There are cases (like loggers) where the code is not just in one place, or a small set of places, but spread among all classes. Logging is **not modularized**.

Redundant code is the same fragment of code repeated in many places of the application.

- Difficult to reason about:
  -  Non-explicit structure;
  - The big picture of the tangling isn't clear.
- Difficult to maintain:
  - Having to find all the code involved;
  - Being sure to change it constantly;
  - Being sure not to break it by accident.

The AOP idea comes to help us in this situations, with the idea of **crosscutting**, which is inherent in complex systems.

Crosscutting concerns have a clear purpose and a natural structure (defined set of methods, module boundary crossings, points of resource utilization, lines of dataflow).

## Aspects

We explicitly capture the structure of crosscutting concerns in a modular way with linguistic and tools support using **Aspects**.

Aspects are well modularized crosscutting concerns, they are:

- Concerns that crosscut (design level);
- A programming construct that enables crosscutting concerns to be captured in modular units (implementation level).

AOP improves the modularity of the crosscutting concerns.

**AspectJ** is an aspect-oriented extension to Java that supports general purpose aspect-oriented programming.

We will face the problem from two different perspectives:

- **Problem Analysis**: Crosscutting in the design and how to use AspectJ to capture that.
- **AspectJ Language**: Language mechanisms, like crosscutting in the code and mechanisms that AspectJ provides.