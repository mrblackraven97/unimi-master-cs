# Computational Reflection

The Reflection is an activity processed by a software system to represent and manipulate his own structure and behavior.

Reflection became the starting point from which object orientation were born.

We define:

- A **computational system** is a system that can reason about a specific set of data (a program).
- A computational system is **causally connected** at his own domain if and only if a change on his domain reflect on his system and vice versa (debugger).
- A **meta-system** is a computational system which applicative domain is another computational system (debugger).
- The **reflection** is the property of reasoning and acting about themselves.

After that we can define that a **reflective system** is a meta-system casually connected to itself.

A system is divided in:

- A **base-level**
- One or more **meta-levels**

The system that works on a given meta-level manipulates the system on the lower level and each level knows nothing about the upper ones. This structure is known as **reflective tower**.

A reflective system is classified according to what it can do and when.

What:

- Structural/Behavioral reflection
- Introspection/Intercession

When:

- Compile-time
- Load-time
- Run-time

The **behavioral reflection** allows the program manipulate its own computation (trap a method, monitor and object state, create new objects,...). This activity happens most of the time at run-time.

The **structural reflection** allows the program to alter its own structure (class code modified/removed, creation of method/fields,...). This activity take place at run-time and it needs a specific support from an execution environment (VM, RTE,...).

## Reification

Base-level entities (*referents*) are reified into the meta-level, so they have a representative into the meta-level: the **reification**.

Reification is an encoded data-structure of the lower level (not at compile-time), it has to act like the corresponding referent, and needs to be kept consistent to the referent, because it is subjected to manipulations by the meta-level entities. This way, the base-level entities, are protected from potential inconsistencies. Any change carried out on the reification has to be reflected on the corresponding referent.

*Shadows*: Portions of code that weren't part of the original code that were added to control the flow of execution at run-time.

*Critical issues*: Modifying the code is a very sensitive operation, for example deleting a method in the moment in which it is running it's an obvious problem. Modifying a class makes it impossible to use objects created in a previous version.

Reflective activities take place at:

- **Run-time**: with explicit causal connection that must be kept consistent (code + exec).
- **Compile-time**: with implicit causal connection, there is no reification, base and meta-levels are fused together.
- **Load-time**: where things have already been loaded, i can interact with what i have in memory. Behaves mostly like at compile-time.

The execution shifts between meta-levels, and the switch is realized with two actions:

- *Shift-up*: the computational flow passes into the meta-level (an element changes or an action is going to be done). Usually managed by call-backs.
- *Shift-down*: the computational flow goes back from the meta-level, decided by the program.

The objects running in the meta-level (*meta-objects*) are associated to the objects running in the base-level (*referents*).

The connection among referents and meta-objects is called:

- **Causal connection** when it is a two-way link
- **Meta connection** when it is a one-way link

The **Meta-Object Protocol** (MOP) is the set of messages that a meta-object can understand.

An example of computational reflection is adding meta data to an applications is adding a `doLog()` call to save data, modifying the flow of execution of the program.

## Refining our approach

In a **meta-class approach**, classes carry out the reflective activity and the reflective tower is realized by the inheritance link:

- All the instances of a class share the same meta-class and have the same behavior (granularity at class level). 
- Classes have to be available at run-time.

With a **meta-object** approach, we have some special objects instantiated by a special class that are associated to the base-level objects:

- The reflective tower is realized by clientship.
- Granularity of reflection at the object level.
- It cannot manage communication between objects (lack of global view).

To manage communication, we use some special objects that reify the messages exchanged among the base-level objects (the special objects deal with the reflective computation), so that:

- Granularity is at the level of method calls (flexible). 
- We can reflect on the whole message exchange. 
- Meta-entities proliferation. 
- The life-cycle of the meta-entities is strictly tied to the life-cycle of the message exchange (lost the history of the reflective computation).

## Conclusions

The computational reflection:

- Allows to open up a system to postpone some decisions (late-binding mechanism)
- It depends on the awareness that a system has on itself (the "self" of object-oriented programming) 
- It specializes some of the object-oriented basic mechanism (constructor invocations, and so on).
- Its use produces a better comprehension of the object-oriented mechanism and of their implementation.
The reflective systems are composed by two or more layers called:

- **base layer**: the base layer is the deeper layer of the reflective tower and it contains the running code unaware of the reflection
- **meta layers**: is one or more layers and contains different structures for the reflective operations.

The connection between meta-levels and base-level is called **causal connection**.
