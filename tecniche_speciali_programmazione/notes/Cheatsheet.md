# TSP CHEATSHEET: PART 1

## Class\<T\>

- forName(String className)
- newInstance()
- isInstance(Object obj)
- getName()
- getSuperclass()
- getModules()
- getInterfaces()
- getDeclaredClasses()
- getDeclaredMethods()
- getEnclosingConstructor()
- getFIelds()
- getComponentType()

## Method

- getReturnType()
- getParameterTypes()
- getExceptionTypes()
- invoke(Object obj, Object ... args)

## Field

- getType()
- get(Object obj)
- set(Object obj, Object value)
- getDeclaringClass()

## AccessibleObject

- setAccessible(boolean flag)
- isAccessible()

## Constructor

- newInstance(Object args)

## Call Stack Introspection

```java
StackTraceElement f = new Throwable().getStackTrace()[1];
String callerFileName = f.getFileName();
String callerClassName = f.getClassName();
String callerMethodName = f.getMethodName();
int callerLineNumber = f.getLineNumber();
```

## InvariantChecking

```java
public class VisiblePoint implements InvariantSupporter {
    ...
    public boolean invariant() {...}
    
    public void foo() {
        InvariantChecker.checkInvariant(this);
        ...
        InvariantChecker.checkInvariant(this);
    }
}
```

```java
public class InvariantChecker {
	public static void checkInvariant(InvariantSupporter obj) {
		StackTraceElement[] ste = (new Throwable()).getStackTrace();
		for (int i = 1 ; i<ste.length; i++)
			if (ste[i].getClassName().equals("InvariantChecker") &&
				ste[i].getMethodName().equals("checkInvariant") ) return ;
		if ( !obj.invariant() )
			throw new IllegalStateException("invariant failure");
    }
}
```

## Annotations

```java
@Retention(RetentionPolicy.RUNTIME)		// Meta-Annotation
public @interface GroupTODO {
	public enum Severity {CRITICAL, IMPORTANT, TRIVIAL, DOCUMENTATION};
	Severity severity() default Severity.IMPORTANT;
	String item();
}

@GroupTODO(
	severity = GroupTODO.Severity.CRITICAL,
	item = "item",
)
public void foo() {
	...
}
```

```java
Class cls = MyClass.class;
boolean todo = cls.isAnnotationPresent(GroupTODO.class);
GroupTODO groupTodo = element.getAnnotation(GroupTODO.class);
String assignedTo = groupTodo.item();
```

## Proxy

```java
public class MyHandler implements InvocationHandler {
    ...
    public Object invoke(Object proxy, Method m, Object[] args) {
        ...
    }
```

```java
public class MyClass implements IMyClass {...}
```

```java

MyClass mc = new MyClass();
IMyClass myProxy = (IMyClass) Proxy.newProxyInstance(
                mc.getClass().getClassLoader(), mc.getClass().getInterfaces(), new 					myHandler(mc));
myProxy.hello();	// Calling a method of MyClass
```

## Class Loader

```java
SimpleClassLoader CL1 = new SimpleClassLoader("testclasses");
Class<?> c1 = CL1.loadClass("Singleton");
```

- loadClass(name, resolve)
  - findLoadedClass(String)       // check if class already loaded
  - loadClass(String)       // called on parent 
  - findClass(String)       // finding actual class
- defineClass(name, byteArray, offset, lenght)
- findClass(name)       // invoked if the parent coulnt find the class

```java
public class SimpleClassLoader extends ClassLoader {
    ...
    public SimpleClassLoader(String path, ClassLoader parent) {
		super(parent);
		...
    }
    public synchronized Class<?> findClass(String n) throws ClassNotFoundException {
        ...
    }  
    protected byte[] getClassData( String directory, String fileName ) {...}
}
```

## Modules

```java
module tsp.module.employee {
    opens tsp.module.employee to tsp.module.reflection ;
	requires tsp.module.reflection ;
	exports tsp.module.employee ;
}
```

## VarHandle

```java
// Given lookup as Lookup, name as String and v as Object
Class<?> cls = this.getClass();
Field f = cls.getDeclaredField(name);
MethodHandles.Lookup privateLookup = MethodHandles.privateLookupIn(cls, lookup);
VarHandle handle = privateLookup.unreflectVarHandle(f);
handle.get(this); // handle.set(this, v)
```

```java
VarHandle privateIntHandle = MethodHandles
  .privateLookupIn(VariableHandlesTest.class, MethodHandles.lookup())
  .findVarHandle(VariableHandlesTest.class, "privateTestVariable", int.class);

VarHandle publicIntHandle = MethodHandles
  .in(VariableHandlesTest.class)
  .findVarHandle(VariableHandlesTest.class, "publicTestVariable", int.class);
```

