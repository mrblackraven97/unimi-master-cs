package Es4;

public class RecognizingElements {
	@CFG(
			name=" main method of RecognizingElements class ",
			dependency = {"Es4.Recogn"}
	)
	public static void main(String[] args) {
		Recogn r=new Recogn();
		r.rec();
	}
}
