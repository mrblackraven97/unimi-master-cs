package Es4;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class Recogn {
    @CFG(
            name=" rec method of RecognizingElements class ",
            dependency = {
                    "Es4.GetElem_Methods",
                    "java.util.ArrayList",
                    "java.lang.reflect.Method",
                    "java.lang.reflect.Field"}
    )
    public void rec(){
        try {
            GetElem_Methods g = new GetElem_Methods();
            String nomi[] = {"Es4.Prova","Es4.GetElem_Methods"};
            String metodi_elem[] = g.get(nomi).toArray(new String[0]);
            ArrayList<String> cont = new ArrayList<String>();
            for (int i=0; i<nomi.length; i++) {
                Class<?> c = Class.forName(nomi[i]);
                Method m[]= c.getDeclaredMethods();
                Field f[]=c.getDeclaredFields();
                for (int j=0; j<metodi_elem.length; j++) {
                    for (int z=0; z<m.length; z++) {
                        if (metodi_elem[j].equals(m[z].toString())) {
                            cont.add(metodi_elem[j]);
                            System.out.println("Metodo "+m[z].getName()+" dichiarato nella classe "+c);
                            System.out.println("Signature : "+m[z].toString());
                        }
                    }
                    for (int z=0; z<f.length; z++) {
                        if (metodi_elem[j].equals(f[z].toString())) {
                            cont.add(metodi_elem[j]);
                            System.out.println("Campo "+f[z].getName()+" dichiarato nella classe "+c);
                            System.out.println("Signature : "+f[z].toString());
                        }
                    }
                }
            }
            for (int i=0; i<metodi_elem.length; i++) {
                if (!cont.contains(metodi_elem[i])) {
                    System.out.println("Non tutti gli elementi del secondo array sono descritti");
                    return;
                }
            }
            System.out.println("Tutti gli elementi del secondo array sono dichiarati");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
