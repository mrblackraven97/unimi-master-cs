import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Arrays;

public class DumpMethods {
    public static void main(String[] args){
        try{
           Class temp = Class.forName(args[0]);
            Method[] methods = temp.getMethods();
            System.out.println(Arrays.toString(methods));
        }catch (IndexOutOfBoundsException e ){
            System.out.println("parametro non presente");
        }
        catch (ClassNotFoundException e ){
            System.out.println("parametro errato" );
        }
    }

}
