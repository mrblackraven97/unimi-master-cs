import java.lang.reflect.Field;

public class StateAnalyzer {

    public static Field[] getFields(Object instance) {
        Class<?> mclass = instance.getClass();
        return mclass.getDeclaredFields();
    }

    public static String getFieldValueAsString(Field field, Object instance) {
        StringBuilder sb = new StringBuilder();
        try {
            var value = field.get(instance);
            if (value.getClass().isArray())
                for (Object obj: (Object[])value) sb.append(obj.toString() + " ");
            else sb.append(value);
        } catch (IllegalAccessException e) {
            sb.append("ERROR: unable to get field value");
            // e.printStackTrace();
        }
        return sb.toString();
    }

    public static String getFieldState(Field field, Object instance) {
        field.setAccessible(true);
        StringBuilder sb = new StringBuilder();
        sb.append(field.getName());
        sb.append(": ");
        sb.append(getFieldValueAsString(field, instance));
        return sb.toString();
    }

    public static void changeField(Field field, Object instance, Object newValue) {
        field.setAccessible(true);
        try {
            field.set(instance, newValue);
        } catch (IllegalAccessException e) {
            System.err.println("Unable to set field");
        }
    }

    public static void main (String[] args) {
        TestingFields testObj = new TestingFields(7, 3.14);
        for (Field f: getFields(testObj)) System.out.println(getFieldState(f, testObj));

        for (Field f: getFields(testObj)) {
            if (f.getName().toString() == "s") changeField(f, testObj, getFieldValueAsString(f, testObj)+" passed!!!");
        }
        for (Field f: getFields(testObj)) System.out.println(getFieldState(f, testObj));
    }
}
