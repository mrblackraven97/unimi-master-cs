# TSP

## Lab 1; Exercise 1: Introspection

In order to discover how introspection works in Java, let's write the class `DumpMethods` whose `main()` method gets class name from the command line through args and outputs (prints) all the methods that can be called on an instance of the passed class. **Note** that, we are looking for the public interface of the class not for its declared methods.

### Build

```bash
make
```

### Run

```bash
java DumpMethods CLASSNAME
```

#### Examples

```bash
java DumpMethods String
java DumpMethods java.lang.String
java DumpMethods java.util.Date
java DumpMethods DumpMethods
```
