import java.lang.reflect.Type;

public interface TypeEvaluator {
    public Class<?> evaluate(String input);
}
