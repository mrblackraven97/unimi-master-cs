# TSP

## Lab 1; Exercise 2: Intercession

Let's write the class `InvokeUnknownMethod` that invoked a method whose identity is unknown at compile-time through reflection. Let's suppose that our program would like to invoke one of the methods of the class:

```java
public class Calculator {
    public int add(int a, int b) { return a + b; }
    public int mul(int a, int b) { return a * b; }
    public double div(double a, double b) { return a / b; }
    public double neg(double a) { return -a; }
}
```

without knowing which one until the main is executed; i.e., the method name, the number and type of its arguments should be inferred from the input. An example of activation will be:

```java
java InvokeUnknownMethod Calculator add 7 25
```

**Hints**, let's use the class Pattern to infer the type of the arguments.

### Build

```bash
make
```

### Run

```bash
java InvokeUnknownMethod CLASSNAME METHODNAME [ARGUMENT]...
```

#### Examples

```bash
java InvokeUnknownMethod Calculator mul 3 6
java InvokeUnknownMethod Calculator div 6.0 3.0
java InvokeUnknownMethod java.lang.StringBuilder append "ciao"
```
