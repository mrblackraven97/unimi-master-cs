import java.util.regex.Pattern;

public class DoubleEvaluator implements TypeEvaluator {

    private TypeEvaluator next;
    public static final String DOUBLE_REGEX = "[0-9]+\\.[0-9]+";
    public DoubleEvaluator(TypeEvaluator next) {
        this.next = next;
    }

    @Override
    public Class<?> evaluate(String input) {
        return Pattern.matches(DOUBLE_REGEX, input) ? double.class : next.evaluate(input);
    }
}
