import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class InvokeUnknownMethod {
    public static final TypeEvaluator TYPE_CHAIN_EVALUATOR = new DoubleEvaluator(
            new IntEvaluator(
                    new StringEvaluator()
            ));

    public static Object convertArg(String arg, Class<?> type) {
        if (type == int.class) {
            return Integer.parseInt(arg);
        } else if (type == double.class) {
            return Double.parseDouble(arg);
        } else return arg;
    }

    public static void invoke(String[] args) {
        if (args.length < 2) {
            System.err.println("Usage java -jar InvokeUnknownMethod CLASSNAME METHODNAME [ARGUMENT]...");
            System.exit(1);
        }
        Class<?> mClass = null;
        Method mMethod = null;
        Object mObject = null;
        Object result = null;
        try {
            mClass = Class.forName(args[0]);
        } catch (ClassNotFoundException e) {
            try {
                mClass = Class.forName("java.lang." + args[0]);
            } catch (ClassNotFoundException e1) {
                try {
                    mClass = Class.forName("java.util." + args[0]);
                } catch (ClassNotFoundException e2) {
                    System.err.println("Fatal: class `" + args[0] + "` not found");
                    System.exit(2);
                }
            }
        }
        try {
            mObject = mClass.getConstructor().newInstance();
        } catch (Exception e) {
            System.err.println("Fatal: empty constructor for class `" + args[0] + "` not found");
            e.printStackTrace();
            System.exit(3);
        }
        String[] methodArgs = Arrays.copyOfRange(args, 2, args.length);
        Class<?>[] argsTypes = new Class<?>[methodArgs.length];
        Object[] realArgs = new Object[methodArgs.length];
        for (int i=0; i<methodArgs.length; i++) {
            argsTypes[i] = TYPE_CHAIN_EVALUATOR.evaluate(methodArgs[i]);
            realArgs[i] = convertArg(methodArgs[i], argsTypes[i]);
        }
        try {
            mMethod = mClass.getMethod(args[1], argsTypes);
        } catch (NoSuchMethodException e) {
            System.err.println("Fatal: method `" + args[1] + "` not found");
            System.exit(2);
        }
        try {
            result = mMethod.invoke(mObject, realArgs);
        } catch (Exception e) {
            System.err.println("Fatal: unable to invoke method `" + args[1] + "`");
            System.exit(4);
        }
        System.out.println(result);
    }

    public static void main(String[] args) {
        invoke(args);
    }
}

