import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class StateLoggerTraceHandler implements InvocationHandler {

    private Object originalObject;

    public StateLoggerTraceHandler(Object originalObject) {
        this.originalObject = originalObject;
    }

    public String stateToString() {
        StringBuilder sb = new StringBuilder();
        for (Field field: this.originalObject.getClass().getDeclaredFields()) {
            sb.append(field.getName());
            sb.append(": ");
            field.setAccessible(true);
            try {
                Object fieldValue = field.get(this.originalObject);
                if (fieldValue.getClass().isArray()) for (Object item: (Object[])fieldValue) sb.append(item + " ");
                else sb.append(fieldValue.toString());
            } catch (IllegalAccessException e) {
                sb.append("`Illegal access exception`");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public String buildStateHeader(boolean before, Method method) {
        StringBuilder sb = new StringBuilder();
        sb.append("`");
        sb.append(this.originalObject.getClass().getSimpleName());
        sb.append("` state ");
        sb.append(before ? "before": "after");
        sb.append(" `");
        sb.append(method.getName());
        sb.append("` method\n");
        return sb.toString();
    }

    @Override
    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
        // NOTE: `o` is the proxy itself, don't use it I guess?
        System.out.println(this.buildStateHeader(true, method)+this.stateToString());
        Object ret = method.invoke(this.originalObject, objects);
        System.out.println(this.buildStateHeader(false, method)+this.stateToString());
        return ret;
    }
}
