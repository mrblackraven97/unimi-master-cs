import java.util.ArrayList;
import java.util.List;

public class RecognizingElements {

    public static void main(String[] args) {
        String[] myClasses = {
                "java.lang.String", "MethodsReif", "java.util.Collections"
        };
        String[] myAttrs = {
                "indexOf", "iterator", "isMethod", "parentClass", "reverse", "emptyIterator", "valueOf", "hash", "r"
        };

        List<ClassReif> classes = new ArrayList<>();
        for (String className: myClasses) {
            try {
                classes.add(new ClassReif(className));
            } catch (ClassNotFoundException e) {
                System.err.println("Class `"+className+"` does not exist. Exiting...");
                System.exit(1);
            }
        }

        for (String aname: myAttrs) {
            boolean exists = false;
            for (ClassReif cf: classes) {
                boolean thisClassHasIt = cf.hasName(aname);
                exists = exists || thisClassHasIt;
                if (thisClassHasIt) {
                    try {
                        ClassAttrReif attrReif = null;
                        if (cf.hasMethod(aname)) {
                            attrReif = new MethodsReif(cf.getClazz(), aname);
                        }
                        else {
                            attrReif = new FieldReif(cf.getClazz(), aname);
                        }
                        System.out.println(
                                "\n\n"+(attrReif.isMethod() ? "Method": "Field")+
                                        " `"+aname+"` exists in class `"+cf.getName()+"` as:\n"
                        );
                        attrReif.print();
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.exit(99);
                    }
                }
            }
            if (!exists) {
                System.err.println("Name `"+aname+"` does not exist in any of the classes provided. Exiting...");
                System.exit(2);
            }
        }


    }
}
