import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringJoiner;

public class MethodsReif implements ClassAttrReif, Iterable<Method> {

    private Class<?> parentClass;
    private List<Method> methodsList;

    public static String getMethodSignature(Method method) {
        StringBuilder sb = new StringBuilder();
        sb.append(
                Modifier.toString(method.getModifiers())
        );
        sb.append(" ");
        sb.append(method.getReturnType().toString());
        sb.append(" ");
        sb.append(method.getName());
        StringJoiner sj = new StringJoiner(", ", "(", ")");
        for (Parameter p: method.getParameters()) {
            sj.add(p.getType().toString() + " " + p.getName());
        }
        sb.append(sj.toString());
        return sb.toString();
    }

    public MethodsReif(Class<?> parentClass, String methodName) throws NoSuchMethodException {
        this.parentClass = parentClass;
        this.methodsList = new ArrayList<>();
        Method[] methodsArr = this.parentClass.getMethods();
        for (Method m: methodsArr) {
            if (m.getName() == methodName) {
                this.methodsList.add(m);
            }
        }
        if (this.methodsList.size() <= 0) throw new NoSuchMethodException(
                "Method `"+methodName+"` not found in class "+this.parentClass.getName()
        );
    }

    public Class<?> getParentClass() {return this.parentClass;}

    @Override
    public boolean isMethod() {
        return true;
    }

    @Override
    public boolean isField() {
        return false;
    }

    public static String methodsListToString(List<Method> methodsList) {
        StringJoiner sj = new StringJoiner("\n", "", "");
        for (Method m: methodsList) sj.add(MethodsReif.getMethodSignature(m));
        return sj.toString();
    }

    @Override
    public String asString() {
        return MethodsReif.methodsListToString(this.methodsList);
    }

    @Override
    public Iterator<Method> iterator() {
        return this.methodsList.iterator();
    }

    public String toString() {return this.asString();}
}
