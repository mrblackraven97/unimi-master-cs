import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClassReif {
    private String classname;
    private Class<?> clazz;
    private List<Method> methodsList;
    private List<Field> fieldsList;

    public ClassReif(String classname) throws ClassNotFoundException {
        this.classname = classname;
        this.clazz = Class.forName(this.classname);
        this.methodsList = new ArrayList<>(Arrays.asList(this.clazz.getDeclaredMethods()));
        this.fieldsList = new ArrayList<>(Arrays.asList(this.clazz.getDeclaredFields()));
    }

    public String getName() {
        return this.clazz.getName();
    }

    public Class<?> getClazz() {
        return this.clazz;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ");
        sb.append(this.classname);
        sb.append("{\n");
        for (Field f: this.fieldsList) {
            sb.append("\t" + FieldReif.fieldToString(f));
            sb.append("\n");
        }
        sb.append(MethodsReif.methodsListToString(this.methodsList));
        sb.append("\n}");

        return sb.toString();
    }

    public boolean hasMethod(String name) {
        try {
            MethodsReif m = new MethodsReif(this.clazz, name);
            return true;
        } catch (NoSuchMethodException e) {
            return false;
        }
    }

    public boolean hasField(String name) {
        try {
            FieldReif f = new FieldReif(this.clazz, name);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    public boolean hasName(String name) {
        return this.hasMethod(name) || this.hasField(name);
    }
}
