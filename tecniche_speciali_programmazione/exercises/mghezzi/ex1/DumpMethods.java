package TspLab1.ex1;

import java.lang.reflect.Method;

public class DumpMethods {
    public static void main(String[] args){
        try {
            Class<?> cls = Class.forName(args[0]);
            Method[] methods = cls.getMethods();

            for (Method method : methods) {
                System.out.println(method.toString());
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
