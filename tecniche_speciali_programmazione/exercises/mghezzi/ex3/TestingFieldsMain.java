package TspLab1.ex3;

import java.lang.reflect.Field;

public class TestingFieldsMain {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        TestingFields tf = new TestingFields(7,3.14);
        Field state = TestingFields.class.getDeclaredField("s");
        state.setAccessible(true);
        System.out.println("Value of string: "+ state.get(tf.s));
        state.set(tf.s, "testing... passed!!!");
        System.out.println("Changed value of string: " + state.get(tf.s));
    }
}

