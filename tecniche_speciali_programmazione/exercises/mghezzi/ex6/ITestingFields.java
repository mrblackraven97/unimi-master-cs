package TspLab1.ex6;

public interface ITestingFields {
    public void setAnswer(int a);
    public String message();
}
