package TspLab1.ex4;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class ElementRecognizer {

    // checks that the names in the second array are all declared in one of the classes in the first array
    public boolean check(String[] classesNames, String[] fieldsAndMethods) throws ClassNotFoundException {
        boolean ok;

        // Checking one element at time
        for(int i=0; i<fieldsAndMethods.length; i++){
            ok = false;

            // Getting all infos for a class
            for(int j=0; j<classesNames.length; j++) {
                Class<?> cls = Class.forName(classesNames[j]);
                Method[] methods = cls.getMethods();
                ArrayList<String> methodsNames = new ArrayList<>();
                for(int k=0; k<methods.length; k++) {
                    methodsNames.add(methods[k].getName());
                }
                Field[] fields = cls.getFields();
                ArrayList<String> fieldsNames = new ArrayList<>();
                for(int k=0; k<fields.length; k++) {
                    fieldsNames.add(fields[k].getName());
                }

                // Comparing the element to check if it appears in the class
                for(int k=0; k<methodsNames.size(); k++) {
                    if(fieldsAndMethods[i]==methodsNames.get(k)) {
                        System.out.println(fieldsAndMethods[i] + " is a METHOD of --> " + cls.getName());
                        ok = true;
                    }
                }
                for(int k=0; k<fieldsNames.size(); k++) {
                    if(fieldsAndMethods[i]==fieldsNames.get(k)) {
                        System.out.println(fieldsAndMethods[i] + " is a FIELD of class: " + cls.getName());
                        ok = true;
                    }
                }
            }
            if(!ok) {
                System.out.println("No match found for method: " + fieldsAndMethods[i]);
                return false;
            }
        }
        return true;
    }
}
