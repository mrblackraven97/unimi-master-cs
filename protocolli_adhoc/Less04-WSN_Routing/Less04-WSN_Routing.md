# Routing Algorithms on WSNs

In the last lecture, we got a fair amount of information regarding what are WSNs and which are their features. This time, the focus will be on the routing algorithms in these networks that let nodes communicate with each other, particularly when all nodes are peers.

Starting off by listing the principal solutions:

- **Reverse path forwarding**
  - The main idea of this method is to **remember the path done** while completing a query and tread through it backwards while delivering the response. The backwards path is the same as the forward path.
  - *Example*: a sink queries for certain data in the network and this query reaches a certain node that has this information. The path taken by the response data from the node to get back to the sink will be the same taken by the query from the sink to the node.
  - It is required for this method to work that the links between the nodes are full-duplex, for obvious reasons.
  - The implementation of this solution is the *Directed Diffusion*.
- **Cost-field**
  - The approach here is kinda different, where instead of remembering only the path taken, we also **remember the cost gradient to reach the destination** (sink or event). This way, I can create a map of path so that, the next time, the message will choose the one with the lowest cost.
  - The implementation of this solution is the **Rumor Routing**.
- **Hierarchical**
  - This is by far the easiest method because it **relies on super-nodes**, which are nodes with better resources and capabilities (like we saw in the micro-server hierarchy). These are elected as coordinators and all the computations and optimizations are performed on these, treating the other nodes as "dummy terminals", only dedicated to gathering data.
  - We're going to cover only peer networks, so this solution is not going to be covered.

## Assumptions 

Before looking at the algorithms in detail, let's list our assumptions on the network:

- Sensors are distributed on the area in an **unplanned** way:
  - They know the position in which they're located (however it is NOT used for routing, only for tagging data);
  - Their ID are unique;
  - Their clocks are synchronized and timestamps are used for tagging data;
- Sensors are **fixed in placed** and the network is **dense**:
  - Topology is significantly connected to improve accuracy and partitions will only occur when the network (and sensors) lifetime is near the end;
  - Redundant paths can exists.
- Sensors are **intelligent** and able to do some **computation**:
  - In order to do that they need to know the tasks and the data the application needs (application-awareness);
  - *Example*: data aggregation can reduce information redundancy, thus decreasing power needed for transmission.
- Sensors will communicate via **broadcast wireless** (which means open to interferences and collisions);
- They will use **short hops** to save energy.

# Directed Diffusion

The messages between sinks and sensors are called **interests**. This name is appropriate because, differently from queries, in which I'm almost sure the result exists, we don't know if an event is occurring, occurred in the past or will occur in the future. The sink manifests an interest without knowing if he will ever get an answer from it.

The sink queries for particular data sending interests on the network, which are usually a series of `<key, value>` couples as seeing in the following snippet (it depends heavily on the application though):

```
[Query]	
	type = four-legged animal
	interval = 20 ms
	rect = [-100, 100, 200, 400]
	timestamp = 01:20:40
	expiresAt = 01:30:40
```

Here, the **sink is searching for event data** regarding a four-legged animal in a certain area, bounded by a specified rectangle, monitoring it by some interval (every 20ms here). Together with these informations, a timestamp and expiry time are added: they will tell for how much the sensor should remain alert to monitor for events under those conditions.

When a sensor receives this interest:

- If he's able to detect an event matching with the request:
  - He puts himself in an **alert state**, monitoring for the requested event, answering back with the data gathered from the event if he founds it;
  - Forwards the interest to other nodes.
- If he's not able to detect that type of event or doesn't match the interest filters:
  - He will help the research by just **forwarding this interest** to other nodes.

``` 
[Answer]	
	type = four-legged animal
	instance = elephant
	location = [25, 220]
	intensity = 0.6
	confidence = 0.85
	timestamp = 01:20:40
```

Here the sensor confirms that he detected a four-legged animal (particularly an elephant) at a location within the specified bounds. Locations (like the bounds) are expressed in relative distance from a reference point (distance from sink, coordinator or other), not in latitude/longitude (there is no GPS). 

These informations are enriched by:

- The **intensity** of the detected signal, used to make distance estimates of the event from the sensor;
- The **confidence** of the sensor that the event occurred and that the inferred instance is correct (he's sure at 85% that the four-legged animal is an elephant);
- The timestamp at which the event was detected.

## Main Characteristics

The main features of the Directed Diffusion can be summed up in the following list:

- **Full duplex channels**
  - As we said before, if we want our message to go back the same way it came in, we need bi-directional communication channels.
- **Reactive routing**
  - Instead of the usual proactive routing present in the network layer of the TCP/IP stack, here a reactive routing is preferred: whenever there is an interest requested by a sink, the routing informations are saved on the fly, these informations will persist until the respective interest is satisfied and/or the interest time expires.
  - The nodes on the network will **never need to communicate to others until they receive an interest** from a sink or a forwarded one from other nodes. In order to answer them back with the data they will possibly obtain, they save their address (because they need the reverse-path!).
- **Diffusion of interests through flooding** (roughly)
  - Interests are diffused via flooding, which means that each node in our network will forward the interest in a broadcast way to all its neighbours (one short hop apart).
  - Since this happens in a wireless network, one could think that it would be easy to broadcast interests via flooding, given that the wireless communication is intrinsically broadcast (sphere of waves coming out an antenna). However, there is the duty cycles of the sensors: some may be sleeping and some may be active, so a simple single hop broadcast doesn't guarantee to the interest to cover all the network, since sleeping nodes don't receive and don't forward anything.
- **Methods to build and maintain the best possible reverse path from sensors to sink**
  - The flooding we just mentioned will help us find the best possible path from the data sources (sensors) back to the sink. Naturally, we will also find and save multiple alternative paths, which will help us by giving better reliability and fault tolerance to our network.
  - This quality will often save us from network partitioning because of sleepy, missing or dead nodes. It is possible to "repair" intermediate nodes by replacing them with other nodes, choosing a different alternative path as the new best.
- **Data aggregation in the intermediate hops**
  - Since sensors are intelligent and context-aware, we can apply data aggregation with duplicate filtering to remove redundancies, decreasing power needed for transmission and the chance of interferences.
  - It is also possible to increase or reduce the rate of which information is sent to the sinks: some sinks might want information sent every 5 minutes and others might want to be updated every 20 minutes, according to their needs. This can improve the lifetime of the sensors when an event doesn't need frequent updates.
  - Different sinks might want different data rates: for this reason, the nodes gathering information every 5 minutes for one sink, don't necessarily need to send that data to the neighbour nodes which are connected to sinks that want info every 20 minutes. For this reason, that node can aggregate the data in some way (keep the latest, merging info, ...) and send info to different nodes at the different rates their sinks are asking for.

## Interest Diffusion

For each active interest, the sinks send broadcasts periodically throughout the network where the first one has a high interval, to discover potential sources for the event. This is done because the sink doesn't know if the event will happen or not, so, for the sake of saving energy, he just want the sensor to be (at least) in the alert state for that interest.

Through time, the sinks keeps retransmitting the interests again for reliability, since it could happen that some nodes are asleep or unreachable. Whenever the source for the required interest is found the interval is reduced, allowing the sources to transmit event information more frequently.

![](img/ddgrad.png)

Each node that receives an interest will save it in the cache in order to:

- Remember interests and **discard duplicate** ones, only the latest received is kept;
- Remember the **gradient tuple** for each sink with that interest: `<data_rate, duration, neighbour_ID>` where:
  - `data_rate` represents the rate at which information should be sent to that sink (interval);
  - `duration` represents the difference between the interest timestamp and the expiration time of it;
  - `neighbour_ID` represents the neighbour from which we received the interest and to which send data to, in case the event occurs and its informations are gathered by this node or received by another node. Through this I'm able to achieve the reverse path to the sink. In wireless implementation, this ID usually coincides with the MAC address of the network card of the node.

**N.B.** These "gradients" have nothing to do with the cost-field methodology gradients we mentioned before, since those represent distances between nodes while these "gradients" do not!

### Forwarding Policies

There are different forwarding policies we could implement with Directed Diffusion:

- **Flooding**: Each node asks his neighbours to forward to their neighbours, forwarding only not duplicate interests and those not already forwarded (saving bandwidth);
- **Geographic routing** towards the area target, trying to estimate the event area to direct communications towards it;
- **Cached directions** from previous communications, using those saved data to direct research there.

Depending on our application, one is better than the other, there is NOT a single solution, there is NOT a silver bullet! Direct Diffusion uses flooding only when searching for the event's source, once found, it will use the backwards paths he constructed along the way.

## Directed Diffusion: Example

### Query

![](img/ddfw.png)

Following this example, let's see how the sink queries and receives response about a certain interest:

1. The sink broadcasts his interest forwards to his neighbours;

2. The first two nodes `(1,2)` receiving the interest acknowledge that they are not in the interest's event area, so they just forward the message to their neighbours in broadcast. They will save in the cache that they received an interest, at a certain timestamp, in a gradient with `neighbour_ID = <sink_MAC_address>`;

3. The second two nodes `(3,4)`, in the same situation of nodes 1 and 2, just forward the interest to their neighbours, while saving in the cache the interest.

   Node 3 will answer to node 1, node 4 will answer to node 1 and 2. But then after their own broadcast node 3 will answer to node 4 and node 4 will answer to node 3.

4. Same goes for nodes (5,6);

5. Finally the interest reaches the source node, which is in the event's area.

Remind that the forwarding only happens if interest isn't a duplicate and wasn't already forwarded recently, this is done to save power and bandwidth. 

Having many gradients (thus paths) let's us have more resilience and path repair in our network. Since the sink is unknown, gradients can also help to forward data towards sinks with the same interests.

### Answer

![](img/ddbw.png)

When the source node is on alert for an interest and the related event occurs:

1. The `data_rate` (interval between data transmissions) becomes the max of all the `data_rate` of the gradients for that event (and interest).

2. The source node sends information on the event to all the neighbours for which exists a gradient with an interest for that event. It wakes up ONLY them, avoiding to wake up nodes that didn't care and wasting their power.

3. The intermediate nodes then answer with that data to all the nodes (gradients) who asked for that interest. 

   It is possible to receive duplicates of that data, in the example node 6 receives the same data from source and node 5, but this is expected, since 6 was subscribed to both those two for the same event. However, since now there's a duplication and since he received data first from Source than 5, the 5-6 communication link is pruned because it's slower and the loop is eliminated in order to optimize the path.

   This is done on all intermediate nodes, leaving us with two main paths from sink to Source and vice-versa: 1-3-5 and 2-4-6.

## Reinforcement

![](img/ddrf.png)

When a sink is starting to receive event's data, it **reinforces the best path**, from which he receives the data earlier/reliably/constantly (depends on implementation). Then he can send interests with a shorter interval (bigger data_rate) than before, so the interest cache in the nodes will be then updated accordingly to suit the sink needs.

This way, I can even save more energy by **communicating interests only on the reinforced path in a unicast way**, instead of broadcasting and waking up the nodes along the secondary path. 

The only path saved is the reinforced one.

### Negative Reinforcement

Symmetrically, one can temporarily consider all the paths and, instead of reinforcing positively the path with the best and fastest communication, **reinforce negatively in time the worse paths**, which are those paths that contains problematic nodes (temporarily or permanently).

One could want to keep those bad paths in order to make the network more fault tolerant through redundancy but, at the same time, we want to reduce the energy consumption of unused nodes on a unused path.

To reinforce negatively we have two choices:

- **Soft state**

  - ***Gradient duration expire*** - Letting the gradient duration expire by not refreshing the interest on that path and, by doing that, the nodes will gradually forget about it. Since there is no need to send any interests on the paths to prune, we have no extra handling costs, plus we have a backup path if something goes wrong on the primary path (until it expires of course).
    - *Pros*: fault-tolerant, no extra costs.
    - *Cons*: more energy spent by idle nodes on path to prune.
- **Hard state**
  - ***Reduced `data_rate`*** - Refreshing the interest but gradually or drastically reducing the `data_rate`. There is some extra overhead costs by sending new interests with reduced `data_rate`, however, there still is that path as backup and the network nodes will work with reduced intensity.

    - *Pros*: fault-tolerant.
    - *Cons*: some energy spent by idle nodes on path to prune, extra overhead costs.

  - ***Instant turn-off*** - Refresh the interest by setting the timestamp and the expiry date in order to obtain duration 0. This way the interest will be cleared from the nodes in that path almost instantly, but since we need to send another interest, we have some extra overhead costs.

    - *Pros*: no energy spent by idle nodes on path to prune.
    - *Cons*: no fault tolerance, extra overhead costs.

    Fault-tolerance is important for our networks, so generally the other two options are better.

This allows to save energy by gradually reducing the workload of unused paths ("worse" paths).

### Downconvert

Each node doesn't just re-forward received information about an event, it will check through its cache and send data to other nodes following the gradient's `data_rate`. So, if a report on an event is coming faster than the gradient's requested `data_rate`, some packets will be discarded or aggregated because they're not needed by the gradient. 

*Example*: if the requested `data_rate` is of 0.5 but reports come every second, the node will downconvert the information throughput by forwarding a report every 2 received.

This allows some great fine-tuning for power saving.

## Performance Comparison

Some analysis has been done on these networks, but unfortunately, these data comes from virtual simulations of a network instead of a real one, given the difficulty to measure this kind of informations on physical unplanned WSNs (we would need to deploy the sensors and pick them up every time we need data or update their algorithm to optimize). 

To do these analysis we can use simulators such as NS-3, which have also been used by the authors of the paper on Directed Diffusion to compare its performance with other methods.

Let's compare the Directed Diffusion against:

- *Pure flooding* - Use flooding for every step of the communication between sink and source. Interest forwarding and event data back-warding.
- *Multi-cast on optimal tree* - The goal here is to create an optimal tree which connects the nodes in the network to the sink root, finding optimal paths by using Dijkstra's algorithm or similar ones. It's not really practicable on a WSN since each node has little information on the rest of the nodes and even of himself.

##### Energy consumption

<img src="img/ddnrgcmp.png" style="zoom:60%;" />

- *Flooding* - As expected, the average energy consumption for flooding is extremely high and growing with the network size since all nodes in the network need to juggle and forward data from neighbour to neighbour, remaining active for a long time and wasting energy.
- *Multicast* - Interestingly, multicast uses more energy than the direct diffusion because, differently from the latter, doesn't implement any data aggregation or filtering. Each node is a "dumb terminal" and will only forward data to the sink following the optimal paths of the tree.

##### Latency

<img src="img/dddelaycmp.png" style="zoom:65%;"/>

- *Flooding* - Again, as expected, flooding is the worse of the three because each node has to forward the message to ALL of his neighbours: true, it will find surely the optimal path (by trying all of them), but will take a long time to do so. We also have to take into account all the interferences, collisions and lost messages since every node tries to transmit and re-transmit their data to neighbours: because of these problems, latency will stack up in no time.
- *Multicast* - Multicast has similar delay to Directed Diffusion: this means that the latter can find easily optimal paths thanks to the reinforcement mechanic. Having only one optimal path to which send data to works in favour of both multi-cast and Directed Diffusion since they only have to forward data to one known node at a time.

Now test how effective is Directed Diffusion with or without duplicate suppression or negative reinforcement.

##### Negative Reinforcement - Energy consumption

<img src="img/ddnegcmp.png" style="zoom:80%;" />

Negative reinforcement benefits our routing algorithm since it suppresses over time redundant and under-performing paths, reducing the energy wasted by keeping them up. A network using negative reinforcement also scales well with its size, unlike one that doesn't.

##### Duplicate suppression - Energy consumption

![](img/ddsupcmp.png)

Duplicate suppression contribute as well to save energy in our nodes on the long run: this is because we can filter data on each node and avoid duplicate transmissions of the same information. Energy consumption remains really low and stable with suppression, even in large networks.

# Rumor Routing

Directed Diffusion, is a routing algorithm based on the *Reverse Path Forwarding* paradigm, which:

- Tolerate that loops can be formed and exist (but we can also take advantage of them);
- Doesn't tolerate sink mobility, because otherwise I'd need to reconstruct all the gradients in the nodes on the reverse path, since the old gradients would point to a certain "location" in which the sink is not anymore.

For this reason, let's try the opposite approach, so instead of creating paths from the event to the sink, let's try to **tread paths from the sink to the event**, following the **Cost-field based** paradigm, in which cost information to reach a node from another are memorized.

This is implemented it in the RUMOR approach:

- It doesn't form loops;
- Supports sink mobility (from one query to another);
- Each node state is not anymore a set of gradients, but a scalar that denotes a distance (in Rumor it represent the distance to the event, not the sink!)

## Main Features

Let's see what Rumor has to offer:

- **Trade-off optimization between updates and queries**
  - Directed Diffusion uses flooding is costly but it's used in a smart way, by using it to create a path from the sink to the source, then work with the reverse paths. 
  - In the same way, Rumor Routing wants to use the flooding, just the right, low amount:
    - When there are a **few events to track but many queries** about them, **data flooding** is used. This way the information will be spread as much as possible, so that it will be easy for the data to reach the nodes that requested it.
    - When there are **many events to track but a few queries** about them, **query flooding** will be used. This is kinda similar to the Directed Diffusion, since the query flooding is the same as the interest flooding.
- **Random diffusion of both notification of events and queries**
  - The main idea here is to reduce as much as possible the flooding process while still being able to bring the event data to the interested nodes. 
  - When a sensor detects an event, instead of diffusing the complete information about it, he only **diffuse the information that it happened and its type**. If a node wants more information on it, he's given the path to reach the node which holds all the information and its cost. The diffusion of this partial information is done by randomly jumping to one of the neighbours at every node.
  - In the same way, the **queries will also be diffused jumping random neighbour** by random neighbour, so that if we're "lucky" and we reach a node that casually came through the event information we wanted, he can "tell us" the path to reach it along with its cost. After that, the node that made the query knows which path to take to the event and doesn't need to query randomly again. This path is obtained by calculating the intersection between the path of the query and the path of the partial information.
- **Acceptable probability of finding the event while limiting the energy consumption**
  - In other words, diffusion and queries should run into each other, sooner or later

![](img/rrex.png)

As described in the picture, nodes inside the event area start sending partial information about the event to one random neighbour and the latter will save it locally and do the same, creating a path of nodes to the event (black path we can see in the picture). This **traveling piece of information is also called agent**.

A node which queries for an interest is called **query source** and to diffuse its query will do the same as interest forwarders: he will choose one random neighbour and send the query to him and he will do the same and so on, creating the white path.

Since **the probability of the query and event line to intercept at some point is sufficiently high**, especially in dense networks, we will be able to find consistently the information about the queried event and create a path to it, intersecting the query (white) path with the event (black) path.

Given this high probability, we can totally avoid any flooding by favoring this random and unicast forwarding of information. To implement this, each node must find in some way the list of his neighbours through the wireless channel and store it. This is done in two phases:

- **Beaconing**, in which the nodes transmit information about them (MAC address) in a broadcast way to his neighbours;
- **Listening**, in which the nodes listen to their surrounding and catch the beacons of their neighbours to make the list.

There's always a trade-off between qualities, because the probability to reach a goal is slightly lower and the query could take longer, however by avoiding flooding, we save much more energy compared to Directed Diffusion.

## Errors 

In this kind of network, each node has some level of confidence when reporting that an event occurred. It is **possible then to commit errors or false measurements**, essentially, we could have false positives and false negatives.

- A **false positive** happens when a sink queries for an event and find informations about it even if the event never happened;
- A **false negative** happens when a sink queries for an event and doesn't find informations about it even if that event happened.

By thinking about some examples, it's easy to see how a false negative is the worst case scenario. What if a sensor detects a fire but the event isn't found by the sink and never reported to authorities? What if something fails in a nuclear plant and the event isn't reported because the sink didn't get any information about it?

Intrinsically to the mechanics of Rumor Routing, we're bound to have some false negatives since statistically we have an high but not so high probability (~70% on uniformly dense networks) that the query path crosses an agent: **it could happen that my query will never found information about an event that occurred**.

For this reason, we'll see how to get this probability up as much as possible: for example, we could use the Rumor Routing mechanic for a certain number of hops or until the query expires and, when it does, we could only then use flooding as a last resort to obtain information about the event (trying to be sure it didn't happen).

## Agent Processing: Pseudocode

```pseudocode
// S = state of the node, R1,2,3 = random seeds
// Whenever the node detects the occurrence of an event
upon event do
	// Given the state and a random seed, f1 function decides to create an 	   // agent of the event or not
	take (bool) retransmit decision D <- f1(S, R);
	
	// If decision was positive I generate an agent from a list of     	       // recent events
	if (D = yes) then
		
		// Since a sensor can detect multiple events (fire, smoke, humidity         // and others), it can pack his agent with all the events that  	    // happened since he woke up from sleep or as much as it can fit    	    // into the frame payload
		generate agent M <- list of recent events;
		
		// From neighbours list, choose 1 recipient through a random  		    // function
		forward to 1 dest <- f({neighbours}, R1);
		
// Whenever the node receives an agent
upon agent reception do

	// The agent may grow in size, because this node may have received 		// other agents or detected other events: if the frame payload isn't 	 // full, it can add the local information on the agent
	synchronize events on agent with local events; 
	if (agent's TTL expired)
		discard agent;
	else
		forward to 1 dest <- f2({neighbours}, R2);
	
```

<img src="img/rrex2.png" style="zoom: 80%;" />

As seen, the agent can be enriched with information about other events by the nodes, as also seen  in the pseudocode, where each node synchronizes the agent with local event data. This aggregate path can increase the probability for queries to find the event they searched for.

## Loop Removal

Since Rumor Routing is loop-free, let's see the mechanism that lets it remove them.

![](img/rrl.png)

We can see that from node `A` following node `C` we'll get to

- `E2` in 2 hops;
- `E1` in 4 hops.

However, an agent is approaching node `A` from node `B` with the information that event `E1` is only 3 hops away from node `A`. In order to optimize the paths and remove loops, node `A` will choose the best path in order to reach `E1`:

- Going through `C` will take 4 hops;
- Going through `B` will take 3 hops.

Choosing `B` as direction for `E1` event queries works best so node `A` will update his local cache accordingly. The agent will then tread off to other nodes bringing with himself the updated event distances (he added `E2`, since he got through `A` which held `E2` event information).

## Query Processing: Pseudocode

```pseudocode
// When an event is important and must be looked for to avoid false        // negatives, node can start a query search and, if not found in a certain // T, he can try flooding, retry or resign
when (event must be looked for) do 
	send query to a dest <- f({neighbors},R);
	if (no reply within T) 
		then retry or flood query or resign;
	
// Whenever the node receives a query generated by another node
upon reception of a query do
	// If the complete info on event is held on the node, just reply with       // it back --> black node in the event area (he experienced the event)
    if (info on event held locally)
    	then reply;
    // White node with TTL = 0
    else if (query's TTL expired) 
    	then discard query;
    // If only partial info on event is held on the node, route query to    	// the next node in the path --> black node outside event area
    else if (path to event) 
    	then route query; 
    	// If path is found, TTL is not decremented anymore
    // If the node is still searching for an agent, then forward randomly
    // white node with TTL > 0
    else 
    	send query to a dest <- f({neighbors},R2);
```

## Optimizations

Let's see what can be done to optimize this algorithm the best way possible. For starters, the goal is to **bolster the intersection probability** between query and agent as much as possible. 

To do so, it is possible to force these two entities to **travel only along straight lines**. This way those random cases in which the agent stays confined in one area instead of spreading through the network are avoided.

Keep in mind that, to do this, we're introducing deterministic elements in this algorithm.

## Straight Lines

To implement the communication in straight lines there are different solutions:

- The **Hardware-way**: Embarking sensors with additional hardware could do the trick, by using compasses to determine the North/East/South/West axes and using unidirectional antennas to transmit along them. 

- **Beacon positioning**: Assuming that the nodes know their position. For now they are using it only to add semantic at application level to the event they tracked. Now let's assume that they're fixed at that position, then it is possible to **add to the periodic beacon** they are already sending to let others know about their presence **the information of their position**, other than the usual MAC address.

  This way, by slightly adding more computation to do in each node (doesn't waste too much power), they could **calculate the position of their neighbours and forward the agent (or query) towards the closer based on axis**. The line isn't totally straight, since we're usually in an unplanned network, however is straight enough to get the job done at zero extra costs (we don't need new hardware, we just need to give some computation to nodes and add one more information to the beacon).

  - *Example*: let's say that we want to forward an agent along the North axis: the node will obtain the list of his neighbours, will calculate which one has the highest Y coordinate and the closest X coordinate to his, and finally will select that node to which send the agent.

- **Magnetic repulsion**: Supposing that assumptions about using location only for data enrichment is still valid, the main idea here is to avoid loops and enforce a straight line by **not letting the agent visit a node that was already visited in his path or his neighbours**.

  To achieve this, the agent must not only **carry the information about** the node from which he came, but also the **list of the neighbours of that node**. This way, the node which need to decide the new "random" direction of the agent will exclude automatically the precedent node and his neighbours (that could be in range of this node too!), increasing the straightness of the path.

  This is **almost totally cost-free**, because there is no extra computation and no extra hardware. It is just needed to include a list of neighbours in the agent along with the node from which he came from. However, the downside is that this list of MAC Addresses (48 bit each) is going to **take some space off the payload**, so it can't be jammed inside it as much data as before about local events at each node. This handicap can be reduced by using only a set amount of payload for this list, and if it is getting full, old entries will be replaced with new ones following a FIFO policy because it's supposed that they should be far away from the first nodes in the path.

## The Promiscuous Mode

Another way to increase those probabilities is to activate the **promiscuous mode** on the network. Usually when transmitting data in a wireless network, the transmission medium is broadcast and usually a neighbour can receive the signal and read it. However, since the transmission mode is unicast, only the receiver MAC address uses that data.

![](img/rrprom.png)

When activating promiscuous mode, the nodes that received the message but weren't the recipient (the red neighbours in the image) will hold the message in their cache: we can use this to **leave a bigger and wider trace when an agent is treading his path**, so that if a query comes across one of these promiscuous node, he will forward the query to the (black) neighbour from which the agent passed through.

As always there is a trade-off: to use this mode, we need to wake nodes around the agent every time to let them save in their caches the information that an agent passed through closely. For this reason, we're trading higher probabilities (up to 90%) for the query to find the event for an higher consumption of the nodes battery.

Cost in Directed Diffusion depended on the network size because of the flooding of the interest; in Rumor Routing, instead, **cost is based on the number of queries and events that happen**, lowering the overall energy consumption.

## Reliability

For Rumor Routing to be successful one must also make sure that agents and queries have an high TTL and have re-transmission priority to overcome collisions (hop-by-hop reliability), all in order to reduce the possibility of false negatives and improve reliability of the system. It is not allowed to miss an event just because an agent was lost by a node that went asleep, died or didn't receive it because of interferences.

To be sure that an event was properly received by the recipient, there is no need of an ACK system since it comes for free with broadcast transmission. **When the node to which I transmitted the agent transmits it further to another, that means that he received it correctly**. In the same way, if I don't hear another transmission from him, probably the agent was lost and I should re-transmit it.

## Performance

**Rumor routing doesn't always perform better than the flooding**, to do so the network must be studied: the frequency of queries and events and, only then, choose some parameters that will change how the agents will work.

<img src="img/rrperf.png" style="zoom:80%;" />

The fine-tuning of these parameters is the problem to solve. Respectively, fine-tuning is done for:

- ***A*** - Number of agents;
- ***La*** - Agent time to live (in hops);
- ***Lq*** - Query time to live (in hops).

## Conclusions

This way Rumor Routing appears similar and compatible to a publish-subscribe content-centric model, where: 

- The sensors in the event area are the **publishers** which sends agents to inform the rest of the network about the occurrence of an event;
- The sensors outside that area querying for that event information are instead the **subscribers**.