# Internet of Things

## Introduction

Internet of Things has become a buzzword recently so let's see what it really means.

> Internet of Things represent a world-wide network of interconnected objects uniquely addressable, based on standard communication protocols

Let's analyze these qualities:

- World-wide network of interconnected objects
  - We should be able to let objects communicate world-wide
- Uniquely addressable
  - Since we need to be able to address every possible object in order to connect to it and communicate with, we need a capable addressing system, which is obviously IPv6 with the strength of its $2^{128}$ possible addresses (practically unlimited) 
- Standard communication protocols
  - We need to be able to connect different networks that talk in their own language and protocol: for example, I'd want a sensor in a WSN that uses Directed Diffusion to be able to communicate to Internet his informations (without a sink gateway) or to a node in a VANET, which uses GPSR, that could take advantage of this information

What we're going to do is exactly how we can merge the separate worlds of WSNs, VANETs and normal networks by creating connectors and let them intercommunicate through a standard, shared and common protocol.

Many other definitions have been created by different stakeholders, thus from different points of view:

- "Connectivity, anytime and anyplace, for anyone and for anything" (ITU - International Telecommunication Union)
- "Things having identities and virtual personalities operating in smart spaces using intelligent interfaces to connect and communicate within social, environmental and user context" (European commission)
- "Things that can automatically communicate to computers and each other providing services to benefit the human kind"

## Possible applications

We have countless of possible applications of the concept of IoT in our everyday lives:

![](img/itapp.png)

- Transportation and logistics
  - Similarly to what we saw in VANETs, IoT can helps us achieve some tasks in this sector: augmented maps, which can consider traffic condition other than the mere topology of the streets, assisted driving, automated toll paying and environment monitoring for traffic and pollution
- Health-care
  - IoT can be very useful in healthcare, which can help patients and medics alike. For example, there are applications for identification of the patient and authentication of the medic for usage of certain medicines (and avoid dosage or prescription errors), for data collection of the patient life parameters, for sensing patient actions and detect distress (automatically calling for help by contacting the medic through the smartphone or smart bracelet)
  - An example project we can cite is the smart medicine cart: with sensors, the cart can detect which medicines are loaded into it and checks whether the medic or the nurse inserted in it the wrong medicines, noticing him of this error.
- Smart environments
  - IoT can be used to enhance and adapt the environment to the user that enters into them: for example, 
  - Comfortable home/office application detect whenever a person is entering in a certain room and change the environment for that person that specified his own room parameters (regulating the light intensity and color, regulating the temperature and so on...).
  - Smart museums and even cities which can enhance signs, statues and all kinds of objects with augmented reality features, which can give descriptions of them and links to other informations about them. Plus smart cities can use traffic informations to regulate the semaphores and help traffic flow.
- Personal and social
  - IoT can be used to track our objects and relocate them later in case of theft or loss, but also for tracking the user feelings, actions and locations for him to share in a social networking environment.
- Futuristic
  - IoT is essential in some of the most futuristic and ambitious project such as autonomous driving, with robot taxi and other socio-economical utilities.

The Internet of Things is a vast argument, thus very different and numerous kinds of technologies coexists in order to make everything work

![](img/thtc.png)

- **Things-oriented vision** : technologies used to realize the objects
- **Semantic-oriented vision** : technologies used to reason on the gathered data
- **Internet-oriented vision** : technologies used to let the objects intercommunicate
  - This is the vision we're going to focus since, as we said before, we want to study how to connect different types of networks using different types of protocols
  - IPSO (IP for Smart Objects) : since we said that each object should be uniquely addressed, we must think how to do so in an efficient and effective way

As we can see in the next figure, IoT networks can REALLY be heterogeneous and complex, having different nodes with different operative systems on different hardware that follow different protocols and communication technologies.

![](img/shp.png)

Letting all these objects intercommunicate will be a challenge so let's start to get into the problem in more detail and tackle each issue one at a time.

### Low-power Lossy Networks (LLNs)

When we talk about these kind of IoT networks, we usually talk about LLNs, which are networks which packet drops, partitions and link failures are frequent because nodes are low-powered and can only maintain a very short communication range.  

The nodes of these networks are devices embedded in every day object and, for this reason, have usually limited power, memory and processing resources (otherwise the device wouldn't blend in with the everyday object).

Furthermore, we could have two kinds of devices

- Full function, which are equipped with enough resources to be able to intercommunicate through different protocols than the one used in the network they're into. 
- Reduced function, which are only able to communicate with the protocol used by the network they're into

For example, if my network is a WSN and nodes talk through directed diffusion, if a node is able to interface with the IPv6 protocols and communicate via Internet (since they can be properly and uniquely addressed), then they are full function. Otherwise, if a node can only communicate through directed diffusion, it is considered as reduced function.

As we said, there are still ongoing strong standardization efforts to achieve compatibility and integration in spite of heterogeneity, since our main goal is node intercommunication and cooperation.

### Smart object

We talked about smart objects but what really defines one as such? A smart object has

- A minimum amount of communication functions
- A unique identifier (IPv6 address) and a name and a human-readable description that explains his functions
  - He also must be able to follow a publish/subscribe model, in which we want to find a service, an interest, instead of a mere ID of a device: if this was implicitly used in WSNs since messages were guided by data, here it's explicit.
- Limited computational power
- A possibility of being able to measure physical phenomena

### What's different from WSNs and VANETs?

What is different between LLNs and the network we already saw? 

- **Heterogeneity**
  - In WSNs and VANETs we always supposed that all sensors and all vehicles would use the same protocol and the same communication technologies and that performed the same functions (sensing, communicating, etc...)
  - In LLNs instead, heterogeneity is brought up to another level, up to the point in which every node can run on a different hardware/software, performs different tasks and functions and uses different communication protocols (even proprietary ones)
- **Research and composition**
  - Here in LLNs, nodes with (simple) functionalities need to intercommunicate with each other to collaborate and make complex applications happen, all through self-organization
- **Machine-to-machine communication (M2M)**
  - M2M communication involves drones, robots, sensors, to communicate between themselves and self-organize based on exchanged information. This communication model must be integrated with Internet, that is device-to-device instead.

For this reason, new solutions were developed both at network layer (6TiSCH, M2M), which we'll focus on later, and application layer (CoAP, MQTT).

Because of these differences, we have a new set of challenges to face:

- We need better **scalability** in communication, elaboration and coordination between nodes, since in these networks we have a massive amount of devices that produce data and we want to treat it in the most efficient way
- We need **unique addressability** of the nodes, so IPv6 is compulsory for our IoT networks: each object should be uniquely identifiable so we need conversion mechanisms to go from different formats (RFID, IPv4) to IPv6, which can be done through encapsulation, translation and so on...
- We need to provide **privacy and security** to be considered trustworthy by the users and for legal matters: this is because all the objects that gather data around us can discover and bring around sensible and private information. Cryptography is difficult to implement on smart objects, given the low computational power, so we must find alternative solutions.
- We need to **maximize energy efficiency** for many reasons (reduce overall energy consumption for "green"-friendliness but also to maximize battery duration) while still having a certain degree of **reliability and robustness** when there are problems in the network.

These problems are not easy to solve in conjunction and this is proved by the fact that, after many years of research (which is still on-going), we still haven't found the optimal solution.

### Technology and architecture used

In IoT we favour low energy communication technologies for the reasons we said before: the most known on the market are RFID, NFC, ZigBee, Bluetooth Low Energy (BLE) and many more. As we can see, we're still far away from an unified standard.

The main architecture here is service-oriented (SOA): it creates a service abstraction layer upon the device characteristics and primitives in order to create a common API with which interrogate the device and obtain information from it. 

<img src="img/iotsoa.png" style="zoom:80%;" />

It is composed by different layers, one upon the other, as we can see in the picture:

- **Object abstraction**: it acts as an interface between the object primitives and the network "language" (API-like)
- **Service management**: it acts as a public catalog of all the different services offered by each node. Its job is to find new services from the objects in the network, monitor the existing ones and configure them for final usage.
- **Service composition**: depending on the application needs, combines the useful services coming offered by the objects to help the application achieve the higher goal

The SOA architecture must comply with the trust, privacy and security challenges transversally (in each layer). However, this is more of a semantic architecture of an IoT network, so we're not really interested in covering this architecture view.

We want, instead, to look more at how these devices interact physically with themselves and with other agents, like in the next figure

![](img/iotarc.png)

We can see that both constrained (Arduinos, Raspberry) and unconstrained (smartphones, tablets) can communicate to an Open Platform in which every device is considered equal and speaks the same language: this is done through drivers, smart gateways and middleware which are able to convert their own language into the common one.

Then upon this open platform we can base our services to be used from end users (which can be humans or other smart things) however this is more of a World of Things subject (WoT). We'll stick more to the IoT part, which means at low-level communication, closer to the hardware.

## Introduction to Industry 4.0

When IoT technologies are used in the industrial sector, we come across the Industry 4.0 environment.

In these particular network everything is connected through either IoT or IoP (Internet of People) technologies.

The main goal of this is to render everything as autonomic as possible, with cyber-physical systems that are able to operate autonomously, without the supervision of humans in the loop (M2M communication).

These are usually deterministic networks which, however, require a very short communication latency (< 5msec) and, at the same time, a very high reliability (>99%). This is because the industrial environment can be very dangerous (toxic gases, high temperature fluids and solids, heavy objects...) and an high communication accuracy is required to react quickly and avoid any potential hazard to the environment, people or other things.

Note that these properties are already hard to fulfill in a normal environment, let alone considering all possible problems that can emerge from the environment: heat, humidity, dust, interferences are very common in industrial processes. Furthermore, at the same time, we have to guarantee the usual IoT requirements:

- Resilience to failures (through self-diagnosis coupled with self-reconfiguration)
- Low energy consumption
- Device heterogeneity: from smartphones, to sensors, to machines and all kinds of devices (with added mobility)
- Scalability to thousands of devices in a relatively small area (high density)
- Universal open standards for interoperability

All of this is identified with a single name: Industrial Wireless Network (IWN)

![](img/ind4.png)

As we can see in the figure we have different smart entities, each represented as a node in the internal IWN: inside of it, a sink node acts as an intermediary with the external world by communicating with an access point and a gateway. In the end, all these informations travel through Internet on mobile or wired networks and can be used for display, analysis or manipulation purposes by end users.