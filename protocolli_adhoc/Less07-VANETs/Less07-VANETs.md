# VANETs - Vehicular Ad-Hoc Networks

Now moving to a different type of challenged network, which still retain some of the problems we encountered in WSNs while adding more challenges to face: Vehicular Ad-hoc Networks (VANET for short).

A VANET is a **network made of many wireless devices, usually deployed on vehicles or on stationary devices** which need to communicate with them (ex. infrastructure on the side of the road that can communicate with smart cars).

The VANETs enable the **Intelligent Transport System (ITS)** paradigm, which proposes several innovations:

- **Communication in-vehicle** (application units)
  - These are the typical objects in direct contact with the final user (Navigator, on-board computer).
- **Communication between vehicles** (on-board units)
  - On-board units usually mediate application units and roadside units;
  - Car-to-car, which means communications between manned vehicles;
  - Machine-to-machine, which means communications between unmanned vehicles, such as robots (this is more common in Industry 4.0, we're gonna see it later).
- **Communication between vehicles and infrastructure** (roadside units)
  - These are typically roadside devices that have a certain memory, thus can: 
    - Contain buffered information about passed vehicles in order to give it to incoming ones (info about traffic, incidents and so on);
    - Be connected to the Internet infrastructure to give services and more informations about tourism, refueling station, parking and others.
- **Integration with other transport systems**
  - On rails, air or sea, the European Telecommunications Standards Institute (ETSI) has a bigger picture of pervasive inter-communicating systems.

![](img/etsi.png)

Here there are various application of VANETs:

- Satellite communication + terrestrial broadcast : traffic and meteorological conditions for both air and sea;
- MAN connecting the metropolitan area for navigation, trip planning and traffic conditions;
- Fleet management (BIMBO project in Canada, makes sure that the vehicle passes all checkpoints on his way to the destination, if not call for support);
- Adaptive cruise control to avoid hazardous situations (distraction, tiredness of the driver);
- Safety systems to avoid road blocks for hazards or work in progress;
- Travel assistance for paying highway tolls on the go, without the need of tollbooths (like TelePass).

## VANETs Infrastructure

VANETs are made of many different objects:

![](img/obursuau.png)

- **Application Unit (AU)** - These are the applications located onto vehicles that provide services through an UI to the passengers. 
  - ***Communication: in-vehicle*** - Through the OBU, it can manage sensors to give information about the car and its surroundings to the passengers.
  - Technologies used: ZigBee, Wireless USB, Ultra wide band...
- **On-Board Unit (OBU)** - This is the main communication interface between a vehicle and other vehicles (OBUs) or stationary systems (RSUs). Its job is to manage networking and mobility. It provides a base layer for AUs, which can use it to power their applications.
  - ***Communication: ad-hoc*** - two types of infrastructures:
    - Vehicle-to-vehicle (V2V) : between OBUs, MANET-like;
    - Vehicle-to-infrastructure (V2I) : OBU-RSU;
  - Technologies used: WiFi IEEE 802.11p (special version of the well known WiFi protocol).
- **Road-side Unit (RSU)** - It extends the communication range of vehicles, by storing their information for longer and spreading them to incoming OBUs. It can forward informations to the AU applications in order to provide various utility services.
  - ***Communication: infrastructural*** - It is connected to the infrastructure (Internet), so it can send and receive updated information on-the-go.
  - Technologies used: 3G, 4G, WiMax, 5G

In this course, the focus will be more on the OBU and RSU side of the VANETs.

## OBU Communication

Until now, there were networks of nodes with little or no mobility (WSNs), so there weren't continuous variations in the topology of the network, with nodes entering or exiting in a short amount of time, but this is exactly what happens in VANETs.

The critical thing in VANETs is NOT the mobility per se, a fleet of trucks moving at the same speed, facing the same direction and keeping the same distance through-out their journey can appear as a simple fixed network since the topology remains the same and nodes aren't exiting from their neighbours range, even if the nodes are moving.

The **critical thing here are variations in mobility**, which renders the links between OBUs on cars susceptible. Even in a simple urban environment, cars could be moving at different speed and directions, entering and exiting each others range continuously. 

This is what VANETs are about: finding a suitable way to let these nodes communicate in an ever-changing network of ephemeral, short-lived links.

## 802.11p - WAVE Protocol

### History

OBUs use a special version of the widely known IEEE 802.11 WiFi protocol so there are some similarities to a technology already know.

Originally there was the **Dedicated Short Range Communication protocol (DSRC)** that was developed in 1999 in the USA for V2V and V2I communication.

At the same time a similar effort was done internationally with the **Wireless Access in Vehicular Environments protocol (WAVE)**. 

Instead of keeping the two protocols concurrent, they found a way to merge the research progress for both of them in order to create an international standard, **creating the 802.11p** standard protocol.

This protocol is also known now by the name WAVE (as its predecessor).

### The Need of a New Protocol

The 802.11p derives from his older brother 802.11a, a protocol for WLANs: it was chosen because it is a wireless technology with a good communication range, necessary in these networks for vehicles to be able to communicate. 

*Example*: Bluetooth (802.15) was too short-ranged for it to be able to convey messages between cars in movement in a possibly heavily disturbed environment.

802.11p has its Data-Link and Physical layer modified accordingly to fit the VANETs needs. Nodes must be able to **communicate at a short-medium distance within a short time window**. This is because, usually, informations transmitted on VANETs are time critical, for example, messages about traffic, car accidents or temporary deviations are crucial and must be transmitted as fast as possible to be effective for the receiver, which, upon arrival, can choose a different path or simply put more attention in driving.

For this reason, all agreements between sender and receiver should be limited as much as possible, such as acknowledgements, handshakes, frequency channel choice and so on... Information is time-critical and we don't want to waste it. A swift communication setup is imperative.

### The Frequencies

The **WAVE protocol communicates on the 5.9 GHz spectrum**. This frequency **bandwidth is free to use but need a license** to be able to communicate onto it. This is done to ensure that every transceiver complies with the VANETs standards and follows the 802.11p protocol, in order for it to be able to communicate on those frequencies with other devices.

![](img/chan.png)

In this picture we can identify seven main channels, divided by colors depending on their usage:

- **GREY** - This is not a real channel but a **filler space**. It could happen that other transceivers of other protocols invade 802.11p lower spectrum because of transmission issues. With this area acting as a guard, I'm sure that the real communication channels aren't affected by this problem.

- **GREEN** - This is the control channel, usable only by street safety applications. This channel is used to share control information in order to **coordinate the communication** happening in the other channels and avoiding collisions between them.

- **CYAN** - This is the channel with the shortest wave-length of the WAVE spectrum and is used for **short-ranged and immediate messages**. For this reason this channel is used for Critical Safety of Life information between nodes:

  ![](img/crashav.png)

  - *Example*: if a vehicle is about to crash into another and the sensors detects it, this message will be sent through this channel as it has the highest priority at that moment in order to avoid the crash.

- **RED** - This is the channel with the longest wave-length of the WAVE spectrum, thus it's used for **long-ranged communications**. It can also be re-transmitted, becoming a multi-hop communication, spreading the information to other incoming vehicles.

  ![](img/safe.png)

  - *Example*: a roadside system can communicate to a car at long-range that an intersection is trafficked, that the semaphore is red or that there is a road work ahead. These kind of informations of Public Safety is useful for drivers to be aware of dangers and choose a different route if needed.

    

- **PINK** - These four channels are used by **all the other user applications** not concerning safety.

  ![](img/trav.png)

  - *Example*: tourist information, meteorological conditions, parking management and travel assistance all travel through these channels.

Let's compare 802.11p with other common wireless protocols:

![](img/table.png)

Differently from others technologies, WAVE has all the features requested from a protocol that manages VANETs:

- It has a theoretical range of 300m, more than enough in an urban environment to connect two or more mobile nodes, even at high reciprocal speeds;
- The frequency is high enough to guarantee a quite high bandwidth (27Mbps), able to transport, even in a short time window, both safety messages and multimedia content (audio-videos) to user applications;
- The license is required but is free, encouraging vehicle manufacturers to support this kind of technology.

## 802.11 Protocol

Let's see the general baseline established for the 802.11 protocol and how 802.11p evolved from it into his own version. 

Essentially, the objective (again) is to **have an address-less communication**, in favour of a content-centric one, just like in WSNs. This is because there is no need to talk with a specific car or roadside system, since they're probably gonna stay in range for a little time because of mobility. The main focus is on information, contents and interests just as before. 

*Example*: vehicles have to communicate to other nodes in search for data about the surroundings for tourists, about traffic for people in a hurry and so on...

What must be modeled here is a publish-subscribe model:

- When transmitting, different nodes are not targeted in a unicast way, the target is a group of node with a certain interest;
- When receiving, there's no interest in talking with a specific node, but to receive contents based on our interests independently from the sender.

#### Basic Service Set

In 802.11 communication takes place between **groups of nodes connected to an access point**. This whole **structure is called Basic Service Set (BSS)** which is also called cell. 

- The **Basic Service Set ID (BSSID)** of a cell is the MAC address of the access point.
- The **Service Set ID (SSID)** is a 32 bytes string name used to identify a group of nodes in the same cell. This is generally used by users to understand which contents circulate in that network and/or its goal.

The communication is single hop, since each node that wants to communicate with another in the same BSS needs to do it through the access point, which will act as an intermediary, managing the interchange of information between them. 

However, interestingly for us, in 802.11 two additional kinds of groups are defined:

- **Independent Basic Service Set (IBSS)**. Instead of having an access point connected to the infrastructure, it will be selected the first node ("initiator") in the group to have his MAC address as BSSID. 

  Other than transmitting and receiving data for their own sake, each node is able to forward messages of the other nodes in the cell. This cell is **also called ad-hoc group because of the peer-to-peer aspect of it**.

  There is a little issue, if the initiator goes offline or outside communication range, the whole ad-hoc group cease to exist. It must be recreated with another initiator if necessary.

- **Extended Basic Service Set (EBSS)**. In this extension, **nodes are considered being in the same group even if they're connected to several different access points**, thus belonging to different cells. Collaboration of the access points of the interested cells is needed, creating a distribution service able to deliver messages from nodes of a cell to another while being in the same semantic group.

  - We can see how the values in the frame change when dealing with a distribution service

    | To DS | From DS | Address 1  | Address 2  | Address 3 | Address 4 |
  | ----- | ------- | ---------- | ---------- | --------- | --------- |
    | 0     | 0       | RA = DA    | TA = SA    | BSSID     | --        |
    | 0     | 1       | RA = DA    | TA = BSSID | SA        | --        |
    | 1     | 0       | RA = BSSID | TA = SA    | DA        | --        |
    | 1     | 1       | RA         | TA         | DA        | SA        |
  
    1. Intra-cell communication
     - In the first case there is the flags To DS and From DS to 0, meaning that the transmission is happening between nodes of the same cell;
       - Address 1 contains the receiver address of this frame, which coincides with the destination address of the node recipient of the message;
       - Address 2  contains the transmitter address of this frame, which coincides with the source address of the node which generated the message;
       - Address 3 is used to store the address (BSSID) of the access point used as intermediary of the communication.
    2. Incoming-cell communication
       - In the second case there is only the flag From DS to 1, meaning that the message is coming from the Distributed Service, in particular from a node of another cell;
       - Address 1 contains the receiver address of this frame, which coincides with the destination address of the node recipient of the message;
       - Address 2 contains the transmitter address of this frame, which coincides with the BSSID (address of the access point) of the cell containing the recipient node, the access point, once received this frame will forward this message to it;
       - Address 3 is used to store the address of the source node from another cell that generated the message.
    3. Outgoing-cell communication
       - In the third case there is only the flag To DS to 1, meaning that the message is going outside of the cell from which originated to the Distributed Service, in particular towards a node of another cell. 
       - Address 1 contains the receiver address of this frame, which coincides with the BSSID (address of the access point) of the cell containing the source node: the access point, once received this frame will forward this message to the Distributed Service.
       - Address 2 contains the transmitter address of this frame, which coincides with the source address of the node which generated the message.
       - Address 3 contains the receiver address of this frame, which coincides with the destination address of the recipient node of another cell. 
    4. Inter-cell communication
       - In the fourth and last case we have both flags to 1, meaning that the message is coming from the Distributed Service and is going to be re-forwarded to it. This is because probably this happens on a intermediate access point between the sender cell and the receiver cell;
       - Address 1 contains the address of the receiving access point, which cell contains the recipient node;
       - Address 2 contains the address of the transmitting access point, which cell contains the source node;
       - Address 3 contains the receiver address of this frame, which coincides with the destination address of the recipient node of another cell;
       - Address 4 is used to store the address of the source node from another cell that generated the message.

There is an additional BSSID called wildcard, with a MAC address of only 1s: this is used only for management and it's usage is limited.

#### Conclusions

All of these steps in 802.11 base WiFi require time to coordinate nodes with access points, even the simple operation of connection of a new node to an access points require seconds just to negotiate and establish a connection. This is unacceptable in VANETs, where every second matter and a late communication of critical informations could provoke a crash or escalate to a disaster.

For this reason, a lot of discussions happened between 802.11p supporters and 802.11 ones in order to decide what to keep as standard. As a result, 802.11p drifted away just enough to make VANET communication possible while adhering to most of the base IEEE standard.

## 802.11p WAVE Basic Service Set (WBSS)

In order to face the aforementioned challenge, the developers of this protocol decided some drastic but needed changes from 802.11 baseline.

Since vehicles can enter and exit each others radio range rapidly and because of the time-criticality of safety messages, being able to send safety messages instantly is imperative (long and short ranged ones). For this purpose only, **all vehicles can always communicate using the wildcard BSSID in these messages**, even if the belong to a different WAVE cell. 

This was not possible in pure 802.11, since each node could have been connected to one and only one cell. This means that every vehicle should have at least two radios: one always connected to the wildcard BSSID, hopping between the safety channels, and the other for non-safety frequencies.

For **non-safety messages**, instead, each WAVE BSSID will **send only 1 beacon packed with all the informations** on the offered services by that cell and on the configuration that the vehicle radio should adjust to. This is what is needed for a vehicle to connect to a WAVE cell. Differently from IBSS, the network group keep existing even after the initiator leaves, because the BSSID is not a MAC address but just a random value different from all 1s (because it aims towards a content-centric network).

## 802.11p Architecture

The main responsibilities of the 802.11p architecture are, as mentioned before, the **Physical and Data-link layers**, the latter with regard to the MAC sub-layer. Other functions of the stack are delegated to other technologies:

![](img/wavearc.png)

- **Security measures** are handled by IEEE 1609.2 standard and is traversal to **both Network and Data-Link layer**.
- The **Network and Transport layer** in their entirety are handled by the IEEE 1609.3 standard , which involves **TCP/UDP on IPv6 or WSMP** (Wave Short Messages Protocol). Its main roles are the connection setup, its management and the routing.
- The **Data-Link layer** is handled by the IEEE 1609.4 standard, this is mainly middleware developed to **hide the underlying complex channel characteristics** to the upper layers. The 802.2 protocol for the Logical Link Control (LLC) and our 802.11p for the MAC layer.
- The **Physical layer** uses the OFDM (orthogonal frequency division multiplexing) to reduce the possibilities of interferences between different transmissions.

## 802.11p Performance

Let's read some plots coming from a research that compares LTE (4G) performances against our 802.11p protocol.

![](img/perf.png)

Let's explain the terminology first:

- **Awareness range** = Maximum distance between communicating nodes of the same group;
- **LTE in-coverage** = Performances of LTE when a dedicated antenna is present to support the connectivity of the devices;
- **LTE out-coverage** = Performances of LTE when there's no dedicated antennas and the nodes need to adopt an ad-hoc group solution;
- **Update delay** = Time passed between two consecutive received beacons.

In the first chart the performances between LTE and 802.11p are kinda similar, with an acceptable packet reception ration even at longer distances and with groups with spreader area.

In the second chart, it's clear who's the winner here. 802.11p is just a bit slower than in-coverage LTE, however, what makes most of it, is the abysmal difference between it and out-coverage LTE delay.

What about 5G? Because of its current technology it can be a valid contestant to 802.11p, however it's still not developed enough to face all the hazards discussed for VANETs. Only time will tell if 5G is going to surpass WAVE or implement it to follow the standards while empowering them.

## VANETs Features

Let's see which are the main focal points of the VANETs, both strengths and weaknesses:

- **Limited mobility**

  - Mobility of the nodes (vehicles) is not completely free but is constrained by the city topography. Vehicles can only travel along streets by following road-signs and semaphores rules. This way it's possible to predict part of the movement of the nodes, however remember that they circulate at high speeds and can turn corners unexpectedly, making line-of-sight becomes a luxury, since radio signal cannot do the same.

- **Nodes are equipped with better resources**

  - Differently from WSNs, we have no problems about batteries, saving energy, computation power or memory capacity (to a certain extent). This helps our nodes greatly since there's no need to take all these problems into account, focusing on the new ones coming from mobility.

- **High number of devices**

  - Since vehicles are treated in urban areas, it's easy to come across networks of hundred or thousands nodes. For this reason one must be careful when sending too many messages simultaneously since collisions can still happen, potentially provoking loss of critical information thus leading to unpleasant situations.

  - A simple but effective solution for the collision problem was developed:

    <img src="img/nocoll.png" style="zoom:50%;" />

    1. Let's suppose a crash happens, the information must be forwarded rapidly to all vehicles in an area, so the first vehicle that detects the crash goes in alert and broadcasts a warning to all vehicles in his radio range. 
    2. The nodes which received this transmission need to forward it, but carefully, to avoid collisions. All vehicles wait for a random amount of time inversely proportional to their distance from the warning source. This means that the node farthest away from it will wait less time to rebroadcast the message than the closest and this brings several benefits:
       - The early rebroadcast of the farthest node serves as an "ACK" to the source, meaning that someone received his warning. 
       - This rebroadcast also tells to the nodes closer to the source (which already received the warning) to stop their timers (thus their rebroadcast) because it would be superfluous and detrimental, since it could create interferences and collisions.
       - It carries the warning to all his full radio range, bringing the warning to even farther nodes. The message practically made a single hop and traveled a long distance. Then again, the farthest node from this rebroadcast node will do the same as he did, spreading the message to even more vehicles.

- **Variable network density**

  - Unlike in WSNs, where sensors were deployed taking node density into account for several reasons (fault tolerance, monitoring accuracy ...), in VANETs network density is not as reliable since it's based on the traffic conditions, sometimes there could be a more dense and clogged network (like in rush hours or in a city) and sometimes a freer but node-starving one (at night or in a rural area for example). 
  - Sometimes there is a road-side system able to store past informations and dispense them to some passer-by vehicle, but it's not guaranteed.

- **Connectivity vs Competition**

  - When partitioning the network we need to balance the trade-off between: 
    - A better transmissive capability with longer links but more prone to collision. This is done by making the most of the range of the radio signal to reach even farther nodes.
    - A shorter range communication, providing less collisions because of the limited range but, at the same time, splitting the nodes in different partitions.
  - In conjunction with the previous matter, one can adapt connectivity to the network density and regulate this trade-off. If the area is crowded with vehicles, then a shorter range communication will be better for limiting collisions, but if instead the street less dense, a node can enhance its transmissive capabilities (transmitting at long range) in order to create as many links as possible.

- **Easier positioning**

  - Since there is no battery problems, it's easier to obtain positioning information thanks to on-board GPS devices, which works well outside (still we should consider the "urban jungle" signal problems).

## Existing Projects

Some successful projects we can take as example are Traffic Information System and CarTel.

## Traffic Information System (TIS)

TIS is a system developed with the **goal of gathering traffic informations**, both current and historical. The information gathering is done directly on vehicles, which are treated as sensors, obtaining from them their direction, position and speed. All these **informations are then forwarded to a roadside unit and sent to a computing cloud** in order to do the required computation.

With this amount of information, it's possible compute optimal routes that can:

- Avoid common traffic chokes, thus car crash risk;
- Reduce overall travel time, saving time and fuel (thus reducing pollution).

## CarTel

The fact that **cars are treated as smart vehicles, able to gather and analyze data** by themselves through the on-board sensors, made different applications flourish.

*Example*: a rising category of applications that makes the most of this are MyTaxi, PlanetTran or Uber. These services analyze traffic, road conditions and other informations in order to optimize the route between the destination of multiple passengers, with the goal of saving fuel while offering the service at a lower price (since the route is shared).

The same usage can also be used for logistics for transport route optimization.

![](img/tis.png)

*Example*: even if the green route is geographically longer, it would take less time since it's less trafficked than the red one, thus it would take less time to travel along that one.

## Car-to-Car (C2C)

C2C is an **European consortium for the development of a world standard** for Intelligent Transport System (ITS), which is the same goal seen when talked about ETSI: an unified world of intercommunicating vehicles that cooperate together.

Its other goals are helping in: 

- Development of driver assistance for better safety driving;
- Traffic control for time and energy saving;
- Find and allocation of free and unused frequencies for exclusive use of vehicular data traffic (like 5.9 GHz in 802.11p);
- Applications and services for drivers and passengers (toll collection, weather forecast ...).

Working under its wing, there are almost 20 research projects and more than 60 participants from research institutes to companies.

Safety application examples:

- Intersection collision avoidance, which gives a warning upon street rule violation;
- Public safety, that notifies when an emergency vehicle is approaching;
- Obstacle signaling, that notifies works in progress on the road and similar obstacles;
- Multi-hop accident signaling, which finds a problem and "bounce" it around to other vehicles.

### Application Requirements

For all these applications to work, though, three core requirements must be satisfied:

- **Speed and reliability** 
  - Emergency safety communications need to be transmitted with the lowest latency possible AND in a reliable way: cannot afford to lose a message that could help avoid a crash. It's easy to notice, though, that this requirement is in plain contrast with the characteristics of the communication medium (since wireless communications are usually broadcast).
  - TCP is slow because it is a reliable AND connection-oriented, which means it waits until all messages are received in order. Let's then split these two concepts, since reliability isn't always a synonym of slowness, and integrate it in those networks. An order for safety messages is not needed, but that they reach their targets reliably and quickly.
- **Privacy and security**
  - Data anonymity should be a priority since informations about vehicles movement associated to a person can disclose sensitive information about it. Sensible data like persons' habits (even private and embarrassing ones, such as going in a red light district) or private data (card credit number) could become of public domain, so think carefully how to treat and spread informations throughout VANETs.
- **Scalability**
  - This quality is also extremely important, since dealing with hundreds to thousands of vehicles at once.

But to test the solutions that we develop? They cannot be tested in a real urban environment without risking to damage other people if the vehicle behaves in an unexpected way because of a bug. For this purpose, a simulator for these kinds of networks is the ideal testing solution (VEINS, VEhIcular Network Simulator).

## A New Communication Pattern

Another critical aspect of VANETs, especially affecting safety messages, is that the communication is broadcast, not only the transmission. Usually don't want to talk to a single, specified vehicle but to a group of vehicles.

For these reasons, let's inspect the usual communication patterns, used in ordinary networks and such, and see how to evolve a new pattern starting from what is known:

- *Unicast communication*
  - This is the typical pattern of a one-to-one communication, in which the sender has to know the exact address of the recipient (IP, MAC or any other) in order to forward messages to it;
  - It's unlikely to use this pattern in our VANETs, almost always is preferred to talk to a multitude of vehicles.
- *Multicast communication*
  - This is the pattern of one-to-many communication, in which the sender has to know the exact addresses of all the recipients in order to forward messages to all of them. It still needs to know all the addresses of the group a priori, but it is a doable task in some situations;
  - *Example*: the communication of an event to all the police vehicles dispatched in an area, since the vehicles are known, it's possible could tag all of them with a certain unique address and pre-configure the on-board units to be part of the same group (IP-like addressing);
  - With multicast a sender could be able to share an information with all the nodes in that group (ex. "transmit to ALL the police vehicles in zone X").
- *Anycast communication*
  - A more reasonable implementation of the multi-cast is the any-cast, because usually it's not easy to reach all the nodes in a group and, in some cases, it's not even necessary;
  - *Example*: if there's a crash the objective is to alert "ANY ambulance at maximum 10 minutes away", not "ALL the ambulances at maximum 10 minutes away".

Following after the anycast pattern, the alert forwarding is connected to vehicles residing in a certain area. Since the pre-configuration can't be done for all the vehicles in the world, it must be done dynamically, when the connection happens.

Then why don't we take advantage of the GPS and the positioning, that comes for free given the VANETs features?

- *Geocast*
  - All vehicles in a certain area at a certain time (when the message is sent from source) belong in one group and addressing is based on that. Basically it's a multicast based on a geographical area, instead of a pre-determined address space.
- *Geographical routing*
  - Route the messages along the VANET by taking advantage of the known positioning of the nodes. Whenever there's the need to send a message, the path this message takes is dictated by the position of the destination. The path is then optimized based on that and on the intermediate nodes (those closer to the destination's node position are favored).

