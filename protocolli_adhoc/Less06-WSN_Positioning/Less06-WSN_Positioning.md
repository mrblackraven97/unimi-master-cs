# WSN Node Positioning

It was previously discussed about how nodes would use their known position to **enrich data** with location informations or, as we saw in **routing**:

- Directed Diffusion uses interest description to filter out data outside of a certain location;
- Rumor Routing could use the localization to spread agents following NESW axes in order to increase intersection probabilities of query and agent.

Additionally, in challenged networks, it's possible to adjust transmission power spent by a node in order to be heard by the receiver. By knowing its position, one could fine-tune the power spent for transmission. 

Furthermore, short hops are preferred in challenged networks, however what could happen if some nodes became isolated because their neighbours started to fail because of errors, battery depletion or other conditions? 

By taking advantage of positioning we could **increase the range of neighborhood**, in order for these isolated nodes to find new neighbors and communicate with the rest of the network. This comes at a greater expense of energy of course, but at least that node can do his job until the end of his lifetime and help reach the network's greater goal.

Positioning helps challenged networks in many ways, however, how do the nodes know their relative or even absolute position, especially in unplanned deployed WSNs?

Let's start by defining what is the absolute or relative localizations:

- **Absolute localization**: A localization based on external means from the network point of view.
  - *Example*: using geographic coordinates of latitude, longitude and altitude of nodes.
- **Relative localization**: A localization based on the network's internal properties.
  - *Example*: using distance, signal-to-noise ratio between nodes.

![](img/pos.png)

*Example*: if nodes don't have an absolute and precise position, the two networks in the figure could appear as similar and indistinguishable. This is because relative positioning is not sensitive to nodes' rotation.

Let's get into the central-most node in the network point of view, he know for sure that he has a certain identifier and, connected to him, there are 4 nodes. He doesn't know how they're placed and who is up or down, since that node only receives the signal from them and just acknowledges that they exist. So for that node, both network layouts, even if rotated, appear the same to him.

## Localization Prerequisites

In order to have a reliable localization in our network there must be:

- **Guarantee operations**, even in harsh and adverse conditions that could interfere with nodes communications. 
  - Blocking obstacles, such as line-of-sight blockers, uneven terrain and so on;
  - Reflecting obstacles, such as rain, metallic objects and others, which can bounce back waves and create interferences (ex. unidirectional antennas on hot rooftops).
- **Limit energy consumption,** especially for non-rechargeable nodes;
- **Measure with accuracy and granularity**, depending on the application needs;
- Look for **infrastructure availability**: 
  - Nodes with GPS or of which position is known are called **anchors**: they're useful to be used as starting point for relative positioning of the other nodes.
  - Hybrid and heterogeneous networks typically use this approach: some nodes are deployed in a planned way and act as anchors, then the others are deployed in an unplanned way and calculate their relative position based on the anchors.
- Taking into account **cooperative and non-cooperative target**:
  - The object to detect and localize emits any signal or not? If they don't, things could get even more difficult, for this reason, in this lecture there is the assumption that nodes are cooperative.
- Taking into account **node mobility**, because If nodes move, their position must be re-calculated and re-localize them.

## Relationship Between Sensors

To calculate relative positioning between nodes there are different criteria and techniques at disposal. To summarize, those are:

- Proximity;
- Distance;
- Angle;
- Acoustic signal.

Usually, proximity and distance are the more apt for WSNs and, for this reason, there's more focus on them and just outline the other two methods.

### Proximity

The proximity rule establish that **two nodes are related if they are in the same radio range**. This **value is either true or false** so it can be a little imprecise to base positioning off of that, however it's the **simplest and easiest solution**.

<img src="img/prox1.png" style="zoom: 150%;" />

In these examples, these two nodes are both in reciprocal radio range in both cases, so they are related. However it's not possible to discern how much they are close to each other: in the first case they are further away than in the second case. This can reduce accuracy of the estimate since we have no way to tell how distant they are.

Other things to take note of:

- Signal fades over distance, so the range isn't a perfect circle but more of a "bell";
- The multi-path fading effect, in which the range, again, won't always be a perfect circle since the signal could bounce off objects and walls. A node could wrongly think to be in a range of another node just because he received a momentary reflection of that signal.

However this method can **give "good enough" estimates on absolute positions**, provided that anchor points exist. A sensor can approximate his position based on which anchor points he can "hear" (that have it in their radio range) and their known position. 

The **more anchor points exist, the more accurate** will be the relative position estimate.

![](img/prox2.png)

In this example, a sensor can understand that his position is somewhere in between of the three anchor nodes because he can hear all three of them: he knows that he resides in that intersection area however he cannot pinpoint his exact location because of the intrinsic inaccuracy of the proximity method.

### Distance

Instead of settling down for the the simple on/off behaviour of the proximity, let's try to **calculate the effective distance between two nodes**, evaluating the pros and cons of the methods used to achieve it.

#### Signal Strength Method

One first approach we could try is **measuring the signal strength of the nodes** a node can hear in order to create an indicator and compare them with it. 

The RSSI (Received Signal Strength Indicator) was developed with this intent, this approach has some notable facts:

- The signal strength depends heavily on **environmental agents** (like multi-path fading, obstacles and such).
  - *Example*: because of multi-path fading I could have my signal bouncing off objects and distorting the estimate by interfering with itself and giving a false measure of it.
- The **initial transmissive power must be known** and common between nodes in our network in order to make assumptions based on how much it fades, thus how far is the source.

Given `R` as the distance from the source, there are different models to represent loss of transmission power due to distance:

- **Usual exponential decay**, where waves decay on average with the square of the distance, thus RSSI is usually calculated with `1/R`<sup>2</sup>. 
- **Tunneled decay**, if waves from source are wave-guided through tunnel-shaped environment, decay is slower and RSSI is then calculated with `1/R`<sup>1.5</sup>.
  - *Example*: Bluetooth device experiment in UNIMI in a corridor (devices communicated at long distance because the corridor acted as a tunnel).
- **Terrain-close decay**, if the source is close to the ground, the RSSI is calculated with `1/R`<sup>4</sup> because the terrain is going to absorb most of the waves, making communication harder.

![](img/rssi.png)

In this graph it's possible to see how the exponential decay affects the RSSI between two nodes at various distances:

- The blue crosses represent the actual measurements;
- The red line tries to approximate with a non-linear regression the various measurements (each red dot has confidence intervals).

RSSI approach requires particular hardware in order to be able to measure each other signal strength and it has to be accurate enough. Usually the measurement unit is the dBm, which is the mW power of the signal transformed in decibels.

All in all, this method is viable but the requirement of special hardware and the intrinsic inaccuracy of measurements caused by environmental agents doesn't make it the ideal approach.

#### Time of Arrival (TOA) / Time of Flight (TOF)

Instead of considering the signal strength, let's **calculate the distance based on how much time passes between the delivery and the receipt of a message from the source**.

The distance can be calculated indirectly by :

​				`distance = propagation_time * propagation_speed (300000 Km/s)`

Differently from RSSI, the **accuracy of distance estimate** is independent from the environment however it relies more on:

- The clock synchronization between sender and receiver;
- The capacity of the nodes of measuring even extremely small displacements of time (if nodes are really close, the time of travel will be small).

The **alternative to this is measuring the round-trip time** ("pinging" a node), which is the time spent for the message to go from sender to receiver and back again to the sender, however, in this case, remember to subtract from the equation the time spent waiting by the receiver or the time spent elaborating a response because the only interesting thing is the effective time for the message to travel back and forward.

This method is by far the simplest, since it doesn't need extra hardware on the nodes, however the **downside of this approach is the clock synchronization**, because, even if we manage to synchronize the clocks through a protocol (which is already hard to do properly), the known problem of the clock drift must be countered in order to avoid later desynchronization. 

Furthermore, nodes must be able to measure small amount of time to ensure measure granularity.

-  *Example*: for indoor nodes travel time is small because the distances are small too.

### Time Difference of Arrival

To avoid measuring small amounts of time and the consequent problems faced in TOA, here we consider a Difference of Times of Arrival (TDOA). In particular, **measuring the difference between the time of arrival of a message sent simultaneously through different antennas with different propagation speeds**.

*Example*: a node could measure the difference of the travel time of the message between ultrasound waves (slow propagation speed) and wireless radio waves (faster propagation speed), thus through different technologies. When doing this, be careful of the environment in which devices are deployed because ultrasounds can create problems in an urban environment (cats, dogs can hear these frequencies and for this reasons aren't allowed).

With this method we would gain better accuracy and less problems than TOA, however the nodes must have the right **hardware and equipment** to be able to do these measurements.

### Angle-Based Distance

If someone could mount an array of antennas on nodes, it could **measure after how much time the source signal is received by each individual antenna** and, based on that, determine the distance of the source.

![](img/angle.png)

As described in the picture, the source hits the array at a certain θ angle, this means each antenna will receive the signal at a different time and this difference helps calculate the distance similarly as how it happened in TDOA.

As in other methods, **additional hardware is required**. The accuracy is good, however antenna arrays are large devices, thus are impracticable to be mounted on sensors in our WSNs. Unfortunately, size can be a problem in some cases.

### Acoustic Signals

Using acoustic signals to determine the position of the nodes offer some advantages:

- These kind of signals don't suffer from multi-path fading, unlike radio waves in RSSI;
- They decay similarly as `1/R`<sup>2</sup>.

but also some disadvantages:

- They suffer hardly from obstacles in the way, similarly to radio waves;
- Emitters and receivers can be a lot larger than the sensor (size problem).
  - As TDOA, whenever working with acoustic signals like ultrasounds, a proper hardware is needed, which is bigger than the sensor itself.

To sum up, this method is not really the most suitable one for WSNs because of the disadvantages outweighing the advantages.

## Trilateration

Now that is possible to estimate the distance between pairs of nodes, excluding rumor, errors in measurement and other problems, what can be done to map the nodes? Until now, it was only revealed if there is a link between two nodes and how distant they are, still far from the nodes knowing their position relative to others.

Let's try to use the information gathered until now to try to find a location for our nodes.

The **triangulation method** was used since the VI century A.C. in cartography, to understand where the ship was located by measuring the angles between the horizon and fixed stars. As we can see, this method would **require for our nodes to measure of the angle** from which the signal is coming: however we don't know this information since we only could obtain the distance between nodes and nothing else.

Instead of triangulation, the **trilateration method** is preferred, which is suitable for us since it only requires the measure of the distance between nodes, which is known from the previous step. On a plain surface, **by measuring the distance between three points, it is possible to obtain the position of a fourth point with respect to them**.

<img src="img/trilat.png" style="zoom:150%;" />

Let's consider a sensor that wants to know his own position: by listening to his surroundings, he picks up three other nodes' signals and estimates his distance from each of them with one of the methods we discussed before.

Each **circumference represents all the possible locations the sensor could be**, since the radius is exactly the distance measured by the previous step. However, since the sensor hasn't heard only one node but three, **he has to be in all three circumferences simultaneously**: to find its location, it is just needed to solve a system of equations between circumferences and find the points on which they intersect.

Why are three nodes needed? Because if only two circumferences were intersected, there could be some cases in which there would be two intersection points and have no means to disambiguate which one is the right location for the sensor. With three circumferences, instead, they will surely have, at best, only one point in common.

Of course, **real world examples should consider spheres** instead of circles and different formulas and the equations for the 3D scenario, but the principle is the same.

In fact, in 3D space, GPS find the devices' position like this, where 4 spheres are needed, which means 4 different satellite signals. The intersection between two spheres is a circle, between three it's a pair of points (like the 2D example) and the fourth gives the unique position between those two points.

![](img/tril3d.png)

Sometimes finding out the right intersection without the fourth satellite is not difficult since the right point should be on the earth surface (and not in space nor underground!); in any case, using the fourth sphere still helps with accuracy of the measure.

## Math Zone

### 2D

Given `R1` and `R2` equations of circumferences centered in  `(α1, β1)`, `(α2, β2)` respectively, the following system can be created:
$$
\begin{cases} 
R1^2=(x - \alpha_1)^2 + (y-\beta_1)^2 \\ 
R2^2=(x - \alpha_2)^2 + (y-\beta_2)^2 
\end{cases}
$$
From this two solutions can be obtained for `(x,y)` (the position of my sensor) since they are 2nd grade equations, and the third circle will help us identify the right one.

### 3D

Given `(α, β, γ)` as sphere centers and `(x, y, z)` as the location that needs to be found, the sphere equation results as:
$$
R^2 = (x - \alpha)^2 + (y-\beta)^2 + (z-\gamma)^2
$$

## Trilateration Usage

By using trilateration, the relative and absolute position of a node can both be obtained, given other three nodes. 

Before it was explained how to deploy some **nodes in a planned position so that they could act like anchors**, and since anchors know their absolute position (because it's hard-coded inside them), they can be **used to determine the absolute location of other unplanned nodes**.

*Example*: if an unplanned node wants to know his own absolute position, he needs to be able to find three anchors and start the trilateration on the distance from them.

The **positioning of these anchors is critical** for other nodes to be able to hear at least three of them. This is a problem of the Operative Research field and also an hard one (NP), so usually the optimization is found using heuristics.

An alternative solution was developed and involves **mobile anchors**: a robot (UAV or UGV) moves in the area constantly and is equipped with a GPS. 

<img src="img/trilatmob.png" style="zoom:80%;" />

When he's close to a node, he **measures his distance from it three times from three different positions while it's moving, in order to create the "three circumferences" as the three anchors**. From these distances, he's able to calculate the nodes position (relative and absolute) and send this information back to him. From here, the only problem to solve is the optimization of the path covered by the drone to make it reach all nodes in a network.

If anchors can't be placed, then it's still possible to obtain the relative positions between nodes because in some applications geographical measuring is not even needed, for example when this information is used to route messages toward the sink in directed diffusion.

## GPS: Global Positioning System

The GPS consists in a **constellation network of 24 satellites circling around the Earth** along the non-geostationary orbit.

![](img/gps.png)

This system is able to **give to any device an absolute position** (altitude, longitude and latitude) and a time signal through broadcast transmissions, each of the two in his own frequency band.

To receive these information from the satellite **nodes must have line-of-sight**, so "being outdoor" is required in order to use GPS effectively. As long as a node is able to receive the signal from at least 4 satellites, he can calculate its position without problems.

The **precision is good**, the estimate is usually (95% of the time) within an error of 15m away from the actual location and can be enhanced with other systems for example trilateration through other wireless devices. This is talking about civil uses, while for military ones the precision is a lot higher (for obvious reasons).

The problem here is that it **spends a lot of energy while trying to obtain the signal from the satellites**, so it must be used cautiously, especially in challenged networks. Usually one time is enough for fixed sensors, while it must be done periodically for mobile sensors, trading off location precision for battery or vice-versa.

## Distance Estimate Procedure

Let's see the procedure through which a node receives transmissions from satellites in order to obtain its position:

1. The receiver node must find the satellites in his "own sky", which means the four closest satellites from which he receives the best signal. 

   ![](img/vissat.gif)

   - This means that as the Earth moves, the satellites won't always be the same through time and only a part of them will be "visible" to our node.

2. It locks on these favorite four satellites, which means the node keep listening to their signals in order to obtain different informations called Ephemeris or Ephemerids.

   - These ephemeris are tables containing different pre-calculated values, in a certain amount of time, about astronomical values of the satellite (orbital values, coordinates ...).
   - The duration of this lock is the amount of time needed to obtain all the ephemeris of those four favorite satellites: ephemeris are transmitted every 30s but since usually are needed up to 24 transmission to obtain the complete informations, 12 minutes are required in total.

3. The ephemeris are memorized in an almanac, for future measures.

   - To avoid other lock-ons, we memorize the ephemeris: however if we move away from our current location for more than 100 Kilometers or the node is inactive for more than two months, then we have to re-obtain this information from the satellites (the same or different ones).

4. The receiver node synchronizes his own clock with the satellites' one.

To measure the distance, nodes do exactly like they did in the TOA method: the satellite emits periodically a code that varies through time (pseudo-randomly on 266 days) representing, through an encryption, the satellite position along its orbit. The node, upon receiving it, will: 

1. Decrypt the code using his almanac;
2. Find out the position `(α, β, γ)` of the satellite in 3D space by searching for the corresponding code in the ephemeris;
3. Measure how much time passed from when the code was sent up until its reception: by using TOA (time of arrival), the node can estimate the distance from the satellite, thus the radius of the sphere;
4. After finding all four spheres from the satellites, the node can use trilateration to approximate his own position.

Common problems we can encounter are:

- Disturbed signal and multi-path fading caused by the environment, like urban jungle, thick foliage, ionosphere phenomena and others can lessen the satellites' signal, impairing GPS accuracy;
- Clock drift can be a problem when calculating TOA: the re-sync every two months is useful also for this reason.

## GPS Alternatives

As good as GPS can get, it will always be a bit problematic in WSNs since, as they're challenged networks, saving energy is a serious issue and, GPS consumes a lot of it. Furthermore, WSNs can be also present indoor or in hostile environments for GPS signal, so this isn't always a viable positioning solution.

So what other positioning methods are there? Well, some methods before GPS were already proposed, such as trilateration through anchor points or simple relative position through proximity, distance and other means. 

## Anchor Beaconing

Let's push forward on the anchor idea. Suppose that there are a **bunch of carefully deployed anchors**, having the same radio range, which **periodically sends beacons in broadcast containing their own position**.

Working in an ideal world:

- Each node would have an omnidirectional antenna, transmitting at the same power, thus having the same range. Since each anchor has the same range, nodes that hear a beacon can immediately understand that they're under an anchor's area of influence.
- There wouldn't be any interferences of sort and a perfect line-of-sight between nodes, so that the propagation of the signal is a perfect sphere.
- Close anchors are able to desynchronize the sending of their beacons in order to avoid collisions between them and interfere with themselves.

<img src="img/beac1.png" style="zoom:150%;" />

Let's see the example in the image: a node is trying to retrieve its own location by listening to the beacons. Supposing to be able to listen only anchor 1 and anchor 2, this means I'm somewhere in between their circular range.

<img src="img/beac2.png" style="zoom:150%;" />

However if only hearing from 1 and 2, and can't hear 3 and 4, it is possible remove the area covered by them from the intersection.

<img src="img/beac3.png" style="zoom:150%;" />

This leaves with only the remaining blue area: the node is somewhere around that area.

To increase position accuracy, an idea is to increase the number of anchor points and, also, try to **position the anchors in a way that the sensors are inside of the "anchor square"** (or simply closer to the middle). We need to keep in mind though that increasing the number of anchors increases the need of energy, so from application to application we must measure the trade-off.

<img src="img/beac.png" style="zoom:150%;" />

We can see that with 9 anchors, intersections are littler, thus the measure should be more accurate.

This model seems good but is **not advised to use it indoor** for two reasons: 

- This model is ideal and while it can work outdoor, it won't indoor. All the anchor circles won't be like in the picture and will be distorted since there will be interferences, multi-path fading, reflections and so on. For these reasons, the accuracy will be low too.
- There still is the optimization problem to solve for the positions of the anchors at the pre-deployment phase and their configuration.

**N.B.** This method is different from trilateration, but is more similar to proximity (on/off).

All in all this method is a good base to start from, let's try to refine it.

## Centroid-Based Method

This method is based on the anchor beaconing. There is a node that wants to know it's position, it will listen to the anchors for a period `t` such that if:

- `T` is the time interval between the sending of beacon samples from the anchors;
- `S` is the significant number of samples wanted to be received from the anchors.

Then the amount of time spent listening should be `t = (S + 1 + ε) T`.

Where the `+1` represents that unlucky moment where the node starts listening just right after the anchor sent the beacon sample (better safe than sorry), while the `+ε` represents a little error that represents various problems that can happen (clock drift, desynchronization, latency ...).

The best anchor points from which we should listen are chosen through the **connectivity metric**:

`CM_i = 100 × Nrecv(i,t) / Nsent(i,t) = 100 × Nrecv(i,t) / S`

Through this a node can measure how good he's receiving these beacon samples from anchors. If in the period of time `t` the anchor sends 100 samples and the node successfully receives 98/95 of them, then the signal with the anchor is good. Since this works on a wireless channel, a bit of error can be tolerated (2/5 lost samples).

The signal is good from an anchor if the connectivity metric CM is greater than the connectivity threshold, which represent the minimum acceptable ratio between samples received and sent and is chosen accordingly to the network needs. In the earlier example, the threshold could have been 90, so the signal with it is good.

**If the signal is good then the anchor is trusted**, otherwise all the information coming from that anchor is discarded because probably not complete (since it doesn't pass our threshold test). The fact that an anchor is not trusted could mean several things:

- The node is almost outside the range of the anchor, so that means that signal is so low that some samples come through but not the majority of them. 
- The node could even be outside of the range of that anchor and still receive some samples because something could reflect or guide the signal, even momentarily!

After this, **the node gathers the positions of all the trusted anchors and calculates a weighted average for the `Xs` and the `Ys`, in order to find a centroid** on which estimate his approximate location (barycenter).

`(X_est, Y_est) = (Σ X_ij/k, Σ Y_ij/k)`  with `k` as the number of trusted anchors.

This proximity method has been experimentally proved to be quite accurate, with an average error < 2m and a max error < 4.5m, given S = 20 (number of significant samples). 

However, this methods works well outdoors, safe from heavy multi-path fading problems, but what can we do indoor?

## Signal Pattern Matching (Footprint)

Before anything else, deploy in the environment some signal sources. Based on these, let's create a **grid map of received signal** (RSSI) by measuring it with the final devices on every point of the area: from this we can highlight areas with high and low signal, identifying emergent problems and features of the environment (area + objects) while getting an idea on how the fading influences signal in these areas.

When the final devices are deployed, they evaluate the signal, comparing it with the data contained in the map and then determine their own position based on the **signal properties** in that particular point they're located.

This method requires a great preparation phase, since we need to map every point in the area and position the signal sources in a smart way.

#### Short-Ranged Proximity

Alternatively, we can use the same proximity technique we discussed about before but using extremely short ranged devices, in order to avoid the multi-path fading issues we talked about before.

Some examples of these are RFID scanners o IR (infrared) waves, which aren't able to go through walls or bounce off of them. For these reasons, all the devices in a network like this should all be in one room.