# Geographic Location Service

Geocasting and geographical routing were already discussed, but in order to make these algorithms work an underlying structure is needed, able to provide a reliable and steady location service.

First of all, what is a Geographic Location Service? Let's see the definition of one of the implementation of them, Grid Location Service:

> GLS (for short) is a new **distributed location service which tracks mobile node locations**. GLS combined with geographic forwarding allows the construction of ad hoc mobile networks that scale to a larger number of nodes than possible with previous work. GLS is decentralized and runs on the mobile nodes themselves, requiring no fixed infrastructure.

A location service, in order to be able to work in highly dynamical "mobile ad-hoc networks", such as our VANETs, needs to be **highly adaptive and reactive** to all the possible (and sudden) change in topology of it.

## The MANET Approach

In the past years, this issue was partially solved by MANETs, the precursors of VANETs. They supported a totally decentralized communication, without the need of an infrastructure to support it. **Each node can be a router**, other than a end-point of the network, which means that they can act as intermediate nodes and forward others message around, routing them towards the recipient.

They had some improvements from the traditional network routing algorithms (RIP and OSPF for Internet), like introducing a reactive routing instead of the proactive one. 

However, these traditional routing algorithms for MANETs are outdated for our VANETs because they **weren't designed for highly mobile nodes** as those in VANETs (cars), but slower ones like humans or robots/drones. Consequently, they aren't sufficiently optimized because:

- **High overhead**. To update the topology, each node has to resort to **flooding**, trying to find a node that knows a path towards the recipient. This process is bound to add some delay to the message forwarding and is costly, since flooding is usually O(n<sup>2</sup>) complexity (with `n` number of nodes).

- **Informations becomes quickly obsolete**. Since in VANETs nodes are more mobile than what was expected in MANETs, the already **costly information obtained with flooding gets obsolete quickly**. Nodes can enter and exit our network suddenly, thus frequently disrupting the known topology and requiring new costly updates.

Both these problems brings us to the **"relativistic" distance effect**:

![](img/dist.png)

1. A needs to send data to K, so in order to find a route to it he starts a flooding.
2. After some hops, K receives the request and answers back to A that he received the routing request, now A knows which path to use to send data to K (which is the reverse of K-J-I-H-G-F-E-D-C-B-A).
3. However, by the time A sees the new topology until K, network could have changed since that information is from 10 hops ago. what if then, while the answer is going back to A, a node leaves the network because it's not anymore in signal range (like I for example)? Remember that this is common in VANETs, where nodes are so mobile that this is the norm.

The information can get obsolete even before it reaches the node that started the flooding. it's **useless to create a knowledge base for the topology when it's ever-changing**. This suggests us that to find a new solution from scratch and completely throw away this model, that is still reminiscent of the Internet routing mechanics.

Its not affordable to create a complete path towards the destination. The main idea here is to **forward the information to the best possible neighbour**, where with best we mean the one that can get the message closer to the destination geographic coordinates. 

*Example*: A could choose B between his neighbours because he his in the closest to the direction towards K coordinates, then B will do the same, choosing C for the same reason. Routing cannot be done by the node A alone, but **every node in the path has to make the best decision** between the forwarding choices he has. 

To recap, the needed a routing should be one that:

- Is **reactive and on-demand**. The node worries about forwarding a message only when he receives one;
- Uses the **local state of the node** that is forwarding the message, instead of using a path chosen by the source;
- **Knows the neighbours habits** and takes advantage of them when choosing to whom forward the message:
  - Where they're going (position, interests): if a node is moving towards the destination of a message, the message should be forwarded it to him;
  - Who they're frequenting (sociality): if a node comes up against the destination of a message frequently, it's a good idea to forward it to him.

## Geographic Forwarding/Routing

The main idea of geographic forwarding is to be able to **make routing independent from the connectivity status of the network**, because relying on it is detrimental in VANETs.

Let's follow the tenets of an ideal routing for a VANET listed before: there is a source node S that wants to send a message to the destination node D.

S doesn't know the topology of the network except for his neighbours, as seen in the picture, however, he does know the rough coordinates of the destination node which are (932, 1097) and his coordinates (128, 173). With this piece of information in hand, he does what we would do immediately: choose the neighbour node closer to the destination node, because he has the highest probability of reaching the destination.

![](img/geofw.png)

**S chooses as his next hop the one closer to the diagonal representing the direction towards the destination D**. That node then will do the same to forward the message again, until D is, hopefully, reached.

Using only local decisions is a big advantage since:

- There is no need to know anything about the topology of the network except the neighbours;
- Step by step, the route is getting more and more accurate towards its destination;
- Once forwarded to the next node, the sender node can stop caring about it and do something else.

However since only local decisions are made, like a greedy algorithm chooses the local minimum, wrong choices could happen and limit ourselves in the future, forcing ourselves to follow a sub-optimal route.

![](img/ang.png)

*Example*: here S could be choosing the right neighbour because it's closer to the destination following the diagonal, this is because this choice is not taking buildings and the street topography into account. 

Since streets are the only place in which vehicles can be, its possible to **assign different weights on the choices depending on the topography**. Also it's possible to over-engineer this by adding the neighbour habits to optimize even more the choices. However let's keep in mind that adding information to process while choosing the neighbour means adding information to the beacons and **heavier beacons means bigger delay** and longer transmission time, with increased risk of collision. To sum up, just avoid packing them with too much data and put only the necessary.

Another problem to care about is **network partition**. It could happen that some streets are not trafficked enough at some time of the day, thus we could have an irregular and uneven distribution of nodes in our network.

![](img/part.png)

*Example*: S would still try to forward the message to his neighbours but reaches a local minimum with the yellow node because there isn't any neighbour able to reach the node D, since the network is partitioned.

What can be done here is **enabling some kind of caching of the messages**, so that when the yellow node finally gets in the range of the nodes in the other partition of the network, he's able to forward the cached message to them so that it hopefully reaches the destination D. This is why **these kind of networks are called opportunistic**, because whenever an opportunity to deliver the message shows up, they make the most out of it.

This approach derives from the **store & forward** behaviour that also exist in Internet routing algorithms, when the packet is stored in the router while it calculates where it should be routed and then it is forwarded. If the next hop doesn't exist and can't be calculated, then the packet is discarded because it doesn't know what to do with it.

In our VANETs however it is preferred a modified version of this approach, the **store, carry & forward**. Instead of discarding the packet, the **node keeps it stored and carries it around** with him until he finds another ideal node to which forward the message. This works because the approach relies on mobility to be able to overcome partitions and restore links towards the destination node.

## Geographic Routing System Assumption

After discussing how geographic forwarding/routing works, let's see which assumptions was made on the underlying system to make it work:

- **Local and neighbour position for each node is needed**

  - This can be obtained through various methods seen in the earlier lessons: GPS, trilateration with anchor point ...
  - A node can know the position of its neighbours through a beacon they send periodically.

- **Location service**

  <img src="img/location server.png" style="zoom:80%;" />

  - It is needed some kind of distributed database through which, given a node identifier, we can get that node coordinates.

- **Data moves faster than the nodes (mobility assumption)**

  ![](img/mob.png)

  - This is a required assumption because if nodes could move faster than data, then delivery could be impossible to achieve.

## Location Service

A distributed database is needed that, given a node identifier, could answer with that node coordinates to this query. Some solutions were developed over time, except from the usual and trivial flooding solution there is a **"static" solution** provided by a system similar to MobileIP.

The "static" solution expects that **each node is connected to a fixed base station** called *home* which is always updated on the position of the nodes he tracks. The problems of this location service arises easily: 

![](img/stat.png)

- What if the home is far from the source when the destination is close? Even if the destination is right beside the source, it still has to wait for the query to arrive to the home and come back with a reply → highly inefficient.
- What if the home is unreachable? → Single-point of failure.

## Grid Location Service (GLS)

**GLS** is a brand new solution for the problem of the location service that tries to overcome these problems. First of all **each node has a set of distributed location servers distributed into the network**. This way:

- There are many location servers spread through the network instead of only one, so it's easy to choose the closest one and avoid waiting too much for the query → Optimized.
- There is no more a single point of failure since, if one location server crashes, the node has alternative ones → Fault-tolerant, load balancing.

The final goal is that **the research for a close node should be found immediately in the neighborhood**, and the cost for a query should not exceed the distance from the queried node (like it happened in the "static" home approach).

Here too there is a set of assumptions to fulfill:

- Geographic forwarding is used for every message in the network: location queries, replies, updates and all data follows this forwarding paradigm;
- Routing is allowed on a 2-hop neighbourhood. This means that we know how to reach a 2nd hop node by using a 1st hop node as intermediate (can be achieved with short 2-hop distance vector algorithm);
- Nodes have knowledge of a common grid overlaid onto the area;
- Network is usually dense of nodes such that it's rare to find uncovered areas and partitions (this is a facilitating assumption).
- Clock are synchronized through GPS.

What is "grid overlay"? Essentially, the goal is to **discretize the covered area in easily addressable sub-areas that follow a hierarchy** so that when any vehicle enters into it, a totem provides them a copy of the grid so they can orient themselves through the location service.

![](img/grid.png)

This virtual space starts from the origin (0,0) with the first order-1 square, which is an area in which all vehicles know each other and can reach their 2-hops neighbours. To satisfy the relative assumption the diameter `d` of each square is set to: `d = √2 * radio_range`.

When the areas grow with these "unit squares" they can be regrouped n higher square orders. As in figure, 4 adjacent order-1 squares make up for a order-2 square, 4 adjacent order-2 squares make up for a order-3 square and so on until all the required area is covered. 

The yellow square in the middle is NOT an order-2 square because, to be so, he needs to be surrounded by other order-2 squares, but instead, it's surrounded by only order-1 squares, thus its positioning is clearly wrong. 

For simplicity, there is the supposition that everything happens under ideal working conditions like, for example, that all nodes boot at the same time and have a copy of the grid. 

Let's see the algorithm in action.

## Network Construction

In this step nodes in the same 1-square (short for order-1 square) have to be connected so that every node in it know each other:

1. Each node periodically sends an Hello message, containing his ID and his location (obtained through GPS);
2. Nodes will find first all the neighbours at 1-hop distance that are in the same signal range, but not all nodes are supposed to be found through this phase because two nodes in the same 1-square can be outside each other signal range;
3. Each node shares his newly acquired knowledge (the set of his neighbours, with ID and position) through a new Hello message, this way, two nodes can find the missing 2-hop neighbours that weren't found in the 2nd phase.

***Hello from outside***: it could happen that a node A inside a 1-square could hear Hello messages from nodes of another adjacent 1-square. Fortunately, this won't lead to any problem since A will check his position on his grid and, if the Hello isn't coming from nodes in his square, he will ignore them.

***Speed estimate***: Another information that could be forwarded through messages is the speed of the node, since this could make easier for other nodes to estimate his location with more accuracy and even make predictions, extrapolating the result. However, thorough simulation tests have proven that this information is not very reliable because could be misleading.

Lemma 1 (Efficiency): if a query from S to D, with S and D in the same 1-square, then query arrives on D in 1-step, where step can correspond to multiple hops in the network.

For a k-square with k>1, an initial assumption is that nodes are still.

Let's see an example of it in action:

![](img/constr.png)

1. Each node sends `<ID, position>` to his neighbours;
2. Each node finds its 1-hop neighbours:
   - 21 find 78 (even if 6 is in his range, 21 sees that 6 is from another 1-square so it ignores his hello and 6 does the same);
   - 78 finds 21, 83;
   - 83 finds 78.
3. Each node shares the knowledge of his neighbours to find (and help find) 2-hop neighbours:
   - 21 finds 83 as 2-hop neighbour and 83 does the same, both through 78.

Now that each node knows all the other nodes in his square, some nodes must be elected as location servers in order to be able to route messages to nodes of other squares because, until now, every square knows nothing about the others.

## Location Server Selection

In this step enough location servers must be found in order to be able to pinpoint a node whenever there's a query. To do so, a selection on them is made, in a way that they are **denser the closer they are to the node**. This means that for each k-square in which the queried node `n` is located, a location server is needed in each (k-1)-square (sub-squares) of that k-square in which `n` is NOT located.

A node `n` will select other nodes from the other squares as location servers, who will hold the information of his position so that `n` can be found by other nodes from other squares. Will choose:

- 3 location servers in the 2-square, 1 for each 1-square in which `n` isn't located;
- 3 location servers in the 3-square, 1 for each 2-square in which `n` isn't located;
- 3 location servers in the 4-square, 1 for each 3-square in which `n` isn't located and so on...

The criteria for which the node will choose a node as location server among others in the same square is the ID. The **chosen node will have the smaller ID of all the nodes greater than the ID of `n`** (in a circular way). 

Neither `n` knows who they are, location update is geographically forwarded to the square in which the location server interested is located (geographic anycast). From here, the update goes on until the appropriate node as a "fake query", leaving update where querier go seek for it.

Let's see an example:

![](img/lss.png)

Let's suppose to select the location servers for the node B with ID 17. As said, the needed operation to do that are:

- 3 location servers in the 2-square, 1 for each 1-square in which B is not located:
  - The upper one only holds 63 so that will be the location server for that square;
  - The left one has 2 and 7: since we said that the lesser of the greater nodes in a circular way is selected, then 2 is the chosen location server: 
    - Supposing that `max_ID` is 99, then when confronting 17 with 2 or 7 we need a greater value than 17 for the comparison, and in order to do so, just sum `max_ID` to the nodes lesser than 17 so that 2+99 = 101 and 7+99 = 106. Between the two the lesser of them is 101 which means 2).
  - The corner one has 41 and 23: it's easy to see that 23 is the location server since it's >17 but <43.
- 3 location servers in the 3-square, 1 for each 2-square in which B is not located
  - The left one has {14, 26, 44, 87}, so the location server chosen is 26 because the min of those bigger than 17;
  - The bottom one has {12, 43, 55, 61}, so the location server chosen is 43 because the min of those bigger than 17;
  - The corner one has {31, 32, 81, 98}, so the location server chosen is 31 because the min of those bigger than 17.

and so on...

Whenever a node `n` needs to update his position to its location servers, he will make a "fake query" to himself, so to reach all nodes that act as location servers for him. This is useful because "queriers" for `n` will find the updated information exactly where they are searching for it.

## Location Query/Reply

Now every node has set up his location servers, which are simply other nodes with added knowledge and part of a distributed system. Now, let's emulate what happens when a node A tries to search for a node B in the grid in order to communicate with him:

![](img/geoloc.png)

Two nodes, 76 and 90, want to communicate with 17. Let's see both paths one by one (all the communications and links happening between nodes are done through geographic routing)

Location servers that were chosen by B are represented in bold, thus know his position. The list of nodes written on each 1-square represents of which nodes that node acts as a location server.

### Node 76

Let's call node 76 as A and node 17 as B as in the picture: 

1. Node 76 searches in the list of nodes of his 1-square if 17 is there or if there is a smaller node with ID > 17, but since he's the only node in the square he forwards the query to himself.
2. Then 76 searches for 17 in his list of known nodes of other squares (for which he is a location server) and, if he doesn't have it, searches the smaller one with ID > 17, finding 21: this is done because, as we said before, those with ID closer to the queried node have more probability of holding information about it. 76 forwards to 21 his request to find 17, bundled with his own information so that 17 knows his position and is able to answer back.
3. Node 21 receives the request and does the same with his list, but 17 isn't there unfortunately, so it finds 20 (smaller between nodes with ID > 17) and forwards the request to him.
4. Finally, 20 finds 17 in his list and forwards the request to him. Now B knows about A (ID + position) and can start a communication with him.

### Node 90

Let's call node 90 as A and node 17 as B as in the picture. 

1. Node 90 searches in the list of nodes of his 1-square if 17 is there or if there is a smaller node with ID > 17, but since he's the only node in the square he forwards the query to himself;
2. Node 90 doesn't have 17 so he searches for the smaller node with ID > 17 and, circularly, it finds that 70 could potentially know about him;
3. Node 70 doesn't have 17 so he searches in his list too with the same criteria and finds that 37 could potentially know about him;
4. Node 37 has 17 in his list so he forwards the request to him, allowing B to answer back and start a communication.

### Pseudocode

```pseudocode
// Trying to look for destination node D in the initial 1-square
Lookup(D): let S1 be the 1-square of S
	if (D in S1) then EXIT; // found!
	else {
 		i <- 1;
 		// I choose the first relay (location server) to reach D
 		select Ri <- min{n in S1 | ID(D)<ID(n)};
 		send Location_Query(D) to Ri;
	}
	
// Trying to look for destination node D in location servers
receive(Location_Query(D)):
	if (Ri location server for D or Ri=D) then EXIT; // found!
	else {
        i++;
        // I choose the next relay (location server) to reach D
        select Ri <- min{n | R(i-1) location server and ID(D) <= ID(n)};
        send Location_Query(D) to Ri;
	}
```

In both examples, the destination is reached in only 4 steps (including the 1-square internal one). 

This result is very promising and suggests how efficient this algorithm can be. To find a node, there was only needed 4 steps in an area as big as a 4-square.

To generalize this, it can be said that **k steps in an area as big as a k-order square are needed to find a node**. Let's try to prove it.

## Efficiency and Effectiveness Analysis

Let's try to prove the following lemma: "*S creates a query to find D position: if S and D are in the same k-square then the query takes (at max) k steps*".

With `k = 1`, this is easy to prove since, for construction, it was assumed that in a 1-square each node should know each other and to locate D from S, so just one step is needed. 

**N.B.** Keep in mind that with "step", is intended the algorithm step not the actual hops made by the packets to go from a node to another. A step could be made of multiple hops.

![](img/eff.png)

With `k > 1` things changes:

- `R1` is a relay node for a square `S1` such that, for every other node `n` in `S1`, has `ID(D) < ID(R1) < ID(n)`. This is exactly what happens in the algorithm by choosing the smaller of the nodes with greater ID than D.
- `R1` is thus the location server, in the 2-square that contains `S1`, for every `m` node that have `ID(D) ≤ ID(m) < ID(R1)`.
  - By contradiction, let's suppose that `m` didn't choose `R1` as location server but a different node `Rk`. This would mean that if `ID(Rk) < ID(R1)` and `ID(D) ≤ ID(m) < ID(Rk)` then this would break the previous assumption that `R1` was the smallest of the other `n` nodes in the square `ID(D) < ID(R1) < ID(n)` since instead there was another node (`Rk`) smaller than `R1` but bigger than D. Contradiction!
  - So, if `m` = D then `R1` is location server for D and the algorithm has reached its goal!
  - Otherwise another step is done and another relay `R2` is chosen to forward the query to.
- `R2` is relay node such that `ID(D) ≤ ID(R2) < ID(R1)`.
- `R2` is thus the location server for every `q` node that have `ID(D) ≤ ID(q) < ID(R2)`
  - So, if `q` = D then `R2` is location server for D and the algorithm has reached its goal!
  - Otherwise, these same steps are repeated until found...

For each step, the relay identifier (`R1`, `R2` ...) decreases in a monotonic way and since the identifiers are finite and discrete numbers the research must end at some point because then a relay that knows D will be reached. 

 `ID(D) ≤ ID(Rx) < ... < ID(R3) < ID(R2) < ID(R1)`

Because of the previous assumptions, at each step our query is extended to at least an higher order square. Whenever a relay Rx is chosen in the x-square that contains D, then D must have chosen Rx as location server because there was no better intermediate node. 

This suggests us that if D and S live in the same x-square and at most x steps are needed to reach it because at each step it goes up by one order square, thus one Rx.

## Real World Implementation

After discussing this problem and the algorithm that solves it in an ideal world with assumptions that made the modeling of it easier, now this will be adapted to the real world problems (interferences, different boot-up times and so on) in order to be an implementable protocol.

Let's try to see if some of the assumptions made still stand or need a tweak:

1. OK: We needed a geographic forwarding for queries and replies. For now that's true, this matter will be explored in the next lecture.
2. OK: The 2-hop routing for each node is done through the HELLO messages, so there is no problems for this assumption.
3. OK: The common knowledge of the grid is not a problem since it can be solved through road-side units and totems that share the grid informations to all vehicles entering that area.
4. PROBLEM: The assumption of always having a dense network with no partitions was the only surrealistic one, thus the algorithm has to be adjusted for these cases.
5. OK: There are no clock drift problems because they are always synchronized thanks to GPS.

## Location Servers Update

Let's suppose that a node D wants to update his position to the location servers present in his neighboring squares. This protocol tries to be as stateless as possible and the only information that a node has got are:

- The neighbours in his 1-square;
- The nodes for which he is a location server.

For this reason, the **node doesn't know who is a location server for him** in those squares so he cannot contact them directly, he has to geographically route the update message at the barycenter of their squares.

![](img/upd.png)

Let's suppose that D wants to update its position to his 2-square location servers in the 3-square area. The first node in another square that picks up D's message is `R0`. This node doesn't know if he's the right candidate for being his location server so he starts a fake query for D.

- He searches for a `R1` node that is better than him in his own 1-square or database (nodes for which `R0` is location server) such that `ID(D) ≤ ID(R1) < ID(R0)`
  - If he doesn't find one, then no other node `n` can be better than `R0` since `ID(D) ≤ ID(R0) < ID(n)`. `R0` then becomes location server for D because he's the best node.
  - If he does, then forward the update request to him (`R1`) and repeat this step but for `R1`.

This algorithm stops when:

- Current `Rx` doesn't know a better node than him (in terms of ID, of course), thus he becomes the location server for D.
- Current `Rx` knows someone better but that node belongs to another square, outside the square target that D specified. In this case then `Rx` is the best in the square target area and becomes the location server for D.
  - *Example*: in the picture D wants to update a location server in the upper 2-square. If the fake query finds a location server outside that 2-square, that node cannot be used as a proper location server for that 2-square because it is outside of it!

## Data Structures and Messages

### Data Structure

Each node `n` holds just the necessary amount of data:

- A set of neighbours in their 1-square;
- A set of nodes for which the node is a location server (**location table**).

For optimization reasons, the nodes to which `n` forwarded update messages can be saved so in the future, when `n` needs to contact the same nodes for an update or a query, he has already an head start in the research. The information about the previously contacted nodes is called **location cache**.

### Message Structure

In this algorithms there are two types of message circling around the network:

- The **location update** message, which is used by nodes to update their position at location servers, made by:

  ![](img/locup.png)

  - ***Source ID, location and timestamp*** which represent the all the informations about the node that is trying to update its position into location servers. Even if, theoretically, all clocks are synchronized, the timestamp is useful to determine how old is a location update.
  - ***Update destination square*** represents which square of the grid, containing location servers of the source, should be updated. Since the source don't know the identity of their location servers but knows that there should be at least one in the k-order square, he sends the message in the approximate area of that square.
  - ***Update timeout*** represents after how much time the next update will come after each one of them. The source calculates that the next relevant update will come after x amount of time (minutes, seconds...), depending on where it's moving and at which speed. 
    - If the location server doesn't receive a successive update in that amount of time, that means that he is no longer an adequate location server for the source and that probably another location server replaced him in that role. Because of this, the "no-longer" location server can discard this message.
  - ***Next location server ID and location*** represent the ID and position of the next location server in the algorithm previously seen. When `R0` sees that there's a better node than him (`R1`) for being a location server for the source, creates a fake query and forwards it filling these fields with `R1` ID and position.

- The **location query message**, which is used by nodes to find a certain node on the grid to try to start a communication with it:

  - ***Source ID and location*** represent the node that started the query. This is also used by the destination in order to reply to the query and start a conversation with the source.
  - ***Ultimate target ID*** represent the ID of the destination node.
  - ***Next location server ID and location*** represent the ID and position of the next location server in the algorithm previously seen (like in location update message).
  - ***Timestamp from previous server's database*** represents the obsolescence of the information, thus how old was the previous location entry for that node in the server's cache. This means that the query was probably started because the communication with the older location of that node didn't go through.

## Mobility Handling

This is the most "patch-heavy" job we must do to transform this algorithm to a real world protocol. Since our nodes are fast moving vehicles, when and how frequently we must update our grid?

### Location Update

If a node doesn't move away from his 1-square, there is no need to update anything. 

However, fixed a distance `d` in our network, if a node `n` moves away from his older position known by the i-square location server, by a distance greater than `2^{i-2} * d`, he has to update that location server again because the old position is too obsolete as information.

| i-square to update |       Distance        |                           Meaning                            |
| :----------------: | :-------------------: | :----------------------------------------------------------: |
|         1          | `2^{1-2} * d = 1/2 d` | By moving away more than `1\2 d`, all 1-square location servers have to be updated. |
|         2          |  `2^{2-2} * d = 1 d`  | By moving away more than `d`, all 2-square location servers have to be updated. |
|        ...         |          ...          |                             ...                              |
|         i          |     `2^{i-2} * d`     | By moving away more than `2^{i-2} * d`, all i-square location servers have to be updated |

The current location of the node is also piggybacked on each packet they send through, so basically **all nodes from which the packet passed by has this information in their cache**.

### Recover from Unsuccessful Queries

It could happen that a query cannot reach their recipient for some reason, like he moved away, he just woke up, a collision destroyed the packet and so on.

Whenever this happens **a set amount of time should be waited and, if the subsequent queries keep failing, exponentially increase that amount of time**, until the source of the query gives up. This mechanism is called binary exponential back-off and works similarly to the one in CSMA/CD whenever a collision is detected.

This is done in order to **wait for the destination node to update their location servers** while moving. It could take some time to update the ones further away from him so the amount of time increases because of this. 

There's another insurance that could be implemented to avoid local maximum problems and losing destination location.

A **local maximum is the node which is the closest one to the destination**. What would happen if the destination moves away from its range? The message would be stuck in that node because he only needed an hop but now the destination is unreachable from him (just like the relativistic effect we saw before).

A patch for this problem is to **notify all nodes of a 1-square whenever the destination D goes from a 1-square `S1` to another 1-square `S2`**:

- All nodes currently in `S1` have a forwarding pointer towards `S2`, saving this data in their cache, so:
  - Whenever a `S1` node exits from `S1`, he will forget and discard this pointer since he won't need it anymore;
  - Whenever a node enters `S1`, he will obtain this information through the HELLO messages coming from `S1` nodes.

Thanks to this mechanism, the local maximum node now knows where destination node went and knows what to do: forward the message towards that square.

## Performance Analysis

Some simulation of the GLS were performed with up to 600 nodes on an area of 2900 x 2900 meters. The radio signal range, which is equal to the edge of a 1-square, was of 250 meters, coherent with 802.11p standard.

The density was of 100 nodes per Km squared, this should be enough to avoid partitions but not surely. The speed of the nodes was in a range from [0, 10] m/s = [0, 36] Km/h, similar to the speed of vehicles in a trafficked urban area.

The mobility followed a random way-point model where each vehicle chooses a random way-point on the map (random coordinates, a point of interest...) and travels there at a random speed, then once arrived waits for a random amount of time and chooses another way-point randomly with another random speed... This model could be not 100% realistic for a worker car, but can be similar to the movement of a taxi, which moves in different, seemingly random, directions of the city.

![](img/grpf1.png)

The ratio between the response path length and the query path length is exposed. The query path length is slightly longer than the response because the path is created through relays (location servers) from the source to the destination, but it's not a big deal.

## DSR - Dynamic Source Routing

These measures were compared to a MANET routing algorithm, the DSR (Dynamic Source Routing): this is also a reactive algorithm that disseminates Route Requests (RREQ) along the network, thus uses flooding. 

Each node that knows a path towards the destination can answer with a Route Reply (RREP): the reply can use the same RREQ path to get to the source if available, if not the destination node has to make another RREQ to the source to find it and then encapsulate the reply in it. 

The source then gathers various RREP at different $\Delta t$ to:

- Route towards the destination again using the reverse path from the RREP;
- Ranks the different paths from the RREPs and chooses the best one while keeping the others for reliability;
- Uses different cut techniques to make paths loop-free.

![](img/grpf2.png)

DSR is kinda on pair with GLS at start but starts losing in delivery ratio for two reasons:

- With less than 400 nodes, the DSR is performing worse because the density is not enough to keep the path stable because of the mobility;
- With more than 400 nodes, DSR suffers a lot because of congestion generated by flooding, with lots and lots of packets lost during delivery;

All of this while GLS keeps his performance almost stable and high, even with the increasing amount of nodes. We kinda expected that, since DSR was designed for MANETs, in which nodes aren't that mobile like VANETs.
