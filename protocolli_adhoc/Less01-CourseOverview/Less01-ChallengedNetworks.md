# Course Introduction

This course focuses on how to design communication protocols in **challenged networks**, which are particular networks characterized by the following challenges:

- Fragile connectivity, because nodes can move and break the their connectivity;
- Network heterogeneity;
- Large communication delays.

These networks, frequently made by sensors, lesser devices and IoT devices, move away from the classic Internet paradigm and protocols (like TCP and UDP) because of:

- **Scarce resources**

  - Challenged networks are made by **thin devices** (also knowns as **nodes**), small devices that are low on resources and heterogeneous between each other. These type of devices have limited bandwidth, memory and computational capabilities. 

    Since we have to work with limited MTU (maximum transmission unit) and a low bitrate, we have to design protocols which **limits as much as possible the header size** of the network packets in favor of the data collected by the sensor to be sent (less header, more payload). 

    They also have a **bounded energy supply**, because normally they are not directly connected to electricity, but have and internal battery.

- **Frequently changing network topology**

  - Nodes usually have bounded energy supplies (batteries), so it's imperative to consume it as less as possible. For this reason, in order to limit battery discharge we have to design a **duty-cycle**, which is the ratio of the device in listen state compared with the period of a regular cycle. That means we can implement a **power-save mode** (or more, basing on the components of the devices we turn off) and a **working mode**. This result in a continuous detachment of a node from the network, constantly changing the topology.

    These devices have to deal with **wireless connection** and its low reliability, with unstable, lossy, noisy, low-power broadcast communication links. 

    The network topology is ever-changing, since devices could turn off, go in sleep mode, or interrupt their communication anytime, we cannot assume that these communication links will survive for a long time.

    Furthermore, devices could be sparse in the environment or even mobile, creating **network partitions**.

    All of this generally leads to **long latencies and delays**, since devices have to wait that new communication links are established in order to send their messages along the network.

We also have to **balance the power spent for transmission** for each device, because transmitting with more power means making communications smoother at long ranges, however this way the battery is depleted faster.

Messages can be lost also due to a **memory problem**, if a buffer is full the massage can't be stored and it will be discarded.

Wireless channels are broadcast, if two stations speaks at the same time a third station won't reach anything because those two signals will overlap. Need to **regulate access to the communication channel**.

Communicating in these kind of environment can be really difficult, so the communication protocol must be really thought out in order to make possible and simplify the interaction between these devices in a network.

# Wireless Networks

There are two types of wireless networks:

- **Single-hop** 
- Each node communicates directly with the Base Station, and the latter is in charge of handling the communications to the receiver (there is a **single hop from node to base**);
  - Subsequently, **no node can communicate directly with each other**, as every message has to be sent through the base station. The base station then will redirect the message from sender to receiver;
  - This is the mode in which we use wireless connection, with our phones, laptops, etc... to connect to the wireless router, ISP antennas and so on (which are the base station).
  
- **Multi-hop**
  - Known as **MANETs**, each node can communicate directly with other intermediate nodes in order to reach the receiver (it works like a **peer-to-peer network**);
  - A base assumption is that **nodes are mobile**, but not too much, so it is possible to think of an optimal path from source to destination that is supposed not to change too easily;
  - Each node can be sender, receiver or router.
  - Highly dynamical topology;
  - Highly node mobility;
  - Hight probability of network partition;
  - Low resource availability;


The main focus is on multi-hop networks since thin devices generally don't have a single base station that acts as a coordinator (orchestrator) between them.

MANETs can be specialized and customized basing on their purpose into:
- **Wireless Sensors Networks (WSN)**, made of light sensors, actuators, and other devices that aren't that mobile. These are the easiest MANETs to create and run, however, they suffer from battery problem, so they should face the duty cycle problem;
- **Pervasive Urban Sensing Networks**, made of mobile sensors integrated in other objects in an urban environment. Sensors present in our smartphones, smartwatches (GPS, accelerometer) live in this category;
- **Vehicular Networks (VANET)**, made of mobile sensors integrated in vehicles, drones, robots and other more complex systems. Sensors present in autonomous driving cars are an example, which, through VANETs, communicate to each other informations about environment (traffic signals, cars, people) in order to drive the car safely. These sensors are highly mobile, however they don't suffer battery problems since they get powered by the car itself;
- **Internet of Things Networks (IoT)**, made of "intelligent" objects in a environment that are able to communicate with each other;
- **Industry 4.0 networks**, IoT networks that follow some rules and constraints in order to be reliable in an industrial environment. These are the hardest networks to manage because they have both mobile and battery problems to face;
- **Delay-Tolerant Networks (DTNs)**, networks made of deep-spaced nodes, like oceanic and opportunistic networks.

# Wireless Sensors Networks (WSN)

To introduce these concepts, lets see some successful WSN examples: these are specialized in **habitat monitoring**.

- GoodFood, from the University of Firenze, 2005, an ambient intelligence to monitor cultures;
- Monitors for precipitations on Ande through sun alimented sensors;
- EURODEER Project (2009 - today) and ZebraNet (Princeton University, 2002-2004);
  - These two projects monitor the movements of the animals (deers/zebras) in order to understand and study their behaviour, lifestyle and habits. Each animal is equipped with a micro-collar provided with a GPS that tracks its movement and communicates its position with a central station.
- Rainwater monitoring on the Andes;
  - The Andes have a rough terrain, so it's useful to monitor rainwater so that flooding of farms and houses can be predicted and prevented. Considering that it's difficult to power these sensors and change their batteries, since they're in the soil and are hardly reachable, they have been provided with little solar panels to power themselves autonomously, allowing them to work in these harsh conditions indefinitely.
- Temperatures measures from the ocean deep.

## GoodFood

GoodFood was a project started in 2005 in which a network of wireless sensors was used to monitor **different properties of a cultivation land**, such as the soil humidity, temperature, wind, light level and soil Ph.

All these information was sent to a central elaboration node in order to **check that these parameters were in the allowed limits**, and if they weren't, actuators were turned on according to the current necessity:

- Low soil humidity → Activate sprinklers:
- Low/high Ph of the soil → Send appropriate fertilizer;
- Low/high light level → Activate greenhouse curtains to *let light pass* / *shield from light*;
- ...

The technology used is the **high density WSN** since there is no particular mobility or partitioning problems. However since there is a duty cycles to attend to, the network topology will change constantly.

The sensors communicate between them a **wireless base network**. Not all the sensors are equal, since there could be:

- Measuring sensors, specialized in gathering information on the environment;
- Communication sensors, specialized in gathering information from other sensors and send it to the central station.

Finally, there is a **fixed base network**, which is where the central station resides and from which receives data from the sensors and elaborates it, even in a Big Data context (since there are a lot of sensors gathering data).

The functionalities that this network should provide are the following:

- **Synchronization** is fundamental to have communication and coordination between WSN nodes. This implies the capability to tag nodes with temporal values;
- **Security** is essential since the communication is on a highly insecure and broadcast channel which is wireless. There may be data that should not be intercepted, and to avoid that we can *using authentication techniques* to ensure security by controlling the access to the sensor;
- **Localization** of sensors, being able to know where a specific node is, in order to give more meaning to his data. It is not always possible to use a GPS because it's too energy consuming, other solutions are required;
- **Data-centric communication** (instead of address-centric). Usually, Internet protocols are address-centric, which means that if we want to communicate with a host, we have to know its address, but in sensor networks usually we don't care about talking with a particular sensor, but we prefer to talk with an area of adjacent sensors that bring determinate data with them;
  - For example, if we have an area of fire sensors, we want to know the area in which the sensors are activated, not necessarily the particular sensor activated.
- **Scalability**. Usually, there is a high number of sensors we have to communicate with. To do so, we need to design the network in a way in which at least O(10<sup>3</sup>) sensors are supported;
- **Self-organization**. Sensors act coordinately to know more on the network and on each others by self-organizing basing on the course of events. Sensors are likely to have problems due to external factors, but cannot modify the external environment, so they must adapt to each situation by cooperating;
- **Heterogeneity of the nodes**. Not all sensors are equal, some are more specific for data gathering and others for communication. Some nodes might be stronger than others and must be handled according to that.

## Smart Dust

Smart Dust follows the idea of designing micro-electromechanical systems (MEMS), which are extremely tiny sensors (thus the name), that could detect details over the environment and communicate wireless.

These kind of sensors are extremely helpful in different kinds of environments:

- Ambient intelligence, interacting between people and environment;
- IoT applications, like magazine monitor or pervasive health, these sensors can be used to monitor a patient's condition without keeping it in a hospital, with various benefits coming with it (patient can live freely and normally, conditions are continually and automatically monitored, and so on);
- Technologies:
  - RFID (supermarket tags), where these sensors can be used to track and identify goods and packages;
  - NFC, Bluetooth Low Energy.

# Urban / Participatory Pervasive Sensing

As an evolution of WSN, where common sensors are replaced by smartphones with multiple sensors abroad that gets carried around by a user. Need to track low mobility targets in an environment and obtain information by them to pursue their goal.

Many project were born based on this network:

- The **Urban Sensing** project was defined by the European Community, where all devices keeps transmitting informations;

- The **MetroSense** is a Dartmouth university project uses microphone and accelerometer from users smartphone to monitor the environment. The network created is a multi-hop one, in which the information hops smartphone by smartphone. Communication is data-centric, so there is no need to know the precise smartphone address that gives that data, but instead of his approximate region or location in order to enrich and tag the information received.

  For example, a network between skiers' smartphones could gather much environmental data about each region they're located into, such as meteorological conditions, track features and so on...

# VANETs - Vehicular Ad-Hoc Networks

A VANET is a sensor network made by wireless sensors placed on vehicles or in devices on the side of the road.

These kind of network is more advanced than a WSN because it usually needs to gather data on highly mobile sensors in a urban environment.

Some of their notorious applications are:

- The **Traffic Information System** that uses various information gathered by vehicles in a city in order to create a map of it and enrich it with information (such as traffic condition, car accidents...). All this data will be useful to a navigator application (e.g. TomTom, Maps...) which will help drivers in choosing favorable routes.

  - For example, if two vehicles crash into each other, their sensors would detect it and inform surrounding vehicles that an accident happened. Then this information will be spread also by the other cars who received the warning and possibly change route to avoid creating traffic jams. This information can also be forwarded to road billboards, medical staff and even to an artificial intelligence that could handle traffic lights also in nearby roads to avoid jams.

  <img src="img/traffic.png" style="zoom:67%;" />

- The MIT's **CarTel** project, that is investigating how cars themselves could be used as ubiquitous, highly reliable mobile sensors;

- The **Intelligent Transport System**, that gathers other types of informations that could be useful to:

  - Assist the driver, letting him know of nearby services, such as gas stations, diners, hotels and so on;
  - Giving information about tolls and their cost;
  - Giving advices to reduce consumptions and pollution;
  - Handle vehicles fleet, checking their position and information about the environment-

All those projects share the objective to build city road maps based on current traffic in order to find faster routes, avoiding traffic.

This infrastructure implies the presence of big data and machine learning to work on such an enormous amount of data that also needs to be processed quite fast because this type of data gets deprecated pretty quick.

VANETs are also used for unmanned ground/air vehicles (UAV / UGV), drones and robots that need as much information as possible from the environment to orient themselves and work towards the objective, to achieve some simple commissions.

The key is sharing information around as much as possible in the network.

# DTNs - Delay Tolerant Networks

These networks usually have to handle **highly mobile nodes in sparse subnetworks**, since they are prone to partitions and messages often face connection problems. For this reason, nodes should be able to **save messages in a buffer** while they wait to find a path to the receiver of that message, hence the appropriate name of these networks suggests that **messaging delay should be expected** and for this reason, the applications should be made delay tolerant too.

Typical DTNs can be found in regions without an established infrastructure and with a **low amount of users**, because it wouldn't be cost-favorable for telecommunications companies to set antennas there. DTNs can be found in rural areas, but also in other places unusual for a network such as deep-space or underwater.

For example, to monitor the San Andreas fault, or a lot of floating sensor buoys are placed in the sea and used to gather information about seismic waves.

This is an highly challenged network, since communication with these buoys is highly variable depending on their movement: I could communicate with a sensor and his network because they are close, but if they get carried away by the water stream, I wouldn't be able to reach them anymore until they come back in range. 

This happens in the station-sensor communication and, in equal measure, in sensor-sensor communication.

## DAKNET

DAKNET is an example of an indian delay tolerant network made to reach those areas in developing countries.

![](img/daknet.png)

Communication is made possible by the combination of a mobile access point and a series of kiosks. Each kiosk will store the messages of the local village that needs to be sent to other villages or the town. 

When the mobile access point (often located on a humanitarian service truck) comes nearby a village it will:

- *Upload* to the kiosk the messages directed to that village;
- *Download* from the kiosk the messages written by that village for the other villages or town.

## Opportunistic Networks

Usually considered a sub-category of the DTNs, opportunistic networks are made by **individuals equipped with wireless devices** powered by Wi-Fi or Bluetooth alike, where the **data exchange is given by contact opportunities**.

For example, when exchanging a song or other data with a friend on smartphones via Bluetooth it is a direct approach, without any intermediate nodes.

This way of communicating is also possible by 4G standard and is called **offloading**: the base station, if it is overloaded, can tell the nodes that are close enough (low distance) to just communicate directly, without the need of him to intervene as an intermediate node and handle traffic between them. This way, it's possible to communicate without the use of the cellular network since we don't need to communicate with the base station but only with a local node.

Applications:

- Mobile social networking;
- Micro-blogging;
- Augmented reality (Wikitude);
- Content sharing;
- Question&Answer (Quora);
- Interaction with the environment;
- Micro-payments;
- Foursquare;
- ...

# IoT - The Internet of Things

The IoT networks are usually made by the **interaction of "smart things" between them and with users**.

Those "Smart things" are usual "dumb" things, such as doors or lamps, that are "made intelligent" and can be interacted in a unique way, for example, a user could turn on and off a lamp by pressing a button on his smartphone application, or there could be a light sensor that detects the light level and communicates with the lamp that is too dark and that he should turn on.

This, as exciting and useful as it can be, must not be taken lightly, because there are several **security issues** we might face, in order to avoid that the system is altered in an unwanted way.

### An IoT Example

![](img/iot.png)

In this example, there is a patient with two sensors: the ECG and blood pressure. 

These informations are vital to monitor his health conditions, however they should be restricted only for the group of people (or smart things) that will use them in the "right" way, since this is sensible data. It must be avoided that an unauthorized person could turn off the ECG monitor or tamper with it!

