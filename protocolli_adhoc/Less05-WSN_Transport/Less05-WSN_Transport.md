# Transport Algorithms on WSNs

## Introduction

In the previous lecture the routing level and how it is handled in WSNs was explained, but now the focus will be on how the transport level is implemented in this context.

In a WSN a reliable transport layer is needed, which is able to guarantee limited latency and scalable energy saving on networks of O(10<sup>2</sup>-10<sup>3</sup>) or bigger.

Most of the research in this area focused on creating a **reliable transport from sink to sensor**, because the other way around (sensor to sink) is pretty much guaranteed by the mechanisms we saw in the last lecture and the intrinsic features of the network.

At routing level, the sensor->sink path is reliable thanks to the density of the nodes in our unplanned network, because since there are many nodes, many can detect events and forward them to other nodes. This way there is enough redundancy to have a reliable service, so that something about the events will surely arrive to the sink.

- At data-link level, the sensor->sink path is reliable thanks to the fact that a node, without needing an ACK, can understand if the node to which he sent an agent, interest or query has retransmitted it or not.

Let's see what has been researched in the sink->sensor path.

## Reliable Transport

It's important that the sink has control over his network of sensors, this is because without his directives, sensors wouldn't be able to coordinate or know what to do since the sink/controller can give directive on sensors and actuators functioning, modifying their behaviour on the run.

- *Example*: in Directed Diffusion the sink starts by flooding an interest to his network of sensors in order to obtain information about an event: in this way sensors will wake up and stay alert for it and forward interests to others. Now the nodes have a purpose and it's satisfying the sink request.

Real life accidents teach us why reliability is so important, especially in critical industrial environments: what happens if the sink cannot get information about the nuclear core because sensors didn't get the query? what if the controller then isn't able to activate the security measures on the actuators if core melting is detected?

Why can't we use TCP? It works well on the Internet... well not so much in our challenged networks because:

- There can be **multiple destinations** for network messages (multicast):
  - TCP is optimized for one-to-one communication between two hosts on the network, while here there is a need that our sink/controller is able to communicate with many sensors/actuators at once;
  - There is an **asymmetric communication**: the sink doesn't know exactly all the sensors he's communicating with, since this is a content-centric paradigm;
  - TCP's ACK system isn't useful in this context, because even if we knew all the sensors, having an ACK structure and expecting one from each of the node is too complex and not maintainable... and we're not even considering that an ACK system would require long hops from nodes to sink, which is something we discarded because of interferences and power consumption (ACKs could be lost because of collisions!).
- Problems don't really come from congestion but from an **high error rate on the channel**:
  - TCP works under the assumptions that the network works fine and if a message isn't received, it's because of congestion;
  - TCP has lots of mechanisms to overcome congestion, however this isn't a thing in a closed network like those: there are different problems to face (network partitioning, duty cycles, battery saving) and TCP isn't fitted for it;
  - In modified versions of TCP, when there is a high error rate instead of transmitting slower, like if there was a congestion, the transmission comest in a more aggressive way using other sub-frequencies, hoping that at least one of those messages can reach its destination.

## Error-Rate Influence

Let's see how the error-rate influences the capabilities of our network in the transport layer.

Let's suppose that `p` is the error-rate on the channel. then, on `n` hops, **the probability that the message arrives successfully is `(1-p)`<sup>n</sup>**. Unfortunately `p` is quite high in challenged sensor network like those, about 5-10 %, increasing even further as the network size grows and more hops are necessary to transmit data around.

![](img/errorate.png)

This suggests that:

- It's **better to operate on hops** instead of paths.
  - Inversely from TCP, no end-to-end operations between the sender and the recipient but hop by hop.
- It's **better to operate with NACKs** instead of continuous and repeated ACKs.
  - Using ACKs is impossible for various reasons (ACK loss, implosion, structure complexity ...): what if we take the opposite approach, in which nodes stay silent until there is a problem, reporting it through a NACK.
  - Operating on hops instead of paths bolster this NACK feature, because two intermediate nodes can coordinate by themselves on a retransmission if there was a communication problem. Since this problem is solved locally, there isn't a NACK implosion: even if many instances of this problem occur at the same time and they're not in the same wireless range, this disruption stays confined and doesn't accumulate globally.
- It's better to **recover data as soon as possible**, instead of accumulate data for a later recovery.
  - Since our sensors are usually limited in memory, it's a bad idea to "bufferize" these problems for a later use. 

All of these suggestions were taken into mind when developing and implementing PSFQ protocol (Pump Slowly, Fetch Quickly).

## PSFQ (Pump Slowly Fetch Quickly)

The mantra of the PSFQ protocol is to:

- **Pump Slowly**, which means inject data into the network slowly and at a steady rate, so that if a problem has happened, there's time for recovery through the quick fetch;
- **Fetch Quickly**, which means fetch quickly any lost message in a hop (and recover quickly from this critical situation).

The goals of PSFQ are:

- **Reliability on data delivery**, within a maximum number of recovery retries;
- **Minimum support required from the underlying infrastructure**. It is needed, at least, that one-hop broadcast is supported, any routing protocol will do the trick (ex. Directed Diffusion, Rumor Routing, ...): just need a direction for our hops;
- **Minimum required control message exchange for reliability**;
- **Guarantee correctness of the service even if channel quality is very low**;
- Loosely **guarantee a certain delivery latency**:
  - Delivery is not exactly real-time, since it probably needs to recover on some hops. So we can't exactly calculate the time needed for the delivery but it's possible, at least, to estimate an upper bound, within which the delivery has been completed or it failed because some hops weren't successful.
  - This upper bound can help us configure our nodes, changing their behaviour for that amount of time, in order to ease data delivery by working as a cohesive network.

In this protocol **sinks and controllers are renamed into *user nodes*** (since will be the ones interfacing with potential users) and will be the source of data requests and behaviour-changing communication to the other nodes (sensors / actuators), which are called receiver nodes.

## PSFQ Algorithm

The algorithm is organized in 3/4 steps:

1. **Pump** - In the first step, the user node sends data with a sequence number (similarly to TCP), however, it does it slowly, so that it's possible to recover quickly from errors before the next data sending.
   
   - Usually, the wireless technology used is something like Bluetooth and not Wi-Fi, because the latter consumes too much energy and, since there are only short hops, it would be a waste to use it. For this reason, the payload size of the frames are usually smaller and messages need to be split in multiple parts and sent separately (thus the sequence number for each).
2. **Check** - The node that receives a message can detect the loss of a part of it if he receives the part `m-1` and then `m+1`, without getting `m`.
3. **Fetch** - If a loss is detected in check phase, the receiver node must immediately answer back with a negative acknowledgment (NACK), in order to recover quickly from the loss.
   
   - Even if this step could appear as a frenzy state, in which nodes asks hastily for a retransmission, it is implemented very methodically: to avoid wasting too much power, it tries to avoid sending duplicate NACKs or responses.
4. **Report** - Sometimes pump and fetch is not enough. There are special cases in which the node misses all the messages, so he doesn't even know he was the receiver of a transmission, or when he doesn't know if the transmission is ended or he should wait for more messages (because he missed the ending messages).

   This step serves this exact purpose, trying to **put a patch on this problem by reconstructing the current situation and what happened**.

Let's see these steps in detail.

## The PUMP Step

The pump step was the one in which the user node "pumps" data slowly into the network, let's see how it works.

Initially, the **user node sends data in broadcast to his neighbours every `T_min`**. 

**All nodes keep all messages received in a cache** so:

- If message was already in the cache, then discard the message. 

  This could have happened if neighbour sent a NACK to the sender and the message is received again or, because of the density of the network, I receive the same message from 2 or more nearby nodes because I'm in both their range.

- Otherwise, the node will re-broadcast the message after a random wait in the interval `[T_min, T_max]`: this is done to wait for all the neighbours to recover from eventual failures and to avoid that all nodes transmit at the same time, with the intent of avoiding collisions. 

- `T_min` and `T_max` are chosen depending on the particular network in which we're implementing this protocol, their values depend on the density of the network, which means that the denser the network the higher `T_max` (to let all the nodes have sufficient time to transmit in their turn and avoid collisions):
  
  - `T_min` used for flow control (to avoid "data congestion" on the node) and quick recovery;
  - `T_max` used for desynchronization to avoid collisions;
  - The re-broadcast should always be slower than the user node, in fact the time of re-broadcast is `T_min + random_wait`
  
- **Sequence numbers of the messages are used to avoid loops and to sort them later**.

It's possible to estimate the end-to-end upper-bound latency: given `n` the number of data messages in the sequence on a multi-hop, the **approximate latency is `D(n) = T_max * n * num_hops`**.

### PUMP Step Example

Let's see an example of the Pump step in action:

<img src="img/recover.png" style="zoom:80%;" />

Messages number `1` and `2` are transmitted and propagated through all nodes without any issue. 

The message with `seq = 3` is lost between the user node and node `A`, however `A` isn't aware of having lost a packet until he receives the message with `seq = 4`, since the message between `n+1` and `n-1` is missing, he starts the recovery in the Fetch step.

Node `A` now does 2 things:

- Starts the Fetch step, sending a NACK, which means **asking in broadcast to his neighbours if anyone has the message `3`**;
- **Doesn't forward the message 4 after recognizing the loss**: provided the recover possibility quickly, it can be avoided that the further nodes `(B,C)` send a NACK in their turn because they didn't receive `3`, which would result in generating many conflicting NACKs in the neighborhood with the risk of losing the NACKs too due to the interference. 

  Since in this protocol everything is alright if there are no NACKs, losing a NACK is catastrophic because nobody will be able to recover from that loss in little time.

Node `B` and node `C` didn't receive anything from their predecessors in the network tree: for this reason they'll think that everything is okay and they'll just wait for an input, or will help `A` retrieve lost message `3` if they received it from broadcast in some way.

This suggests that **all the nodes, except the user node, stay passive and reactive**, instead of proactive: they don't do anything on their own initiative and will only act when they receive external input (a NACK or a message to forward).

## The FETCH Step

To sum up, the fetch step and how it comes into place is described as the following:

- A node detect that he lost a message when he received message `n+1` after `n-1`, skipping `n`;
- When a node loses a message, it will go in Fetch step and send NACKs, trying to retrieve it from neighbours;
- The other nodes can help the node that lost the message if they happened to receive it in broadcast, otherwise they'll stay quiet, saving battery and avoid generating unwanted interferences and collisions.

So, what's the probability of success of the Fetch step, given these 3 sub-steps? Let's remember what we said about message arrival probabilities.

It was said that a `p` is the error-rate of the channel, then surely a certain message has probability `(1-p)` of being delivered correctly. Let's suppose a message is lost, then we can define `Φ(i)` as the probability of success of the Fetch step at the i-th retransmission:

- `Φ(0) = 0` 

  - The probability to recover the lost message with 0 retransmissions is obviously zero (I didn't even try).

-   `Φ(1) = (1-p)`<sup>3</sup> 
  
  - The probability to recover the lost message with 1 retransmissions is `(1-p)`<sup>3</sup> exactly because we have 3 distinct but consequential transmissions that are dependent from each other:
    - `(1-p)` is the probability of the `n+1` message to be received, so that the node can understand that he lost message `n`;
    - `(1-p)` is the probability that the NACK reaches the nearby neighbour nodes;
    - `(1-p)` is the probability that the lost message, from the neighborhood, reaches the node that lost it, thus requested it again.
    
    Combining these 3 dependent probabilities we obtain
    
    ​								 `Φ(1) = (1-p)*(1-p)*(1-p) = (1-p)`<sup>3</sup> 
  
- `Φ(n) = (1-p)`<sup>2</sup>`*[(1-p) - Φ(1) - Φ(2) - ... - Φ(n-1)]` 
  
  - The generalized formula for `n` retransmissions includes:
    - `Φ(1-p)`<sup>2</sup> is the probability that the `n+1` message is received and that the NACK is sent correctly;
    - `[(1-p) - Φ(1) - Φ(2) - ... - Φ(n-1)]` is the probability, at i-th retransmission, to recover the lost message minus the probabilities of the previous retransmission attempts.

`Φ(i)` is the punctual probability at exactly the i-th retransmission. Let's calculate then the **cumulative probability `Ω(n)` until the n-th retransmission**:

​											`Ω(n) = Φ(1) + Φ(2) + ... + Φ(n)`

This is just the cumulated sum of the `n` punctual probabilities to recover the lost message.

Given `Φ(n)` and `Ω(n)`, it can be said that, in a NACK system, **the probability to recover the lost message in `n` attempts**, with `n≥1` is:

​													`(1-p) + (p * Ω(n))`

Where 

- `(1-p)` is the probability that the message arrives correctly;
- `(p * Ω(n))` is the probability that the message was lost but after `n` retransmissions it was found and recovered.

## The "Magic Number"

<img src="img/lossrate.png" style="zoom:80%;" />

The success rate, with more retransmissions, can keep up with the packet loss quite well. However, after a certain point, retransmissions aren't able to maintain that success rate and even incrementing them wouldn't help.

For this reason, **a "magic" number of retransmissions was chosen**, balanced to have an high success/loss ratio and as low as possible to be successful: the number chosen was **4**.

The properties of this number is also used in the pump step, where if a node receives the same message from 4 other neighbours, aside from always discarding the message because it's duplicate, it will also stop forwarding that message.

This is done so that node can understand that many others already have that message in forwarding and it would be useless to retransmit it, better save power and avoid interferences, but of course, that node will keep the message in cache and forward it to those who lost it.

However, the original and primary use of the magic number is in the fetch phase, as the maximum number of attempts a node would make in order to recover a lost message.

## NACK and Recovery in Detail

Let's see in detail how the recovery works. The node that detects one or more gaps in their sequence:

1. **Creates a NACK in which he aggregates all gaps detected and asks for them to its neighbours**. Ideally the recovery should happen between `[T_min; T_max]`, however it is not always possible and gaps can accumulate so **a single NACK can refer to multiple gaps** (unlike ACKs in TCP which refer only to a single packet).
   - *Example*: if a node received (3, 5, 6, 9, 11) he detects three gaps: (4), (7,8) and (10).
2. It **waits for a random amount of time between `[0, Δ]`** with Δ arbitrarily small (this is done to guarantee the desynchronization of the transmissions). This Δ value has to be:
   - Small enough to avoid waiting too much to send a NACK and recover quickly;
   - Big enough to guarantee a wide enough window of retransmission to enforce desynchronization and avoid interferences.
3. After this time, the node **sends the first NACK** only if he doesn't receive other identical NACKs for the same gaps (this is done to guarantee duplicate suppression and reduce network load).
4. Until he receives the messages for the gaps or until the specified attempt threshold, **he re-sends NACKs with the same methodology once every `T_r`** such that `Δ<<T_r<T_max`
   - *Example*: the threshold can be the magic number 4 for the retry attempts.

The node that, instead, receives NACKs and have in cache the messages asked by the others:

1. Will **reply after waiting a random period of time** between  `[1⁄4 T_r, 1⁄2 T_r]` . This interval was chosen to reply
   - At best at quarter `T_r`, to enforce desynchronization and recover quickly;
   - At worst at half `T_r`, to avoid the NACK retransmission before my reply arrives (and avoid collisions).
2. For the sake of duplicate suppression, if he hears that **someone is already answering** to those nodes with the same messages he was asked for, he will **not reply** to avoid interferences and such.

## NACK: Special Cases

To increase recover probabilities, PSFQ developers have created additional behaviour and variants to use in order to enhance networks:

- **2-Hop Help**
  - When a node sends a NACK for "threshold" times (4 times for example), that means he wasn't able to recover lost messages from his neighbours. This could've happened for various reasons, such as the nodes didn't have in cache what he was searching for or the replies were lost in the way. 
  - The neighbours, unable to help that "poor" node directly, try to assist anyway they can by rebroadcasting his request to their own neighbourhood, effectively extend the research range to 2-hops away from the "poor" node, because maybe these additional nodes have answers for him:
    - If they have answers, then the original neighbours of the "poor" node will ask for them and then re-forward the messages to him;
    - Otherwise, they realize they can't help too so they'll resign from the research.
- **Signal Analysis**
  - By analyzing the signal quality of his neighbours, a node can **choose a favorite parent/neighbour node** in the node tree with the highest signal-to-noise ratio (for example);
  - Since the other nodes have less signal quality than the favorite one, it would be common to lose messages from them: to avoid sending too many NACKs, the node will trust more his favorite node and will **only send NACKs if he loses one or more messages from him**. That's because if I lose messages from my favorite (and with better signal) node, something has gone wrong and I must ask neighbours for the lost message.
  - In the same way, the node would **prefer that his favorite node could reply to the NACK first**, and only then, if the node still don't hear from him, accept replies from other neighbours. To implement this, just to increase the response time `T_r` for nodes who aren't the favorite node:
    - *Example*: we can keep this interval for the favorite node `[1/4 T_r, 1/2 T_r]`, while for the other neighbours we can set it to `[1/2 T_r, 1/3 T_r]`. 

## PSFQ Message Structure

PSFQ includes different message headers, each one for a different step of the algorithm previously seen

### Inject Message

![](img/inj.png)

The header of an inject message possess the following fields:

| Field           | Usage                                                        |
| --------------- | ------------------------------------------------------------ |
| File ID         | It represents an unique identifier for the file or data that is being diffused by a sender node. |
| File Length     | It represents the size of the file diffused by a sender node. Receiver nodes can deduct if they received the full file by comparing the sum of the sizes of the segment received with the size of the total file. |
| Sequence number | It represents the number of this segment of the full file. Receiver nodes can use this to sort the segments and detect any gaps in the stream of data they received. |
| TTL             | It is a composite field made by a report bit, used in report step, and the real Time To Live counter. The TTL is calculate to avoid that the message circulates in an endless loop in search for the destination. The number of hops in the TTL is usually roughly the diameter of the network (worst case scenario). |
| Payload         | It contains part of the original file or data that the sender node wanted to diffuse. |

### NACK Message

![](img/nack.png)

The NACK message is a little bit different from the others because it has a variable amount of fields.

The first two fields are the same in the inject message and represent the id and the size of the file the node is asking for.

He will specify the missing segments and the gaps in the **loss window** variable field: for example, in the sequence of segments (3, 5, 6, 9, 11) we have the following gaps (4) , (7,8) , (10). Each of this gaps is represented with its own loss window that is represented using the two ends of the loss range. In other words.

- Gap (4) is represented as loss window (3, 5) with 3 as the last message before the loss and 5 as the first message after the loss;
- Gap (7,8) is represented as loss window (6, 9) with 6 as the last message before the loss and 9 as the first message after the loss;
- Gap (10) is represented as loss window (9, 11) with 9 as the last message before the loss and 11 as the first message after the loss.

This way of representing loss windows is strange but offers several benefits:

- Use **less space to represent the loss**. Because of the use of just two integers to represent the range of lost messages instead of representing them one by one. This space efficiency lets us represent more loss windows.
- **Good identifiers**, because representing through ranges proves to be a good identifier since they are unique and unambiguous. 
- **Faster parsing**. The node replying to the NACK need to do just minimal computation in order to retrieve the exact range of messages between those two integers. 

In networks like these, especially with wireless connection, errors are not small and constant but happen seldom, whenever there is a collision, and with great loss of data. This is why losing more than one message is common and optimizing loss windows is important.

In the variant mentioned earlier, there is an **optional field for the favorite neighbour**: if the node receiving the NACK matches the ID of the favorite (ex. MAC address), then he will reply earlier than those who aren't.

## The Last ACK Problem

In general, all problems which require an ACK system to be solved, thus a reliable transport (such as TCP, PSFQ, ...) have a common challenge to face: the last ACK problem.

We define the Last ACK problem as the following:

> The last ACK problem occurs when a receiver node isn't able to tell if he's not receiving any more messages because of all the data was sent (transfer complete) or because of a network problem (sender node crashed, segments were lost and so on).

In TCP, if a receiver node didn't receive anything for a set amount of time, will presume that the sender has finished sending or has crashed, so, in order to avoid wasting resources, he will close the connection (after the timeout). This can be disruptive, because maybe it was only a temporary network problem and I would have wasted half work for nothing, and we know that in our challenged networks we have plenty of problems like this (kinda long latencies even with quickly recovery).

Fortunately, we defined in all our PSFQ headers until now the file length, so if the sum of the sizes of all the segments is the same as the original file length, then I'm sure that I received all the messages. If not, probably I've lost some messages, so I should send a NACK. The last ACK problem is not solved yet, but at least we have something to work with.

Let's consider that we need to deal with a trade-off: we don't want nodes to send NACKs:

- Too early, because we want to avoid getting in the way of other nodes before us already trying to NACK to solve the problem upstream;
- Too late, because we want to obtain the lost messages before they expire and are replaced, since nodes need to keep all messages they forward in their limited memory resources.

## Proactive Recovery

Instead of the usual reactive recovery we discussed about now, we opt for a more proactive recovery. The node activates himself in order to preemptively solve a problem, instead of just reacting to it when it happens. 

Usually in those networks, a reactive approach is usually better to avoid wasting energy to fix a problem that doesn't exist. However, since there is the assumption that no NACK = no problem, nodes could clear their cache inadvertently before a late NACK asking for the cleared content, arrived late because of waiting.

In this proactive version of PSFQ, **if a node doesn't receive anything for a set amount of time `T_pro` he will send a NACK because he suspects something is missing**. This way, the node will still wait a bit to give time to recover upstream but, at the same time, will avoid that the cache overflow deletes the messages that the node needs.

`T_pro` is calculated as the following: 

`T_pro = min{α*(S_max-S_last)*T_max, α*n*T_max} con α ≥ 1`

Where:

- `S_max` is the maximum sequence number received. This can be deducted from the file length and payload length. 
  - *Example*: if file length is 5KB and payload length is 1KB, then `S_max` should be exactly 5.
- `S_last` is the last sequence number received.
- `n` is the max number of messages storable in cache. This value should be considered thoughtfully, since the network could be heterogeneous (so different nodes would have different cache capabilities). Given this, it should be tailored to specific needs.
  - *Example*: it's possible to choose to set `n` to be the max number of messages storable in the smallest cache of our network. 
- `α` is a scalar that gets higher the poorer the channel quality gets, with an high error rate. This coefficient helps to approximate how probable is that upstream nodes will have problems in their transmission, giving them more time to recover as the channel error rate grows. 
-  `T_max` is, as we remember, the maximum time allotted for recovery, after which a new message can be sent from the user node.

So `T_pro` is the minimum value (considering `α`) between:

- `n * T_max` - This is maximum time that can be waited when considering cache capability, since a new message is surely sent after `T_max` time, then the caches will be full after `n * T_max` transmissions. After this, the subsequent transmissions will overwrite data inside the cache
- `(S_max-S_last) * T_max` - When `(S_max-S_last)` is high, that means that transmission started recently and we still need to receive a lot of messages, so there's no need to rush to get lost messages since the caches are relatively empty at this point.
  - When `(S_max-S_last)` is low, that means that transmission is almost ended and we should hurry up to recover lost messages before they're cleared from others node caches.

## Report Step

The second situation that could happen in a last ACK problem is whenever the **user node needs to send data reliably through only one message**, maybe because it fits perfectly in only one payload and don't need multiple segments.

The arising problem is evident: if this single message is lost, the receiver nodes won't even know that they should have received a message, plus they can't rely on the file length to understand if there are some missing messages or not.

To solve this problem, when user needs to send only one message, **in the Inject step the report bit is set at 1**. This means that:

- If downstream nodes got the message, they need to report back to the sender their status (like an ACK);
- If no reports are coming from the receiver nodes, the message could be lost: the sender needs to retransmit that message.

This is the only case in which we **replace the NACKs with normal, positive ACKs**. For this reason, receiver nodes will reply back to the parent only after a casual amount of time between `[0, Δ]`. Since all receivers have to answer back with an ACK, remember to avoid collisions at all costs! With this desynchronization of replies, it will be less likely that two ACKs collide and are lost.

After this premise, let's see the report step in detail: when a node receives an Inject message with report bit set at 1:

- If the TTL = 1, then he will answer back to the parent node with his own ID and with `S_last` field. This means that the message reached a leaf in the tree of paths that message has traveled.
- Otherwise, he enters **report mode** and forwards the message downstream.
  - Nodes in report mode will await for answers from the child nodes (the nodes to which they forwarded the message). These intermediate nodes will receive the report message from their children and will add their own `<ID, S_last>` couple before sending the updated report message to their parents.
    - If they don't receive answers from their children after a set amount of time (`T_report`), they will report anyway with only the reports until that node.
    - If they receive a report message but its payload is full (it could happen on large, dense networks), they will generate a new personal report with his data and send the new report first, and ONLY THEN forward the full report to their parents. This way the parent nodes can re-use the new report to insert their own information, instead of creating a new one each time (which would increase number of transmissions, power waste, interferences and so on....)

## Performance Evaluation

PSFQ has proven to be a well-rounded and optimized reliable transport protocol. 

It has been tested on the simulator against SRM (Scalable Reliable Multicast) and performed better than it in every case:

- PSFQ can hold an acceptable delivery delay until the error rate is below 50% vs 40% of SRM.

  ![](img/vs1.png)

- PSFQ has almost 100% delivery ratio until 70% error rate on long paths, even on 7-8 hops from user, unlike SRM which collapses under that error rate.

  ![](img/vs2.png)