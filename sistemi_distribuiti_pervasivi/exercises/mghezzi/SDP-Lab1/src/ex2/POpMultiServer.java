package ex2;

import java.net.ServerSocket;
import java.net.Socket;

public class POpMultiServer {
    public static void main(String argv[]) throws Exception {
        ServerSocket welcomeSocket = new ServerSocket(6789);

        while(true) {
            Socket connectionSocket = welcomeSocket.accept();

            POpServerThread thread = new POpServerThread(connectionSocket);
            thread.start();
        }
    }
}