package ex1;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class SumServer {
    public static void main(String argv[]) throws Exception
    {
        int listenPort = 0;
        String num1;
        String num2;
        int sum;

        Scanner in = new Scanner(System.in);
        listenPort = in.nextInt();

        /* Creating a "listening socket" on user specified port */
        ServerSocket welcomeSocket = new ServerSocket(listenPort);
        System.out.println("Server started");

        while(true) {
            /*
             * Viene chiamata accept (bloccante).
             * All'arrivo di una nuova connessione crea una nuova
             * "established socket"
             */
            Socket connectionSocket = welcomeSocket.accept();

            System.out.println("Client port: " + connectionSocket.getPort());
            System.out.println("Client IP: " + connectionSocket.getInetAddress());

            /* Inizializza lo stream di input dalla socket */
            BufferedReader inFromClient =
                    new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));

            /* Inizializza lo stream di output verso la socket */
            DataOutputStream outToClient =
                    new DataOutputStream(connectionSocket.getOutputStream());

            /* Legge una linea (terminata da \n) dal client */
            num1 = inFromClient.readLine();
            num2 = inFromClient.readLine();
            sum = Integer.parseInt(num1) + Integer.parseInt(num2);

            /* Invia la risposta al client */
            outToClient.writeBytes(Integer.toString(sum));

            connectionSocket.close();
        }
    }
}
