package ex2;

import java.util.Random;

public class Cat extends Thread {
    private VetRoom vetRoom;

    public Cat(VetRoom vetRoom) {
        this.vetRoom = vetRoom;
    }

    @Override
    public void run() {
        try {
            vetRoom.enterRoom(this);
            System.out.println("A cat entered the room");

            Random rand = new Random();
            int randomNum = rand.nextInt((5000 - 1000) + 1) + 1000;
            Thread.sleep(randomNum);

            vetRoom.exitRoom(this);
            System.out.println("A cat left the room");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
