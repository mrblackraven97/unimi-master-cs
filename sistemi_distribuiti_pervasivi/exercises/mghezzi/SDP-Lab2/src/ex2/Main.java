package ex2;

public class Main {
    public static void main(String[] args) {
        VetRoom vr = new VetRoom();

        for(int i = 0; i < 10; i++) {
            Cat cat = new Cat(vr);
            cat.start();

            Dog dog = new Dog(vr);
            dog.start();
        }
    }
}
