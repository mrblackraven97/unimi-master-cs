package ex2;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class AdderServer {
    public static void main( String[] args )
    {
        // Starting service on port 8080
        try {
            Server server = ServerBuilder.forPort(8080).addService(new AdderServiceImpl()).build();
            server.start();
            System.out.println("Server started!");
            server.awaitTermination();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
