package ex1.protobuff;


import ex1.impl.Residence;
import it.ewlab.researcher.StudentOuterClass.Student;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ProtobuffStudentClient {
    public static void main(String[] args) throws IOException {
        Socket s = new Socket("localhost", 9999);

        Student r =
                Student.newBuilder()
                        .setName("name")
                        .setSurname("surname")
                        .setBirthday("birthday")
                        .setResidence(Student.Residence.newBuilder().setComuneNascita("comNasc")
                                .setComuneResidenza("comRes").setIndirizzoResidenza("indRes").build())
                        .addExam(Student.Exam.newBuilder().setExamName("examName").setVote("Vote")
                                .setVerbDate("verbDate"))
                        .build();

        r.writeTo(s.getOutputStream());
        s.close();

    }
}
