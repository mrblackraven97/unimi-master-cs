package ex1.impl;

import java.util.ArrayList;

public class Student {
    String name = null;
    String surname = null;
    String birthday = null;
    Residence residence = null;
    ArrayList<Exam> exams = new ArrayList<Exam>();

    public Student () {}

    public Student (String name, String surname, String birthday, Residence residence) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.residence = residence;
    }

    public void addExam(Exam e) {
        exams.add(e);
    }

    @Override
    public String toString() {
        return "hi";
    }
}
