package ex1.impl;

public class Exam {
    private String examName;
    private int vote;
    private String verbData;

    public Exam(String en, int v, String vd) {
        examName = en;
        vote = v;
        verbData = vd;
    }

    public String getExamName() {
        return examName;
    }

    public int getVote() {
        return vote;
    }

    public String getVerbData() {
        return verbData;
    }
}
