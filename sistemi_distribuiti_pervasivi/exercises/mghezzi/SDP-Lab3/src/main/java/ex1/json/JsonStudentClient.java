package ex1.json;

import com.google.gson.Gson;
import ex1.impl.Exam;
import ex1.impl.Residence;
import ex1.impl.Student;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class JsonStudentClient {
    public static void main(String[] args) throws IOException {

        // Socket connection
        Socket s = new Socket("localhost", 9999);

        // Building a student
        Exam exam = new Exam("a", 0, "a");
        Residence residence = new Residence("a", "a", "a");
        Student student = new Student();
        student.addExam(exam);

        // Conversion to JSON of a student
        Gson gson = new Gson();
        String jsonStudent = gson.toJson(student);

        // Sending the JSON String to the server
        DataOutputStream outToServer = new DataOutputStream(s.getOutputStream());
        outToServer.writeBytes(jsonStudent);
        s.close();


    }
}
