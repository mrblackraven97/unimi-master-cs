package ex1.protobuff;

import it.ewlab.researcher.StudentOuterClass.Student;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ProtobuffUniversity {
    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(9999);

        Socket s = serverSocket.accept();

        Student r = Student.parseFrom(s.getInputStream());

        System.out.println(r);
    }
}
