package ex1.impl;

public class Residence {
    private String comuneResidenza;
    private String indirizzoResidenza;
    private String comuneNascita;

    public Residence(String cr, String ir, String cn) {
        comuneResidenza = cr;
        indirizzoResidenza = ir;
        comuneNascita = cn;
    }

    public String getComuneResidenza() {
        return comuneResidenza;
    }

    public String getIndirizzoResidenza() {
        return indirizzoResidenza;
    }

    public String getComuneNascita() {
        return comuneNascita;
    }
}
