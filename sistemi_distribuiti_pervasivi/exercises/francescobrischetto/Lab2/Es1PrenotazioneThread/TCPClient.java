import java.io.*;
import java.net.*;

class TCPClient {
    public static void main(String argv[]) throws Exception {
        final int port = 6789;
        final String server = "localhost";
        String message, result;

        try {
            Socket clientSocket = null;
            try{
                clientSocket = new Socket(server, port);
            }catch(IOException e){
                System.err.println("An I/O error occured while opening the socket!\n");
                System.exit(-1);
            }catch(IllegalArgumentException e){
                System.err.println("The port parameter is outside the specified range of valid ports!\n");
                System.exit(-1);
            }catch(Exception e){
                System.err.println(e.getMessage());
                System.exit(-1);
            }
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));


            message = "prenota\n";
            outToServer.writeBytes(message);
            System.out.println("I'm sending the request to the server");
            result = inFromServer.readLine();
            System.out.println("The server reply is: " + result);
            clientSocket.close();

        }catch(IOException e){
            System.err.println(e.getMessage());
            System.exit(-1);
        }
    }
}