import java.util.Random;

public class Prenotazioni {
    private int posti;
    private int prenotati;
    public Prenotazioni(int posti){
        this.posti=posti;
       this.prenotati=0;
    }
    public synchronized int Prenota() throws InterruptedException {
        if( prenotati>=posti) {
            //Thread.sleep(10000);
            return 0;
        }else{
            //Thread.sleep(10000);
            return ++prenotati;
        }
        // int result = prenotati==posti? 0 : ++prenotati;
    }
}
