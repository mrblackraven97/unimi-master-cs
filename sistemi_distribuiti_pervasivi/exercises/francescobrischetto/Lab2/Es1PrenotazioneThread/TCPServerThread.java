import java.io.*;
import java.net.*;

public class TCPServerThread extends Thread {
    private     Socket connectionSocket = null;
    private     BufferedReader inFromClient;
    private     DataOutputStream  outToClient;
    private     Prenotazioni p;

    public TCPServerThread(Socket s, Prenotazioni p) {
        this.p = p;
        connectionSocket = s;
        try{
            inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            outToClient = new DataOutputStream(connectionSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        String clientSentence;
        String result;
        try {
            clientSentence = inFromClient.readLine();
            result = Integer.toString(p.Prenota());
            outToClient.writeBytes(result);
            connectionSocket.close();
        } catch (InterruptedException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}