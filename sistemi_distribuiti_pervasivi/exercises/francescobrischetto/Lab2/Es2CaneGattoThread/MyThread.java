package com.company;
import java.util.Random;

public class MyThread extends Thread {
    private Random rnd;
    private Animal a;
    private WaitingRoom w;
    private int id;

    MyThread(int id, Random r,WaitingRoom w) {
        this.id = id;
        rnd = r;
        this.w = w;
    }

    public void run() {
        int randomNumber = rnd.nextInt(10) + 1;
        a = randomNumber%2==0? new Cat() : new Dog();
        try
        {
            System.out.println(String.format("Thread %d started. My class is %s waiting to enter room.",this.id,this.a.getClass().toString()));
            w.enterRoom(a);
            System.out.println(String.format("Thread %d entered in room. Will sleep for %d seconds.",this.id,randomNumber));
            Thread.sleep(randomNumber*1000);
            w.exitRoom(a);
            System.out.println(String.format("Thread %d exited room. ",this.id));
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }

    }

}
