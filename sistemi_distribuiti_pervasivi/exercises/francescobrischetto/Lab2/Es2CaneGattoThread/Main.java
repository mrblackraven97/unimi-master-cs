package com.company;
import java.util.ArrayList;
import java.util.Random;

public class Main {

    public static void main(String arg[]) throws Exception {
        WaitingRoom w = new WaitingRoom();
        Random r = new Random();
        ArrayList<Thread> threads = new ArrayList<Thread>();
        //create some threads
        for (int i=0; i<10; i++) {
            MyThread mt = new MyThread(i,r,w);
            threads.add(mt);
        }
        System.out.println("All threads have been created.");
        for (Thread t: threads) {
            t.start();
        }
        for (Thread t : threads) {
            t.join();
        }
        System.out.println("All threads have finished");
    }

}

