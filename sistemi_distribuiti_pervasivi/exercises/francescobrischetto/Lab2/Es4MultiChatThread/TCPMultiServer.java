import java.io.*;
import java.net.*;

class TCPMultiServer {
    private static Queue<TCPServerThread> partecipants;
    private static Queue<String> buffer;
    //è colui che andrà a leggere i messaggi dal buffer ed avvertire tutti i partecipanti nel caso
    //in cui un nuovo messaggio arrivi. Per questo ha bisogno di una coda dei partecipanti, oltre che al buffer dove scrivere
    private static Consumer c;
    private static int id;

    public static void main(String argv[]) throws Exception
    {
        id = 0;
        partecipants = new Queue<TCPServerThread>();
        buffer = new Queue<String>();
        ServerSocket welcomeSocket = new ServerSocket(6789);
        c = new Consumer(partecipants,buffer);
        c.start();
        while(true) {
            Socket connectionSocket = welcomeSocket.accept();
            System.out.println("New client Connected. Assigning id: " + id);
            /* Creazione di un thread e passaggio della established socket */
            //Questi thread sono tutti produttori, mentre c'è un solo consumatore che avverte tutti
            TCPServerThread handleThread = new TCPServerThread(id,connectionSocket,buffer);
            partecipants.put(handleThread);
            /* Avvio del thread */
            handleThread.start();
            buffer.put("/enter "+id+"\n");
            id++;
        }
    }
}