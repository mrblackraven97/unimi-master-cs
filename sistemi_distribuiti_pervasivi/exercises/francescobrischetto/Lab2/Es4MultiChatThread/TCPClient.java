import java.io.*;
import java.net.*;


class TCPClient {
    public static void main(String argv[]) throws Exception {
        int port = 6789;
        String address = "localhost", message, result;
        try {
            BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
            Socket clientSocket = null;
            try {
                clientSocket = new Socket(address, port);
            } catch (IOException e) {
                System.err.println("An I/O error occured while opening the socket!\n");
                System.exit(-1);
            } catch (IllegalArgumentException e) {
                System.err.println("The port parameter is outside the specified range of valid ports!\n");
                System.exit(-1);
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.exit(-1);
            }
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            message = "";

            class ClientThreadReader extends Thread {
                public void run() {
                    String message = "";
                    while (!message.equals("/exit\n")) {
                        try {
                            message = inFromUser.readLine() + "\n";
                            outToServer.writeBytes(message);
                        } catch (IOException e) {
                            e.printStackTrace();
                            break;
                        }
                    }
                }
            }

            class ClientThreadWriter extends Thread {
                public void run() {
                    String message = "";
                    while (!message.equals("/exit")) {
                        try {
                            message = inFromServer.readLine();
                            System.out.println(message);
                        } catch (IOException e) {
                            e.printStackTrace();
                            break;
                        }
                    }
                }
            }

            ClientThreadReader cr = new ClientThreadReader();
            cr.start();

            ClientThreadWriter cw = new ClientThreadWriter();
            cw.start();

            cr.join();
            cw.join();
            clientSocket.close();

        } catch (IOException e) {
            System.err.println(e.getMessage());
            System.exit(-1);
        }
    }
}