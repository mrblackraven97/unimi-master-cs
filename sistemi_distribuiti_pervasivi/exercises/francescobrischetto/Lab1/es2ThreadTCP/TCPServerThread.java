import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.*;
import java.net.*;

public class TCPServerThread extends Thread {
    private     Socket connectionSocket = null;
    private     BufferedReader inFromClient;
    private     DataOutputStream  outToClient;

    public TCPServerThread(Socket s) {
        connectionSocket = s;
        try{
            inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            outToClient = new DataOutputStream(connectionSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean testOperation(String stringtest){
        char test = stringtest.charAt(0);
        if((test=='+') || (test=='-') || (test=='*') || (test=='/')) return true;
        else return false;
    }
    public void run() {
        String clientSentence;
        String result;
        int param = 1;
        String[] clientReceived = new String[]{"","",""};
        float firstNum, secondNum;
        char operation;
        try {
            while(param<4){
                clientSentence = inFromClient.readLine();
                if(param<3){
                    try {
                        Float.parseFloat(clientSentence);
                        clientReceived[param-1]=clientSentence;
                        param++;
                        outToClient.writeBytes("ACK\n");
                    }catch (NumberFormatException e){
                        outToClient.writeBytes("NACK\n");
                    }
                }else{
                    if(clientSentence.length() == 1 && testOperation(clientSentence)){
                        clientReceived[param-1]=clientSentence;
                        param++;
                        outToClient.writeBytes("ACK\n");
                    }else{
                        outToClient.writeBytes("NACK\n");
                    }
                }
            }
            /*this implements the evaluation of the string using javascript scriptengine*/
            clientSentence = clientReceived[0]+clientReceived[2]+clientReceived[1];
            ScriptEngineManager mgr = new ScriptEngineManager();
            ScriptEngine engine = mgr.getEngineByName("JavaScript");
            try {
                result = String.valueOf(engine.eval(clientSentence));
            }catch (ScriptException e){
                System.err.println(e.getMessage());
                result="Errror in server evaluation!";
            }
            outToClient.writeBytes(result);
            connectionSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
