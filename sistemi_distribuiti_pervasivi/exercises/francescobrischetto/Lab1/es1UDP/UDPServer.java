import java.io.IOException;
import java.net.*;

class UDPServer {
    public static void main(String args[]) throws Exception {


        String clientSentence;
        String resultString;
        int num1,num2,port=0;
        ServerSocket welcomeSocket = null;
        Socket connectionSocket = null;

        //porta dagli argomenti della linea di comando
        try{
            port = Integer.valueOf(args[0]).intValue();
        }catch(ArrayIndexOutOfBoundsException e){
            System.err.println("Insert Server Port! \nCorrect syntax: <programName> <server_port>");
            System.exit(-1);
        }

        /* Inizializza la datagram socket specificando la porta di ascolto */
        DatagramSocket serverSocket =null;
        try{
            serverSocket  = new DatagramSocket(port);
        }catch(SocketException e){
            System.err.println("Socket could not be opened or bind to the specified local port");
            System.exit(-1);
        }

        byte[] receiveData;
        byte[] sendData;

        while(true)
        {

            receiveData = new byte[1024];

            /* Prepara la struttura dati usata per contenere il pacchetto in ricezione */
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

            try{
                /* Riceve un pacchetto da un client */
                serverSocket.receive(receivePacket);
            }catch(PortUnreachableException e){
                System.err.println("The socket is connected to a currently unreachable destination");
                System.exit(-1);
            }catch (IOException e){
                System.err.println("I/O error occured while sending");
                System.exit(-1);
            }catch(Exception e){
                System.err.println(e.getMessage());
                System.exit(-1);
            }

            clientSentence = new String(receivePacket.getData(), 0, receivePacket.getLength());

            /* Ottiene dal pacchetto informazioni sul mittente */
            InetAddress IPAddress = receivePacket.getAddress();
            int portReceived = receivePacket.getPort();

            /*Stampo le info del client*/
            System.out.println(String.format("\nNew UDP Message from Client! %s %d\n",IPAddress.toString(), portReceived));

            /* Leggo i numeri dal messaggio*/
            String[] splitted = clientSentence.split("\\r?\\n| ");
            num1 = Integer.valueOf(splitted[0]).intValue();
            num2 = Integer.valueOf(splitted[1]).intValue();

            /*eseguo il calcolo*/
            int resultint = num1+num2;

            /* Creo la stringa risultante*/
            resultString = String.valueOf(resultint).toString() + '\n';

            sendData = resultString.getBytes();

            /* Prepara il pacchetto da spedire specificando
             * contenuto, indirizzo e porta del destinatario */
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, portReceived);

            try{
                /* Invia il pacchetto attraverso la socket */
                serverSocket.send(sendPacket);
            }catch(IllegalArgumentException e){
                System.err.println("The connected address and the packet address are different");
                System.exit(-1);
            }catch(PortUnreachableException e){
                System.err.println("The socket is connected to a currently unreachable destination");
                System.exit(-1);
            }catch (IOException e){
                System.err.println("I/O error occured while sending");
                System.exit(-1);
            }catch(Exception e){
                System.err.println(e.getMessage());
                System.exit(-1);
            }
        }
    }
}
