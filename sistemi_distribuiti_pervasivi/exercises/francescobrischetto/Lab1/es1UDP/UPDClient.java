import java.io.*;
import java.net.*;

class UDPClient {
    public static void main(String argv[]) throws Exception {

        int num1,num2,port=0;
        String address="",message, result;

        /* indirizzo e porta dagli argomenti della linea di comando */
        try {
            address = argv[0];
            port = Integer.valueOf(argv[1]).intValue();
        }catch(ArrayIndexOutOfBoundsException e){
            System.err.println("Insert Server Address and Port! \nCorrect syntax: <programName> <server_address> <server_port>");
            System.exit(-1);
        }

        /* Inizializza l'input stream (da tastiera) */
        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));

        DatagramSocket clientSocket=null;
        try {
            /* Crea una datagram socket */
            clientSocket = new DatagramSocket();
        }catch(SocketException e){
            System.err.println("Socket could not be opened or bind to the specified local port");
            System.exit(-1);
        }
        /* Ottiene l'indirizzo IP dell'hostname specificato
         * (contattando eventualmente il DNS) */
        InetAddress IPAddress = InetAddress.getByName(address);

        byte[] sendData;
        byte[] receiveData = new byte[1024];

        /* Leggo da tastiera i due numeri */
        System.out.print("Insert First Number: ");
        num1 = Integer.parseInt(inFromUser.readLine());
        System.out.print("Insert Second Number: ");
        num2 = Integer.parseInt(inFromUser.readLine());

        /* Costruisco il messaggio */
        message = String.format("%d %d\n", num1, num2);
        sendData = message.getBytes();

        /* Prepara il pacchetto da spedire specificando
         * contenuto, indirizzo e porta del server */
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);

        try {
            /* Invia il pacchetto attraverso la socket */
            clientSocket.send(sendPacket);
        }catch(IllegalArgumentException e){
            System.err.println("The connected address and the packet address are different");
            System.exit(-1);
        }catch(PortUnreachableException e){
            System.err.println("The socket is connected to a currently unreachable destination");
            System.exit(-1);
        }catch (IOException e){
            System.err.println("I/O error occured while sending");
            System.exit(-1);
        }catch(Exception e){
            System.err.println(e.getMessage());
            System.exit(-1);
        }

        /* Prepara la struttura dati usata per contenere il pacchetto in ricezione */
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

        try {
            /* Riceve il pacchetto dal server */
            clientSocket.receive(receivePacket);
        }catch(PortUnreachableException e){
            System.err.println("The socket is connected to a currently unreachable destination");
            System.exit(-1);
        }catch (IOException e){
            System.err.println("I/O error occured while sending");
            System.exit(-1);
        }catch(Exception e){
            System.err.println(e.getMessage());
            System.exit(-1);
        }

        result = new String(receivePacket.getData(), 0, receivePacket.getLength());

        System.out.println("FROM SERVER: " + result);
        clientSocket.close();
    }
}

