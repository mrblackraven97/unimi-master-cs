package main.java;

import com.example.grpc.SumServiceGrpc.SumServiceImplBase;
import com.example.grpc.SumServiceOuterClass.*;
import io.grpc.stub.StreamObserver;

import java.util.HashSet;
import java.util.LinkedHashSet;

public class SumServiceImpl extends SumServiceImplBase {

    @Override
    public void simpleSum(IntegerCoupleInput input, StreamObserver<IntegerResult> responseObserver){

        int num1 = input.getNum1();
        int num2 = input.getNum2();
        int result = num1+num2;
        IntegerResult response = IntegerResult.newBuilder().setResult(result).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();

    }

    @Override
    public void repeatedSum(IntegerCoupleInput input, StreamObserver<IntegerResult> responseObserver){

        int n = input.getNum1();
        int t = input.getNum2();
        for(int i=0;i<t;i++){
            IntegerResult response = IntegerResult.newBuilder().setResult(n + i*n).build();
            responseObserver.onNext(response);
        }
        //completo e finisco la comunicazione
        responseObserver.onCompleted();
    }
    @Override
    public StreamObserver<IntegerCoupleInput> streamSum(final StreamObserver<IntegerResult> responseObserver){

        return new StreamObserver<IntegerCoupleInput>() {

            @Override
            public void onNext(IntegerCoupleInput integerCoupleInput) {
                int num1 = integerCoupleInput.getNum1();
                int num2 = integerCoupleInput.getNum2();
                int result = num1+num2;
                IntegerResult response = IntegerResult.newBuilder().setResult(result).build();
                responseObserver.onNext(response);
                System.out.println("finito");
            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println("ERRORE!");
                System.out.println(throwable.getMessage());
            }

            @Override
            public void onCompleted() {
                responseObserver.onCompleted();
            }
        };
    }

}