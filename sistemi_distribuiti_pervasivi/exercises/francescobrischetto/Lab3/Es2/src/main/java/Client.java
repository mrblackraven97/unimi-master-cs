package main.java;
import com.example.grpc.SumServiceGrpc;
import com.example.grpc.SumServiceGrpc.*;
import com.example.grpc.SumServiceOuterClass.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

public class Client {

    public static void main(String[] args) throws InterruptedException, IOException {

        System.out.println("Trying to call SimpleSum synchronous method:\n");
        synchronousCall();
        System.out.println("\n...Done!");
        System.out.println("--------------");
        System.out.println("Now calling repeatedSum asynchronous method:\n");
        asynchronousStreamCall();
        System.out.println("\n...Done!");
        System.out.println("--------------");
        System.out.println("Now calling streamSum asynchronous method:\n");
        dataStreamCall();
        System.out.println("\n...Done!");
    }

    public static void synchronousCall(){
        final ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:9999").usePlaintext(true).build();
        SumServiceBlockingStub stub = SumServiceGrpc.newBlockingStub(channel);
        System.out.println("Input value are: 5 and 8");
        IntegerCoupleInput request = IntegerCoupleInput.newBuilder().setNum1(5).setNum2(8).build();
        IntegerResult response = stub.simpleSum(request);
        System.out.println("Result of sum is: "+response.getResult());
        channel.shutdown();

    }

    public static void asynchronousStreamCall() throws InterruptedException {

        final ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:9999").usePlaintext(true).build();
        SumServiceStub stub = SumServiceGrpc.newStub(channel);
        System.out.println("Input value are: 6 and 9 (lmao)");
        IntegerCoupleInput request = IntegerCoupleInput.newBuilder().setNum1(6).setNum2(9).build();
        stub.repeatedSum(request, new StreamObserver<IntegerResult>() {
            private int i = 1;
            public void onNext(IntegerResult response) {
                System.out.println("On the "+i+"-th response received: "+response.getResult());
                i++;
            }
            public void onError(Throwable throwable) {
                System.out.println("Error! "+throwable.getMessage());
            }
            public void onCompleted() {
                channel.shutdownNow();
            }
        });
        //you need this. otherwise the method will terminate before that answers from the server are received
        channel.awaitTermination(10, TimeUnit.SECONDS);
    }

    public static void dataStreamCall() throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        final ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:9999").usePlaintext(true).build();
        SumServiceStub stub = SumServiceGrpc.newStub(channel);
        StreamObserver<IntegerCoupleInput> serverStream = stub.streamSum(new StreamObserver<IntegerResult>(){
            @Override
            public void onNext(IntegerResult integerResult) {
                System.out.println(integerResult.getResult());
            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println("ERRORE!");
                System.out.println(throwable.getMessage());
            }

            @Override
            public void onCompleted() {
                channel.shutdownNow();
            }
        });

        System.out.println("You can now send integers to be added: ");
        while(true){
            try {
                System.out.println("Insert the first number:");
                String message = br.readLine();
                if (message.equals("quit")) {
                    //we communicate to the server that we are done and we exit the loop
                    serverStream.onCompleted();
                    break;
                }
                int num1 = Integer.parseInt(message);
                System.out.println("\nInsert the second number:");
                message = br.readLine();
                if (message.equals("quit")) {
                    //we communicate to the server that we are done and we exit the loop
                    serverStream.onCompleted();
                    break;
                }
                int num2 = Integer.parseInt(message);
                serverStream.onNext(IntegerCoupleInput.newBuilder().setNum1(num1).setNum2(num2).build());
            }catch(NumberFormatException e){
                System.out.println("Not a Number! Try again");
            }catch(Exception e){
                System.out.println(e.getMessage());
                serverStream.onCompleted();
                break;
            }
        }
        System.out.println("Goodbye!");
    }
}