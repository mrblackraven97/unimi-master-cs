package main.java;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;

public class StudenteClass implements Serializable {
    private String Nome ;
    private String Cognome ;
    private Date AnnoDiNascita ;
    private Hashtable<String,Integer> Esami;
    private String via ;
    private int numero;
    private String scalaePiano;

    public StudenteClass(String n, String c, Date a, Hashtable<String,Integer> e, String v, int num){
        Nome = n;
        Cognome = c;
        AnnoDiNascita = a;
        Esami = e;
        via = v;
        numero = num;
    }
    @Override
    public String toString(){
        String str1 = String.format("Student\nNome: %s\nCognome: %s\nAnno di Nascita %s\n",Nome,Cognome,AnnoDiNascita.toString());
        for(int i=0; i<Esami.size();i++){
            str1+=String.format("Esame: %s\n Voto Esame: %d",Esami.keySet().toArray()[i], Esami.values().toArray()[i]);
        }
        str1+= String.format("\nIndirizzo: via %s n° %d",via,numero);
        return str1;
    }
}
