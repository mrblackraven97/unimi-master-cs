package main.java;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

import com.google.gson.Gson;
import main.proto.Studente.StudenteOuterClass.Studente;

public class ServerUniversità {

    public static void main(String[] args) throws IOException {


        ServerSocket serverSocket = new ServerSocket(9999);
        while(true){
            try {
                Socket s = serverSocket.accept();
                Studente stu = Studente.parseFrom(s.getInputStream());
                System.out.println(stu);
                //getting the size of student
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos);
                oos.writeObject(stu);
                oos.close();
                System.out.println("Size: "+baos.size());

            }catch(IOException e){
                System.err.println(e.getMessage());
                System.exit(-1);
            }
        }

    }


}