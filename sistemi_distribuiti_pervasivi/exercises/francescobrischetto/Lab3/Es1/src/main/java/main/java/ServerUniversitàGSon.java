package main.java;

import com.google.gson.Gson;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerUniversitàGSon {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(9998);
        while(true){
            try {
                Socket s = serverSocket.accept();
                BufferedReader inFromClient = new BufferedReader(new InputStreamReader(s.getInputStream()));
                Gson gson = new Gson();

                StudenteClass studente = gson.fromJson(String.valueOf(inFromClient.readLine()),StudenteClass.class);
                System.out.println(studente);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos);
                oos.writeObject(studente);
                oos.close();
                System.out.println("Size: "+baos.size());

            }catch(IOException e){
                System.err.println(e.getMessage());
                System.exit(-1);
            }catch(Exception e){
                System.err.println(e.getMessage());
                System.exit(-1);
            }
        }
    }

}
