package main.java;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Date;
import java.util.Hashtable;


import com.google.gson.Gson;
import main.proto.Studente.StudenteOuterClass.Studente;

public class ClientStudente {
    public static void main(String[] args) throws IOException {

        Socket s = new Socket("localhost", 9999);
        Socket s1 = new Socket("localhost", 9998);
        Studente r = Studente.newBuilder()
                        .setNome("Francesco")
                        .setCognome("Brischetto")
                        .setAnnoDiNascita(1997)
                        .setResidenza(Studente.Res.newBuilder().setVia("Via Birago").setNumeroCivico(44))
                        .addEsami(Studente.Esame.newBuilder().setNome("Sistemi Distribuiti e Pervasivi").setVoto(26).setDataDiVerbalizzazione(
                                Studente.Data.newBuilder().setGiorno(1).setMese(6).setAnno(2020)
                        ))
                        .addEsami(Studente.Esame.newBuilder().setNome("Protocolli ad hoc").setVoto(25).setDataDiVerbalizzazione(
                                Studente.Data.newBuilder().setGiorno(6).setMese(7).setAnno(2020)
                        ))
                        .build();

        r.writeTo(s.getOutputStream());
        s.close();


        Hashtable<String,Integer> ht = new Hashtable<String,Integer>();
        ht.put("Sistemi Distribuiti e Pervasivi",26);
        //Con GSon
        Gson gson = new Gson();
        StudenteClass studente =  new StudenteClass("Francesco","Brischetto",new Date(2020,6,1), ht ,"Birago",44);

        DataOutputStream outToServer = new DataOutputStream(s1.getOutputStream());

        String json = gson.toJson(studente);
        outToServer.writeBytes(json + "\n");
        s1.close();


    }
}