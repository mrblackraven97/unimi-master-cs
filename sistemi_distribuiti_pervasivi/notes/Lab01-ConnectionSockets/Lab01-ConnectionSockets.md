# Sockets and Web Programming

A **socket** is an endpoint for a double edge communication between two web processes. Sticking at process level, socket are used to build an interface on TCP and UDP level.

Each socket is bound to a port number, so that the transport level (UDP or TCP) can identify to which application give the data received via the internet connection. A connection between two process is specified by a couple IP/Port between two computers.

![](img/socketlevel.png)

There are different kinds of sockets:

- **Listening socket**, on server side, it's a socket that waits for new entering connections;
- **Established socket**, used in TCP, it refers to an active connection between endpoints;
- **Datagram socket**, used in UDP, when there is no connection to send and receive data.

# Client - Server TCP Communication

Client creates a socket specifying the receiver address and a port. When the connection gets established, the established socket created is used as a communication interface.

Server creates a listening socket specifying a port. When a new connection is requested, a **new** established socket gets created. The entire communication proceeds via server's established socket to client's one. 

![](img/tcpcomm.png)

**N.B.** When client moves to established socket, he will always talk on the same port. On server side, port will be always the same even if the socket changes, because connection gets verified from both sides.

## Java.net Package

For working with sockets and connections, Java offers a complete package full of useful classes:

- Client and TCP server classes;
- UDP Datagram class;
- Multicast Datagram class.

This package, as a Java package, allows the creation of connections independently from the operative system, convenient and reusable. 

Remember to manage the connection errors with exceptions!

Main classes for package `java.net`:

- `java.net.Socket`;
- `java.net.ServerSocket`;
- `java.net.DatagramSocket`;
- `java.net.DatagramPacket`;
- `java.net.MulticastSocket`;
- `java.net.InetAddress`;
- `java.net.URL`;
- `java.net.URLConnection`;
- `java.net.URLEncoder`;
- `java.net.HttpURLConnection`.

## Socket Class

Class constructors:

- `Socket(InetAddress, int)`: Connects to the specified address and port;
- `Socket(String, int)`: Connects to the named host and port;
- `Socket(InetAddress, int, InetAddress, int)`: Allows the specification of both endpoints.

Useful methods:

- `getInputStream()`: Return an `InputStream` for the socket, allowing the user to write stuff;
- `getOutputStream()`: Return an `OutputStream` for the socket;
- `getInetAddress()`: Return the remote address;
- `getPort()`: Return the remote port;
- `getLocalAddress()`: Return the local address;
- `getLocalPort()`: Return the local port;
- `close()`: Close the connection.

## ServerSocket Class

Class constructors:

- `ServerSocket(int)`: Create a server socket on a specified port;
- `ServerSocket(int, int)`: Create a server socket with a specified backlog length;
- `ServerSocket(int, int, InetAddress)`: A server socket with backlog that can specify local address.

**N.B.** Ports are unique, only one connection can stay on a specific port.

Useful methods:

- `accept()`: Listen for a connection, blocked until connection is made;
- `getInetAddress()`: Returns the local address;
- `getLocalPort()`: Returns the local port;
- `close()`: Close this socket.

## Server

```Java
// Bind on port 80.
ServerSocket serverSocket = new ServerSocket(80);

// Creating an established socket with a single client. Binding.
Socket s = serverSocket.accept();
```

Server binds on `accept()` until a client requests a connection. When the connection is received, the established socket and the execution flow go on.

Multiple connections from clients are moved into a queue, then accepted one at a time.

## Socket Communication

As said before, the `Socket` class provides two distinct methods: `getInputStream()`(binding) and `getOutputStream()`. 

Those are two unidirectional channels, and can be wrapped with any class that can handle input/output stream, like:

- `BufferedReader`;
- `BufferedWriter`;
- `DataOutputStream`;
- `PrintWriter`;
- ...

Always remember to explicitly close a socket connection using the method `close()` available for `Socket` and for `ServerSocket`.

Also remember to handle exception.

![](img/closeconn.png)

## Example: Upper Case Server

- Client reads a row from keyboard (stream `inFromUser`) and sends it to a server via socket (stream `outToServer`);
- Server reads the line from socket;
- Server converts the row to upper case and sends it back to client;
- Client reads from server the new line (stream `inFromServer`) and prints it on scream.

*TCPClient.java*

```java
import java.io.*;
import java.net.*;

class TCPClient {
	public static void main(String argv[]) throws Exception {
		String sentence;
		String modifiedSentence;
		
        /* Inizializza l’input stream (da tastiera) */
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
        
		/* Inizializza una socket client, connessa al server */
		Socket clientSocket = new Socket("localhost", 6789);
        
		/* Inizializza lo stream di output verso la socket */
		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        
		/* Inizializza lo stream di input dalla socket */
		BufferedReader inFromServer = new BufferedReader(new 		 	   									InputStreamReader(clientSocket.getInputStream()));
		
        /* Legge una linea da tastiera */
		sentence = inFromUser.readLine();
		
        /* Invia la linea al server */
		outToServer.writeBytes(sentence + ’\n’);
		
        /* Legge la risposta inviata dal server (linea terminata da \n) */
		modifiedSentence = inFromServer.readLine();
		System.out.println("FROM SERVER: " + modifiedSentence);
		clientSocket.close();
    }
}
```

*TCPServer.java*

```Java
import java.io.*;
import java.net.*;

class TCPServer {
	public static void main(String argv[]) throws Exception {
		String clientSentence;
		String capitalizedSentence;

        /* Crea una "listening socket" sulla porta specificata */
		ServerSocket welcomeSocket = new ServerSocket(6789);
		while(true) {
            /*
            * Viene chiamata accept (bloccante).
            * All’arrivo di una nuova connessione crea una nuova
            * "established socket"
            */
            Socket connectionSocket = welcomeSocket.accept();
            
            /* Inizializza lo stream di input dalla socket */
            BufferedReader inFromClient = new BufferedReader(new
            		InputStreamReader(connectionSocket.getInputStream()));
            
            /* Inizializza lo stream di output verso la socket */
            DataOutputStream outToClient = 
                	new DataOutputStream(connectionSocket.getOutputStream());
            
            /* Legge una linea (terminata da \n) dal client */
            clientSentence = inFromClient.readLine();
            capitalizedSentence = clientSentence.toUpperCase() + '\n';
            
            /* Invia la risposta al client */
            outToClient.writeBytes(capitalizedSentence);
        }
    }
}
```

# Client - Server UDP Communication

UDP is a protocol aimed to support connection-less communication.

Client:

- Creates a datagram socket;
- Prepares a datagram packet containing the server address and the destination port;
- Sends the packet through the datagram socket;
- Waits a response package from server on the datagram socket.

Server:

- Creates a datagram socket, specifying a listening port;
- Waits for a packet from client on the datagram socket;
- Extract address and client port from that packet;
- Prepares a new datagram packet to answer the request, specifying client address and port;
- Sends the packet through datagram socket;
- Returns in his waiting state.

## DatagramSocket Class

Class used for creating packets for UDP connections.

Constructor:

- `DatagramSocket()`: Binds to any available port;
- `DatagramSocket(int)`: Binds to the specified port;
- `DatagramSocket()`: Can specify a local address.

Useful methods:

- `getLocalAddress()`: Returns the local address;
- `getLocalPort()`: Returns the local port;

## Example: UDP Upper Case Server

*UDPClient.java*:

```java
import java.io.*;
import java.net.*;

class UDPClient {
	public static void main(String args[]) throws Exception {
        /* Inizializza l’input stream (da tastiera) */
        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
        
        /* Crea una datagram socket */
        DatagramSocket clientSocket = new DatagramSocket();
        
        /* Ottiene l’indirizzo IP dell’hostname specificato
        * (contattando eventualmente il DNS) */
        InetAddress IPAddress = InetAddress.getByName("localhost");
        byte[] sendData = new byte[1024];
        byte[] receiveData = new byte[1024];
        String sentence = inFromUser.readLine();
        sendData = sentence.getBytes();
        
        /* Prepara il pacchetto da spedire specificando
        * contenuto, indirizzo e porta del server */
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length,
        IPAddress, 9876);
        
        /* Invia il pacchetto attraverso la socket */
        clientSocket.send(sendPacket);
        
        /* Prepara la struttura dati usata per contenere il pacchetto in ricezione */
        DatagramPacket receivePacket =
        new DatagramPacket(receiveData, receiveData.length);
        
        /* Riceve il pacchetto dal server */
        clientSocket.receive(receivePacket);
        String modifiedSentence = new String(receivePacket.getData());
        System.out.println("FROM SERVER:" + modifiedSentence);
        clientSocket.close();
    }
}
```

*UDPServer.java*:

```java
import java.io.*;
import java.net.*;

class UDPServer {
    public static void main(String args[]) throws Exception {
        /* Inizializza la datagram socket specificando la porta di ascolto */
        DatagramSocket serverSocket = new DatagramSocket(9876);
        byte[] receiveData = new byte[1024];
        byte[] sendData = new byte[1024];
        while(true) {
            /* Prepara la struttura dati usata per contenere il pacchetto in ricezione */
            DatagramPacket receivePacket =
            new DatagramPacket(receiveData, receiveData.length);
            
            /* Riceve un pacchetto da un client */
            serverSocket.receive(receivePacket);
            String sentence = new String(receivePacket.getData());
            
            /* Ottiene dal pacchetto informazioni sul mittente */
            InetAddress IPAddress = receivePacket.getAddress();
            int port = receivePacket.getPort();
            String capitalizedSentence = sentence.toUpperCase();
            sendData = capitalizedSentence.getBytes();
            
            /* Prepara il pacchetto da spedire specificando
            * contenuto, indirizzo e porta del destinatario */
            DatagramPacket sendPacket =
            new DatagramPacket(sendData, sendData.length, IPAddress,
            port);
            
            /* Invia il pacchetto attraverso la socket */
            serverSocket.send(sendPacket);
        }
	}
}
```

# Iterative Server

Let's examine the full process for an iterative server:

![](img/iterserver.png)

![](img/iterserver2.png)

Problem: Bottleneck effect, services can only be processed one at a time, there is a need of parallelism.

# Thread

Servers need to elaborate multiple client requests at the same time, to do so they need the use of **threads**, creating **multithread servers**.

Threads are **distinct instructions sequence of the same process that share the same memory** and scheduled as they are process themselves.

Threads are useful for concurrent applications:

- Asynchronous events;
- Overlapping of input/output operations;
- ...

In multithread servers, thread are used to run different execution instances and handle many client at the same time.

## Thread in Java

Threads in Java can be implemented in two ways, by:

- Inheriting class `Thread`;
- Extending the `Runnable` interface;

### Extending Thread Class

Make a thread by:

1. Extending the class `Thread` defining a constructor that takes as arguments referrals to data structures on witch the thread will work on;
2. Redefining method `run()` so that the thread can execute his specific task.

You can call and start a thread made this way by:

1. Create the thread object instance;
2. Call method `start()`.

### Implementing Runnable

This procedure consists in:

1. Implementing the interface `Runnable` defining a constructor that takes as arguments referrals to data structures on witch the thread will work on;
2. Implementing method `run()` so that the thread can execute his specific task.

You can call and start a thread made this way by:

1. Create a `Thread` object instance, passing to his constructor an instance of the class that implements `Runnable`;
2. Call method `start()`.

### Confronting Methodologies

![](img/threadmaking.png)

After creating and starting a thread there are two different executions of code running: one is the thread's `run()`, the other is the point immediately after the call on `start()`.

A thread father can have a referral on a child thread's object. This referral is useful for manipulating and handling threads in code.

**N.B.** Once method `run()` ends, his thread is killed and can't be used again, it must be re-instantiated.

## Concurrent Servers

Request from different clients can reach concurrently the listening port on the server and on iterative servers, those requests are moved into a queue and then processed once at a time sequentially. This is quite inefficient.

A solution could be a server that can handle more simultaneous connection at the same time with the use of threads, by running a thread for each client connection (multithread server).

![](img/multiserver.png)

*Example: MultiServer.java*:

```java
import java.io.*;
import java.net.*;

class TCPMultiServer {
    public static void main(String argv[]) throws Exception {
        ServerSocket welcomeSocket = new ServerSocket(6789);
 		
        while(true) {
        	Socket connectionSocket = welcomeSocket.accept();
        
            /* Creazione di un thread e passaggio della established socket */
            TCPServerThread theThread = new TCPServerThread(connectionSocket);

            /* Avvio del thread */
            theThread.start();
        }
    }
}
```

*Example: ServerThread.java*:

```java
import java.io.*;
import java.net.*;

public class TCPServerThread extends Thread {
    private Socket connectionSocket = null;
    private BufferedReader inFromClient;
    private DataOutputStream outToClient;
    
    /* L’argomento del costruttore e’ una established socket */
    public TCPServerThread(Socket s) {
        connectionSocket = s;
        try{
        	inFromClient = new BufferedReader(
        			new InputStreamReader(connectionSocket.getInputStream()));
        	outToClient = new DataOutputStream(connectionSocket.getOutputStream());
        } catch (IOException e) {
        	e.printStackTrace();
        }
    }
    
    public void run() {
        String clientSentence;
        String capitalizedSentence;
        try {
            clientSentence = inFromClient.readLine();
            capitalizedSentence = clientSentence.toUpperCase() + '\n';
            outToClient.writeBytes(capitalizedSentence);
            connectionSocket.close();
        } catch (IOException e) {
        	e.printStackTrace();
        }
    }
}
```

