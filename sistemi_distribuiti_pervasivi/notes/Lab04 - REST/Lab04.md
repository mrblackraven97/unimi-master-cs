# Puntatori A Strumenti Moderni

Tecniche più recenti che astraggono i concetti precedenti di synchronized wait e notify.

Da non usare nel progetto perchè troppo facile.

Java mette a disposizione le Concurrency API java.util.concurrent che consentono di gestire problemi di concorrenza in ambito multi-thread semplificato

Gestire i thread è tedioso e molto error prone, quindi java mette a disposizione degli executorservice che fa partire i tread in automatico

i task assegnati sono dei runnable che vegnon aggiunti tramite metodo submit

[esempio codice]

Vari tipi di executors

- fixed thread pool dove specificare un numero fisso dithread che lavorano su una coda condivisa
- cached thread pool che crea nuovi thread solo quando serve, se sono già disponibili altri si usano quelli
- scheduled thread pool
- single thread executor, un singolo worker thread che opera su una coda

un executorservice va stoppato explicitamente, altrimenti rimane sempre in attesa di istruzioni

- shutdown()
- shutdownnow()

possibilità di avere dei runnable che restituiscono un valore tramite oggetti Callable che possono essere passati agli executors

per prendere il risultato di una callable si usano i future. creo collable, la do all'executor e quello che viene restituito è un oggetto di tipo future che in futuro ci permette di prendere il risultato

l'uso di executor non garantisce nulla rispetto alla sincronizzazione, ordine di esecuzione non deterministico

synchronized nasconde molto del meccanismo di lock, ma le concurrency API ci fanno semplificare ancora di più utilizzando un ReentrantLok per decidere di prendere o rilasciare il lock con la clausola finally, clausola che permette di eseguire l'istuzione qualsiasi cosa succeda.

le concurrency api permettono di avere strutture per la sincronizzazione

astrarre api su wait e notify, i thread attengono su una particolare condizione finchè non vengono risvegliati

ogni condizione è legata ad uno specifico lock, dove ogni condizione è legata ad uno specifico lock e ne gestisce l'acquisizione e il rilascio atomico.

## Condition Example

...

## cd latche

uno o piu trhead aspettano finche le operazioni eseguite da un altro gruppo dithread non finisce

un contatore da implementare inizializzato da un intero n dove un thread si mette in await

n tread

...

## fork join

implementazione per gestire parallelismo hardware, l'idea è di usare tutta la capacità di processamento per velocizzare l'esecuzione, una specie di map.reduce

fork join si divide nei passi fork e join, dove si divide un task grande in sottotask più piccoli, fino ad arrivare ad un sottotask che puo essere processato da un singolo thread per poi ricombinare i risultati intermedi tramite join

# Server Rest

Representational State Transfer per la comunicazione machine to machine tramite HTTP, uno stile architetturale che si basa su 4 metodi di HTTP: Get post put delete

rest è basato sui 4 principi:

- utilizzo esplicito di metodi http
- essere stateless, non tiene traccia dello stato del client
- ogni risorsa è identificata da un uri uniform resource identifier e la struttura è simile e analoga a quella delle directory
- utilizzo xml o json per trasmettere informazioni

### pratica comune

utilizzo sempre get

definisco parametro operazionale con l'idea di aggiungere un nuovo utente al sistema, facendo una chiamata di tipo get

problema semantico, il metodo get è definito per ottenere informazioni, non per inserirle, concettualmente sbagliato

inoltre dei web crawler potrebbero non intenzionalmente cambiare i dati del server

corrispondenza uno a uno tra i metodi di http e le operazioni crud

- Create ↔ POST
- Read ↔ GET
- Update ↔ PUT
- Delete ↔ DELETE

## ßerver stateless

non ha in memoria lo stato corrente del client, ogni richiesta deve essere indipendente

un client deve fare richiesta con tutte le informazioni necessarie per poter forire la richiesta, senza informazioni pregresse

## risorse

in ogni operazione di fatto il client riceve o comunque lavora su una particolare risorsa

importante organizzarle nel modo più consono possibile



























