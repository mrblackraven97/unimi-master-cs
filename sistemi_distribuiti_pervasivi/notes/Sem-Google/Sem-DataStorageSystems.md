# Large Scale Data Storage and Processing on Google's Distributed Systems

Google's mission is to **organize the world's information and make it universally accessible and useful**. This mission consists in handling an enormous amount of data and, to do so, Google has developed many forms of distributed systems, minimizing the developers' concern on how their algorithms are executed in a distributed way.

Organizing information: 

- Protocol buffer;
- Google File System;
- Bigtable;
- Spanner/F1.

Processing information:

- MapReduce;
- Flume;
- MillWheel.

Serving information:

- High Availability

## Organizing Information

Over the years many different storage technologies have been developed at Google for storing enormous amounts of data, for example:

- Web index;
- Cache archive of web pages;
- All YouTube videos;
- Maps and related information;
- GMail data;
- Google Photos;
- Google Drive.

## Storage Systems

All the technologies developed in Google (code and implementation) are proprietary and reserved, but the results and main concepts are shared with the research community. This led to the development of many open-source alternative implementations available for general consumption.

Example names:

- Chubby;
- Google File System;
- Bigtable;
- Megastore;
- Colossus;
- Blobstore;
- Spanner;
- F1.

Website containing a set of Google research documents: http://research.google.con/

## Protocol Buffers

During the development of these storage systems, Google needed a vector with a common language to let these systems communicate. That's because each system has its own language, like in the case of a Java server that wants to communicate with a C++ one.

From these needs, Protocol buffer was born. It is a **structured representation of data**. Differently from JSON and XML (human-readable plain text documents), protocol buffer's **message definition is binary and compiled into native classes** and is supported in different languages.

While JSON and XML are somewhat human-readable formats, protocol buffer messages are completely compiled and serialized as binary files, trading off readability for: 

- Efficiency;
- Compactness in size;
- Safer for backwards compatibility.

Let's see an example of protocol buffer message:

```protobuf
message WebPage
{
	required string url = 1;
	required string html = 2;
	repeated string keywords = 3;
	optional bool visited = 4;
}
```

Protocol buffer's structure is pretty simple, similar to JSON with name-value fields.

## GFS: Google File System 

The GFS is a **massively distributed and fault tolerant file system that efficiently stores and retrieves data**. 

It is essentially a distributed storage file-system: it's like a traditional os file-system, since it provides basic POSIX calls for files, such as `open()`, `read()`, `write()`, `close()`, however with the possibility of multiple clients accessing it simultaneously.

To improve the data availability, it stores several copies of it in different machines.

![](img/gfs.png)

The files and their copies are stored in different **chunkservers**, managed by **master** servers. Chunkservers are logical components, replicated in different servers.

When a file is requested by a client, it only talks with the master server or one of its various replicas (masters are replicated to be able to handle multiple clients). Clients interact with chunkservers with a distributed load weight because of replicas, this avoids overloading a single server.

The master-server then finds the requested file in chunk-servers and, if found, prompts them to send the file directly to the client.

## Colossus: An Advanced GFS

The original implementation of GFS is old of 15 years, several improvements have been applied to it, resulting in the newer technology called Colossus, whose main features are:

- It uses Bigtable technology to store metadata (we'll see it soon);
- Increases file size limits;
- Improves the latency;
- Decrease storage usage and improve fault-tolerance:
  - Redundancy can be achieved by using **Reed-Solomon encodings**, a fault-tolerant encoding method for a distributed storage environment.
  

*Example*: In order to distribute data across *k* individual servers for improved storage capacity or bandwidth. Such a configuration risks significant data loss in the event of server failure. The Reed-Solomon encoding produces a storage coding system which is robust to the simultaneous failure of any subset of *m* nodes. To do this, we adding *m* additional nodes to the system.

## Bigtable

Bigtable is one of the ancestors of the NoSQL paradigm, that caused the creation of many data storage solution not necessarily based on SQL/Relational model. Those are solution capable of handling any data size with efficient access time.

Bigtable is a **distributed, multi-dimensional sorted map**, designed to store billions of rows, scaling across thousands of machines. Since a certain network speed and latency is expected for this system to work correctly, all it's a database that doesn't have any feature from the relational schemas and tables. Every machines should be in the same data-center for the best performance, however, it can provide data replication across multiple data-centers.

On his general implementation it was built in stack on top of GFS, but in time it became more self-efficient, it does not need a GFS back-end, it has one on its own.

![](img/bigtable.png)

Inside of the database, each table is defined with a scheme where rows are strings. It is possible to save different version in time of a web page in the same cell tagged with different timestamps:

- **Rows** only contain strings:
  - If a developer wants to store a different type of data, he has to convert it into a string in some way;
  - In the example, there is a row relative to the website "www\.cnn\.com";
- **Columns** can contain different data relative to that string:
  - Each column can contain simple or aggregate data;
  - In the example, at different timestamps, the HTML contents of that web-page is saved, so there is an array of various sub-cells in our column.

### Tablets

These **large tables** we spoke about are then **broken into smaller tablets at some row boundaries**, each containing a certain range of contiguous rows: These tablets should be from 100 MB to 200 MB in size for better performances, there is a tradeoff between the number of tablet in cache to not overload the tablet server and limit accesses.

The mechanism for the work division is the identification of extreme boundaries indicating that the first table that contains from a string "A" to a string "C", so if a client requires the string "B" it knows where to look.

Each server machine (a tablet-server) is responsible for roughly 100 tablets, and if one crashes, a fail-safe mechanism triggers (**fast recovery**): 100 others machines each pick up 1 tablet from the failed machine, in order to balance the load and maintain the fault-tolerance capability.

**Fine grained load balancing**: To balance the load even further and more accurately, tablets are migrated away from overloaded machine to newer or stable machines. All these migrations and load balancing activities are handled by the master.

## Bigtable Problems

Bigtable is good for various reasons but, because of its "NoSQL properties", it lacks support for:

- Transactions, because atomicity is only guaranteed at row-level, it is only known that some data has been written only if the server returns something (*eventually consistent system*);
- Relational tables, because it doesn't obviously support for relational paradigm. For applications requiring complex, evolving schemas, Bigtable can be difficult to use and maintain.

Other systems were built on top of Bigtable trying to fix this: for example Megastore implemented transactions and replication. There is, however, only so much you can do without adding too much complexity, at some point, it's just better to redesign a new system.

## Spanner

Spanner is a new **distributed multi-version database** that has been studied and developed for years to provide stronger guarantees than Bigtable with transactions and global replication. It supports:

- **General-purpose transactions** which ensure by ACID properties:
  - Atomicity, transactions are executed completely or not executed at all;
  - Consistency, only valid data is conserved;
  - Isolation, transactions don't affect each other;
  - Durability, written data will not be lost.
- **SQL query language**;
- **Schematized tables**;
- **Semi-relational data model**:
  - Differently from full relational model, in which every table can have arbitrary relations with others, here there are "master-tables" which can have relationships with lesser tables. This paradigm ensures that many optimizations on the database model can be applied.

Spanner was designed to cover the lack of capabilities of Bigtable if the machines were distributed along different data-centers instead of only one; it keeps some similarities, though.

Just like in Bigtable, **data is versioned and tagged with a timestamp** at commit time. 

**Global distribution of data is configurable**, which means that data can automatically be moved closer to its most frequent users, consequently reducing the latency for those (smart migration).

Spanner provides **strong consistency guarantees**: 

- Ensures external consistency of reads and writes;
- Ensures global consistent reads across the database given a certain timestamp.
  - *Example*: Reading all the data from the database that was present at a certain timestamp. This is particularly useful for backups, atomic schema changes and so on...

#### Implementation

![](img/spanner.png)

There are two central coordinators: the **universemaster** and the **placement driver**, they coordinate the different logical zones, spread in different data-centers, in which the Spanner system is highly replicated through the zone-master.

Each zone has his numerous **spanservers**, which act exactly like tablet-servers, holding 100-1000 tablets. These tablets are replicated across data-centers (zones).

This model lets **transaction take the lock** not of the full table, but only **on the rows it is affecting** (including those from other tablets with relationships to those rows). This kind of **distributed locking happens at a tablet-level**:

- At each transaction the commit timestamp is confirmed across zones using Paxos;
- This is made possible by synchronization through a particular "uncertain" time representation (TrueTime) based on atomic clocks with possible calculated time drift. Performance is highly dependent on this parameter. 

Let's describe an example of a transaction in Spanner:

1. A client wants to make a transaction, eventually specifying a timestamp. To obtain the lock for that transaction the system must ensure that no one else is writing already at that timestamp;
2. Instead of considering a single point in time, TrueTime gives a window of time (as we can see below) in which the client can execute the transaction. This is done because it's difficult to ensure a perfect synchronization between the clocks of our machines in our distributed storage system.

<img src="img/truetime.png" style="zoom:67%;" />

## F1 Query System

F1 is the last piece of software that completes the data-storage distributed system stack. F1 is an **added system over Span, that uses it as a back-end for data**.

<img src="img/f1.png" style="zoom: 70%;" />

F1 server has a relational schema, compatible with SQL, and provides  **interfaces with the client, receiving all the data queries from him**. These queries are then assigned to multiple query workers, which will interrogate the Spanner server in his own language to retrieve the requested data. Each spanner server has underneath them a GFS system, from which obtain the data (some of which is stored in its memory too).

F1 features:

- A relational schema with:
  - Consistent indexes;
  - Extensions for hierarchy and rich data types (such as Protocol Buffers);
  - Non-blocking and asynchronous schema changes.
- Multiple interfaces that the client can use to interact with it and query for data:
  - SQL;
  - Key/Value;
  - MapReduce.
- Change Notifications, a mechanism that gives a notification when someone makes changes over the system.

F1 architecture is made by:

- Sharded Spanner servers: Data on GFS and in memory;
- Stateless F1 server;
- Worker pools for distributed SQL execution.
  - Because Spanner does not support SQL, complex queries are distributed among query workers that computes them and bring the result back in a compatible format to Spanners.

In F1 **columns data types are mostly Protocol Buffers**: Instead of creating a different column for a different type of data (name, surname, age, date, etc...) there is a single column which only store Protocol Buffers, which is a highly customizable format, and can be stored like blobs in Spanner. 

Furthermore, it is possible to translate the SQL queries to search for nested fields in our Protocol Buffers, just as if they were treating normal tables. This guarantees a coarser schema with fewer tables and much more in-lined objects.

To sum up, F1 greatly improves the handling of data storage by simplifying the schema:

- Since Protocol buffer is so pervasive in Google there is no impedance mismatch;
- Simplified schema and code because applications use the same objects (Protocol Buffer) and they are very efficient;
- Need to implement foreign keys or joins, even if data is in-lined.

#### Example: F1 SQL Query on Protocol Buffers

<img src="img/f1ex.png" style="zoom: 80%;" />

Here we can see a great example of how F1 and Protocol Buffers can greatly simplify the query of the same information: we can take advantage of the simple representation of the customer on F1 to join that information with the one on the relational table. 

This mechanism is called **PROTO JOIN**: through this, I'm able to perform an INNER JOIN between the simplified table that is made by Protocol Buffers on a certain field (here I'm joining on the feature_id) with the corresponding relational table with the same name. (probably not accurate example, fix this please).

# The MapReduce Model

The MapReduce is a programming model for **processing and generating large datasets**. This model, appearing quite similar to the functional programming model, can be applied to a large variety of problems, allowing for **parallel computation** of them in the following form, allowing to regroup and convert tuples to a single value list:

- Map: `(k1, v1) → list(k2, v2)`;
- Reduce: `(k2, list(v2)) → list(v2)`.

Where `k1` and `v1` is an input key-value map, while `v2` is the desired output.

For example, let's suppose to count the number of occurrences of each word in a large set of documents. Our functions, following the MapReduce model, will obviously be:

- **Map**: Our map function will emit a symbolic "1" for each word;

  ```pseudocode
  map(String key, String value):
  	// key: document name
  	// value: document contents
  	for each word w in value:
  		EmitIntermediate(w, "1");
  ```

  

- **Reduce**: Our reduce function will get called once per each unique word, with the list of values (the "1"s I calculated with the map) associated to it. It will sum all the occurrences of "1"s for that word and report the result in the output list

  ```pseudocode
  reduce(String key, Iterator values):
  	// key: a word
  	// values: a list of counts
  	int result = 0;
  	for each v in values:
  		result += ParseInt(v);
  		Emit(AsString(result));
  ```

However, we still need a shuffle mechanism between the Map and the Reduce, in which:

- Shuffle: `list(k2,v2) → (k2, list(v2))`;

Which means that given a list of key-values, the output is a tuple of unique keys and a list of values.

In the example, after all words are mapped with the symbolic "1", we need to condensate the same words into one unique entry, adding all the relative "1"s to a list.

The Reduce function will then condense the "1"s list to the total number of occurrences, as we showed before.

Luckily, the system can perform the shuffle (intermediate) phase for us.

![](img/mapreduce.png)

How a MapReduce system works: 

- The MapReduce program is enclosed in an user program, and when it's launched it can act as: 
  - **Worker**, in which will compute the two main phases of the program (map and reduce), each on a different split of the input files;
  - **Master**, in which will handle and coordinate the job of the workers.

We can see how easily we can distribute the workers amongst our, obviously, distributed system! Because of its qualities, there are plenty of possible applications, for example:

- Distributed Grep
  - Map-only, emit result if a keyword is found;
- Count URL clicks
  - Input is weblogs;
  - Map emits the couples <URL, 1>, just like in the word-count example
  - Reduce emits the couples <URL, count>
- Reverse Web-Link graph (given a target page, identify all source pages that links to that target page)
  - Map scans all the target links from each source page, emitting <target, source>
  - Reduce concatenates the list of all sources, emitting <target, list(source)>

The strength in this system is also the ability to concatenate multiple MapReduce for solving more complex algorithms: however, in this way the system can become highly unmaintainable.

## FlumeJava 

In order to solve the MapReduce pipeline concatenation problem, a new library, bundled with its API, has been developed: FlumeJava.

This Java library has been designed for writing data-parallel pipelines, with ad-hoc classes and methods to manage possibly huge immutable collections (the typical containers in a MapReduce operation).

The MapReduce will be organized following an execution graph, which tells the order that the operations will follow.

FlumeJava is also shipped with:

- An **optimizer**, capable of fusing similar data-parallel operations together in a pipeline, organizing the MapReduce in the most efficient way possible;
- An **executor**, which runs the optimized execution graph and manage it during the execution, offering garbage collection, task parallelism, fault tolerance and monitoring.

#### Example: Top Words

Let's see an example: Given a text file, let's find the top words in it:

```java
// read a txt file and obtain each line separated by \n, saving it in a PCollection
PCollection<String> lines = readTextFile("...");

// call (thus map) the ExtractWordsFn function on each line in a parallel way, 
// obtaining a PCollection of words
PCollection<String> words = lines.parallelDo(new ExtractWordsFn());

// Let's create a PTable made by the couples <word, occurrence> using the count()
// method from the Flume API
PTable<String,Long> wordCounts = words.count();

// From the precedent PTable, let's extract a reduced PTable with the top 1000 words 
// given an order function WordOrderFn() (top is also from Flume API)
PTable<String,Long> topWords = wordCounts.top(new WordOrderFn(), 1000);

// From the table, let's create a PCollection of the formatted string that represent
// each couple from the top table, given a format function FormatFn() in a parallel way
PCollection<String> formattedOutput = topWords.parallelDo(new FormatFn());

// write the collection of formatted output on a file
formattedOutput.writeToTextFile("...");

// run the pipeline
FlumeJava.run();
```

We can see the usage of the ad-hoc data containers PTable and PCollection: these immutable collections are then used by different FlumeJava methods in order to apply the MapReduce paradigm in a parallel way. For example, the `parallelDo()` method executes a function (executing the Map step) on every element of a PCollection in a parallel way.

In fact we can see that, for example, `FormatFn()` used by parallel do to format the output extends `MapFn()`, thus has a `map()` function to override

```java
class FormatFn extends MapFn<Pair<String,Long>,String>() 
{
    public String map( Pair<String, Long> pair) 
    {
        return pair.getFirst() + ": " + pair.getSecond();
    }
}
```

#### Execution Plan and Optimization

Let's see how it executes this pipeline we've just seen. We need to represent our pipeline as a graph and, following what we just did in the example, we need to use the basic operation primitives offered by Flume, which are:

- `ParallelDo(doFn)` - Typical map function step;
- `GroupByKey` - Typical shuffle function step;
- `CombineValues(CombFn)` - Typical reduce function step;
- Flatten.

Finally, we should end with a graph like the following:

<img src="img/flume.png" style="zoom:80%;" />

The optimizer notices that:

- `ExtractWords` and `Count/Map` are both Map operations, so instead of doing them separately, it merges them into one;
- The same goes for `Top/Reduce`, `Format` and `Write`.

For these reason, it encloses the first block (Extract... to Count/Combine) and the second block in a two different MapReduce steps.

## MillWheel

In time, FlumeJava evolved in a way to not depend too much on the MapReduce model anymore, while still maintaining its success and qualities. The developers saw an opportunity to turn the Flume model into an independent one from MapReduce and finalized it into the **MillWheel** model.

Flume and MapReduce were suitable models for processing data in batch: This means that the only way to process new input data is to reprocess all the input dataset and if the dataset is massive that step is going to take a long time.

MillWheel instead was designed to process large and **continuous streams of data**, up to millions of events per second. For this reason, low-latency analysis became increasingly important in this model; let's see some examples:

- Breaking news articles;
- Intrusion detection;
- Quickly shutting down spammers.

Many custom, problem-specific solutions were being built: MillWheel aims to build a general framework for them and, in general, streaming analysis systems.

![](img/millwheel1.png)

The base structure of MillWheel is based on:

- **Computations**, which are the user-defined code runs;
- **Streams**, which connect computations between them, acting as an input or output channel;
- **Records**, which flow along the graph edges as <key, value, timestamps> tuples

Processing is per key: Computations can access and mutate state for the current record's key and produce records in output accordingly. It also offers APIs to save this data in a persistent state and timers to wake the code whenever there's code in input.

<img src="img/millwheel2.png" style="zoom: 50%;" />

Each computation in the graph represents processing a key-space (not only one key, but a range of keys). Edges are shuffle paths which will carry <key, value, timestamp, sequence number> quadruples along with them. Timestamps are determined by data sources, not by MillWheel.

However, each computation is range-sharded across many workers, so that a single key is managed by a single worker at a time, allowing us to consistently update per-key persistent state.

![](img/millwheel3.png)

Let's see an example of this:

![](img/millwheel4.png)

We can see, for example that Computation 1 has 4 key-spaces, each processing the respective keys and outputs, while Computation 2 has 3 key-spaces. Each key-space will send the processed output to the right key-space: in the example the record (toy, 8, 10:25, 1) is sent to key-space `[riy,)` since the key `toy` belong to that alphabetical range. 

