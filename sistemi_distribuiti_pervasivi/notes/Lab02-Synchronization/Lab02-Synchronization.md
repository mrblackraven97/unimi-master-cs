# Concurrency on Web Applications

A concurrent program is an instruction set that could be executed simultaneously, based on multithread execution.

Do not confuse this with a distributed system, which consists in different process running at the same time communicating with a protocol. A concurrent process could also run on only one processor (pseudo-parallelism).

The order in which instructions are executed is not granted, there is a **non-determinism**, depends on scheduling algorithm.

### Problems

Threads share the same memory for the process they belong to.

Efficient information exchange, but with:

- Thread interference;
- Memory inconsistency.

In order to avoid those problem a good synchronisation implementation is required.

## Thread Interference

When more threads run at the same time, they could run into a race condition problem.

```java
class Counter {
    private int c = 0;
    
    public void increment() {
    	int newValue = c + 1;
    	c = newValue;
    }
    
    public void decrement() {
    	int newValue = c - 1;
    	c = newValue;
    }
    
    public int value() {
    	return c;
    }
}
```

- Two threads A and B are working at the same time on the same `Counter` object;
- A invokes `increment()` and B invokes `decrement()`;
- The order in which those operation are executed is non-deterministic, this could cause problems.

Execution example:

- `A: newValue = c+1 = 1`;
- `B: newValue = c-1 = -1`;
- `A: c = newValue = 1`;
- `B: c = newValue = -1`.

The result of A operation is lost, like it didn't happen. Methods `increment()` and `decrement()` have pieces of code that cannot be run in parallel, otherwise there may be problems.

## Memory Inconsistency

```java
class Store {
    private int c = 10;
    
    // return true if can sell
    public boolean sell() {
        if(c>0){
        	c--;
        	return true;
        }
        return false;
    }
}
```

Method `sell()` other then a thread interference problem, it may cause a memory inconsistency after a while.

After a couple of calls, `c` has value 1. Execution order:

- `A: if (c>0)`;
- `B: if (c>0)`;
- `A: c--; return true`;
- `B: c--; return true`.

The last item has been sold twice!

## Synchronized Access

In Java, each instance of `Object` (every object) has a hidden **intrinsic lock** associated, also called **monitor**, that allows the programmer to handle synchronization problems.

Methods can be declared with the keyword `synchronized`, so that when the method is called, the intrinsic lock is obtained for the calling object, allowing only one thread to use that code.

Declaration esample: `public synchronized int methodName(int param)`.

Each object instance has an intrinsic lock, for each of those object it's important to know "whose object is this" to know who is executing it now and who will run it later.

When a thread runs a synchronized method on an object the value of the intrinsic lock associated to the object gets checked:

- If "available":
  - Lock value set to "unavailable";
  - Method execution;
  - Lock value set back to "available".
- If "unavailable":
  - Waits until the lock becomes "available".

## Example: A Collection of Objects

Let's consider a class `MyCollection` that contains a set (collection) of object.

A thread wants to order that set.

Another thread wants the smallest element of the set.

Synchronization problem: possible different order could lead to different element returned by second thread.

```java
class MyCollection {
    synchronized void sortItems() {
        // Sorting implementation
    }
    synchronized Object getSmallest() {
    	// Complicated code to get the minimum
    }
}
```

**N.B.** Attributes can't be declared `synchronized`.

Access on data structure that need synchronization can't be done directly, but only through methods.

*Improved `Counter` class*:

```java
class Counter {
    private int c = 0;
    
    public synchronized void increment(){
    	int newValue = c + 1;
    	c = newValue;
    }
    
    public synchronized void decrement(){
    	int newValue = c - 1;
    	c = newValue;
    }
    
    public synchronized int value(){
 	   return c;
    }
}
```

A invokes `increment()` and B invokes `decrement()`, only one possible execution flow thanks to `synchronized`:

- `A: newValue = c+1 = 1`;
- `A: c = newValue = 1`;
- `B: newValue = c-1 = 0`;
- `B: c = newValue = 0`.

Consistency achieved!

## The Synchronized Keyword

The keyword `synchronized` allows not only method synchronization, but also the definition of a synchronized code block that executes in mutual exclusion by passing as parameter the object on which get the intrinsic lock.

```java
synchronized(objVar) {
	// some code
}
```

The intrinsic lock is NOT part of a method, but it's part of the object instance that calls it. There may be different intrinisc lock.

The specified parameter is the object that contains the intrinsic lock that is wanted to be used, `objVar` typically is also the object on which we want to achieve atomicity.

To synchronize on primitive types (`int`, `float`, ...) "dummy" objects are created, used only for the lock.

Use of `synchronized` as statement is used to get a finer synchronization and to avoid "synchronization excess". 

![](img/synchdiff.png)

Supposed having two variables `c1` and `c2` as private fields of the same class. They are NOT related to each other.

*FineGrainedSynchronization.java*

```java
public class FineGrainedSynchronization {
    private long c1 = 0;
    private long c2 = 0;
    private Object lock1 = new Object();
    private Object lock2 = new Object();
    
    public void inc1() {
        synchronized(lock1) {
        	c1++;
        }
    }
    public void inc2() {
    	synchronized(lock2) {
    		c2++;
    	}
    }
}
```



## Static Methods

Even a static method can be defined as `synchronized`.

Static methods are class methods that cannot be instantiated. In this case the lock is bound not on the instantiated object, but on the class itself. Each Java class has one and only one object that contains metadata for the class, and that's where the lock is taken from.

```java
class Foo{
	synchronized static void foo(){
		// stuff 
	}
}
```

This is the same as writing: 

```java
class Foo{
	static void foo(){
		synchronized(Foo.class){
			// some boring stuff
		}
	}
}
```

## Deadlock

A situation in which the system is stuck, a synchronization error, no thread can carry on its execution because each one of them is waiting for another one to finish.

```java
public class Model {
	private View myView;
    
    public synchronized void updateModel(Object someArg) {
        doSomething(someArg);
        myView.somethingChanged();
    }
    public synchronized Object getSomething() {
    	return someMethod();
    }
}

public class View {
    private Model underlyingModel;
    
    public synchronized void somethingChanged() {
    	doSomething();
    }
    public synchronized void updateView() {
    	Object o = myModel.getSomething();
    }
}
```

What happens if `updateModel()` and `updateView()` get called by two threads at the same time? 

## Observations

The intrinsic lock associated to an object `obj` gets used by all `synchronized` methods and `synchronized` statement that specifies `obj` as a parameter.

Each synchronized object has a queue of objects that are waiting for its lock to be available, but the execution order is not granted. The execution order of threads waiting for a lock to be released may not be the same as the order of threads that requested that lock.

The use of `synchronized` grants two properties:

- **Mutual exclusion**: Only one thread at a time can have a specific lock;
- **Visibility**: Changes on shared data before lock release must be made visible to threads that will acquire it later.

By using synchronization, it is granted that all cache memories gets updated and all threads can access the freshest data.

**N.B.** Always synchronize threads that write and read on the same data.

![](img/visibility.png)

As optimization, a thread can copy variables from the central memory in a CPU cache. In a multi-core environment, each thread can copy variables in a different cache. Without synchronization, there are no guarantee on data freshness.

## The Volatile Keyword

The keyword `volatile` is to be assigned to variables, as a lighter version of `synchronized`, i grants only the visibility property, but not mutual exclusion.

Threads automatically see the fresher value for `volatile` variables, atomicity granted only for write and read operation.

The keyword is used only for simplicity and scalability, but it must be handled with caution.

On a `volatile` variable can only be written values independent from each other program state, variable itself included.

**N.B.** Operations like `x++` on a `volatile` keyword are not thread-safe, because it is not a direct write. A direct write is something like an assignment, a single operation, `x++` is formed by many intrinsic operations.

```java
class Worker {
    volatile boolean shutdownRequested;

    public void shutdown() { 
        shutdownRequested = true; 
    }
    public void doWork() {
        while (!shutdownRequested) {
            // do some random stuff
        }
    }
}
```

A thread executes method `doWork()` in loop, another one will stop the work with `shutdown()`.

Keyword `volatile` assures that the two threads have the same view, simplifying the use instead of `synchronized`.

# Working With Threads

Java gives some methods used to coordinate the execution of multiple threads, some of those are:

- `wait()`;
- `notify()`;
- `join()`;
- `sleep()`;

## Thread.join()

Method `join()` is used to wait the end of a specific thread. A thread that calls method `join()` stops until the thread associated with the object on which it is invoked ends its execution.

This method can be invoked with an optional parameter that specifies a timeout. In this case even if the thread does not end before the timeout, the requesting thread will resume it's execution anyway.

## Thread.isAlive()

A thread state can be checked by method `isAlive()` 

- The method returns `true` if thread is still active;
- The method returns `false` if thread is terminated.

## Thread.sleep()

Method `sleep()` temporarily stops the current thread execution for a specific amount of time.

It is used to elaborate periodic operation or let other thread go ahead.

It's a static method, called usually directly from the `Thread` class, whose parameter is expressed in milliseconds (ex. `Thread.sleep(10000)` for a 10 second waits).

## Object.wait() and Object.notify()

Those are methods of class `Object` (not `Thread`), so they are applicable to each object instance in Java.

Each object has a list of waiting treads associated: A thread that calls `wait()` on a specific object gets suspended until another thread does wake him up through a `notify()` call on the same object.

Many threads can be put in a sleep state on the same object, but a `notify()` call only wakes one thread. It is not granted that the order on which threads are awakened is the same on which they were on the sleep queue.

To awake all threads it is possible to call method `notifyAll()`.

*Example: Checkpoint.java*

```java
public class CheckPoint {
    boolean hereFirst = true;
    synchronized void meetUp () {
    	if (hereFirst) {
    		hereFirst = false;
    		wait();
    	}
    	else {
    		notify();
    		hereFirst = true;
    	}
    }
}
```

Let's create a class that allows two threads wait each other before proceed with their execution: The lock on which `wait()` is called is an instance of `CheckPoint`, so when `wait()` is called, the intrinsic lock gets released temporarily, until a `notify()` call, where that lock will be reassigned as soon as the thread that made this call ends.

This works because `wait()` releases the intrinsic lock associated on the object on which it's invoked, then the second thread can go on and awake the first thread, and when that happens, the first thread needs to acquire the lock again before proceeding.

## Busy Waiting

```java
...
while (true) {
	if (eventHappened){
		doSomething();
	}
	Thread.sleep(SOME_TIME_IN_MILLISECONDS);
}
...
```

A resource waste where there is a cycle that keeps checking if a specific event has happened without actually doing anything. The CPU is used pointlessly, taking away space to other threads for a useful computation. This occurs even with a `sleep()` call.

The solution is the use of signaling systems with the combination use of `wait()` and `notify()`.

```java
...
while (!eventHappened) {
	wait();
	if(eventHappened)
		doSomething();
	}
...
```

Instead of keeping a loop waiting for a change, the thread is being put in `wait()`, so another thread will awake him if the specific event occurs with `notify()`.

This works in `synchronized` statement too, because `wait()` and `notify()` are always bound to the synchronization object.

## Java.util.concurrent

Java provides libraries to ease concurrent programming, providing some instruments like:

- Synchronization high level primitives like locks and semaphores;
- Data structures that supports concurrency by default;
- Atomic data types like `AtomicInteger`.

Pretty useful tools, but those **won't be allowed in the course project**.

## Producer-Consumer Pattern

A multi-process communication pattern where **producer** is an entity (process/thread) that send/generate data and **consumer** is an entity (process/thread) that receive/process those data.

Data gets transmitted via a shared buffer memory, typically a queue.

It is possible to have multiple producers and consumers.

Problem: Avoid that a consumer constantly checks the buffer to check if there is data available (busy waiting).

*Example: Queue.java*

```java
public class Queue {
    public ArrayList<String> buffer = new ArrayList<String>();
    
    public synchronized void put(String message) {
    	buffer.add(message);
    	notify();
    }
    public synchronized String take() {
    	String message = null;
    	while(buffer.size() == 0) {
    		try {
                wait();
            }
    		catch (InterruptedException e) {
                e.printStackTrace();
            }
    	}
    	if(buffer.size() > 0) {
    		message = buffer.get(0);
    		buffer.remove(0);
    	}
    	return message;
    }
}
```

