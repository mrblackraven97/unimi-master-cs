# Static Analysis

## Testing

Running tests for our programs makes sure that they run correctly on those specific set of inputs. 

**Benefits**: Concrete failure proves issue, aids in fix.

**Drawbacks**: Expensive, difficult, hard to coder all code paths, no guarantees.

## Code Auditing

Code auditing is the technique to convince someone else your source code is correct.

**Benefits**: Humans can generalize beyond single runs.

**Drawbacks**: Expensive, hard, no guarantees.

## Static Analysis

A malicious adversary is trying to exploit anything you miss, what do we do?

The static analysis is a practice that aim to **analyze program's code without running it**. In a sense, we are asking a computer to do what a human might do during a code review.

**Benefit**: Much higher coverage.

- Reason about many possible runs of the program, sometimes all of them, providing a guarantee;
- Reason about incomplete programs (ex. libraries).

**Drawbacks**: 

- Can only analyze limited properties;
- May miss some errors, or have false alarms;
- Can be time consuming to run;

Thoroughly check limited but useful properties, it is possible to **eliminate categories of errors**, so **developers can concentrate on reasoning**.

It encourages **better development practices**:

- Develop programming models that avoid mistakes in the first place;
- Encourage programmers to think about and make manifest their assumptions by using annotations that improve the tool precision.

Seeing increased commercial adoption.

## The Halting Problem

The **halting problem** says: "Can we write an analyzer that can prove, for any program P and inputs on it that P will terminate?"

![image-20200122161858427](img/img11.png)

Unfortunately, the halting problem is **undecidable**. That is, **it is impossible to write such an analyzer**: it will fail to produce an answer for at least some programs (and/or some inputs).

Perhaps security-related properties are feasible, for example that all accesses `a[i]` are in bounds. But these **properties can be converted into halting problems** by transforming the program.

- For example, a perfect array bounds checker could solve the halting problem, which is impossible.

Other undecidable properties (Rice's theorem):

- Does this SQL string come from a tainted source?
- Is this pointer used after its memory is freed?
- Do any variables experience data races?

## An Approximate Analyzer

So, is static analysis impossible?

A **perfect** static analysis is **not possible**. But we can have a **useful** static analysis, which is **perfectly possible**, despite:

- Non-termination - Analyzer could never terminate;
- False alarms - Claimed errors are not really errors;
- Missed errors - No error reports does not means we are error free.

Non-terminating analyses are confusing, so tools tent to exhibit only false alarms and/or missed errors, and falling somewhere between **soundness** and **completeness**.

**Soundness**. If analysis says that X is true, then X is true. If the program is claimed to be error free, then it really is. (Alarms do not imply erroneousness).

**Completeness**. If X is true, then analysis says X is true. If the program is claimed to be erroneous, then it really is (silence does not imply error freedom).

![image-20200122163022263](img/img12.png)

Essentially, most interesting analyses are neither sound nor complete, they usually lean towards one of them.

Analysis design tradeoffs:

- **Precision** - Carefully model program behavior, to minimize false alarms;
- **Scalability** - Successfully analyze large programs;
- **Understandability**: Error reports should be actionable.

Code style is important: Aim to be precise for "good" programs, it's ok to forbid yucky code in the name of safety. 

False alarms viewed positively: reduces complexity. Code that is more understandable to the analysis is more understandable to humans.

# Tainted Flow Analysis

The root cause of many attacks is **trusting unvalidated input**:

- Input from the user are labeled as **tainted**;
- All various data used in a program that comes from a known source is said to be **untainted**.

Examples of untainted data:

- Source string of `strcpy` (<= target buffer size);
- Format string of `printf` (contains no format specifiers);
- Form field used in constructed SQL query (contains no SQL commands).

## Format String Attack

Adversary-controlled format string

```C
char *name = fgets (..., network_fd);
printf(name); 		// Oops
```

1. Attacker sets `name = "%s%s%s"` to crash program;
2. Attacker sets `name = "...%n..."` to write to memory (yields code injection exploits).

These bugs still occur in the wild. Too restrictive to forbid non-constant format string

## Problem in Types

Specify our requirement as a *type qualifier*

```c
int prinf(untainted char *fmt, ...);
tainted char *fgets(...);
```

- **Tainted** = possibly controlled by an adversary;
- **Untainted** = must not be controlled by adversary.

```c
tainted char *name = fgets(..., network_fd);
printf(name);		// FAIL: tainted =/= untainted
```

## Analysis Problem

**No tainted data flows**: For all possible inputs, prove that tainted data will never be used where untainted data is expected.

- **Untainted annotation**: indicates a trusted sink;
- **Tainted annotation**: an untrusted source;
- **No annotation**: not sure what to expect (analysis figure it out).

A solution requires inferring flows in the program:

-  What sources can reach what sinks;
- If any flows are illegal (ex. whether a tainted source may flow to an untainted sink).

We will aim to develop a **sound analysis**.

*Example: Legal execution flow*

```c
void f(tainted int);
untainted int a = ...;
f(a);
```

*f* accepts *tainted* data, so taking *untainted* data does not cause any trouble. The operation is accepted. 

`untainted --> tainted` is **OK**.

*Example: Illegal execution flow*

```c
void g(untainted int);
tainted int b = ...;
g(b)
```

*g* accepts only *untainted* data, so the *tainted* variable *b* is not accepted. 

`tainted --> untainted` is **not OK**

Think of flow analysis as a kind of type inference: if no qualifier is present, we must infer it.

Steps:

1. **Create a name** for each missing qualifier (α, β, ...);
2. For each statement in the program, **generate constraints** (of the form `q1<=q2`) on possible solutions. 
   - For example, statement `x=y` generates constraints `qy<=qx` where `qy` is `y`'s qualifier and `qx` is `x`'s qualifier.
3. **Solve the constraints** to produce solutions for α, β, ... 
   - A solution is a substitution of qualifiers (like tainted or untainted) for names (like α, and β) such that all of the constraints are legal flows.
   - If there is **no solution**, we may have an **illegal flow**.

```c
int printf(untainted char *fmt, ...);
tainted char *fgets(...);
```

```c
α char *name = fgets(..., network_fd);
β char *x = name;
printf(x);
```

1. tainted ≤ α
2. α ≤ β
3. First constraint requires α = tainted;
4. To satisfy the second constraint implies β = tainted;
5. But then the third constraint is illegal: tainted ≤ untainted
6. Illegal flow! No possible solution for α and β

## Flow Sensitivity

Our analysis is **flow insensitive**: each variable has **one qualifier** which abstracts the taintedness of all values it ever contains.

A **Flow sensitive** analysis would account for variable whose contents change:

- Allow each assigned use of a variable to have a different qualifier ( ex. α1 is `x`’s qualifier at line 1, but α2 is the qualifier at line 2, where α1 and α2 can differ);
- Could implement this by transforming the program to assign to a variable at most once, called static single assignment (SSA) form.

*Example: Reworked*

```c
int printf(untainted char *fmt, ...);
tainted char *fgets(...);
```

```c
α char *name = fgets(..., network_fd);
β char *x1;
y char *x2;
x1 = name;
x2 = "%s";
printf(x2);
```

- tainted ≤ α
- α ≤ β
- untainted ≤ γ
- γ ≤ untainted

No Alarm! Good solution exists: γ = untainted and α = β = tainted

*Example: Multiple conditionals*

```c
int printf(untainted char *fmt, ...);
tainted char *fgets(...);
```

```c
void f(int x) {
    α char *y;
    if (x)
        y = "hello!";
    else
        y = fgets(..., network_fd);
    if (x)
        printf(y)
}
```

- untainted ≤ α
- tainted ≤ α
- α ≤ untainted

False Alarm! no solution for α (and flow sensitivity won’t help).

## Path Sensitivity

An analysis may consider *path feasibility*.

*Example: f(x) can execute path*

```c
void f(int x) {
    char *y;
    [1]if (x) 
        [2]y = “hello!”;!
 	else
 		[3]y = fgets(...);!
 	[4]if (x) 
        [5]printf(y);!
[6]}
```

- 1-2-4-5-6 when x is not 0;
-  1-3-4-6 when x is 0;
- Path 1-3-4-5-6 *infeasible*.

A path sensitive analysis checks feasibility, for example, by qualifying each constraint with a path condition:

- x ≠ 0 ⟹ untainted ≤ α 		(segment 1-2)
- x = 0 ⟹ tainted ≤ α	          (segment 1-3)
- x ≠ 0 ⟹ α ≤ untainted         (segment 4-5)

Flow sensitivity **adds precision**, and path sensitivity adds even more, which is good. But both of these **make solving more difficult**:

- Flow sensitivity also *increases the number of nodes* in the constraint graph;
- Path sensitivity *requires more general solving procedures* to handle path conditions.

In short, **precision (often) trades off scalability**, limiting the size of programs we can analyze.





























