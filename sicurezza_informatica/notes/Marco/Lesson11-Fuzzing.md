# Fuzzing

Fuzzing is a kind of **random testing** whose inputs are randomly (or semi-randomly) generated.

Fuzzing's goal is to **make sure certain bad things don't happen** (like crashes, exceptions or non-termination), no matter what. All of these things can be the foundation of security vulnerabilities, for example, a non termination vulnerability can cause *denial of service attacks* (DOS).

Complements functional testing:

- Test features (and lack of misfeatures) directly;
- Normal tests can be starting points for fuzz tests.

Fuzz testing can be a useful tool but is limited: it cannot know when our program will produce wrong outputs, which is a *bad thing*, because i cannot know what should be our expected output, but only that it must not crash or terminate. Therefore, fuzz testing is a complementar activity that **does not replace traditional functional tests**.

There are three kinds of fuzz testing:

- **Black box** - The tool knows nothing about the program or its input. 
  - It's easy to use and get started, but will explore only shallow states unless it gets lucky;
- **Grammar based** - The tool generates input informed by a grammar.
  -  Requires more work to use, to produce the grammar, but can go deeper in the state space;
- **White box** - The tool generates new inputs at least partially informed by the code of the program being fuzzed.
  -  Often easy to use, but computationally expensive.

## Input Management

There are different ways on which a fuzzing tools generates input to pass to the target program:

- **Mutation** - Take a legal input and mutate it, using that as input. Legal input might be human-produced, or automated (ex. from a grammar or SMT solver query). Mutation might also be forced to adhere to grammar;
- **Generational** - Generate input from scratch, like from nothing, randomly or from a grammar.
- **Combinations** - Generate input using a combination of the previous input methods. Generate initial input, mutate it n times, generate new inputs and so on (mutations generated according to grammar).

Fuzzers can be taken in two ways: based **on files** or based **on networks**.

## File-Based Fuzzing

Fuzzer tool creates a new input (mutated or generated), then runs the program with that input and sees what happens.

### Examples: Radamsa and Blab

**Radamsa** is a mutation-based black box fuzzer that mutates inputs that are given, passing them along.

```pseudocode
% echo "1 + (2 + (3 + 4))" | radamsa --seed 12 -n 4!
5!++ (3 + -5))!
1 + (3 + 41907596644)!
1 + (-4 + (3 + 4))!
1 + (2 + (3 + 4!
% echo ... | radamsa --seed 12 -n 4 | bc -l
```

**Blab** generates inputs according to a grammar (grammar-based), specified as regexps and CFGs.

```pseudocode
% blab -e '(([wrstp][aeiouy]{1,2}){1,4} 32){5} 10’!
soty wypisi tisyro to patu
```

### Example: American Fuzzy Lop

It is a mutation-based white-box fuzzer. He processes in the following way:

- **Instrument target** to gather run-time information (tuple of `<ID_of_current_code_location, ID_last_code_location>`);
- **Run a test, then mutates test input** to create a new one if unseed tuple generated; otherwise discards the test (mutations include bit flips, arithmetics and other standard stuff);
- **Periodically call gathered tests**, to avoid stopping on local minimums

```pseudocode
% afl-gcc -c ... -o target
% afl-fuzz -i inputs -o outputs target
afl-fuzz 0.23b (Sep 28 2014 19:39:32) by <lcamtuf@google.com>
[*] Verifying test case 'inputs/sample.txt'...
[+] Done: 0 bits set, 32768 remaining in the bitmap. ...
———————
Queue cycle: 1n time : 0 days, 0 hrs, 0 min, 0.53 sec ...
```

**N.B.** Microsoft Sage uses the symbolic execution as his technology to generate tests. It's white box.

## Network-Based Fuzzing

Network-based fuzzing are based on network communications:

- **Acts as ½ of a communicating pair**. Inputs could be produced by replaying previously recorded interaction, and altering it, or producing it from scratch (like from a protocol grammar.);

- **Acts as a "man in the middle"**. Mutating inputs exchanged between parties (perhaps informed by a grammar).

### Example: SPIKE

**SPIKE** is a fuzzer creation kit, providing a C-language API for writing fuzzers for network based protocols

```pseudocode
s_size_string(“post”,5);			// Backpatch for length field
s_block_start(“post”);				// ... for this block
s_string_variable(“user=bob”);		// prefix of string to fuzz
s_block_end(“post”);				// end of block; include length
spike_tcp_connect(host,port);		// connect to server
spike_send();						// send this buffer
spike_close_tcp();					// close connection
```

### Example: Burp Intruder

Burp **automates customized attacks** against web applications.

Similar to SPIKE in allowing the user to craft the template of a request, but **leave "holes"** (called *payloads*) for fuzzing. Nice GUI front end.

Integrates with the rest of the Burp Suite, which includes a proxy, scanner, spider and more.

## Crash Handling 

While we are running a fuzz and a crash occurs, how do we handle this situation?

First we need to answer some questions:

- What is the **root cause**, so it can be fixed?
  - Is there a way to make the input smaller, so it is more understandable?
  - Are two or more crashes signaling the same bug? 
    - Yes if they "minimize" to the same input.
- Does the crash signal an **exploitable vulnerability**?
  - Dereferencing NULL is rarely exploitable;
  - Buffer overruns often are.

## Memory Errors

Find unsafe memory:

1. Compile the program with an **address sanitizer** (ASAN). Instruments accesses to arrays to check for overflows and use-after-free errors;
2. Fuzz it;
3. Did the program **crash with an ASAN-signaled error**? If yes, i can worry about exploitability.

Similarly, you can *compile with other sorts of error checkers* for the purposes of testing, like `valgrind` memcheck.

## More Fuzzers

### Cert Basic Fuzzing Framework (BFF)

Fuzz based in part on Zzuf.

Found bugs in Adobe Reader and Flash Player, Apple Preview and QuickTime, and others

### Sulley

Provides lots of extras to manage the testing process:

- Watches the network and methodically maintains records;
- Instruments and monitors the health of the target, capable of reverting to a known good state;
- Detects, tracks and categorizes detected faults;
- Fuzzes in parallel, if desired.

## Summary

- Penetration testers simulate real attackers, they try to find exploitable vulnerabilities in complete systems.

- Penetrations signal real problems: lack of penetrations is not proof of impossibility.

- Pen testers employ a variety of tools: scanners, proxies, exploit injectors, fuzzers.

- Require ingenuity and guile.