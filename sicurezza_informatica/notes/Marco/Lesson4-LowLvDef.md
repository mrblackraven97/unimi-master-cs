# Low Level Defenses

We saw as in low level attacks, we took advantage of function vulnerabilities and code to inject, control and overwrite specific memory zone (on heap and stacks) for various purposes (execute custom code, spoofing, data stealing, ...).

All those attacks have in common that:

- The attacker is able to **control some data** that is used by the program;
- The use of that data **permits unintentional access to some memory areas** in the program:
  - Past a buffer;
  - To arbitrary position on the stack.

## Channel problem

A **channel problem** is a vulnerability concern where control metadata and program data are mixed in the same channel (heap or stack). In fact, in the heap channel i have metadata with data, while in the stack channel i have the return address with data, and this makes them open to serious vulnerabilities because, as we saw, it is possible to overwrite them with overflow.

To defend us from those attacks, we need to apply some **memory safety and type safety properties**, that, if satisfied, ensure an application is immune to memory attacks.

We also need some automatic defenses like **stack canaries** and the **address space layout randomization** (ASLR).

Those last methods are not 100% efficient, but it makes much more difficult to take advantage of memory vulnerabilities for inexperienced attackers, reducing the attackable memory surface.

# Memory Safety

A program that executes memory safe code:

- Only **creates pointers** through **standard means**:
  - `p = malloc(...)`;
  - `p = &x`;
  - `p = &buff[5]`.
- Only uses a pointer to **access memory that belongs to that pointer**.

By combining those two ideas we have **temporal safety** and **spatial safety**.

If a language is memory safe, than all programs compiled in that language are memory safe. Many languages that are memory safe (Java, Python, Ruby, ...) are also type safe (even more safe).

Even if all safety problems are solved by those languages, C and C++ are still globally used for their extreme performance. Many framework have been created to make them safe, keeping them as performing as possible (CCured, Softbound/CETS, Intel MPX, ...). 

## Spatial Safety

A violation of spacial safety occurs when we try to **access to some successive memory of the one accessible from a pointer**, making overflow.

To implement spacial safety, we can implement **pointers as triples (p,b,e)**:

- **p** is the actual **pointer**;
- **b** is the **base** of the memory region it may access;
- **e** is the **extent** (bounds) of that region (his size).

The access is allowed only if  `b <= p <= e-sizeof(typeof(p))`.

Those triples are defined as **fat pointers**, because they use *b* and *r* as metadata for safety.

Operations on fat pointers:

- Pointer arithmetic increments *p*, leaving *b* and *e* alone;
- Using *&*, *e* is determined by size of original type.

*Example: Fat pointer usage*

``` c
int x;			// assume sizeof(int) == 4
int *y = &x;	// p = &x, b = &x, e = &x+4
int *z = y+1;	// p = &x+4, b = &x, e = &x+4
*y = 3;			// OK:  &x <= &x <= (&x+4)-4
*z = 3;			// BAD: &x <= &x+4 </= (&x+4)-4 
				//  p is greater than e: the operation is not allowed 
```

*Example: Fat pointer usage 2*

```C
struct foo {
    char buf[4];
    int x;
}

struct foo f = {"cat", 5};
char *y = &f.buf;	// p = b = &f.buf, e = &f.buf+4
y[3] = 's';			// OK: &f.buf <= &f.buf+3 <= (&f.buf+4)-1
y[4] = 'y';			// BAD: &f.buf <= &f.buf+4 </= (&f.buf+4)-1
					//  p is greater than e-sizeof(type(p))
```

A buffer overflow violates spatial safety because it will try to go beyond the established bound of *b+e* for pointer *p*, therefore it becomes easy to spot and stop.

```c
void copy(char *src, char *dst, int len) {
	int i;
	for (i=0; i<len; i++) {
 		*dst = *src;
	 	src++;
     	dst++;
 	}
}
```

Overrunning the bounds of the source and/or destination buffers implies either `src` or `dst` is illegal.

**N.B.** Handling fat pointer triples is a heavy performance concern.

## Temporal Safety

A **temporal safety violation** occurs when trying to **access undefined memory**.

If spacial safety assures that the memory access was to a legal region, temporal safety assures that region is still in play.

Memory regions are either defined or undefined:

- **Defined** means allocated (and active);
- **Undefined** means unallocated, uninitialized or deallocated.

We pretend memory is infinitely large, so we can never reuse it.

Accessing a freed pointer violates temporal safety:

```C
int *p = malloc(sizeof(int));
*p = 5;
free(p);
printf(“%d\n”,*p); // violation
```

The memory dereferenced no longer belongs to p.

Accessing uninitialized pointers is similarly not OK:

```c
int *p;
*p = 5;	// violation
```

Temporal safety tries to stop the UAF vulnerability, which makes use of dangling pointers created by the `free()` operation and accessing it.

# Type Safety

In type safety, **each object is associated to a type** (`int`, pointer to `int`, pointer to function, ...) and all operations on the objects are always compatible with the object's type. Type safe programs do not "go wrong" at run-time.

Type safety is **stronger than memory safety**, because it does not let programmer write code that leads programs to crash.

``` c
int (*cmp)(char*, char*);
int *p = (int*)malloc(sizeof(int));
*p = 1;
cmp = (int (*)(char*,char*))p;
cmp(“hello”,”bye”); // crash! memory safe but not type safe
```

Type safety can be granted at a static or dynamic level

### Static Type Safety

In statically typed languages, the type of a variable is directly established in the source code, where it gets explicitly assigned through specific keywords (like `int`, `long`, `float`, `char`, ...).

This way, the type safety can directly be verified at compile-time, by checking that all inserted values in variables are consistent.

### Dynamically Type Safety

Dynamically typed languages (like Ruby and Python), which do not require declarations that identify types, can be viewed as **type safe** as well.

Each object has one type: **Dynamic**

- Each operation on a Dynamic object is permitted, but may be unimplemented;
- In this case, it throws an exception.

## Enforce Invariants

Types really show their strength by **enforcing invariants** in the program, variables that allows to control some properties in the data flow of the program.

Notable here is the enforcement of **abstract types**, which characterize modules that keep their **representation hidden** from clients. As such, we can reason more confidently about their **isolation** from the rest of the program.

**Type-enforced invariants can relate directly to security properties** by expressing stronger invariants about data's privacy and integrity, which the type checker than enforces.

## Type Confusion

Type confusions are attacks that tries to elude type safety by changing bits to point a pointer to another pointer of different type from the compatible one (ex. point an int* to a struct*).

Many of those kind of attacks are made with rowhammer attacks, electromagnetic attacks, of heat or with opportune programs that overloads the memory, aiming to alter the physical RAM, introducing errors in writing and reading, making them confuse types.

## Why Not Use Type Safe Languages?

C and C++ are often chosen for performance reason:

- Manual memory management;
- Tight control over object layouts;
- Interaction with low-level hardware.

Typical enforcement of type safety is expensive:

- **Garbage collector** to avoid temporal violations. Can be as fast as malloc/free, but often uses much more memory;
- **Bounds** and **null-pointer checks** avoid spatial violations;
- **Hiding representation** may **inhibit optimization**. Many C-style casts, pointer arithmetic, & operator, not allowed.

New languages aiming to provide similar features to C/C++ while remaining type safe are rising:

- Google's **GO**;
- Mozilla's **Rust**;
- Apple's **Swift**.

These languages may be the future of low-level programming (or maybe not).

# Avoiding Exploitations

Until C is memory safe, what can we do?

- **Make the bugs harder to exploit**. 
  - Examine necessary steps for exploitation, make one or more of them difficult, or impossible.
- **Avoid the bugs entirely**. 
  - Secure code practices;
  - Advanced code review and testing.

Those strategies are complementary: try to avoid bugs, but add protection if some slip through.

## Avoiding Stack Smashing Attacks

The steps of a stack smash attack are:

1. Putting attacker code into the memory (no zeros);
2. Finding the return address (guess the raw address);
3. Getting `%eip` to point to (and run) attacker code.

To make this kinds of attacks more difficult we can complicate exploitation by changing libraries, compiler and/or operating system. Then we don't have to change the application code, but the fix is in the architectural design not in the code.

## Defence \#1: Canaries

This techniques was first used in mines to see if they were safe for mens: people bring a canary in the many and if it died it means that it was not safe.

We can do the same for stack integrity, by putting a canary in a specific position of stack's metadata and by reading his memory location, at the end of a function, we find that if the canary has not the expected value it means that we probably have a stack overflow and the program stops with a stack smashing error.

![image-20200121180722270](img/img3.png)

In each program function we add code that adds a canary at the beginning and checks it at the end.

``` pseudocode
f1(args)
    x = create_random_canary()
    push x on stack
    save_canary_in_memory(x)
    
    ... (rest_of_function f1) ...
    
    compare(actual_canary, x)
    ret
```

It is possible to have different implementations for canaries with different functionalities to make it safer:

- **Terminator canaries** (CR, LF, NUL ...):
   - Uses values that cannot be inserted by functions like `scanf` or that interrupts the stack smashing with char strings.
- **Random canaries**
   - Write a new random value when each process start;
   - Save the real value somewhere in memory;
   - Must write-protect the stored value.
- **Random XOR canaries**
   - Same as random canaries;
   - But store canary XOR some control info, instead

The canary technique protect us from the first step of the stack smashing: "putting  code into the memory".

## Defence \#2: Make Stack (and Heap) Not-Executable

Even if canaries could be bypassed, no code loaded by the attacker can be executed (CPU will panic).

We define policies that established that everything on the stack/heap are data and not runnable code. This makes the stack smashing useless.

This technique is called **Data Execution Prevention (DEP)**.

## Defence \#3: Address-Space Layout Randomization

This technique will randomly place standard libraries and other elements in memory, making them harder to guess.

![image-20200121182950855](img/img4.png)

Unfortunately, the possibility of stopping the malicious code from being executed is not enough. I could replace to one of the arguments of the shellcode `/bin/sh`  and to the return address a system function that calls `/bin/sh` and avoid the non-execution.

Therefore, to solve this problem we will make harder to guess where the return address is located in the stack, and the **address-space layout randomization** (ASLR) does that.

The ASLR randomizes the position of memory elements (stack, heap, text, ...) and does a new permutation of their location each program execution.

If DEP wouldn't be active, we could do a **spray attack**, by filling the shellcode on all the process memory and then jumping to random addresses hoping to find one of them.

