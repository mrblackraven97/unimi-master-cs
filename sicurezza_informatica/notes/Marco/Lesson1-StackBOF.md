# Buffer Overflow

A **buffer overflow** is a bug that affects low-level code, typically in C and C++, with significant security implications, because those languages don't have default controls on those data structures.

Normally, a program with this bug will simply crash, but an attacker can alter the situations that cause the program to do much worse like:

- Stealing private informations;
- Corrupt data;
- Run code of the attacker's choice.

Even if it's an old bug, the buffer overflow is **still a relevant attack**  because C and C++ are still very popular and because it can happen even if a good programmer tries to fix it.

C and C++ are still popular because they are very efficient for handling memory, especially on devices where this is critical like embedded and real-time systems, web servers and OS kernels.

It is imperative to find solutions to protect us from this bug that also let us keep the efficiency of the language.

## History

The buffer overflow comes from an attack worm from Morris in 1988: a malevolent code autoreplicant code was developed that took advantage of C and C++ vulnerabilities to spread. It caused something like $10-100M worth of damage to a Darpanet network.

Modern examples are:

- Code Red in 2001, 300k servers infected in 14h
- SQL Slammer in 2003

## Conclusions

We will learn how this attacks work and how to protect us from them.

# Memory Layout

How is program data laid out in memory?
What does the stack look like?
What effect does calling (and returning from) a function have on memory?

All programs are stored in memory and each process possess a specific space in it: the instructions themselves are in a virtual space in memory (0x00..0 - 0xff..f) mapped on the real memory.

Location of data areas:

- Set when process starts
  - cmdline & env

- Run-time (dynamic memory)
    - Heap
    - Stack
- Compile-time (static memory)
    - Program text
    - Initialized data
    - Uninitialized data

The most vulnerable parts are those dynamics: **stack** and **heap**.

**N.B.** Stack and heap grow in opposite directions, compiler emits instructions to adjust the size of the stack at run-time

## Stack

When a function is *called* from a program, some of its basic informations are stored in the stack (like the activation frame).

The **stack pointer** is the pointer that points to the top frame of the stack. 

Each time a function is called, new informations about it are stored on the top of the stack (push operation):

- Function parameters;
- The **stack frame**:
    - The **return address**, a memory address that let the execution go back to where it started after the function is resolved;
    - Local variables used;

**N.B.** Local variables are pushed in the same order as they appear in the code, but arguments are pushed in the reverse order of the code.

When the return operation occurs, the information will be removed from the stack (pop operation), so the stack frame is reset and the execution flow follows the return address to get back to the original function call of the code.

## Buffer overflow

The **buffer** is a contiguous block of memory associated to a variable or a field. It is a very commonly used feature, like in C for his string definition (char arrays that ends with a `\0` char).

The **overflow** is a buffer state that happens when we try to insert something bigger than what the buffer itself can hold (more than the allocated/supported memory).

*Example: First buffer overlow*

```c
void func(char *arg1)
{
    int authenticated = 0;
    char buffer[4];
    strcpy(buffer, arg1);
    if (authenticated)
        ...
}

int main()
{
    char *mystr = "AuthMe!";
    func(mystr);
}
```

We're trying to copy the string *"AuthMe!"* of 8 chars in a buffer that can only have 4. This cause an overflow.

By doing so, instead of crashing the program, it is possible to override the variable *authenticated* outside of the buffer (according to the stack memory layout).

This is a serious problem because:

- It expose sensible variables to unintended changes;
- Allows to override all the stack content, even the **return address**, allowing an attacker to manipulate the execution flow.


 In fact, by overriding the variable *authenticated*, his value becomes different from 0 an so i am authenticated an can access come i should not be able to!

To protect us from this vulnerability, we can implement a **"canary"**, by inserting a value between the return address and the local variables: if the canary change value, it means that a stack has been smashed and the program crash before doing unintended stuff.

## Code injection

The fact that in Intel architectures all the readable code inside of the stack can also be executed lead to the creation of a new attack: the **code injection**.

A code injection attack consists in putting malevolent code inside a buffer and make it pointed by the **instruction pointer** `%eip`, and make it run.

The problem is to know exactly the buffer address in the stack, so that we know where to start copying the code. It can be computed a range but it's still complicated obtain it with high precision.

One strategy to maximize the chances of getting to our code is the **nop-sled**: A series of nop operation, particular processor operation that jumps to the next one, are written as a safe jump zone. If i can jump to any nop inside this sled, it will jump until the malevolent code in the buffer.

To make some code injection we have to know:

1. The distance to override (buffer size), by overriding a buffer until it crashed;
2. The malevolent code in assembly, our code will not pass through a compiler, so we have to put an already compiled version of it;
3. The buffer address.