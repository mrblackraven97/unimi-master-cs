# Symbolic Execution - Part 1

As we know, software is always **full of bugs**, and to find them we use extensive tests and code reviews.

However this is not enough, there are bugs that are **still missed**:

- Rare features;
- Rare circumstances;
- Nondeterminism.

In order to make our code as bugless as possible, the **static analysis** becomes handy, because lets us analyze all possible runs of a program. Many tools were born from this idea to help developers to improve software quality.

Static analysis may seem awesome, but the truth is that i can't always find deep, difficult bugs. Commercial viability implies you must deal with developer confusion, false positives, error managements, ...

This means that companies specifically aim to keep the **false positive rate down**: they often do this by **purposely missing bugs**, to keep the analysis simpler.

## Abstraction Issue

Abstraction lets us model all possible runs, but it also introduces conservatism.

Adding sensitivities (flow, context, path, ...) adds precision, reducing conservatism, but more precise abstractions are more expensive and have no scalability on bigger code bases and and they still have false alarms or missed bugs.

Always remember that **static analysis abstraction ≠ developer abstraction**, because the developer didn't have them in mind: he can't always understand if the generated error is true or a false positive.

# The Symbolic Execution

With testing, all reported bugs are real bugs, but each test only explores one possible execution. We hope that test cases generalize as much as possible, but there is no guarantees.

With the **symbolic execution**, the main idea is to follow assertions, **generalizing tests**:

- "More sound" than testing;

- Allows unknown symbolic variables in evaluation;

  ```c
  y = α;
  assert(f(y) == 2*y-1)
  ```

- If execution path depends on unknown, conceptually fork symbolic executor.

  ```c
  int f(int x) {
      if (x > 0)
          return 2*x - 1;
      else
          return 10;
  }
  ```

## Application

![image-20200123103650465](img/image-20200123103650465.png)

The symbolic executor can run a program until it finds where it depends on and unknown value, and then it fork himself to keep track of all possible values of that unknown value that act on the program flow.

Each **symbolic execution path** stands for **many actual program runs**. In fact, exactly the set of runs whose concrete values satisfy the patch condition. Thus, we can **cover a lot more of the program's execution space than testing**.

Viewed as static analysis, symbolic execution is:

- **Complete** but not sound (usually doesn't terminate);
- **Path, flow and context sensitive**.

## A Little History

The symbolic execution idea has been around since 1975, but it is much compute-intensive, so back in the days it was dropped because the computer power was not so great. Nowadays we got stronger computers and better alghorithms for constraint solving, therefore it is possible to solve instance quite quickly, by checking assertions and cutting infeasible paths. 

Symbolic execution had seen his notiriety improved since 2005, mainly for a deeper buf finding compared to standard testing tools.

# Basic Symbolic Execution

## Symbolic Variables

We extend the language's support for expressions `e` to include **symbolic variables**, representing the unknown variables. 

```pseudocode
e ::= α | n | X | e0 + e1 | e0 ≤ e1 | e0 && e1 | ...
```

- n ∈ N = integers;
- X ∈ Var = variables;
- α ∈ SymVar.

Symbolic variables are **introduced** when **reading input** by using functions like `mmap`, `read`, `write`, `fgets`, and similar, so if a bug is found, we can recover an input that reproduces the bug when the program is run normally.

## Symbolic Expressions

We make (or modify) a language interpreter to be able to **compute symbolically**.

Normally, a program's variable contain *values*, but now they can also contain *symbolic expressions*, which are expressions containing symbolic variables.

Examples:

- 5, "hello" - Normal values;
-  α+5, “hello”+α, a[α+β+2] - Symbolic expressions; 

![image-20200123105623123](img/image-20200123105623123.png)

## Path Conditions

Program control can be affected by symbolic values.

```c
x = read();
if (x>5) {
    y = 6;
    if (x>10)
        y = 5;
    else
        y = 0;
}
```

We represent the influence of symbolic values on the current path using a **path condition π**:

- Line 3 reached when α>5;
- Line 5 reached when α>5 and α<10;
- Line 6 reached when α≤5

Whether a path is feasible is tantamount to a path condition being **satisfiable**.

**Solution** to path constraints **can be used as inputs** to a concrete test case that will execute that path:

- Solution to reach line 3: α = 6;
- Solution to reach line 6: α = 2.

### Path and Assertions

Assertions, like array bounds checks, are conditionals.

![image-20200123110347017](img/image-20200123110347017.png)

So, if either lines 5 or lines 7 are reachable (the paths reaching them are feasible), we have found an out-of-bounds access.

## Forking Execution

Symbolic executors can **fork at branching points**. Happens when there are solutions to both the path condition and its negation.

**N.B.** It is possible to have branches even on array access, by checking implicitly if the index is within a valid range or not.

How to systematically explore both directions? Two possible ways:

- **Check feasibility during execution with the constraint solver** and queue feasible path and path conditions for later considerations.
- **Concolic execution**: run the program to completion, then generate new input by changing the path condition.

*Execution algorithm:*

![image-20200123111040339](img/image-20200123111040339.png)

At some point, symbolic execution will reach the "edges" of the application: library, system, or assembly code calls. In some cases it could pull in that code or it creates a runnable "model" of that library without having to inspect the whole library.

## Concolic Execution

Also called, *dynamic symbolic execution*, the **concolic execution** instruments the program to do symbolic execution as the program runs by **shadowing concrete program state with symbolic variables** (real inputs + symbolic inputs).

The concolic execution explores **one path at a time**, start to finish, and next path can be determined simply by negating some element of the last path condition or by solving for it, to produce a concrete underlying value to rely on.

Concolic execution makes it really easy to **concretize**, by replace symbolic variables with concrete values that satisfy the path condition (always have these around in concolic execution).

This allows the analyzer to actually do system calls, but losing symbolicness at such calls, making them able to continue the symbolic execution of the program without stopping in system calls, and can handle cases where conditions too complex for SMT solver appear. 

