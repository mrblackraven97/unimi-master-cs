# ROP

Come abbiamo potuto osservare, l'eterna lotta fra attaccanti e difensori sembra quasi quella di un gatto col topo: a ogni difesa un attacco di risposta e viceversa

- **Defense**: Rendere stack/heap non eseguibile per prevenire iniezioni di codice
  - ***Attack response***: Jump/return to libc
- **Defense**: Nascondere il libc code e i return address tramite l'ASLR
  - ***Attack response***: attacchi spray e brute force (macchine a 32bit) o information leaks
- **Defense**: Evitare libc completamente e usare solo codice nel program text
  - ***Attack response***: Costruire le funzionalità necessarie usando la return oriented programming (ROP)

Vediamo come la ROP riesce a scavalcare quest'ultima difesa.

## Return oriented programming

La ROP si basa sull'idea di non basarsi sulla singola funzione `exec()` di libc per eseguire lo shellcode, ma ***costruirsi la propria funzione assemblando diversi pezzi di codice*** ottenuti dal programma detti gadget.

Le principali difficoltà di questo approccio sono:

- **Trovare i gadget** che ci servono per la nostra soluzione di attacco
- **Come unirli** in una sequenza di operazioni

Ma cosa sono e come sono fatti questi gadget?

I **gadget** sono gruppi di istruzioni assembly che terminano con una `ret`.  In questo modo posso sfruttare lo **stack come codice** per la mia ROP:

- Uso lo stack pointer `%esp` come program counter
- I gadgets vengono invocati uno dopo l'altro attraverso l'istruzione `ret`
- I gadgets ottengono i loro args tramite `pop` proprio perchè anch'essi si trovano sullo stack

*Esempio*: Simple stack smashing for ROP

![](ropexample.png)

1. Setup the gadgets on the stack with a stack smashing attack and make `%eip` point to the first one in the series
2. When the function returns, the modified `%eip` will execute the first gadget code
3. The gadget can read it's arguments in the stack by popping values out of it
4. At the end of the gadget code, there is a `ret` which has to point to the next gadget
5. Continue the execution of string of gadgets until the attack is complete

### Come ottenere i gadget

Per trovare dei gadget utilizzabili per un ROP attack, dobbiamo cercare nel codice disassemblato del programma delle istruzioni `ret`: da lì poi posso tornare indietro e ottenere frammenti di codici che possono esserci utili.

Questa ricerca può essere automatizzata in modo da assicurarci di avere sufficienti gadget per svolgere l'attacco nel minor tempo possibile. Ma cosa significa "sufficienti"? Quanti gadget servono per poter svolgere un attacco?

E' stato dimostrato che per sostanziali codebases, i gadgets possono essere addirittura Turing complete. Quindi maggiore è la quantita di codice da cui posso estrarre gadgets, e migliori sono le possibilità di riuscita di un attacco.

### Difendersi dal ROP

Tornando al giochino del gatto col topo, come può la difesa rispondere a questo tipo di attacco?

**Defense**: Randomizza la posizione in memoria del codice compilandolo in modo da renderlo position independent: in questo modo posso randomizzare gli indirizzi del codice a ogni esecuzione per ridurre le probabilità di permettere all'attaccante di trovare i gadget (soprattutto su macchine a 64bit)

***Attack response***: La tecnica del Blind ROP mi consente di bypassare questa randomizzazione e sconfiggere la difesa ancora una volta. Vediamo i dettagli

### Blind ROP

1. Stack reading: cerco di ottenere informazioni dal sistema in modo da capire dove si trovano i blocchi text, stack e/o heap (bruteforce con 32 bit, information leak con 64 bit). Con l'information leak posso capire circa l'indirizzo dei blocchi osservando gli indirizzi di ritorno nello stack. **TL;DR Trova return address e canary address**

2. Cerca a run-time del codice che esegue una `write(sd, buf, length)` e trovare dei gadget che mi permettono di emularla

   ``` assembly
   POP RDI, RET # arguments della write: in RDI metto il socket descriptor (sd)
   POP RSI, RET # in RSI metto il buf
   POP EDX, RET # in EDX metto la length
   POP RAX, RET # codice della syscall della write da chiamare
   syscall
   ```

   Scansiono quindi il più possibile il codice per ottenere più gadget possibili e mapparli in modo da identificare:

   - PROBE 0xAddress è l'indirizzo del gadget da testare per capire se lavora sullo stack o meno

   - STOP 0xAddress è l'indirizzo del gadget che fa crashare il programma ma lascia la socket aperta

   - TRAP 0xAddress è l'indirizzo del gadget che fa crashare il programma e la socket viene chiusa (crasha la macchina)

3. Ottieni una copia del binario in modo da trovare sufficienti gadgets per una shellcode injection

La Blind ROP è sconfigge definitivamente ogni tentativo di difesa della memoria, in quanto è stato dimostrato come la BROP sia estremamente efficace anche contro macchine remote (server) a 64bit con canaries e ASLR abilitato.

La battaglia è stata persa, ma non la guerra; i difensori, oltre a poter implementare la Memory Safety, possono ancora avvalersi di un ultimo strumento: la ***Control Flow Integrity analysis***.