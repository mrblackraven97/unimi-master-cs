## Meltdown & Spectre (Side Channel Attacks)

Questi tipi di attacchi, chiamati **Side Channel Attacks** stanno a metà tra attacchi *hardware e software*. Sono attacchi per reperire informazioni studiando l'ambiente esterno (*Modello Black Box*).

Ad esempio la **power analysis** cerca di misurare quanta corrente sto usando in modo da calcolare quale operazione il processore sta effettuando per ottenere dati sensibili (come ad esempio la chiave crittografica del chip, nel caso in cui questo fosse un chip di cifratura).

Questi attacchi che andremo a vedere si basano su due prerogative principali:

- Sono degli attacchi alle **memorie cache**
- Sfruttano la **speculative execution**

## Sfruttare le cache

La memoria cache, come sappiamo, è una memoria di prossimità molto veloce, che viene **condivisa tra tutti** i processi che girano nel processore. Essa viene svuotata quando avviene lo switch tra un processo e un altro.

Possiamo pensare a una cache come a una matrice, dove le *cache set* sono le righe della cache e le *cache line* sono le colonne.
Sappiamo che un processo accede a un indirizzo del suo spazio di indirizzamento tramite un indirizzo virtuale. Una parte dell'indirizzo virtuale ci dice qual'è l'indice della matrice della cache.

I nostri side channel attacks cercano di vedere se la cache rilascia informazioni rispetto ai **pattern di accesso** del programma vittima alla memoria cache.

La situazione che dobbiamo immaginare è quella di un programma SPY e uno VICTIM, con SPY che ha accesso al codice sorgente della vittima.
Se SPY riesce a ottenere informazioni sui pattern di accesso della vittima allora può *dedurre in quale parte del codice la vittima* si trova e cosa sta facendo.

Si possono usare due tecniche differenti (assumendo che l'attaccante possa anche controllare come i processi debbano essere eseguiti dentro la mia macchina):

- **Flush and Reload**

  Il programma SPY svuota ( **Flush** ) interamente la cache. Successivamente il programma VICTIM accede all'indirizzo Y che verrà caricato nella cache. Quindi viene prelazionato SPY che ricarica ( **Reload** )  i propri dati nella cache (in tutti gli indirizzi della cache) e calcola i tempi T di caricamento. 
  Se la cache, in quella posizione era vuota allora ci sta un tempo T. Altrimenti ci starà un tempo T' e quindi riesce a dedurre dove aveva acceduto VICTIM (utilizzando una **tecnica di timing**).
  In questo modo riesce a capire  il flusso di esecuzione di VICTIM.

- **Prime and Probe**

  E' la **tecnica inversa** a quella descritta pocanzi. Il programma SPY riempie tutta la memoria cache e la vittima svuoterà solo quella che gli serve. Tramite la tecnica di timing il programma SPY riuscirà comunque a scoprire dove la vittima aveva acceduto.

Può sembrare una informazione banale, ma spesso i programmi di crittografia utilizzano delle tabelle di lookup per applicare una **chiave crittografica**. Capire a quale indirizzo sto accedendo, in questo caso, significa ricostruire la chiave crittografica e quindi ottenere una informazione importantissima. 

Quindi utilizzando il timing (ovvero un side channel effect ) riesco ad attaccare la vittima. 

## Speculative Execution

Con questo termine ci riferiamo ad una *ottimizzazione di alcuni processori Intel*. 

Con questa ottimizzazione i processori eseguono in maniera continua un blocco di istruzioni, **senza curarsi delle dipendenze** (ad esempio se accedo ad una zona di memoria non consentita, loro prima ci accedono per questioni di ottimizzazione, e poi controllano se ci potevano accedere, ma solo successivamente).

Le dipendenze verranno risolte in seguito (nel caso precedente avremo un segmentation fault, ma la cosa importante è che il valore viene messo nella memoria cache).

Vi è quindi una sorta di **Shadow State**, uno stato nascosto dell'esecuzione del programma, che esegue in parallelo un blocco di istruzioni e successivamente controlla le dipendenze (effettuando casomai un rollback). 

Le cache, però, vengono aggiornate da questo shadow state, anche se al sistema operativo non è visibile tale aggiornamento.

Vediamo cosa può accadere. Supponiamo di avere il seguente pseudocodice:

`Access_Kernel(Byte);
Data = Byte;
Access(Probe_Array(Data*4096))`

In questo modo io accedo a un byte dello spazio del kernel, e poi utilizzo questo byte come indice di riga (4096 serve per spostarsi tra le righe) della cache. Utilizzando quindi la stessa tecnica di prima, ovvero quella del timing, riesco a sapere esattamente il **valore di un byte del kernel**. (che sarà la linea della cache che ci ha messo più tempo ad essere acceduta).

Ovviamente un solo processo SPY non basta, perchè dopo aver fatto un accesso al kernel questo processo verrà ucciso con Segmentation Fault Error. Quindi bisogna avere due processi con una memoria condivisa. Il primo farà **probing ** (letteralmente "sondaggio") e l'altro si farà ammazzare (insert 'press F to pay respect meme' here).

Facciamo quindi un attacco **read-only** alla memoria del kernel, leggendo un byte alla volta. Rimane molto potente.

## Ricapitolando

L'attacco Meltdown funziona esattamente come descritto sopra. E' un'attacco alle cache che utilizza la speculative execution per leggere la memoria del kernel.

L'attacco Spectre invece è un pochino diverso, perchè utilizza un'altra ottimizzazione dei processori moderni: il **branch prediction**.

## Branch Prediction

Questa ottimizzazione dei processori, a differenza di quella di prima, vale per tutti i processori.

E' una ottimizzazione molto semplice: il processore esegue entrambi i rami dell'if-else quando si trova di fronte a una diramazione. Successivamente scoprirà dove andare.
Inoltre, se si trova all'interno di un ciclo, ed esso va nel ramo if diciamo X volte, la volta successiva il processore non controllerà più la condizione ed entrerà nel ramo if, effettuando semmai un rollback dopo.

L'attacco quindi è un pò più articolato e permette di leggere la memoria del programma victim. Bisogna *allenare il branch prediction* in modo da andare sempre dentro il ramo if per poi sforare l'array e leggere la memoria del processo (eg. informazioni sensibili, canarini, ecc.) dalla memoria cache usando il metodo di probing descritto per Meltdown.

Questo attacco è stato fatto su HTML e HTML5 in Javascript e permetteva di **leggere l'intera memoria del Browser**.

## Esistono Difese?

Purtroppo essendo un problema hardware non possiamo patchare tutto l'hardware delle macchine legacy. 
Una soluzione, molto costosa per le performance, sarebbe utilizzare **Kaiser** che ci permette di togliere la mappatura di **tutta** la memoria kernel caricata quando il processo si trova in user space. Quindi risolvo il problema concettuale di Meltdown (non leggi la memoria del kernel) ma c'è un problema di performance lentissimo.

Un'altra misura di difesa che si può applicare è utilizzare compilatori particolari che ci permettono di **togliere la speculative execution in alcune parti** del programma, rallentandolo quindi, ma non troppo.

La verità è che *non ci sono delle vere soluzioni*, andrebbe riprogettato l'intero hardware.

## Even More? Row Hammer Attack

Sono altri tipi di side channel attack, che però riescono a **scrivere** sul programma vittima, rompendo quindi il concetto di **isolamento dei processi** introdotto dall'hardware.

Si basano sulle memorie RAM fisiche e sulle loro ottimizzazioni.
Per questioni economiche, infatti, i chip delle ram sono molto vicini tra di loro in un banco di ram e sono, quindi, influenzabili da campi elettromagnetici.

Ci sono attacchi, infatti, che permettono di scrivere particolari caratteri su una ram andando a creare un **campo elettromagnetico** che modifica alcune, precise, locazioni di memoria RAM adiacente.

Inoltre i controlli di parità (ECC) sono spesso disabilitati sui dispositivi per performance ma è stato effettuato un attacco anche in presenza degli ECC.

Il prof ci ha parlato dell'esistenza di un attacco che permetteva a un computer **isolato** di inviare un messaggio GSM ad un telefono vicino ad esso utilizzando solamente i campi elettromagnetici generati dal programma in C che girava nella macchina isolata. ("Ha un aura potentissima!!")

Ancora una volta, quindi, il problema rimane un **problema di progettazione dell'Hardware**.