# Defense against ROP : CFI

Come abbiamo potuto osservare, semplicemente complicare gli step in un attacco di stack smashing non è sufficiente a impedirlo (vedi BROP). 

Se invece potessimo andare a controllare e osservare il comportamento del programma durante l'esecuzione, potremmo notare quando questo behaviour non corrisponde a ciò che ci aspettiamo (quando quindi assume un *malicious behaviour*) e fermarlo per impedire ulteriori violazioni.

Questa metodologia viene definita **Control Flow Integrity** e per applicarla occorre:

- Definire cosa si intende con **"expected behaviour"**
  - Potremmo definire un control flow graph (CFG) che rappresenti come le diverse funzioni del programma interagiscono fra di loro (chi chiama cosa e quando)
- Come **rilevare deviazioni** dall' "expected behaviour" e come farlo efficientemente
  - Potremmo definire un in-line reference monitor (IRM) che per ogni riga del programma verifica che la proprietà del CFI venga mantenuta
- **Evitare manomissioni** nel rilevatore di deviazioni 
  - Potremmo basarci come sempre su una sufficiente randomicità per nascondere i segreti della gestione della CFI e basarci sull'immutabilità del codice

### Efficienza

Il CFI originale (2004) imponeva come media 16% di overhead, risultando quindi molto efficiente nel risolvere il problema; l'unico difetto che aveva era quello di non essere modulare e quindi di non poter utilizzare DLL (dynamically linked libraries).

Il mCFI (2014) aggiunge la modularità, con compilazione separata, e aumenta l'efficienza arrivando a un 5% di overhead medio. 

### Efficacia

E' stato dimostrato come l'mCFI riesce a eliminare circa il 95% dei ROP gadget e ridurre quasi completamente la possibilità di compiere salti arbitrari non previsti dal CFG.

Vediamo quindi i vari componenti teorizzati in dettaglio.

## Control flow graph

Il control flow graph è un **grafo di connessioni fra le funzioni di un programma** e le chiamate che svolgono fra di esse: l'idea è quella di dividere le istruzioni assembly corrispondenti in **"basic blocks"** per cui ognuno di essi termina con un istruzione che cambia il controllo di flusso di programma tramite ***indirect calls*** (ovvero `jmp`, `call` o `ret` che si basano su dati presenti in registri). Questo perchè, aspettandoci che il DEP sia abilitato e il codice sia immutabile, l'indirizzo delle chiamate dirette a funzione non può essere cambiato e quindi non ci interessa monitorarlo.

*Esempio*: CFG di un programma semplice

```c
void sort2(int[] a, int[] b, int len)
{
    sort(a, len, lt);    //sort calls the passed compare func (lt/gt) as a functor
    sort(b, len, gt);    //these 2 are direct calls which do not need to be monitored (if DEP enabled)
}

bool lt(int x, int y) { return x < y; } //lt = less than
bool gt(int x, int y) { return x > y; } //gt = greater than
```

### Basic Blocks

![ControlFlowGraph](ControlFlowGraph.png)

Mappando tutti i basic blocks e quindi tutti i flussi di programma nel mio control flow graph, se dovesse accadere, come nel ROP attack, un controllo di flusso non previsto (e quindi non corretto) mi accorgo dell'attacco!

![BasicBlocksROPable](BasicBlocksROPable.png)

Senza control flow graph a controllare la direzione di esecuzione dei basic blocks appaiono così collegati e non c'è alcuno vincolo nell'ordine di utilizzo (exploitabile da ROP)

![BasicBlocksCFG](BasicBlocksCFG.png)

Definendo adesso un flusso di controllo diretto, con un ordine ben definito, mi accorgo subito se qualcuno sta tentando di usare i blocchi in modo non previsto dal control flow graph, creato in fase di compilazione o dal binario.

### Chiamate dirette

Come possiamo vedere nel codice ho due chiamate dirette verso la funzione `sort()` dalla funzione `sort2()`: questi salti diretti non sono vulnerabili nel caso il DEP fosse abilitato perchè il codice sarebbe immutabile, pertanto posso evitare di monitorarli. 

![](cfgdirect.png)

### Chiamate indirette

Qui invece possiamo vedere tutte le altre chiamate indirette, ovvero salti che si basano su indirizzi dinamici a runtime e che sono potenzialmente manipolabili da un attaccante: è necessario monitorare i blocchi che prevedono questi salti.

![](cfgindirect.png)

## In-line Monitor

Essenzialmente potrei assegnare a ogni basic block una label e quando il programma compie un salto indiretto, inserisco del codice di controllo per verificare che la label sia quella prevista dal grafo: se non lo è do errore e/o termino il programma.

```assembly
CALL [ebx+8]

#controllo iniettato del CFG
MOV eax, [ebx+8]
CMP [eax+4], 12345678h # 12345678h è l'etichetta del basic block previsto in quel flusso
JNE error # se non uguali (NotEqual) lancio un errore: tentato ROP exploit

#qui completa la CALL [ebx+8] se non ho avuto errori
```

Ma che sistema utilizzare per implementare un labeling efficace?

### Labeling semplice

Per implementare le labels nel modo più semplice possibile, basterebbe generare un valore random e assegnarlo a ogni salto indiretto da monitorare nel programma. E' un labeling piuttosto rudimentale perchè:

- Riesce a bloccare chiamate verso blocchi che non hanno labels (come una chiamata a libc)
- Permette, però, di saltare a qualsiasi altro blocco dotato di label (anche se non previsto dal flusso)

![](simplelabel.png)

### Labeling dettagliato

Per risolvere il problema del labeling semplice, posso porre i seguenti vincoli di labels:

- Tutti i punti verso cui la funzione `sort()` ritorna (chiama `ret`) devono avere la stessa label (**label L**)
- Le funzioni soggette a `call` da parte di `sort()`, ovvero `gt()` ed `lt()`, devono avere la stessa label (**label M**)
- Tutto il resto ha la stessa label (**label N**)

![](detaillabel.png)

Questo è il grafico più preciso che possiamo ottenere tramite labeling. L'unico problema rimasto è che sarebbe permesso comunque ritornare arbitrariamente da label N a label L, anche non a fine funzione. Fortunatamente, almeno in questo caso, l'attaccante avrebbe poco da guadagnare da ciò.

## How to counter CFI

E' possibile comunque effettuare attacchi ROP nonostante esista il CFI?

Abbiamo diverse possibilità da testare:

- Iniettare codice che ha una label legale?
  - Ma sappiamo che non funziona perchè assumiamo che i dati non siano eseguibili perchè il DEP è abilitato
- Modificare le label del codice per cambiare il flusso di controllo previsto?
  - Non funzionerebbe perchè il codice è immutabile...
- Alterare lo stack durante il controllo delle label per far sembrare che il check abbia avuto successo?
  - Non funzionerebbe perchè l'attaccante non può alterare registri in cui carichiamo dati rilevanti

Possiamo concludere che **il CFI rende impossibile gli attacchi basati sulla modifica del control flow** (ROP, remote-code injection, return to libc...), ma NON la manipolazione legale del control flow (che segue le label corrette!).

Questo tipo di attacco, che segue il normale flusso del programma ma comunque riesce a ottenere dei privilegi, si dice mimicry attack (i single-label CFG sono molto suscettibili a questi attacchi). 

Il CFI inoltre NON previene comunque data leaks o corruptions, come heartbleed o un `authenthicated` overflow come il seguente

```c
void log()
{
    ...
    int authenticated = 0;
    char buffer[4];
    strcpy(buffer, str); // posso overfloware il buffer e modificare authenticathed
    if(autenticathed)     // il tutto senza che cfi se ne accorga
    {
        ...
    }
}
```
