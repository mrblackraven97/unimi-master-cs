# Heap overflow on metadata

Vediamo ora come è possibile eseguire degli attacchi overflow sullo heap, anzichè sullo stack.

Osserviamo le differenze fra Heap e Stack

|                                        | Stack                                                        | Heap                                                         |
| -------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Tipo allocazione**                   | Allocazioni su **memoria fissa**, conosciute a *compile-time* | Allocazioni su **memoria dinamica**, conosciute a *run-time* |
| **Contenuto**                          | Variabili locali, return addresses, args di funzione         | Oggetti, structs, grandi buffer, dati pesanti e persistenti  |
| **Velocità e modalità di allocazione** | Allocazione veloce e automatica, svolta dal compilatore      | Allocazione lenta e manuale, gestita dal programmatore       |

Tirando le somme, l'heap è un ottimo pool di memoria utilizzato per allocazioni dinamiche a run-time di oggetti di grande dimensione. Per svolgere allocazione e deallocazione si utilizzano le due seguenti funzioni:

- `malloc()`, ottiene e alloca il numero di bytes richiesti sulla memoria dello heap, ritornando un puntatore all'inizio di quell'area di memoria
- `free()`, dato un puntatore di heap, ne rilascia l'area di memoria precedentemente allocata

*Esempio*: **Allocazione e deallocazione di stringhe su heap**

```c
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
    char* buf1 = malloc(128);
    char* buf2 = malloc(256);
    
    read(fileno(stdin), buf1, 200);
    
    free(buf2);
    free(buf1);
}
```

## Struttura dell'heap

Ogni area di memoria allocata sullo heap (**chunk**) è rappresentato da uno struct composto come segue

```c
struct malloc_chunk {
    INTERNAL_SIZE_T prev_size; 	/* Size of previous chunk (if free). */
    INTERNAL_SIZE_T size; 	    /* Size in bytes, including overhead. */
    
    struct malloc_chunk* fd; 	/* double links -- used only if free. */
    struct malloc_chunk* bk;
    
    /* Only used for large blocks: pointer to next larger size. */
    struct malloc_chunk* fd_nextsize; /* double links -- used only if free. */
    struct malloc_chunk* bk_nextsize;
};
```

Possiamo osservare come la memoria sullo heap sia vista come una doubly linked list (doppia) di chunks, basta notare i puntatori a chunks `fd`(forward chunk) e `bk`(backwards chunk).

![](./HeapStructure.png)

### Allocazione su heap

Quindi, se un programma dovesse fare le seguenti chiamate `malloc(256)`, `malloc(512)` e `malloc(1024)`, il memory layout dell'heap sarebbe il seguente:

|      Meta-data of chunk created by malloc(256)      | *Start of first chunk*      |
| :-------------------------------------------------: | --------------------------- |
| The area of 256 bytes of memory returned by malloc  |                             |
|    **Meta-data of chunk created by malloc(512)**    | ***Start of second chunk*** |
| The area of 512 bytes of memory returned by malloc  |                             |
|   **Meta-data of chunk created by malloc(1024)**    | ***Start of third chunk***  |
| The area of 1024 bytes of memory returned by malloc |                             |
|          **Meta-data of the *top chunk***           |                             |
|                Available free space                 | **↓ 0xffffffff**            |

Vediamo come l'entità del **top chunk** rappresenta il chunk contentente tutta la memoria rimanente disponibile sull'heap. Ogni volta che viene richiesta memoria (tramite `malloc()`) il top chunk si divide in due:

- La prima parte viene definita **requested chunk**
- La parte rimanente diventa il nuovo **top chunk**

### Deallocazione su heap

Quando un chunk viene liberato, vengono svolte le seguenti operazioni

- Il bit meno significativo del campo `size` nei metadati del forward chunk deve essere resettato a 0
- Il campo `prev_size` del forward chunk deve essere settato alla dimensione del chunk che stiamo deallocando
- Quando un chunk sta per essere deallocato, viene controllato se il backwards chunk è stato liberato in precedenza: se è così fonde il chunk che sta per essere liberato col free chunk precedente in un unico free chunk

Allocando e deallocando chunks di memoria sullo heap, potrebbero verificarsi problemi di memory segmentation: avendo free chunks di diverse dimensioni, li indicizzo tramite liste multiple, in modo che ogni lista contenga solo free chunk di una determinata dimensione.

In questo modo, quando viene richiesta una allocazione di memoria, prima cerca un free chunk di quelle dimensioni nelle liste di free chunks; se non trova un chunk appropriato, utilizza il top chunk per la nuova allocazione.

### Sommario

Ricapitolando, possiamo vedere come i chunk allocati appaiono così

![](./normalchunk.png)

Mentre i chunk "liberati", ovvero quelli presenti in una free chunk list, appaiono così

![](./freechunk.png)

### Deallocazione di free chunk  (double free() exploit)

Generalmente, nel mondo dell'informatica chiamare due volte (o più) la stessa funzione non dà origine a situazioni catastrofiche: questa cosa non è vera per la `free()`.

Chiamando la `free()` sullo stesso chunk di memoria due volte significa porre quel chunk due volte nella lista dei free chunk list. Se dovessi chiamare poi una `malloc()` e riallocare quel chunk avrei una inconsistenza di stato per quel chunk, infatti appare come allocato ma è presente anche nella free chunk list. Ma non solo.

Il fatto che io abbia "riallocato" quel chunk mi permette di scriverci dentro arbitrariamente e **sovrascrivere** i metadati introdotti quando era un free chunk (i puntatori ai chunk adiacenti). In questo modo posso exploitare la vulnerabilità della funzione macro `unlink()`. 

```c
#define unlink(P, BK, FD) \
{ \
    BK = P->bk; \
    FD = P->fd; \
    FD->bk = bk; \
 	BK->fd = FD; \
}
```

Questa funzione è l'operazione base di rimozione di un nodo da una lista linkata doppia: infatti salva il nodo precedente (`BK`) e nodo successivo (`FD`) del nodo da rimuovere (`P`). Poi assegna il precedente di `FD` a `BK` e il successivo di `BK` a `FD`.

Tuttavia, sovrascrivendo i metadati, sovrascriverei anche i puntatori `BK` ed `FD`, sostituendoli con puntatori a scelta dell'attaccante.

In questo modo, allocando anche la "seconda" istanza di quel chunk che era nella free list, quando la funzione `unlink()` verrà chiamata, i puntatori modificati verranno usati.

La vulnerabilità di glibc che permetteva questo exploit è stata però patchata e questo metodo non è più applicabile tramite la funzione `unlink()`

***Patch***: Semplice controllo sui puntatori dei metadati

```c
#define unlink(P, BK, FD) \
{ \
    BK = P->bk; \
    FD = P->fd; \
    if (__builtin_expect (FD->bk != P || BK->fd != P, 0)) \
		malloc_printerr(check_action,"corrupted double-linked list",P); \
	else {\
        FD->bk = bk; \
        BK->fd = FD; \
    } \
}
```




## Nuove tecniche
Sono nate nuove tecniche con diverse metodologie e che sfruttano diverse vulnerabilità nei programmi.

- The House of Prime. 
- The House of Mind.
- The House of Force.
- The House of Lore.
- The House of Spirit.
- The House of Chaos.

Fra queste, vedremo la tecnica della House of Force.

### House of Force
Le condizioni per applicare questo metodo richiedono che

- esista la possibilità di sovrascrivere il top chunk (sovrascrittura dei metadati)

- che esiste una chiamata a `malloc()` con una dimensione controllabile dall'utente (controllo sull'input)

- che esista una seconda chiamata a `malloc()`

Esiste una variabile puntatore av->top che punta sempre al top chunk. L'obiettivo è quello di **sovrascrivere av->top** con un valore controllato dall'utente (quindi dall'attaccante). 

Quando una `malloc()` viene chiamata, questa variabile è usata per ottenere una reference al top chunk (nel caso non esistano altri chunk di dimensione appropriata, come sappiamo).

Questo significa che se riusciamo a controllare il valore di av->top e a forzare la `malloc()` a usare il top chunk, allora sappiamo (e controlliamo) dove il prossimo chunk verrà allocato. Conseguentemente, possiamo scrivere byte su qualsiasi indirizzo arbitrariamente.

Siccome vogliamo assicurarci che ogni richiesta utilizzi il top chunk, andremo ad effettuare un **buffer overflow** nel tentativo di sovrascrivere i metadati del top chunk.