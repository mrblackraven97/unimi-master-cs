#!/usr/bin/python
from z3 import *
import sys

if __name__ == '__main__':
   
   # creo un oggetto di tipo Solver aka il sat solver
   solver = Solver() 

   # aggiungo la variabile x
   x = [Int(10)]

   # aggiungiamo delle condizioni da verificare
   solver.add(x > 5)
   solver.add(x > 7)

   #andiamo a verificare le sat conditions
   result = solver.check()

   #otteniamo il modello che soddisfa le sat conditions (se esiste)
   model = solver.model()

   print "The solver check result is {}, and the resulting model is {}".format(result, model)
