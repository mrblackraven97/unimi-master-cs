#!/bin/bash

printf "es1 svigruppo\n"
printf "reset esercizio se esiste\n"

if [ -d gitEs1 ]; then
    printf "cartella esistente\n"
    rm -r gitEs1
fi 

mkdir gitEs1
git init

# primo commit

printf "text A\n" >> gitEs1/A
printf "text B\n" >> gitEs1/B

git add *
git commit -m "uno"
git branch student

# secondo commit

printf "text A\n" >> gitEs1/D
printf "text B\n" >> gitEs1/C

git add *
git commit -m "due"

# terzo commit

printf "text C" >> gitEs1/B
printf "text C" >> gitEs1/A

git add *
git commit -m "tre"
