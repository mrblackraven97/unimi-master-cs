#!/bin/bash

if [ -d es4git ]; then
    rm -rf es4git
fi

mkdir es4git
cd es4git
git init

printf "primo\n" > T
mkdir R
printf "linea\n" > R/S

tree .

git add *
git commit -m "feature RT"

git mv R Z
cp Z/S U
printf "aggiunta\n" >> T

tree .

git add *
git commit -m "feature ZUT"
git branch ZUT

git rm -rf T U Z 

git commit -m "end"

tree .

printf "terzo\n" > A
git add A

tree .

cp .git/refs/heads/master .git/HEAD
