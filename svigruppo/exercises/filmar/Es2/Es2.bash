#!/bin/bash

printf "es 2 di svigruppo\n"

if [ -d es2git ]; then 
    printf "Cartella pre esistente... eliminazione\n"
    rm -rf es2git
fi

mkdir es2git
cd es2git
git init

printf "text A\n" > A
printf "text B\n" > B

git add A B 
git commit -m "uno"

tree .git/
tree 

git branch student

mkdir _C
git mv A _C/D
git mv B _C/C
git mv _C C

printf "text C\n" > A
cp A B

git add A B
git commit -m "tre"

tree .git/
tree

rm A B
git mv C _C
git mv _C/C C
git mv _C/D D

git add *
git commit -m "due"

tree .git/
tree

git reset --hard HEAD^
git checkout student

printf "text D\n" > F
git add F

tree .git/
tree
