#!/bin/bash

# auxiliary setup: NOT TO INCLUDE IN THE EXAM
start="${0##*e}"
esName="${start%.*}"
mkdir -p $esName
cd $esName
rm -rf * .* 2> /dev/null
# start git script
git init
printf "primo\n" > T
mkdir R
printf "linea\n" > R/S
git add T
git add R/S
git commit -m "feature RT"

printf "primo aggiunta\n" > T
git mv R Z
cp Z/S U
git add -A
git commit -m "feature ZUT"

git branch ZUT
git rm -r Z/S
git rm T
git rm U
git commit -m "end"

git checkout --detach master
printf "terzo\n" > X
git add X
git reset HEAD X # non voglio che X sia puntato dall'index quindi con --mixed lo rimuovo da esso, ma rimane come blob "volante"/"orfano"

