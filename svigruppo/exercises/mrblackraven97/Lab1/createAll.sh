#!/bin/bash

for f in *.sh; do  
   if [ "$f" != "${0##*/}" ]; then
      bash "$f" -H 
   fi
done