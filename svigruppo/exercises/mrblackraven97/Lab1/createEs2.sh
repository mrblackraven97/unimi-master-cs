#!/bin/bash

# auxiliary setup: NOT TO INCLUDE IN THE EXAM
start="${0##*e}"
esName="${start%.*}"
mkdir -p $esName
cd $esName
rm -rf * .* 2> /dev/null

# start git script
git init
printf "text A\n" >> A
printf "text B\n" >> B
git add A
git add B
git commit -m "uno"

git branch student
printf "text C\n" > A
printf "text C\n" > B
git add A B
mkdir C
printf "text A\n" >> C/D
printf "text B\n" >> C/C
git add C/C C/D
git commit -m "tre"

git rm A
git rm B
git mv C/D .
git mv C/C cc
rm -rf C  	# cant use rm function... what do?
git mv cc C
git commit -m "due"

git reset --hard HEAD^ 
printf "text D\n" > X
git add X
git reset HEAD X # non voglio che X sia puntato dall'index quindi con mixed lo rimuovo da esso, ma rimane come blob "volante"/"orfano"
git checkout student