#!/bin/bash

# auxiliary setup: NOT TO INCLUDE IN THE EXAM
start="${0##*e}"
esName="${start%.*}"
mkdir -p $esName
cd $esName
rm -rf * .* 2> /dev/null

# start git script
git init
printf "pluto\n" > X
printf "pippo\n" > Y
mkdir Z
cp Y Z
git add -A
git commit -m "primo"

printf "pippo e minnie\n" > D
printf "pippo\n" > X
git rm Y
git add -A
git commit -m "successivo"

git branch student
git tag delivered
mkdir zz
git mv D zz
git mv X zz
git mv Z zz
git mv zz Z # rinomino zz in Z
git commit -m "ultimo?"

git checkout student
printf "sbagliato\n" > S
mkdir zz
git mv D zz
git mv Z zz
git mv zz Z
git add S
git reset HEAD S
