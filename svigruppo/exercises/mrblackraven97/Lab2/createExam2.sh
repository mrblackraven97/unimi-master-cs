#!/bin/bash

# auxiliary setup: NOT TO INCLUDE IN THE EXAM
rm -rf mySolution 
mkdir mySolution
cd mySolution

# start git script
git init
printf "pippo\n" > Y
printf "pluto\n" > X
mkdir Z
cp Y Z
git add -A
git commit -m "primo"

git rm X
git mv Y X
printf "pippo e minnie\n" > D
git add -A
git commit -m "successivo"
git branch student
git tag "delivered" HEAD

mkdir newZ
git mv D newZ
git mv X newZ
git mv Z newZ
git mv newZ Z
git add -A
git commit -m "ultimo?"

git checkout student
git rm X
mkdir newZ
git mv D newZ
git mv Z newZ
git mv newZ Z

printf "sbagliato\n" > A
git add A
git rm -f A

printf "Ultimo" > O
