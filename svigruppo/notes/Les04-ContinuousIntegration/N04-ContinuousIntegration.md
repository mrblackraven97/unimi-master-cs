# Continuous Integration (CI)

Usually, the development of a software system can follow two main methodologies: bottom-up and top-down. It's not really possible to use only one or another purely and exclusively, especially when programming in OOP.

The main idea of the continuous integration is to integrate working pieces of code progressively until the system is fully developed: this technique is, of course, endorsing agile methodologies and incremental development, instead of following a waterfall and horizontal development (first we do db, then backend, then frontend ...)

When the integration is not continuous it becomes an unwelcome event since all developers need to collaborate to fix all the possible bugs introduced by the merge on the build present on the mainline AND on the integration test server. Continuous integration instead allows the integration to become daily, thus a non-event: bugs are found and fixed at each build, avoiding them to amass, thus leading to faster delivery.

Continuous Integrations doesn't get rid of bugs, but it does make them dramatically easier to find and remove. 

Bugs are also cumulative. The more bugs you have, the harder it is to remove each one. This is partly because you get bug interactions, where failures show as the result of multiple faults - making each fault harder to find. It's also psychological - people have less energy to find and get rid of bugs when there are many of them - a phenomenon that the Pragmatic Programmers call the Broken Windows syndrome.

With continuous integration development and deployment becomes one, generating a new professional figure that serves both roles: the DevOps (**Dev**eloper**Op**eration**s**).

## Continuous Integration Principles

Martin Fowler in 2006 defines the [key principles](http://www.martinfowler.com/articles/continuousIntegration.html) of continuous integration:

1. **Maintain a Single Source Repository**
- In general you should store in source control everything you need to build anything, but nothing that you actually build. That means that you should be able to walk up to the project with a virgin machine, do a checkout, and be able to fully build the system.
   - This is in compliant with other principles and techniques of agile methods such as Shared Code (and responsibilities).
2. **Automate The Build** (e.g. Make, Gradle ... )
   - A big build often takes time, you don't want to do all of these steps if you've only made a small change. So a good build tool analyzes what needs to be changed as part of the build process, in order to make it faster and simpler.
3. **Make Your Build Self-Testing** (e.g. XUnit, JUnit )
   - Write tests before you write the code that makes them pass - in this mode the tests are as much about exploring the design of the system as they are about bug catching (Test Driven Development).
   - Tests don't prove the absence of bugs. However imperfect tests, run frequently, are much better than perfect tests that are never written at all.
4. **Everyone Commits To The Mainline Every Day**
   - Doing this points out integration problems and conflicts, which the sooner they are fixed the better. 
   - No separate branches exists for too long. Integration is primarily about communication: by committing to the mainline developers tell other developers about the changes they have made. Frequent communication allows people to know quickly as changes develop.
   - Frequent commits encourage developers to break down their work into small chunks of a few hours each. This helps track progress and provides a sense of progress.
5. **Every Commit Should Build the Mainline on an Integration Machine**
   - Every developer should ensure that regular builds happen on an integration machine and only if this integration build succeeds should the commit be considered to be done. Since the developer who commits is responsible for this, that developer needs to monitor the mainline build so they can fix it if it breaks.
   - This problems can arise from the differences (environment, software, hardware, ...) between the developer machine and the integration machine.
   - Testing builds on integration machine often has the same benefits: shorter and easier debug sessions, which are even more critical on integration machines.
6. **Fix Broken Builds Immediately**
   - A key part of doing a continuous build is that if the mainline build fails, it needs to be fixed right away. The whole point of working with CI is that you're always developing on a known stable base.
   - At the end of the day, the mainline build should be clean (without errors) and integrated with the work of that day.
7. **Keep the Build Fast**
   - The whole point of Continuous Integration is to provide rapid feedback. And since CI demands frequent commits, this adds up to a lot of time. In order to achieve this one should have a testing build (fast) and a mainline build (complete).
8. **Test in a Clone of the Production Environment**
   - The point of testing is to flush out, under controlled conditions, any problem that the system will have in production. A significant part of this is the environment within which the production system will run. If you test in a different environment, every difference results in a risk that what happens under test won't happen in production.
   - As a result you want to set up your test environment to be as exact a mimic of your production environment as possible. Use the same database software, with the same versions, use the same version of operating system. Put all the appropriate libraries that are in the production environment into the test environment, even if the system doesn't actually use them. Use the same IP addresses and ports, run it on the same hardware or virtualize it.
9. **Make it Easy for Anyone to Get the Latest Executable**
   - Anyone involved with a software project should be able to get the latest executable and be able to run it: for demonstrations, exploratory testing, or just to see what changed this week.
   - Make sure there's a well known place where people can find the latest executable: this is easy to do with a versioning system.
   - Decide where you wanna share an executable (simple but heavy) or the compilable source (not at everyone's reach but light and reproducible)
10. **Everyone can see what's happening**
    - Again, Continuous Integration is all about communication, so you want to ensure that everyone can easily see the state of the system and the changes that have been made to it.
    - Versioning systems act like logs, containing the history of changes made to the repository, which translates to "the evolution of the software in development"
11. **Automate Deployment**
    - To do Continuous Integration you need multiple environments, one to run commit tests, one or more to run secondary tests. Since you are moving executables between these environments multiple times a day, you'll want to do this automatically. So it's important to have scripts that will allow you to deploy the application into any environment easily.
    - A natural consequence of this is that you should also have scripts that allow you to deploy into production with similar ease. If you deploy into production, one extra automated capability you should consider is automated rollback. Being able to automatically revert also reduces a lot of the tension of deployment, encouraging people to deploy more frequently and thus get new features out to users quickly.



