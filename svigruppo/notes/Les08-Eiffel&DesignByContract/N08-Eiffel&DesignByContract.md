# Design by Contract

## Contracts : an evolution of the assertions

If used extensively, assertions alone can make up for the specifications of the components of a software system. 

The idea behind Meyer's Design by Contract is to use the same language for the specifications and the implementation: this is because the specification should be part of the code-base since it's strictly correlated to it. 

The specification is part of the "contract", according to which each component provides his own services and functionalities to the rest of the system.

### Hoare triples

So how these specifications can be formulated? When we consider the former assertion examples, we can see that sometimes we have to check some preconditions, some other times postconditions and so on. Because of this we can generalize that: 

Given a program S, every execution that starts from a state that satisfies the pre-conditions P and that **terminates** ends in a state that satisfies the post-conditions Q. 

```math
{P} S {Q}
```

Every programs that terminates is correct if and only if this property is verified.

### Contracts

The Hoare triple {P}S{Q} can become a contract between the one who implements S (developer) and the one who uses it (user), in which:

- The developer implements S and *promises* a final state in which Q is verified for every initial state that satisfies P
- The user is committed to use S in a state that satisfies P in order to obtain in which Q is verified

Il principio di sostituzione di Liskov stabilisce che, perchè un oggetto di una classe derivata soddisfi la relazione is-a, ogni suo metodo: 

- deve essere accessibile a pre-condizioni uguali o piu deboli del metodo della superclasse; 

- deve garantire post-condizioni uguali o piu forti del metodo della superclasse; 

Altrimenti il “figlio” non puo essere sostituito al “padre” senza alterare il sistema.

...

Eiffel doesn't follow Liskov conditions: this is because it prefers language expressivity over that. Eiffel follows covariance: covariance because the method restrictions varies with the object called on (co-varies).

For example calling animal eat food

In eiffel se c'è una funzione di cui sto facendo l'overriding (in una classe figlia di una classe madre) non basta scrivere ensure ma serve scrivere ensure then perchè le postcondizioni devono essere in and con quelle della classe madre. Stesso vale per le precondizioni: serve require else perchè le precondizioni devono essere in or con quelle della classe madre.