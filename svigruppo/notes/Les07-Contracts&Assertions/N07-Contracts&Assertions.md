# Contracts and Assertions

## Division of the work

As Brooks' said, we need to parallelize as much work as possible in order to reduce the continuous need of intercommunication and coordination. 

Of course, a complete isolation of work subgroups is not possible because each of them need to know how to fit their piece of code with the rest of the system.

Particularly, they need to know at least how the other pieces of software behave

- in physiological situations (**correctness**)
  - How much the system or component is ***free from faults*** in its specification, design and implementation
- in pathological situations (**robustness**)
  - How much the system or component can ***work correctly*** in the presence of ***invalid inputs*** or under ***stressful environmental conditions***

A specification is a description of the properties of a component used to solve a problem (defined by the project requirements). This only covers the description of the components of the solution (**what**): it doesn't say anything about **how** they work to solve the problem.

So the specifications act as an **interface** of the components each workgroup is developing: this interface is useful to other workgroups to know the minimum needed to understand how to use each component together in a complex system.

For example, just think of the interface of a complex system like an operating system: the user can use it fruitfully and in many ways in order to reach its goal: however the user knows nothing on how it works inside.

So, the only thing it's necessary to coordinate on is the **specification** of the various components, which means according on **what** is simpler than on **how**. 

To sum up, each component can work in isolation in **how** it solves its problem, while needs to adhere to the interface specifications(**what**) decided during the coordination phase.

### Common interface faults

However, this is not a perfect mechanism: Perry & Evangelist identified a series of "Interface Faults" which plague even modern complex systems: the coordination phase can suffer misunderstandings between team members and these will result in interface faults between components. 

Some examples are

- Misuse of interface
- Data structure alteration
  - For example, the Java String is immutable by definition: however many people think otherwise, treating it as an array of characters and leading to many errors. This is a clear interface fault since the misunderstanding is so frequent
- and so on ...

### Assertions : How to check the adherence to specifications

In order to avoid these unpleasant interface faults, the base mechanism to verify that the specifications are fulfilled are the **assertions**.

An assertion is a logical expression specifying a program state that must exist at a particular point during program execution. This means that if a set of conditions is not satisfied at some point of the program, the assertion "complains loudly" if a design assumption is not respected.

This encourages developers to not use `printf()` to debug, since these only tell the state of the program, not if it's right or wrong. Assertions instead formalizes the developers' hypothesis and thoughts in real code

The assertion mechanism is now implemented and usable in almost every language.

But how to use them properly?

Assertions come in different flavors and patterns, which serve different purposes:

- **assume**
  - A certain state (pre-conditions) is assumed to be verified before the following instructions. 
- **promise**
  - A certain state (post-conditions) is promised to be verified after the following instructions
- **return**
  - A certain state which is expected to return from a function
- **assert**
  - A certain state which is asserted in order to continue (needs to be verified)

If these assertions aren't verified the execution is aborted, since the input doesn't adhere to the specifications (misused interface => interface fault). 

Rosenblum defines a preprocessor to translate all these terms in assertions in C language:

*Example 1*: ***Declaration of a sqrt function with its expected behavior***

```c
int square_root(int x);
/*@
    assume x >= 0;			 // it's assumed that x is positive otherwise we would have an error
    return y where y >= 0;    // the expected return value is positive
    return y where y*y <= x   // the expected return value is y such that y*y <= x (y is root of x)
    && x < (y+1)*(y+1);		  // but also that (y+1)*(y+1) > x
@*/
```

*Example 2*: ***Declaration and definition of a swap function with its expected behaviour***

```c
// declaration
void swap(int* x, int* y);
/*@
	assume x && y && x != y;	// it's assumed that x and y are different and both non-zero
	promise *x == in *y;		// it's promised that the value of x will be in location of y
	promise *y == in *x;		// it's promised that the value of y will be in location of x
@*/

// definition
void swap(int* x, int* y) {
	*x = *x + *y;
	*y = *x - *y;
	/*@ assert *y == in *x; @*/   // it's asserted that the value of y is in location of x
	*x = *x - *y;
}
```

The classical uses of assertions are the following:

- **Consistency between arguments**
- **Non-null pointers**: common check before the use of a pointer
- **Intermediate summary of processing**: halfway assertion in the middle of a process to check if everything is going as expected
- **Dependency of the return value on the arguments**: the relation between the output value given the input values
- and so on...

