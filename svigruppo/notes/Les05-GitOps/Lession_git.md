# GIT #


### Stash ###

> mette da parte WorkingDirectory e index per poi fare reset

passaggi senza stash:

- abbiamo fatto modifiche, sia su index che su WorkingDirectory e dobbiamo pullare salvando prima endrambe le modifiche 
- nuovo branch e checkout e committo l'index su questo nuovo branch
- aggiungo e committo anche la WorkingDirectory sul nuovo branch
- torno su master e faccio pull
- poi vado sul nuovo branch e ribaso questo branch su master 
- prendo commit index e lo metto nell'index
- prendo commit WorkingDirectory e la metto nella WorkingDirectory
- sposto MASTER al commit prima di index

complesso eseguirlo spesso senza stash, uno script personale non gestirebbe tutti i casi di errore 

è possibile clonare un repository locale, e copia il repository senza considerare index e WorkingDirectory

### il comando git "da grandi poteri derivano grandi responsabilità" filter-branch ###

permette di riscrivere la storia di un branch aumentando anche i dettagli visibili della stessa
nonostante il comando sia pericoloso.
I cambiamenti non sono del tutto definitivi ed esiste un modo per ricuperare i precedenti commit tramite il riferimento "original". Questo è un puntatore a un branch appena creato che corrisponde a una copia della storia prima delle modifiche

### git add -p ###

permette di modificare c' ho che voglio aggiungere all'index riga per riga


## git Hooks ##

gli hooks sono degli script che racchiudono delle operazioni utili a risolvere problemi di molto comuni.
Si dividono in due tipi:

- CLIENT side
- SERVER side

##git flow ##

- tipi di branch
- nuove operazioni 

### feature ###

branch automatico generalmente locale che permette di di sviluppare una caratteristica del progetto localmente senza influenze dal progetto globale

- git flow feature start *myfeature*
- git flow feature end *myfeature*

### release ###

crea un branch in cui non vengono mai implementate nuove feature ma solo bugfix delle feature gia implementate.
é però possibile pullare i bugfix della release nel development 

- 

### hotfix ###

branch che viene aperto sul master per correggere un errore dal punto in cui si è manifestato senza tutti gli sviluppi successivi per poi essere mergiato al master


