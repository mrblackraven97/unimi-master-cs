# Bloomberg tech

## Come l'agile influisce sullo sviluppo

...

### Ruoli chiave

...

### Meeting giornalieri

Uno sprint a Bloomberg dura due settimane; per favorire l'intercomunicazione nel team ci sono diversi meeting, fino a 6 diversi tipi:

- Stand-up meeting
  
  - Meeting giornaliero veloce svolto in mattinata prima di lavorare: ognuno esprime cosa ha fatto ieri, cosa farà oggi e quali problemi lo affliggono per cui ha bisogno di una mano

- Product grooming
  
  - Meeting svolto il primo e il terzo giorno della seconda settimana per ...

- KTLO grooming
  
  - Meeting svolto il secondo giorno della seconda settimana per ...

- Sprint review
  
  - Meeting svolto l'ultimo giorno dello sprint per finalizzarlo e fare un recap di ciò che è stato svolto sul prodotto con il product owner

- Retrospective
  
  - Meeting svolto l'ultimo giorno dello sprint per riflettere internamente con il team su cosa è andato bene e cosa è stato difficoltoso nello sprint

- Sprint planning
  
  - Meeting svolto per decidere e organizzare cosa fare nel prossimo sprint (aka prossime due settimane)

Da notare l'ultimo giorno dello sprint: l'innovation friday. Durante questa giornata non si lavora per lo sprint ma si lavora per testare, giocare e imparare nuove tecnologie che favoriscano il team o rimodernare, fixare o ottimizzare il software già prodotto, magari scritto velocemente per lo sprint.
