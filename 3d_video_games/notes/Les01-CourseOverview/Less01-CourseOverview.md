# 3D Video Games

Videogames were a great thrust to the development of computer science technologies: thanks to them, GPUs, pixels, colored monitors and such were created to respond to the videogames' needs.

This course has the goal to teach how intricate the system of videogames developing is.

Course plan :

- Lez1 - **Intro**
- Lez2 -  **Math for 3D games** 
  - Points, vectors, versors 
  - Spatial transformation 
  - Quaternions
- Lez3 - **Scene Graph**
- Lez4 - **Game 3D Physics**
  - Dynamic
  - Collisions
- Lez5 - **Game particle systems**
- Lez6 - **Game 3D Models**
- Lez7 - **Game Textures**
- Lez8 - **Game 3D Animations**
- Lez10 - **Networking for 3D Games**
  - Link with Online Game Design
- Lez11 - **Artificial Intelligence**
  - Link with AI for Videogames
- Lez12 - **Game 3d Rendering Techniques**

## How to categorize videogames

It's difficult to categorize videogames since there are a lot of variables in play: we could consider 

- **Developers** (Publisher vs Indie)
  - How much was it spent to create a game? Was it publisher backed (AAA games) or self-financed (indies) ? 
  - Publishers have the job of funding development, distributing and packaging the copies, marketing and localization.
  - Indies: mostly self financed, with less money, but with more freedom of movement and usually novelty in gameplay. Usually needs additional crowd funding for high quality outcome.
  - AAA Games: backed by big publishers,with lots of money, an expected high quality (more constraints) and large devs teams.
- **Players** (Casual vs Hardcore)
  - Which players the game has as a target? Casual gaming (Candy Crush, Solitaire, ...) or "hard-core" and dedicated gaming (CS:GO, LOL, Starcraft ...) ?
- **Gameplay**
  - Which gameplay it offers? Action, puzzle, platform, adventure, strategy, rpg, and so on.

Since we're going to study how 3D games work, let's see in detail what differentiates them from their older 2D counterpart.

## 2D games

2D games usually are made of a bunch of *sprites* on a *tilemap*, made of tiles from a *tileset*.

![](img/2dsprites.png)

These three objects are the **assets** of a 2D games, which are the skeleton of the game rendering.

The "**blit**" (bit blit, aka bit block transfer) is an hardware technique which copies a batch of sprites from the GPU memory and applies it directly on the screen applying some basic transformations like rotation, flipping, scaling, transparency and recoloring. 

Given the low memory of the original 80s, 90s consoles one couldn't imagine to store all the sprites in the memory because all wouldn't fit: with blit I ***don't have to store the same sprite in different variations***, saving precious memory

For example, I don't need to store the turtle normal sprite and the turtle upside down sprite; another example is that if I need to represent a more challenging enemy, like an ogre, I can use the same ogre sprite but changing its color. The blitting technique also helped develop GUI windows in OSes.

The same problem arises when storing levels in memory: it isn't possible to do this in a naive way, so tilemaps/tilesets were born. A **tileset** is a collection of cells (tiles) each containing a 2D bitmap image, while a **tilemap** is simply a collection of indexes pointing to a cell (tile) of the tileset.

This way, I only have to hold in memory these two objects (which aren't that heavy) to generate whole levels, taking advantage also of blitting filling the screen tile per tile instead of pixel per pixel.

Creating videogames was, and is, creating ad-hoc solutions and technologies in order to close the gap from "what the machine can do" to "what we want the machine could do"

## 3D games

3D games instead are made from *3D Models* animated with *3D animation techniques* placed in *3D Scenes* displayed with *3D rendering techniques*.

![](img/3dmodels.png)

### Game engines and re-usability

In a 3D videogame there are tasks that need to be faced: 

- 3D Rendering
- 3D Physics
- 3D sound and networking
- Input management
- Program structure (event loop, ex. `update()` loop)
- Memory management
- Artificial Intelligence

Since these are common between all 3D games, the common practice is to reuse code relative to those tasks that has already been developed, by you or other people.

This way it's easier to create games, instead of starting from scratch: we have a solid base to start from. We have less freedom of movement than starting from zero, but usually the constraints are minimal.

For this reason game engines and tools flourished in this ambient.

![](img/engine.png)

As we can see, the less a tool or engine is specialized, the bigger the effort is to create a game with it (peaking with naked C++ aka almost from scratch). There are two engines though that offer great capabilities and flexibility with little effort needed: these are Unity and Unreal.

### Development team

A 3D game team is generally made by three kinds of persons

- **designers**
  - Those who design the game logic in its whole (like a movie director)
- **digital artists**
  - Those who create the artistic assets used by the techs to develop the game such as
    - 3D data (models, textures, shaders, ...)
    - Audio
    - Video (in decadimento, perchè i giocatori preferiscono l'interattività rispetto alle cutscene)
    - 2D art (GUI elements, fonts, screen splashes, ...)
    - Text (dialogues, messages, ...)
    - Other stuff (stats, balancing, ...)

- **techs**
  - Those who have a CS background that can program and implement game logic.
  - The tech staff (us) are the ones that ***develop, customize and integrates these assets*** in the game engine (which is used by the game) 
  - The tech staff also has to develop ***game tools***, which are used by digital artists to create the aforementioned assets

### Development process

![](img/develop.png)

As we can see once the *techs* developed the game tools, the *digital artists* can work to create the game assets. Once these are done, the *techs* can use them in the game engine to effectively implement the game logic and create the game.

At the end of the development all game tools developed in the process are rendered public for eventual *modders* to create mods for the game.

![](img/mods.png)

But to do that we have to keep in mind how "hard wired" is a given-video game feature. That means, is it easy or doable to change that feature in a way to fit our (modder) needs? 

![](img/hardwired.png)

So, modders will find it easy to mod a game if the most of the features are less hardwired (which means in assets).

For example, lets say a modder wants to change the damage of a machine-gun or the shooting animation effect: if the feature is hardwired in an asset, it's easy since we only need to swap that asset; however, if it's hardwired in the code, it's difficult to impossible to mod it, since we will need to change the code directly to edit the feature. It would be even worse if it was hardwired in the game-engine or in the hardware.

### Procedural-generated vs asset

Usually, there are several reasons for which choosing to make something in an asset (less hardwired) or in the code (more hardwired). One of these reasons is the procedural generation for a feature. 

For the best example, lets consider the death animation of a character in a game: we could achieve these in generally two ways

- **Asset-stored**

  - The animation is built during the development of the game, "scripted" in technical slang

  ![](img/deathasset.png)

  - **Quality** - The quality is usually good, since it's made by an artist
  - **Control** - Since it is under control of the artist, he cannot prevent some inconsistencies such as dying on the stairs keep some floating bodies (like in the picture)
  - **Real-time efficient** - The asset just need to be loaded and executed, so it's ***less computation*** for the engine, but ***more space*** is required to hold the asset
  - **Moddable** - Asset-stored means less hardwired, so it's easy to maintain (for devs) and customize (for modders)

  

- **Procedural generated**

  - The animation is express made during the game execution, since it's dynamically computed by a procedure

  ![](img/deathproced.png)

  - **Re-playability** - The quality is not as good as artist-made, but offers different variations depending on the context
  - **Flexibility** - It adapts to the in-game context: for example uses the engine physics to let the dead body fall and have the classical and "realistic" rag-doll effect
  - **Space efficient** - There is no asset to load, so we can ***save space*** from memory (RAM, HDD, ...), however computing the death procedure ***costs on the CPU***
  - **Scalable** - The flexibility offered makes it also scalable and reusable: there is no need to create more assets (or death animations in this case), since it is all calculated in real-time during the game. However, this makes it harder for modders to enhance the game post-release.

### Baking 

The "baking" techniques is the opposite of the proceduralization: it **stores in an asset the result of a computation** for later use (usually an heavy one). 

This way we gain some CPU/GPU time on the user machine by just loading the baked computation, at the expense of flexibility and space on RAM, HDD. 

Baking is different from ***caching***, since the latter only keeps it for a short amount of time, and not indefinitely as an asset.