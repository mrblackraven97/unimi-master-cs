# What is a Game Engine?

Unity is not just a game engine, it is much more, there are many things aggregated besides of a game engine, but there are more feature to allow a user to create a game.

Back in the days (DOOM 1994) there was a strict separation between core software components, a separation between software and data assets, with a strong code reusability enforced during development. But the word "engine" was not there yet.

Other games have been designed with a modular architecture allowing modding and focusing on code reuse (Quake, Unreal Tournament, ...):

- Scripting language (such as Quake C) started to be part of the distribution;
- The game engine is now a standalone product (and a profitable one!);
  - *Example*: Quake III can be considered as a paid for demo of the actual product, the quake engine.
- Customer are no longer players but developers.



## A Game Engine

A really technical description of a game is:

> A real-time interactive agent-based computer simulation

And a game engine is a software made to implement such a system. More in detail:

- **Real-time (and interactive)** - Must respond to player input in a timely-bound manner;
- **Agent-based** - Independent entities (agents) live and interact with each other within the engine;
- **Simulation** - It is capable to describe a model representation of a virtual world. It must be a *mathematical* description.

![](img/in_unity.png)



## Building a Game Engine

A game engine is made of many component, starting from the inner and deepest one:

- **Core** - For resource management and coordination. What is making your game "run" is a very small piece of code in charge to manage the basic simulation.
- **Evolution Rules** - The core does not know how to make the system evolve, it just applies "evolution rules" provided with the game. Core is focused on performances, it must be able to apply any kind of rule.
- **Runtime** - Core and basic evolution rules (physics and lighting) are bundled and hidden from user, like in a black box. This black box has a hidden content for commercial purposes and is referenced as *"The Runtime Component"*.
- **(Easy to use) Interface** - There is no need to know the content of the black box to create a game, all we need to know is how to use it. Possibly by means of a convenient user interface.

A game engine is made of two main parts:

- **Tool Set**

  - To compile software to work within the game engine;
  - To help you describe rules;
  - To manage assets;
  - To create content.

  The GUI is usually the front-end to the toolset

- **Runtime** 

  - A library/middleware/sandbox/virtual machine;
  - This will run your rules on your assets;
  - Must be distributed with the game.

You use the GUI to put "stuff" (assets) inside the black box and then ask it to create an executable file.

**N.B.** A (large) piece of the core will be inside each executable. This is because the game must evolve also outside the black box. This is why we need redistributable licenses.

All we need to understand is how the black box is working. As a matter of fact, it is not required for a game engine to have a GUI (ex. Source from Valve).



## A Black Box for Rules and Assets

A game engine is a container for **rules**:

- You **explain** how the world is evolving;
- You **do not** create the code to make it evolve.

The black box will follow the rules and apply them on the assets you provided. At every step, these rules will change the box status and its assets, making your world magically evolve.

### What is an Asset?

An asset is whatever you may think about to show, listen, or feel while playing:

- Texture;
- 3D meshes;
- Material definition;
- Particles;
- Visual effects (shaders);
- Music;
- Scraps of code.

### What is a Rule?

A rule is the definition of a behavior you attach o an asset in order to define:

1. How to interact with the user (ex. reacting to controls);
2. How to interact with the environment (ex. falling and casting shadows);
3. How to interact with other assets (ex. collisions).

Creating a believable NPC means creating the "right" rules based on the surrounding context.

### Script and Rules

Of course, the easiest way (for a computer scientist) to describe a rule is by means of a scrap of code... but:

- A script is technically and asset;
- A script turns into a rule when is compiled and run inside of another asset.

There are two kinds of rules:

- **Built-in rules**, wired in the black box for everyday swill-army knife use;
- **Rules provided by the user**. This is what is making your game unique, you will call then "gameplay programming".

There is no actual difference between the two kinds of rules. It is possible to change built-in rules at will (ex. switch to Havok or Bullet physic engines).



## Runtime Architecture

![](img/runtime_arch.png)

Specialized developers are required for engine runtime development. Very difficult to debut, but luckily you do not need to do that.

A game engine is:

- Huge;
- Complex;
- Made of layers (like many other complex softwares, such as O.S. kernels).

Let's try to analyze this runtime architecture. From the bottom layer we have:

- **Hardware** - The base layer of everything, we can't go anywhere without it;
- **Device Drivers** - As in any operating system, device drivers will provide uniform and manageable interfaces to device controllers;
- **Operating System** - The OS will allow your application (game) to use the hardware via the device drivers (consoles have their own OS too);
- **Third Party SDKs and Middlewares** - The OS is hosting third parties' SDK and middleware to let you use the OS in a way which us convenient for a game, this is usually different from the way ordinary applications are using the same OS;
- **Data Structures and Logics** - The middleware is implementing data structures and logics (extremely optimized for the current platform) useful for any kind of game. Or, at least, for a very large variety of games;
- **Platform Independent Layer** - The middleware (and its component) are architecture-specific, we do not want to ask the developer to create code to be used only once on only one architecture, so we must provide an additional middleware to create a platform independent access to the engine inner functionalities;
  - In the platform independent layer all platform dependent data structures and logics are replicated to offer a set of standard and stable APIs to developers.
- **Core System** - Implementing the core in a lower level could improve performances, nevertheless, will also require more effort to keep all the platforms in sync and introduce additional platform specific bugs and, at last, we can build a platform independent core system on top it;
- **Resource Manager** - The core uses resources to make the system evolve and, as said, we use the core by providing it resources (such as code). A resource manager is in charge to coordinate resources and feed them to the core and to guarantees consistency for their data structures.
- **Game Virtual Machine** - Everything will provide a virtual execution environment (a virtual machine) where your game will run and evolve.
- **Toolset** - Inside the virtual machine, all tools used to manage the game engine are implemented. This is the toolset of our game engine. From another perspective, it is also the lid of our black box.
- **Game-specific Subsystem** - We implement the game from outside the black box using the toolset in the top layer of the architecture.

What about rules?

![](img/runtime_arch2.png)

.. and how are they used?

![](img/runtime_arch3.png)



## Graphics Conversion Pipeline

Let's look at what happens to a graphic asset from its creation to its execution on hardware:

![](img/graphics_pipeline.png)

There is no strict definition of engine modules:

- The rendered might know how to render a full fledge ogre or may just provide basic functionalities;
- The network manager may implement SOAP or provide just sockets.