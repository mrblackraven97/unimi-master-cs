# From Map to Graph

A simple problem with a complex solution

![](img/from_to.png)



## Division Schemes

A division scheme is a representation of a level where you split the area in linked regions. Each division scheme is defined by:

- Quantization and localization;
- Generation;
- Validity.

### Quantization and Localization

We need to define a way to convert level locations into graph nodes and vice-versa.

- Quantization = Linking a position to a graph node.
- Localization = Linking a node to a position.

### Generation

This is the policy (algorithm) we use to split a map assign its piece to graph nodes.

We have two options:

1. Manually
   - The old school approach, not discussing this.
2. Algorithmically
   - Tile graphs;
   - Dirichlet domains;
   - Points of visibility;
   - Navigation meshes.

### Validity

Validity is about asking if quantization and generation are dependable for planning.

If a character needs to move from node X to node Y, will it always be able to carry out that movement? If the quantized regions around X or Y are not allowing this, then the pathfinder is creating useless plans.

Taking into consideration any couple of connected regions, a division scheme is valid if all point of one region can be reached from any point of the other region.

![](img/invalid.png)



## Tile Graphs

![](img/tiles.png)

Tiles for tile graphs can be any regular size (square, hexagons, rectangles, ...) and, technically, they can be also mixed (not all tiles have to be the same).

In the following, we are going to use squares, just because they are both convenient to manage and quite common in games. 

**N.B.** In your games, never underestimate the choice of your tiles.

For square tile graphs:

- Quantization

  - Extract a tile (node) number/coordinate from the position. A tile (node) is associated to a "`tileSize x tileSize`" area. The tiles are set as in a matrix covering all the space and (`xTile`, `yTile`) is the coordinate of the target tile.

    ```c#
    float xPosition, yPosition;
    float tileSize;
    
    int xTile = (int)Math.Floor(xPosition/tileSize);
    int yTile = (int)Math.Floor(yPosition/tileSize);
    ```

- Localization

  - The reverse calculation returning a location in the tile (the center?). Reverse calculation but the geometric position is associated to the center of the tile (hence, the +0.5 to the position).

    ```c#
    int xTile, yTile;
    float tileSize;
    
    float xPosition = (xTile + 0.5f) * tileSize;
    float yPosition = (yTile + 0.5f) * tileSize;
    ```

It's more convenient to build it around `Node` in the `Graph` class.

```c#
protected float tileSize;
protected Node[,] matrix;
// A Dictionary associates each tiles to its coordinates
protected Dictionary<Node, float[]> map;

// Quantization
// See accessors in C# for details about how these works
public Node this [cloatx, float y] {
    get {
        return matrix[
            		  (int)Math.Floor(x/tileSize),
        			  (int)Math.Floor(y/tileSize) 
        			  ];	
    }
}

// Localization
public float[] this [Node n] {
    get {
        if (!map.ContainsKey(n))
            return null;
    }
}
```



## Tile Graph Generation

Tiles can be generated at runtime when needed, also connections between tiles, if you do i completely random.

If you set a `y` (height in Unity) dimension, the steepness should contribute to the distance (weight) calculation

```c#
public class TileGraph {
    protected float tileSize;
    protected Node[,] matrix;
    protected Dictionary<Node, float[]> map;
    
    public TileGraph(float x, float y, float ts) {
        tilesize = ts;
        matrix = new NOde [
            (int)Math.Floor(x/tileSize) + 1,
            (int)Math.FLoor(y/tileSize) + 1
        ];
        map = new Dictionary<Node, float[]>();
        for(int i = 0; i< matrix.GetLenght(0); i += 1) {
            for (int j = 0; j< matrix.GetLenght(1); j += 1) {
                Node n = new Node ("" + i + "," + j);
                matrix[i, j] = n;
                map[n] = new float[] {
                    (x+0.5f)*tileSize, (y+0.5f)*tileSize
                };
            }
        }
    }
}
```



The graphs validity depends on your level design. If a tile is either completely empty or full, then the graph is guaranteed to be valid. If a tile may be partially blocked, it will depend on the shape of the blockage and your movement system

![](img/validity.png)



## Dirichlet Domains

Once we can identify a set of characteristic points on the map, we surround them with an area. This area will be made of all locations that are closer to that point than to any other

![](img/dirich.png)

Connection between Dirichlet domains can be solved algorithmically using **Delaunay triangulation** to find bordering domains:

- Quite complicated;

- Each domain can be weighted to tune cone's falloff. Cone fallof is causing problem with overlapping cones;

  ![](img/overlap_cones.png)

- This approach will not take obstacles into account;

- Usually, we prefer to delegate this to the level designer.

1. Quantization
   - We need to find the closest characteristic point on the map;
   - Complexity will be O(n);
   - We usually want to partition the map so that we only perform calculation for nearby points.
2. Localization
   - Given a domain, we return its characteristic point.

No panic, we are not going to calculate any Dirichlet or Voronoy diagrams (today).

There is **no formal way to demonstrate validity** because:

- Domains will form an intricate tasselation;
- There is no formal way to demonstrate that the line connecting two characteristic points belong to two connected domains is not passing through a third domain.

![](img/dirich2.png)

Delegating points selection to the level designer might solve the issue:

- Obstacles can have their own domain;
- The steering behaviour of your NPC can help you.



## Points of Visibility

Any optimal path (in a 2D environment) will have inflection points. In these inflection points, movement direction will change.

Inflection points are located at convex vertices in the environment. Avatar shape becomes a variable here.

Since inflection points are part of the shortest path, we can use them to build our navigation graph. We can use all convex point to build a level graph... but they are way too many! we usually need to simplify the structure (take them from colliders only, but then you must test your level for wall-walking) or pick them by hand (once again, the level designer must do it for you).

To understand connections, just use a ray cast between points:

- If the ray can hit the target point without bouncing in another object (the two points are in line of sight), you have a connection;
- Hence, the name.

Points of visibility can be used as characteristic points to build Dirichlet Domains.



## Walking the Path (Smoothing)

Walking straight from a tile center to another tile center may result in a very unrealistic movement.

Identify intermediate points and reducing changes for movement direction will help increase the realism. A steering behaviour may also be your friend here.

A smoothing algorithm:

1. Find a path using Dijkstra or A*;
2. Start from the first node;
3. Select the next node and perform a ray cast from current position to its position;
4. If the destination is not visible, then move to the last visible position;
5. If the destination is visible, and in the last position, move to destination and terminates.
6. Go to step 3

This can be done in-line or calculated before movement to better smoothed through a steering behaviour.

**N.B.** This algorithms take division scheme validity for granted.



# Navigation Meshes

Navigation meshes are a widely used methodology and it is kind of cheating for you, because Unity will take care of everything.

We already asked our level designer for help, the game designer must describe regions and how they are connected anyway (a lengthy process!) but the game itself is made of polygons

Let's leverage on the graphical structure as the foundation of our pathfinding representation.

A **mesh** is a data structure used to describe shapes inside a game engine. In Unity, `Mesh` is a class made (mainly) by:

- A list of vertices in space (an array of `Vector3` objects);
- A sequence of triangles (an array of integers where every element is and index to use in the array of vertices).

We can use floor polygons to build a graph, therefore, each node has at most N connections where N is the number of sides of each polygon (triangles -> N = 3). Optimizations can be performed starting from this assumption.

![](img/navmesh.png)

Quantization:

- Each position is associated to the polygon containing it.
  - We could end up searching a huge number of polygons;
  - It is possible to make some assumptions to improve performances
- Uses the coherence assumption
  - Like the "principle of locality";
  - If an NPC is moving, most probably it will move in a connected polygon. So, start checking there first.
- May be problematic with falls and jumps

Localization:

- Any point in the polygon will do, the geometric center of a triangle is not a bad choice.
  - Noob tip: To obtain the center of a triangle we can just average the position of its vertices.

### The Jump Problem

![](img/jump_problem.png)

 It's hard for A* to calculate the path if a character is flying (or jumping), a solution could be to use gravity as a projection, but wrong quantization will produce wrong paths.

### Validity

May be problematic to evaluate because not all points in a polygon can move to any point of a connected polygon.

Not all floor can be used depending on context (ex. The space under a table).

Size of NPC gets difficult to compute. Large NPCs must "stay far away" from walls.



## Navigation Meshes in Unity

1. Open the dedicated editor window (window -> AI -> Navigation)

   - A new tab will pop up in the inspector panel.

2. Select all object that you want to be part of the navmesh and set them as "Navigation static"

   - Try this on scene "Empty Scene" inside the Pathfinding/NavMeshes folder

   **N.B.** You must select the game objects holding the `MeshRenderer` components. Selecting the platform is not enough

3. Adjust agents' movement parameters

4. "Bake" to create a navigation mesh asset

Baking will create a new asset that:

1. Will be created in a subfolder with the same name as the scene;
2. Can be moved in a more convenient location;
3. Is linked to specific gameobjects in your scene.

But this is not enough!

The navmesh alone is **just a data structure**, you need to create an agent to walk it and make some sense out of your scene.

Create a placeholder for your NPC and put inside a NavMeshAgent component (yes, it is this easy)

![](img/agent_nav.png)

Then, give directions to the agent.

```C#
// This will make sure we have a NavMeshAgent component available in our gameobject
[RequiredComponent(typeof(NavMeshAgent))]
public class GoSomeWhere : MonoBehaviour {
    public Transform destination;
    
    void Start() {
        // The only thing we must do is setting a destination to your NavMeshAgent
        // and enjoy the show
        GetComponent<NavMeshAgent>().destination = destination.position;
    }
}
```

Of course if we run too fast, we are going to bump into obstacles... but there is no `rigidbody` component, we have no evolution rule providing "bumping", we did not put any wall there, what the hell is the NPC bumping into?

`NavMeshAgent` will force a `Rigidbody` behaviour on your agent, not 100% real for what i am concerned, nevertheless it will "sit" on the navigation mesh and push other rigidbodies and navmeshes agents.

Invisible walls will be inherited from the navmesh boundaries.

Changing the environment on the fly is also supported. Bake the navmesh as no "navigation static" to do so.

You are in charge to tune the steering behaviour with respect to your level outline. The first turn in the clip is a clear example for that, because there is no wall to bump into!



## A Chasing Behaviour

In order to make a simple chase behaviour, you just need to:

1. Put a player controller to move the target;
2. Re-calculate destination periodically on the agent.

And that's it!

Player controller generic script:

```c#
// To make sure there is a Rigidbody component, if not, add one
[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour {
    [Range(0.0f, 20.0f)] public float movementSpeed = 4f;
    [Range(0.0f, 100.0f)] public float rotationSensitivity = 90f;
    
    void Start() {
        ;
    }
    
    // Change position and/or orientation based on input (arrows from keyboard)
    // or thumbsticks from gamepad. Unity will do the mapping for us.
    void Update() {
        Rigidbody rb = GetComponent<Rigidbody>();
        // Gas and brake are converted into a translation forward/backward
    	rb.MovePosition(transform.position + transform.forward * movementSpeed *
                       (Input.GetAxis("Vertical") * Time.deltaTime));
        // Steering is translated into a rotation
        rb.MoveRotation(Quaternion.Euler(0.0f, rotationSensitivity *
                         (Input.GetAxis("Horizontal") * Time.deltaTime), 0.0f) *
                         transform.rotation);
    }
}
```

Chase script:

```c#
[RequireComponent(typeof(NavMeshAgent))]
public class ChaseSomething : MonoBehaviour {
    public Transform destination;
    public float resampleTime = 5f;
    
    void Start() {
        StartCoroutine(GoChasing());
    }
    
    private IEnumerator GoChasing() {
        while(true) {
            // Set the destination every resampleTime second
            GetComponent<NavMeshAgent>().destination = destination.position;
            yield return new WaitForSeconds(resampleTime);
        }
    }
}
```



## FAQ

Why my view is different?

- You are looking into the game window. I took the recording from the editor window.

Why blue cube is not moving?

- Because you switched to editor window after starting the simulation. Unity is not collecting inputs if the game panel. To solve the situation, move the game tab in another point of the interface so that it is not sharing the same panel as the editor.

Why the camera is chasing me?

- The project is in third person perspective. The camera is attached to the player (see the object hierarchy).

Why the agent is stopping in some points?

- Because the navmesh agent destination is set every 5 seconds. When the last sampled position is reached it will wait for the next timeslot to move again. To solve this, just reduce the value of `ResampleTime` in the agent inspector.

Why did i fall out of the platform?

- Because there are no walls.

Why the agent stopped when i fell?

- Because you fell vertically, and it stopped at the closest point to your planar position.



## Chasing with Line of Sight

The agent has a concept of "last known position" for the chase.

The target position will get updated only if the chase is in line of sight, this can be done periodically as well as when reaching the destination, this for you to decide.

```c#
private IEnumerator GoChasing() {
    while(true) {
        Vector3 ray = destination.position - transform.position;
        RaycastHit hit;
        // This if will be verified when the ray, applied from the agent position
        // will hit something
        if (Physics.Raycast(transform.position, ray, out hit)) {
            // This iff will be verified when the ray hit the destination instead
            // of another object in the middle, there is line of sight between
            // agent and player
            if (hit.transform == destination) {
                GetCommponent<NavMeshAgent>().destination = destination.position;
            }
        }
       	yield return new WaitForSeconds(resampleTime);
    }
}
```

The main purpose of this code is to define a ray from agent (chaser) to player (chased). You can try this yourself, running the "Chase with LOS scene" (there will be walls).















