# Pathfinding Basics

Pathfinding is **all over videogames**:

- It is about having your NPC strolling around;
- It is extremely important to do it the right way!

Nevertheless, pathfinding is a special case of planning:

- In video games it must be believable, it does not need to be perfect;
- Still the "Game AI" vs "Classical AI" thing, remember?

By looking into the book, pathfinding is between *movement* and *decision making*

![](img/book_map.png)



## Pathfinding

Pathfinding is a very easy problem:

1. You make a **representation** of your space (a graph);
2. You run an algorithm to understand "the best" way to go from point A to point B.

Be very aware that **representation means graph**, this is not a game data structure and that you have to define what "best" means.

![](img/graph.png)



## Graph

A graph is a formal mathematical structure defined as follows: `G = (V, E)`

Where:

- `V` is a set of vertices (or nodes);
- `E` is a set of edges (links between nodes).

Every edge induces a symmetric relation denoted as `~` on V. `~` is called **adjacency relation**. 

Also note the relation ` ∀ {x, y} ∈ V**2, x ~ y → x and y are adjacent to one another`.

If two nodes, x and y in the graph are adjacent, then there is an edge linking them together.

There are different kind of graphs:

- Simple/Undirect
  - Those previously described.
- Direct
  - The set `{x, y}` is ordered (only one direction is allowed);
  - We are not excluding `{x, y}` and `{y, x}` to exist at the same time;
  - We draw the graph with arrows.
- Oriented
  - Same as directed, but only one direction is allowed between `{x, y}` and `{y, x}`.
- Mixed
  - Some edges are directed and some are not.
- Multigraph
  - We have more edges connecting the same two nodes.
- Weighted
  - Every edges has associated a numerical value.



## Graphs in Games

How are graphs used in games?

- *Simple/Undirect* - For open worlds or labyrinths ("The Sims");
- *Direct* - Connections may be two-ways or one-way, like driving a car in a city ("Grand Theft Auto");
- *Oriented* - Connections are only one way, like in a rail shooter or an infinite runner ("Out Run");
- *Mixed* - When you move freely in a level but you have one-way to the next one, like in many dungeon crawler ("Transistor");
- *Multigraph* - Many way to cover a path, like when you can choose between stealth and assault approach ("Deus Ex");
- *Weighted* - Not all connections have the same importance/cost, like having different probabilities to get spotted depending on the path you take ("Counter Strike").

![](img/graph_games.png)

The AI it is going to need just the graph transposition of this map, it works on that.

![](img/graph_games2.png)



## Graph Implementation

To be ruled out from our implementation:

- Negative weight - Algorithms are not working well or are too complicated to be implemented with good performances;
- Undirected edges - Because we do have one-ways;

We are left with positive-weighted oriented graphs:

- Need undirected? Just use two edges;
- No use for weight? Just set everything to 0.



## Graphs In Unity (C#)

Let's start by defining the data structure for the graph. This code will be "generic" C#, there is no need to fiddle with the internal data structures of unity.

```c#
public class Node {
    public string description;
    
    public Node(string description) {
        this.description = description;
    }
}
```

All we care about a node right now is to give it a name in order to let the user tell it apart from the others.

```c#
// Transition between two nodes
public class Edge {
    public Node from;
    public Node to;
    public float weight;
    
    public Edge(Node from, Node to, float weight = 1f) {
        this.from = from;
        this.to = to;
        this.weight = weight;
    }
}
```

An edge goes from a node to another node with an optional weight (defaults to 1)

```c#
public class Graph {
    // Holds all edges going out from each node
    private Dictionary<Node, List<Edge>> data;
    
    public Graph() {
        data = new Dictionary<Node, List<Edge>>();
    }
    
    public void AddEdge(Edge e) {
        AddNode(e.from);
        AddNode(e.to);
        if(!data[e.from].Contains(e))
            data[e.from].Add(e);
    }
    
    // Used only by AddEdge
    public void AddNode(Node n) {
        if(!data.ContainsKey(n))
            data.Add(n, new List<Edge>());
    }
    
    // Returns the list of edges exiting from a node
    public Edge[] getConnections(Node n) {
        if(!data.COntainsKey(n)) 
            return new Edge[0];
        return data[n].ToArray();
    }
    
    public Node[] getNodes() {
        return data.Keys.ToArray();
    }
}
```

Note that:

- A graph is a collection (a `Dictionary`) of nodes linking to each node the list of outgoing edges;
- Adding an edge means adding the linked nodes and then assigning the link inside the dictionary (if not already there);
- Adding a node will just create a new entry in the dictionary;
- To explore the graph it is useful to get all outgoing edges from a given node.



## Navigate the Graph: Dijkstra

The Dijkstra's algorithm is not a pathfinder, just a shortest path finder inside of a graph, but if we associated the map to a graph it becomes the same thing.

Actually Dijkstra's algorithm calculates the shortest path from a starting point to anywhere in the graph, so we have a superset. This means that resources will be wasted and it is also quite inefficient.

**N.B.** A* is an improvement of Dijkstra's.

![](img/dijkstra.png)

Input:

- A graph;
- A start node;
- A goal node.

Output:

- A list of edges to go from start to goal with minimal weight, considering the total weight as the sum of the weight of all edges.

It's also important to state that the result may not be unique because there might be many ways with the same weight to go from start to goal (as in previous picture).

The classical algorithm works like this:

1. We define two set of nodes: visited and unvisited.
   - Set the unvisited set as the complete list of nodes and the visited set as empty.
2. For every node we define a **tentative distance** (from the start) value and a predecessor node.
   - Set 0 to the start node and infinite for all other nodes.
3. Select a current visited node as the one in the unvisited set with the smallest tentative distance.
4. Process the current node:
   - Assign tentative distance and predecessor to all its neighbors only where the new tentative weight is lower than the current;
   - Neighbors’ value will be the sum of local tentative distance and the weight of the edge leading to the neighbor
5. Mark the current node as visited.
6. If the unvisited list is not empty go back to step 3.
7. Return the minimal path by backtracking from the goal node.

### Millington's Version

Instead of using visited and unvisited sets, the author defines three sets:

1. Unvisited nodes.
   - Unvisited nodes with infinite tentative distance.
2. Open Nodes.
   - Unvisited nodes with a non infinite distance.
   - These are the border of the visited nodes set.
3. Closed Nodes
   - Visited nodes

Just the same but avoids to deal with the “infinite” value. There are good (performance) reasons to do this.

What if we loop back to a visited/closed node with a shortest path? Dijkstra proved that it is NOT going to happen



## Dijkstra's Implementation

Let's define a class that is just a code container to run the Dijkstra algorithm inside Unity

```c#
public static class DijkstraSolver {
    // Two sets of nodes
    static List<Node> visited;
    static List<Node> unvisited;
    
    // Data structures to extend nodes
    private struct NodeExtension {
        public float distance;
        public Edge predecessor;
    }
    
    static Dictionary <Node, NodeExtension> status;
    
    public static Edge[] Solve(Graph g, Node start, Node goal) {
        // Setup sets
        visited = new LIst<Node>();
        unvisited = new LIst<Node>(g.getNodes());
        
        // Set all node tentative distance
        status = new Dictionary<Node, NodeExtension>();
        foreach (Node n in unvisited) {
            NodeExtension ne = new NodeExtension();
            ne.distance = (n == start ? 0f : float.MaxValue); // Infinite
            status[n] = ne;
        }
        
        // Iterate until all nodes are visited
        while(unvisited.Count > 0) {
            // Select next current node
            Node current = GetNextNode();
            // Assign weight and predecessor to all neighbors
            foreach(Edge e in g.getConnections(current)) {
                if (status[current].distance + e.weight < status[e.to].distance) {
                    NodeExtension ne = new NodeExtension();
                    ne.distance = status[current].distance + e.weight;
                    ne.predecessor = e;
                    status[e.to] = ne;
                }
            }
            // Mark current node as visited
            visited.Add(current);
            unvisited.Remove(current);
        }
        
        // Walk back and build the shortest path
        List<Edge> result = new List<Edge>();
        Node walker = goal;
        
        while (walker != start) {
            result.Add(status[walker].predecessor);
            walker = status[walker].predecessor.from;
        }
        result.Reverse();
        return result.ToArray();
    }
    
    // Iterate on the unvisited set and get the lowest weight
    private static Node GetNextNode() {
        Node candidate = null;
        float cDistance = float.MaxValue;
        foreach (Node n in unvisited) {
            if (candidate == null || cDistance > status[n].distance) {
                candidate = n;
                cDistance = status[n].distance;
            }
        }
        return candidate;
    }
}
```

We use a `struct` to "enrich" each node with information about its predecessor and its distance from the start node. Association between a node and its extension is performed via a dictionary.

Technically, storing the distance in the `struct` is redundant space-wise, we could re-compute the path each time, but doing that will not be a good performance-wise.

The `Solve` method is simply an implementation of the Dijkstra algorithm, but there is a potential bug: we are assuming that in no way there will be a possible path longer than `float.MaxValue`.

All nodes linked from node N will have tentative distance as the sum of the distance of N plus the weight of the linking edge. They will stay in the unvisited set but will get a tentative distance lower than `float.MaxValue`, thus being eligible as a possible next node.

To return the list of edges we walk back from the goal predecessor by predecessor and put everything in a list. The list is then reversed and converted into an array.

`GetNextNode` is a utility method to select the node with the lowest distance inside the unvisited set. In case of more nodes with the same distance, the first one is returned.



## From Theory to Practice

Let's create a random Manhattan-like maze and use Dijkstra's algorithm to find the shortest path between two opposite corners

![](img/manhattan.png)

To do this, we must first re-design the graph classes and make then aware they "live" inside unity.

We must link each node to a place on the or, at least, a `GameObject`.

```c#
using UnityEngine;

public class Node {
    public string description;
    public GameObject sceneObject;
    
    public Node(string description, GameObject o = null) {
        this.description = description;
        this.sceneObject = o;
    }
}
```

The "description" field could be replaced by the Unity `GameObject` name.

`sceneObject` is the reference object for this node. It is not required to have a physical object: if the object in question has only the transform component, then it is the equivalent of a coordinate in world space and can easily be a rally point.

The graph is random, **we cannot assume a solution exists**.

If there is no way to use an edge to expand the visited set, then `GenNextNode` will return a node with distance `float.MaxValue`.

Returning a zero-length array is a way to communicate that the goal is unreachable.

[...] 

