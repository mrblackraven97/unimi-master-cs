# Decision Making Basics

So far, we saw pathfinding... which is a kind of planning. We understand what to traverse in order to reach a destination, but **pathfinding is not a decision-making process**.

To decide to go somewhere is a completely different thing from planning how to go there. The actual decision process is about picking the destination.

![](img/reach_moon.png)

![](img/decision_process.png)

Decision making is about having a character to decide "what to do" and now "how to do it".

- Might be inter-character and intra-character;
- A possible action might be "planning";
- The ability to carry out the decision is given for granted.

![](img/npc_decisions.png)

Internal knowledge:

- Available resources;
- Own goals;
- Personal behavior;
- History.

External knowledge:

- Position of other NPCs;
- Level layout;
- Status of objects;
- Whatever is coming from sensors.

**N.B.** We know everything but it is up to the algorithm to decide what to use.

## Example: An FPS Bot

Internal knowledge:

- Health;
- Available weapons;
- Available ammunitions;
- Role in the team;
- Directions from the player.

External knowledge:

- Map layout;
- Status of obstacles;
- Weather conditions;
- Position of other NPCs and players.

Decision maker will take into account:

- Field of vision;
- Explored portion of map;
- Encountered NPCs.

Action request:

- Fire to kill;
- Fire for cover;
- Move to cover;
- Run away;
- Crawl;
- Generate aggro.

# Decision Trees

Decision trees are the simplest and most used approach to take decisions because they are:

- An easy data structure;
- Fast to walk;
- Not only for characters;
- Very popular for animation control.

The goal of decision trees is, given a set of knowledge, to select an action from a set of available actions:

- Mapping may be complex;
- The same action may be used for several (disjoint) set of inputs;
- Small changes in the input may be important;
- Some inputs may be more important than others.



## Trees' More in Detail

Decision trees' structure is made as follows:

- We have a set of decision points:
  - Usually, we are asked for a quite simple decision at each node;
  - Best practice suggest with two or three possible outcomes maximum.
- One decision point is the "beginning" of each decision (**root node**).
- All decision points are linked in an acyclic graph (a tree). This will guarantee termination and bound execution time.
- Leaves declare actions to perform.

Now lets see how trees work:

- Starting from the root node, we must select one of a set of outgoing options (links);
- Each decision is taken based on the character's knowledge;
- The algorithm continues until a leaf is reached;
- As soon as a leaf is reached, its action is performed.

![](img/decision_tree.png)

![](img/decision_tree2.png)



## Working on Complexity

Since our NPCs have to reach quickly to our game world, we have to be sure our implementation is light enough.

![](img/decision_tree3.png)

Features of the execution:

1. Takes no memory because each node is independent from the previous one;
2. We can assume a decision is taking a constant amount of time (if we have all simple and/or binary decision this is a safe assumption).

Time is linear with number of visited nodes (thanks to point 2 above).

Complexity is the same as a tree visit algorithm:

- If the linear time condition above holds;
- Optimal case is a balanced z-branched tree *O(log<sub>2</sub> n),* where *n* is the number of nodes in the tree.

Mind the bottleneck! Some decisions may take sensibly more time if you need data from outside the game (ex computing a complex problem or using a hardware sensor such as GPS on a phone).

Balancing trees is a serious issue to be careful.

![](img/balance_trees.png)

![](img/balance_trees2.png)

Not all decisions are the same! The usual assumption when balancing a tree is that *all decisions have the same probability to be taken*, but this is WRONG!

- An action (leaf) is taken (reached) very often?    --->    Put it at the beginning of the queue.
- A decision (node) is creating a bottleneck (slow to compute)?    --->    Put it at the end of the queue.

![](img/balance_trees3.png)

![](img/balance_trees4.png)

To save space and avoid defining multiple times the same action we might define decision structures that are **not directed graphs**.

They are working, but are also:

1. More difficult to draw (crossing links);
2. More difficult to debug.

Personal suggestion: Always merge branches on leaves never do that on nodes.



## Mind the Loops

Inserting a loop is always a huge mistake:

- Termination is not guaranteed anymore;
- Processing time will neither be linear nor bounded.

![](img/loop.png)

In large (branch-merging) trees the loop may be long and very difficult to spot



## Randomness

Adding randomness may be helpful to spice up your game:

- Add unpredictability in NPCs behavior;
- Variate gameplay.
- Easy to implement.

![](img/randomness.png)

Computational warning: **random is CPU intensive** so:

- Do not roll at every frame;
- Do not roll when not needed.



## Implementing Trees

For **data representation**, trees use the same data types as the rest of the game, so this makes your AI quick and easy to code, but change in the underlying code (or logic) might break your AI (and these bugs are very difficult to squash).

For **code integration**, mileage varies with languages (in C++ use virtual functions, in Java/C# use runtime types). A tip is to make no distinction between decision and actions.



## The Sentinel (Example in Unity)

A sentinel is hidden and keeps looking around, if an intruder is within range it pops out and if the intruder is still there when the sentinel is out an alarm is activated on the next AI frame.

When the intruder leaves, the alarm is turned off and the sentinel goes back hiding.

(Go check the example in professor's git repository)

![](img/sentinel.png)

The detection delay is bounded by the AI frame resolution. The same problem we had when chasing the player on a navigation mesh.

To make the sentinel more responsive, just lower the duration of an AI frame, but the final result will be different (mind, this will also increase CPU usage).

Also here, game mechanics and user experience can be affected by a technical decision (pay a lot of attention on this!).

Bad news:

- Decision trees are not native in Unity;
- There are some (visual) plugins in the asset store.

Good news:

- Implementing a decision tree by yourself is very simple.



## A Library for Decision Tree Management

(The following code is available on Maggiorini's git repository)

![](img/code1.png)

![](img/code2.png)

![](img/code3.png)

![](img/code4.png)

Let's make it clear: This implementation sucks (big time!) because

1. No error checking;
2. No sanity in parameters;
3. No optimizations (uses recursion).

It just... works.

![](img/planning.png)

Actions are coded as follows:

![](img/actions.png)

Decisions are coded as follows:

![](img/decisions.png)

Beware of the tags! Because on one side they are a drop down, while on the other they are a string.

![](img/tags.png)

It is very easy to break your build if you are careless.

Code references:

![](img/code_references.png)

In `MonoBehaviour` the implementation becomes:

![](img/mono_impl.png)

![](img/mono_impl2.png)





















