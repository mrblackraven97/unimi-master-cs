# Finite State Machines

Very often, an NPC must switch between a set of given states (conditions) where each state is significative of a specific condition or activity.

Once in a state, the NPC keeps doing the same thing until an event occurs, then it will move to another state and iterate this process.

We could achieve the same result with decision trees, but state machines are just simpler... and much easier to design.

![](img/fms.png)

- Each agent is a given state;
- NPC available actions and behaviors are associated to each state;
- As long as the agent remains in that state, it will continue carrying out the same set of actions:
  - **N.B** "Action" might be "walk a specific decision tree".
- States are linked by transitions;
- Each transition has a set of associated conditions;
- If the conditions of a transition are met, then the NPC changes state to the transition's target state.



## Finite State Machine: Formal vs Game

A formal state machine is defined as:

- A finite set of states `Q`;
- A finite set of input symbols called *alphabet* (`Σ`);
- A transition function  (`δ : Q × Σ → Q`);
- An initial or start state  (`q0 ∈ Q`);
- A set of accepted states  (`F ⊆ Q`).

In games:

- A finite set of states `Q`;
- <s>A finite set of input symbols called *alphabet* (`Σ`)</s>;
  - FMSs scan the environment, you are not providing any input.
- <s>A transition function  (`δ : Q × Σ → Q`)</s>;
  - If there are no input symbols, we have no use for a transition function.
- Sensors to trigger transitions;
  - The transition function is substituted by sensors triggering transitions.
- An initial or start state  (`q0 ∈ Q`);
- <s>A set of accepted states  (`F ⊆ Q`)</s>.
  - Since we are not recognizing any input, we do not need to label any state as "final" (actually, many NPCs are designed to run forever).



## How To Use an FSM

We call the state machine's update function periodically. This update function will:

- Check to see if there are enabled conditions;
- Schedule the first match to fire transition;
- Perform actions (it is possible to have more than one);
- Fire the transition if one is triggered.

It must be also possible to assign (one or more) actions to:

- Entering a state (trigger);
- Firing a transition (trigger);
- Exiting a state (trigger);
- Staying in a state.

Implementing transitions:

![](img/impl_transition.png)

Implementing states:

![](img/impl_states.png)

Implementing an FSM:

![](img/impl_fms.png)



## FSM Complexity

Good news: it is always linear:

- We always start from the current state --> 0(1)
- We scan at most *n* outgoing link --> O(n)

Whatever we do with an FSM we can also do with decision trees, but FSMs are just faster because a DT must trace back the current state using internal and external knowledge. Instead an FSM has memory of the situation (the current state).



## Example: On/Off Sentinel

A sentinel is guarding a dark place.

If a friendly agent comes into range, the sentinel switches on the light.

When the friendly agent leaves, the light is turned off.

![](img/sent_fms.png)

![](img/sent_impl.png)

![](img/sent_fms2.png)

![](img/sent_impl2.png)



## Example: Dimming Sentinel

A sentinel is guarding a dark place.

If a friendly target comes into range, the sentinel switches on the light.

When the friendly target leaves, the light is dimmed off.

If another friendly target comes into range while dimming, the light is switched back on.

![](img/dimm_fms.png)

![](img/dimm_impl.png)

![](img/dimm_impl2.png)

![](img/dimm_impl3.png)



## Example: Alarm Sentinel

A sentinel is guarding a dark place.

If an enemy comes into range, the sentinel calls the alarm (the alarm is a red/yellow light alternation).

When the enemy leaves, the alarm is turned off.

### The Wrong Way

![](img/alarm_fms_wrong.png)

How do i switch between red and blue? Automatic transitions are fine but hints to redundancy.

```c#
public bool AlwaysSwith() {
    return true;
}
```

This can be used as a delegate, but everyone gets the feeling we are wasting resources.

Alarm semantic is wired inside the FSM schema, and is constrained by the AI framerate.

What if we change the frame time?

- Gaming experience will be different;
- Game mechanics might break.
  - Ex. If the player need to do something only when the light is red.

### A Better Option

![](img/alarm_fms_good.png)

![](img/alarm_impl.png)

![](img/alarm_impl2.png)



## Hierarchical FMSs

![](img/alarm2_fms.png)

A problem for FSM are alarm behaviours: If we have something important to do and we must suspend the current FSM.

We can oblige with "standard" FSM, but the solution will be clumsy and difficult to maintain:

- Putting in every state of a FSM a transition going to the state (or more than one state) managing the alarm;
- Doing that for every alarm;
- How to come back to the "right" interrupted state?

The right answer is a **Hierarchical FSM** (HFSM) where we put FSMs one inside another like a matryoshka.



## Example: Cleaning Robot

![](img/clean_fsm.png)

With power management...

![](img/clean_fsm2.png)

![](img/clean_fsm3.png)

For a possible implementation, we can instantiate two FSMs:

- A "main" FSM;
- A "cleaning" FSM.

In the main loop, we update only the main (outer) FSM. When we stay in the cleaning state, we can just update the cleaning FSM.

- Quick and dirty: `cleaningState.stayActions.Add(CleaningFSM,Update);`



## Priority

What if we have multiple transition ready to fire on different levels of the HFSM?

**Hierarchy is the priority**, the outer FSM is always more important. A higher-level transition will fire first even if there are lower-level condition verified.

You must plan your AI accordingly. Usually this means defining level of abstractions (or divide your AI in simple and reusable tasks).

Exiting from a composite state:

![](img/clean_fsm4.png)



## Full Implementation of HFSMs

This may get a bit messy.

Easy way: set a condition (a variable value?) to force transition at a different level in the next frame:

- Basically you put an option on the future. What if a condition with an even higher priority is verified in the next frame?
- If polling time is long, this is going to be a problem. Player will see a sluggish response.
- Formally not clean, because we are breaking the definition of FSM, which is not bound to temporal information.

Complex way:

- Extend the transition class to address a state in another FSM;
- Rewrite the way an FSM is updated to use the new transition.



## Combining FSM and DT

We can combine the two approaches by replacing transitions with a decision tree.

- This can be useful if conditions are very complex and partially overlapped.

The leaves of the tree are transitions to new states:

- Basically, we walk a DT to understand if there is a valid condition leading out of the current state.
- The leaf we reach is the state whose transition should fire.

![](img/fms_dt.png)













