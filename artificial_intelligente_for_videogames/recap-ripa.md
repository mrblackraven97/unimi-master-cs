# RIPA

# DESIGN GAMES AI

Groups

- Movement
- Decision Making
- Tactical/Strategic AI
- Infrastructure
  - Animation
  - Physics
  - Perception
  - Optimize CPU and memory usage

## Shooter Games

- Movement
  - Pathfinding + Animation Management
  - Same controls as player
  - Settings:
    - Indoor (pathfinding + repulsion)
    - Open areas (pathfinding + collision + formation motion system)
- Firing
  - Trade off accuracy and fun
- Decision Making
  - Finite state machines
  - Behaviour trees
- Perception
  - Putting a radius around each enemy
  - Sense management system
- Pathfinding
  - Waypoints
  - Nav mesh
  - Hierarchical pathfinding
- Tactical AI

## Shooter-Like Games

Platform and Adventure, differences:

- Movement
- Pathfinding
- Decision Making

## AI and Driving Games

Movement is crucial

- Urban
- Racing

Movement

- Racing lines
- AI dries the car
  - Steering behaviours
  - Chase the rabbit

## Driving-Like Games

Extreme sports games and futuristic racers

## AI and Real Time Strategy Games

- Pathfinding
- Group Movement
- Tactical and strategic AI
- Decision Making

Use of:

- Tiled grid
- Heightmaps
- Deformable terrain

Decison on 3 levels:

- Indivitdual
- Group
- Side

### Turn Based AI Games

- Timing
- Automate Boring tasks
- Assistive AI

## AI and Sport Games

Multi-level AI:

- Strategic
- Coordinated motion
- Single unit

Physic prediction

Playbooks

# Movement For Gaming

Define next position

Inputs:

- Chase
- Collisions
- Steering

Outputs

- Forces
- Velocity

Description of moving character:

- Position
- Orientation
- Velocity
- Rotation

static data -> steering -> dynamic data -> attuators -> forces

## Kinematic Algs

Static data, no physics

- Seek/Flee
  - Arriving circle + range of velocity
- Wandering

## Dynamic Movement

Add:

- Acceleration
- Rotation

Used for flocking behaviours

One algorithm for each behaviour:

- Seek/Flee + Arrive
- Align
- Velocity Match
- Repulsion

Delegated Behaviours:

- Face
- Looking where going
- Pursue/Evade
- Wander
- Path following (chase the rabbit)
- Repulsion
- Collision Avoidance
- Obstacle Avoidance (project volume)

Combine Behaviours:

- Blending
- Arbitration

Blending:

- Weighted
  - Ok in open environment
- Flocking

Weighted blending problems:

- Unstable Equilibria
- Constrained Environments
- Nearsightedness

Solution: use priorities

Cooperative arbitration:

- Context sensitive behaviours

Steering pipeline:

- Intermediate between blending and planning
- 1) Targeters - Find target
- 2) Decomposer - Subgoals
- 3) Check constraints (deadlock)
- 4) Actuator

Motor Control

- Physics simulation
- Output filtering
  - Filter SB on capabilities
  - Problem: Too small accelerations possible
- Capability sensitive steering
  - Actuation into behaviour
  - Common attuation:
    - Humans
    - Cars/motorbikes
      - Decision arcs

## Aiming and Shooting

Requirements:

- Shoot
- Respond

Prediction:

- Straight lines
- Trajectories
  - Gravity but no drag
  - Predicting landing spot
    - Projectile size
    - Avatar height
    - Ground changes
  - Drag
    - No parabolic arc
    - Iterative solution (make a guess)

## Jumping

Fail sensitive action

Jump supports:

- Jump points + minimum velocity set
  - Velocity matching SB
  - Weaknesses:
    - No info about difficulty
    - Not all failed jumps are equals
- Landing pads
  - Trajectory calculation

# Genetic Algorithm

Simulating population evolution

Operators

- Selection
  - Proportional
  - Mating Pool
- Crossover
- Mutation

Selection Types:

- Proportional
  - Convergenza prematura
  - Stagnazione
- Rank selection
  - Heavy
- Tournament
- Elitist

Crossover Types:

- Single point
- Double point
- Uniform

Process stop:

- Fitness below threshold
- After `n` iterations

# Deep Reinforcement Learning

Progress in E-Sports

- Team composed by AI players possible?

Machine Learning

- Unsupervised
- Supervised
- Reinforcement

OpenAI 5

- Long time horizon
- Partially observed stage
- Continuous action space
- Continuous observation space
- Technologies:
  - Five neural networks
  - Proximal policy optimization

Goal:

- Mimic training for good at competitive FPS
  - Hostile 3D environment
  - Random events
  - Leaderbord

How to:

- Machine Learning
- Neural Networks
- Procedural Generation of game prototype
- Agent training
- Result analysis

Environment:

- Scene perception
- AI of agents
- Brains (behaviours)
- Academy

System

- Proximal Policy Optimization
- Actions Space
- Rewards

Agent's perception

- Simulate human
  - Camera screenshot
  - Raycasting
  - Observation space
- Temporal dimension

Benchmarks:

- Learning phase
- Inference phase









