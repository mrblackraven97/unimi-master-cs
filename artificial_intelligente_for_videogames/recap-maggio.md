# Maggio

# Pathfinding

Space as Graph

- Simple
- Directed
- Oriented
- Mixed
- Multigraph
- Weighted

No need for:

- Negative weight (set 0)
- Undirected edges (double edge)

Dijkstra

- Not a pathfinder
- Get shortest path
- Algorithm
  1. Define 2 set of nodes (visited / unvisited)
     - Millington use 3 set
  2. Define tentative distance
  3. Select unvisited node with less tentative distance
  4. Process node
  5. Mark as visited
  6. Loop until unvisited list not empty
  7. Return minimal path

Uneven Terrain and Slopes

- Battlefields not always flat
- Downward > Upward

# A*

Evolution of Dijkstra

Algorithm:

1. Define two set of nodes (visited/unvisited)
2. Define tentative distance
3. Select unvisited node with **smallest cost**
4. Process node
5. Mark as visited
6. Loop until **goal reached**
7. Minimal path by **backtracking**

Estimation

- Define heuristics
- Dijkstra is the worst

Avoid loops

- Recompute distance

No proof of having best path

- Not exhaustive search, for performance reasons

Heuristics

- Underestimation -> Dijkstra
- Overestimation -> Suboptimal path
- Always underestimate a bit
- Precalculation
  - Keep cache of last N estimations
- Examples
  - Euclidean
  - Manhattan
  - Bisector / Full Bisector
  - Zero

Clustering Heuristics

- Cluster nodes in well-connected regions
- One big graph divided in regions
- Techniques
  - Define
    - By Hand
    - Graph algorithms
  - Lookup table
  - Different clusters can have different heuristic

Hierarchical Pathfinding

- Group nodes in different graphs
- Cost calculation problem
  - Minimum distance
  - Maximin distance
  - Average minimum distance
- Apply A* from the highest level graph (where start/end are in different nodes) to lower
- Progressive calculation
- Trimming pathfinding

# From Map To Graph

Division schemes

- Quantization and localization
- Generation
- Validity

Tiles

- Any size
- Mixed
- Generated at runtime
- Validity
  - Tiles 0/1 -> easy ok
  - Tiles partially -> depends
- Dirichlet Domains
  - Connection with delaunay triangulation
  - Validity not formally demonstrable

Points of visibility

- Inflection points
- Avatar shape to compute an offset
- Use of raycasting

Waking path

- Intermediate points between tiles
- Steering behaviour
- Smooth Algorithm

# Navigation Meshes

Game meshes are made of triangles and vertices

- Easy built-in tiling

Problems

- Not touching ground

Validity

- Problematic
  - Not all floor can be used
  - Size of NPC difficulty

No rigidbody, simulated physics

- Tune the steering behaviour

Can be static/dynamic

# Decision Making

Character deciding what to do

Planning is an action

Internal+external knowledge -> decision maker -> action -> internal+external changes

# Decision Trees

Simplest approach for decisions

Structure:

- Set of decisions
- Starting decision
- Acyclic graph / Tree
- Leaves are actions

Usage

- Start from root
- Explore decision reaching leaf
- Perform action

Boolean algebra can be applied

Complexity

- No memory for execution
- Linear time
- Bottleneck possible
  - Balancing trees
    - Actions often used at beginning

Avoid loops

Randomness

Sentinel example

# Finite State Machines

Switch between a set of states

Actions bound to a state

State linked by transitions (conditions)

Formal:

- Finite set of states
- Finite alphabet
- Transition function
- Initial state
- Set of accepted states

Game:

- Finite set of states
- Sensors to trigger transitions
- Initial state

Usage

- Periodically call function update
  - Check for transition
  - Find and perform actions
- Actions can be assigned to
  - Enter state
  - Firing transition
  - Exiting state
  - Staying in state

Complexity

- Always linear
- Alternative to Decision Tree

Examples:

- On/Off Sentinel
- Dimming Sentinel
- Alarm Sentinel
  - Alternative light problem

Alarm behaviours

- Suspend current FSM
- Hierarchical FSM
  - Example: Cleaning robot (with power management)
    - Two FSM
  - Multiple transition on different level
    - Hierarchy is priority
  - Implementation
    - Force transition on condition
    - Extend transition and how FSM is updated

Combining FSM and DT

- Replace transition with a DT
  - Leaf are transitions

# Behaviour Trees

Multiple techniques

- Hierarchical State Machines
- Scheduling
- Planning
- Action Execution

Not planning

Not decision making

- Blurry distinction

How to perform action

Building a BT:

- Task
  - Condition
  - Action
  - Composite
    - Selector
    - Sequence
- Behaviour
- Top-down approach

Random Selector

- Actions checked in the same order is boring
- Random action picked

Decorator

- Wrap tasks to modify behaviour
- Types
  - Filter
  - Limit
  - Until fail
  - Inverter
- Nesting is possible
- Usage as semaphore

Example: Bully Sentinel

- Multithreading Issue: solve with coroutines

Limits

- Unpractical for State Based Behaviours
  - Uses work pipelines
- Alarm behaviours are difficult
- Best: combine FSM with a BT for each state 

# Procedural Content Generation

Algorithmic creation of game content

- In game content
- Design content

Benefits:

- Reduced creation time
- Technically infinite
- Tailor to taste/need of player

Properties:

- Bounded computational time
- Reliability
- Controllability
- Minimum expressivity/diversity
- Beliavability

Taxonomy:

- Online - Offline
- Necessary - Optional
- Degree - Dimensional controll

Generating Spaces

- Common usage

- Dungeon creation

  - NPC + Decoration + Objects

  - Space partitioning

    1. Start with whole area
    2. Select random cell
    3. Split cell
    4. Loop
    5. For every cell create a room
    6. Connect corridors from lower level

  - Agent-Based Crawler

    1. Agent in random place
    2. Dig a tile
    3. Decision:
       1. Place a room
       2. Change direction
    4. Action done = set action probability to 0
    5. Action not done = increase probability
    6. Loop until satisfied

    - Improvement
      - Inform agent about surrounding

  - Cellular Automata

    - Large areas
    - Avoids square rooms
    - Huge diversity

Generating landscapes

- About heightmap
- Each terrain point is connected to adjacent ones
  - Interpolation
    - Bilinear
    - Bicubic

Perling Noise

- Mathematical model associating to each point a value dependent to surroundings
- Can be used for everything (1/2/3 dimension)

Gradient-Based Terrain Generation

- Interpolation height generator
- Use of perling noise
- Non lattice points height via four closest lattice neighbours
  - 4 heights to interpolate

Fractal Terrain

Agent-Based Terrain Creation

# Movement

Two ways:

- Kinematic
- Dynamic

Kinematic:

- Looking in the right direction
- Stopping at the right distance
- Avoid sinking

Dynamic:

- Move RigidBody
- Push RigidBody
  - Multiple for acceleration
  - Once per throw
- Acceleration for belivability
- Steering
  - Delegated Behaviours
    - Generate Directions
      - Align
      - Seek
      - Escape
      - Velocity Match
    - Steering on global acceleration
    - Bending
  - Problem
    - Bumping
    - Avoid obstacles
      - Use raycast
      - Lack of internal knowledge
      - Vision angle too small/large
        - Solution box-casting

Other

- Throwing
- Jumping
- No steer behavior (?)

# Flocking Behaviours

Composed by boid

- Behaviour for each boid
  - Align + Cohesion + Separation
  - Emergent Behaviour
- Field of view
  - Consider neighbours for steering

Problem

- Scalability
- Reuse memory
- Use simple and quick code

Beware of parameters

- Cohesion and separation cannot diverge too much
- Repulsion must not be too low
- View distance not too big

Improvements

- Change cohesion and separation over time
- Set different parameters for each boid
- Add some noise

# Markov Chains

Mathematical model for a sequence of events over time

State changes on probabilistic distribution

Memoryless

State of Markov chain =/= State where token located

Uses:

- Modeling transition
- Distribution of resources
  - Conservative
    - Model probabilities
  - Non Conservative
    - Model values/resources
    - Multiple transition matrices

Markov State Machine

- Decision tool
- Numeric values for states
- React to conditions and events by applying a specific transition matrix to its state
  - Timeout -> default transition

# Goal Oriented Behaviours

Goal-based decisions

Element

- Goal
- Insistence
- Action

Simple approach

- Find most insistent goal
- Find most rewarding action

Refine: 

- Discontentment
  - Insistence over goals
  - Select overall minimum
  - Apply non linear scaling
- Action timing
  - Not part of agent internal knowledge
    - Estimate change rates
      - Instantaneous change rates
        - Gaining experience

Planning

- Find best sequence over time
- Goal Oriented Action Planning
  - Actions
    - Inter-dependent
    - Sharing resources
    - Constrained by time
  - Utility
    - Find minimum combination of `k` steps
      - Deep-first visit
  - Exclude actions with high discontentment

GOAP with IDA*

- Find goal in graph
- Best path redefined
- Problem:
  - Possible goal unachievable (infinite)
  - Go as close as possible
  - Iterative Deepening A*
- IDA*
  1. Set threshold
  2. DFS exploration
  3. Stop if goal reach
  4. Change  threshold and loop

# Rule-Based Systems

Define agent evolution with rules

Same as DTs + FSMs

Architecture:

- Rule
  - Condition
  - Action
- Arbiter
- Database

Define rule syntax

- Use C# delegates
- Only precompiled

Database

- All NPC knowledge (int + ext)
- Contains all game-related data
  - Set of couples (k, v)
  - Datum object

Actions can change database

Rules arbitrations:

- First applicable rule
- Least recently used
- Random
- Most specific
- Dynamic priority (based on context)

Rules unification

- Define a rule as a recurring pattern

Rete

- Matching rule algorithm
- Rules in DAG
  - Direct clauses or pattern nodes
  - Join or combined nodes
  - Rules
- Database applied at the top to reach bottom rule
- Binding union (not boolean)

# Machine Learning

Perform tasks without explicit programming or rules

Built over a Neural Network

- Many layers
- Classification
- Training
  - Unsupervised learning
  - Supervised learning
  - Reinforced learning

Steps:

1. Gather data
2. Explore data
3. Prepare data
4. Build, train, evaluate a model
5. Tune parameters and train again (if need)
6. Deploy model

TensorFlow

- Machine learning platform
- Open source
- End-to-end
- In Python















