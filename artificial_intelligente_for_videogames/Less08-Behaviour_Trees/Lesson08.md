# Behaviour Trees

A Behavior Tree is a convenient way to create a character AI. It's a convergence of several techniques:

- Hierarchical State Machines;
- Scheduling;
- Planning;
- Action Execution.

They are originally a software engineering solution:

- Friendly to non-programmers;
- Easy to read and understand.



## What is a Behaviour Tree For?

We already decided what to do, so this is *not for decision making*.

We already planned ahead about actions, so it is *not for planning*.

We have to perform an action following a believable behavior:

- How many ways do you have to "open the door"?
- How many ways do you have to "cross the street"?

Edges are blurry... once again. Behaviour Trees are not a decision-making strategy (and the book confirms that), nevertheless, while you are walking a tree, you end up taking small decisions and planning in order to take out the action:

- If the door is locked, then bash it;
- If there is a guard, then take a detour;
- To fire, load, lock and pull trigger.

The distinction is sometime a matter of personal taste:

- My own personal taste suggests that since everything is hard-coded in the tree structure, then we are not actually planning or deciding much;
- You are free to have you idea but Maggiorini doesn't care so tell him what he wants to hear and stfu.



## Building a Behavior Tree

The basic building block is a "task":

- A definition about a simple "something to do";
- (Mostly) Self-contained;
- With a standard interface.

Tasks can be combined (in a small tree) to describe complex actions.

Actions can be combined (in a larger tree) to describe a behavior.



## Anatomy of a Task

A task will "do something" and report success/failure. Tempted to implement using a boolean function?

- How about returning error codes?
- How about integrating with the scheduling system?

There are three types of tasks:

1. Condition - To test a property (from the internal or external knowledge).
2. Action - To alter the state of the game (change something in the internal or external knowledge).
3. Composite - The behavior is based on the behavior of its children (organized as a small tree).



## Composites

A selector for a fighting behaviour:

- The NPC will attack, if that is not possible, will taunt. If taunt will not be possible as well, it will just stare to the enemy.
- If all the actions fail, the fight will fail.

![](img/comp.png)

A sequence for an escape behaviour:

- The NPC will check if the enemy is visible. If so, it will turn around and then run away.
- If one of the actions fails, the escape will fail.



## Sample BT

![](img/sample_bt.png)

![](img/sample_bt2.png)

![](img/sample_bt3.png)

![](img/sample_bt4.png)

![](img/sample_bt5.png)

Pseudo-code equivalent:

```pseudocode
if is_locked(door)
	move_to(door)
	open(door)
	move_to(room)
else:
	move_to(room)
```

Refactoring is possible, just apply some boolean algebra rules. Then, we discover that out tree was sub-optimal and can be reduced.

```pseudocode
if is_locked(door):
	move_to(door)
	open(door)
move_to(room)
```

![](img/door_bt.png)

![](img/door_bt2.png)

![](img/door_bt3.png)

Behavior is:

- If the door is open, we enter the room.
- Otherwise, we move close to the door and:
  - If it is locked, then we just open it;
  - Otherwise (like if it's stuck) we barge into it and check if it is now open.
- Then, we can enter the room.

If, after barging, the door is not open, the sequence will fail, and the sequence failing will make the selector failing, and the selector failing will make another sequence failing, and that will cause the whole tree to fail, and the AI will know the NPC was not able to carry on the action.



## Implementing Behaviour Trees

![](img/bt_impl.png)

![](img/bt_impl2.png)

![](img/bt_impl3.png)

![](img/bt_impl4.png)



## Random Composite Tasks

Performing actions and evaluating conditions always in the same order leads to a predictable behavior. To spice up player experience we may want to randomize execution order (granted that order is not important for correctness).

Randomizing only a subset may also be convenient and will generate the required effect. Always remember, complexity is in the mind of the player, you do not need to be complex in order to have the player perceive complexity (or chaos).

![](img/random_bt.png)

### Implementing Randomness

![](img/random_impl.png)

![](img/random_impl2.png)



## Decorators

As in software engineering, decorators wrap other tasks and modify their behavior. A decorator task has always only one child.

We have some standard decorators:

1. Filter - Execute the task if and only if a condition is verified.
2. Limit - Execute a task at most a limited number of times.
3. Until Fail - Keeps executing the task until it fails.
4. Inverter - Returns success when the child fails and vice-versa.

Of course, decorators can be nested.

![](img/decorators.png)

![](img/decorators_impl.png)

![](img/decorators_impl2.png)

![](img/decorators_impl3.png)

![](img/decorators_impl4.png)



### Decorators as Semaphores

Some tasks may use limited and/or mutually exclusive resources. Running a task unconditionally might jeopardize data consistency.

Possible solutions:

- Hard-code the test in the behavior. This will make your implementation much less portable.
- Create a condition task to perform the test and use a sequence. But then, how do you ensure data consistency while performing a long sequence?
- Use a decorator to guard the resource. Make sure the resource is accessed in mutual exclusion only when you need it.

From a C# standpoint, use the `System.Threading.Semaphore` class inside the decorator and you will be fine.

**N.B.** The above considerations do not hold in Unity.



## A Bully Sentinel

A sentinel is hidden, waiting for an intruder. When the intruder comes closer than the sensing range (10 meter) the sentinel pops up and start chasing the intruder (chasing means going as close as 5 meters from the intruder).

If the intruder engages the sentinel, the sentinel escapes and calls for backup (engaging means to go closer than 4 meters to the sentinel).

![](img/bully.png)

![](img/bully2.png)

![](img/bully3.png)

![](img/bully4.png)

![](img/bully5.png)

![](img/bully6.png)



### Actions for Bully Sentinel

![](img/bully7.png)

![](img/bully_action.png)



### Conditions for Bully Sentinel

![](img/bully8.png)

![](img/bully_cond.png)



### Decorators and Composites for Bully Sentinel

![](img/decors_comps.png)



## Final Result: STUCK

![](img/stuck.png)

Why is this not working? Unity, as already said many times, is executed as a single thread due to data integrity and performance reasons.

When you start the game, the Behaviour Tree is evaluated and, when it start waiting for the player to get closer than the sensing range, the control is never released back to the engine.

The really bad news is that you cannot run it as a multi-threaded application:

- Multi-threading is possible in Unity, but only the main thread can interact with scene objects;
- You can defer computational work (typically number crunching) to secondary thread, but nothing more than that;
- You cannot spawn a secondary thread to evaluate the tree since you will not be able to "sense" what is on the scene (ex. the distance between sentinel and player).

![](img/stuck2.png)



## How to Make it Work?

Our only option is to revert to coroutines and use the main thread for the rest of the game. Nevertheless, we cannot just evaluate the tree from a coroutine, because the coroutine is run inside the main thread and we will get stuck... again.

We must create a coroutine to evaluate the tree one step at a time and releasing each time control to the main thread.

First, we must modify the code to make it coroutine-compliant:

![](img/coroutines.png)

![](img/coroutines2.png)

![](img/coroutines3.png)

![](img/coroutines4.png)

![](img/coroutines5.png)

![](img/coroutines6.png)

![](img/coroutines7.png)



## Limitations of Behaviour Trees

They are unpractical to represent state-based behaviors. Every time we start evaluation from the root node. There is no memory information.

Alarm behaviors are difficult to implement because that is requiring to break the tree syntax or to check for all possible alarms at every single step.

Behaviour Trees require to avoid thinking in term of states and use work pipelines. It's not impossible, just a bit difficult.

Best combination: use FSM with a BTs connected to each state. The current state of the FSM will determine which BT  the character is running (how to represent the current state of action).

















