# Pathfinding: A*

A*, pronounced *'A-Star'*, is an evolution of Dijkstra's algorithm. Actually, it is the same, but for a small detail.

This algorithm works like this:

1. We define two sets of nodes: visited and unvisited.
   -  Set the unvisited set as the complete list of nodes and the visited set as empty.
2. For every node we define a **tentative distance** value (from the start) and a predecessor node.
   - Set 0 to the start node and infinite for all other nodes.
3. Select a current visited node as the one in the unvisited set with the **smallest estimated total cost of the path to the goal**.
4. Process the current node:
   - Assign tentative distance, predecessor and estimate distance to the goal for all its neighbors only where the new tentative distance is lower than the current;
   - Neighbors' value will be the sum of local tentative distance and the weight of the edge leading to the neighbor.
5. Mark the current node as visited.
6. If the goal was not reached and the unvisited list is not empty go back to step 3.
7. Devise the minimal path by backtracking from the goal node.



## A* vs Dijkstra

There are two main differences:

- Instead of selecting as next current node the one closest to the starting point we select "the most promising one". For example, we select the node that, based on out estimation, is on the shortest path from source to destination.

  This will keep "pushing" the exploration toward the destination and consider alternate routes only if no direct way is found.

- We drop the exploration as soon as we reach the destination.

  This will avoid exploring the whole graph and same some time.

We must define a heuristic way to estimate distance to the goal. A good heuristic leads to a great result, while a bad one can compromise our AI.

**N.B.** The worst heuristic is Dijkstra, because it is "too perfect".

Our problem just changed to "finding a good estimation".



## Beware of Loops

Unlike Dijkstra, we may find shortest paths to already visited nodes. We are following a "lead": we may just get it wrong and discover a better way later. In such cases we must re-compute the distance of the node from the start and put it back in the unvisited list. That node will be re-evaluated and nodes on its outgoing edges might be as well.

Just be aware, looping is NOT going to happen all the times: it depends on the estimation we use.

The "found a better loop" issue applies also to the destination.

Even if we reached the destination there is no proof (without an exhaustive search) that we did it using the best possible path (and "exhaustive search" implies "falling back to Dijkstra", so better not to).

We can use two approaches:

1. Wait for the goal node to have the shortest distance from the start when compared to all unvisited nodes and be more accurate;
2. Terminate immediately and save time.



## Implementing A*

```c#
// This delegate allows us to set the estimation policy from the outside
// and avoid touching this code when changing heuristic
public delegate float HeuristicFunction(Node from, Node to);

public static class AStarSolver {
   
    // A flag to stop as soon as we reach the destination or to keep exploring
    // until we aresatisfied 
   public static bool immediateStop = false;
    
    // 1) Two set of nodes
    public static List<Node> visited;
    public static List<Node> unvisited;
    
    // 2) Data structures to extend nodes
    private struct NodeExtension {
        public float distance;
        // We extend the NodeExtension struct to accomodate the estimation
        // of the path traversing the node
        public float estimate;
        public Edge predecessor;
    }
    
    static Dictionary<Node, NodeExtension> status;
    
    // Thanks to the "delegate", we can pass the estimation function as a
    // parameter to the solver
    public static Edge[] Solve(Graph g, Node star, Node goal, HeuristicFunction heuristic) {
        // 1) Setup sets
        visited = new List<Node>();
        unvisited = new List<Node>(g.getNodes());
        
        // 2) Set all node tentative distance
        status = new Dictionary<Node, NodeExtension>();
        foreach(Node n in unvisited) {
            NodeExtension ne = new NodeExtension();
            // Since the distance is infinite, the estimation is also infinite
            // when tarting the solver. To simulate infinite we use float.MaxValue
            ne.distance = (n == start ? 0f : float.MaxValue);
            ne.estimate = (n == goal ? 0f: float.MaxValue);
            status[n] = ne;
        }
        
        // 6) Iterate until goal is reached with an optimal path
        // We need to perform a more complex evaluation here
        while(!CheckSearchComplete(goal, unvisited)) {
            // 3) Select net current node
        	Node current = GetNextNode();
            
            // Graph is partitioned
            if(status[current].distance == float.MaxValue)
                break;
            
            // 4) Assign weight and predecessor to all neighbors
            foreach(Edge e in g.getConnections(current)) {
                if(status[currend].distance + e.weight < status[e.to].distance) {
                    NodeExtension ne = new NodeExtension();
                    ne.distance = status[current].distance + e.weight;
                    // The estimate is calcualted as the current distance from the
                    // start and the value of the heuristic calculated on the node
                    ne.estimate = ne.distance + heuristic(e.to, goal);
                    ne.predecessor = e;
                    status[e.to] = ne;
                    // Unlike Dijkstra's, we can now discover better paths
                    // If we have outgoing edges leading to visited nodes, then we
                    // found a shortest loop and all outgoing nodes must be put in
                    // the unvisited set
                    if(visited.Contains(e.to)) {
                        unvisited.Add(e.to);
                        visited.Remove(e.to);
                    }
                }
                // 5) Mark current node as visited
                visited.Add(current);
                unvisited.Remove(current);
            }
        }
    }
    
    // Iterate on the unvisited set and get the lowest weight
    protected static Node GetNextNode() {
        Node candidate = null;
        float cDistance = float.MaxValue;
        foreach(Node n in unvisited) {
            // The selection of the best candidate is now based on the estimate
            // field of the NodeExtension struct
            if(candidate == null || cDistance > status[n].estimate) {
                candidate = n;
                cDistance = status[n].estimate;
            }
        }
        return candidate;
    }
    
    // This method is new in A*
    // Check if the goal has been reached in a satisfactory way
    protected static bool CheckSearchComplete(Node goal, List<Node> nodeList) {
        // Check if we reached the goal
        if(status[goal].distance == float.MaxValue)
            return false;
        // Check if the first hit is ok
        if(immediateStop)
            return true;
        // Check if all nodes in list have longer of same paths
        // If we reached the destination and did not stop immediately, then we must 
        // check all the nodes in the graph. If one or more nodes have a distance 
        // from the start shorter than the path we found, we must go on exploring
        foreach(Node n in nodeList) {
            if(status[n].distance < status[goal].distance)
                return false;
        }
        return true;
    }
}
```

Dijkstra's average result:

![](img/dijkstra_average.png)

Using A*:

![](img/astar_average.png)

A* in Unity

![](img/astar_unity.png)

How to get there:

```c#
public class AStarSquareBasic : DijkstraSquare {
    void Start() {
        if(sceneObject != null) {
            // Create a x*y matrix of nodes (and scene objects)
            // Edge weight is now the geometric distance (gap)
            matrix = CreateGrid(sceneObject, x, y, gap);
            
            // Create a graph and put random edges inside
            g = new Graph();
            CreateLabyrinth(g, matrix, edgeProbability);
            
            // Ask A* to solve the problem
            // This is the only change we need from DijkstraSquare
            Edge[] path = AStarSolver.Solve(g, matrix[0, 0], 
                                            matrix[x-1, y-1], EuclideanEstimator);
            
            // Check if there is a solution
            if(path.Lenght == 0) {
                UnityEditor.EditorUtility.DisplayDialog("Sorry", "No solution", "OK");
            } else {
                // If yes, outline it
                OutlinePath(path, startMaterial, trackMaterial, endMaterial);
            }
        }
    }
    
    private float EuclideanEstimator(Node from, Node to) {
        return (from.sceneObject.transform.position - 														to.sceneObject.transform.position).magnitude;
    }
}
```



## Choosing a Heuristic

The best choice is a heuristic which can predict the exact minimum path between each node and the destination. This is not possible thou, because it implies a previous knowledge of the result, and if i already know the result there is no point in running the algorithm.

Underestimation:

- A* will be biased toward exploring nodes close to the start;
- If we always underestimate, A* will converge to Dijkstra (setting the estimation to 0 is replicating Dijkstra);
- Underestimating is good for accuracy but bad for time.

Overestimation:

- A* will be biased toward the heuristic;
- Will create sub-optimal paths with fewer nodes and higher total cost;
- If the heuristic overestimate is bounded by X, so it is the sub-optimality of the path.



## Back in the Loops

Loops cannot occur in those cases where we can demonstrate A* is optimal (ex. A* can always find the optimal solution).

It can be demonstrated that A* is optimal if the heuristic we are using is admissible. An admissible heuristic is **always underestimating** the shortest possible path to the destination. An admissible heuristic is optimistic about the distance to cover.

While this is fine from a mathematical standpoint, we cannot make assumptions about the function our developer is going to implement.

From a software engineering standpoint, we must assume the heuristic is not admissible unless it is hardcoded (and verified) inside the A* implementation.



## Heuristic Pre-Calculation

Performance question: is it ok to perform the estimation all the time for every node? Could it be better for us to compute the estimation on startup for each node and then consider it as a feature of the node?

Yes, this can be done, but there are a number of reasons not to do so:

- Estimation might change dynamically based on external parameters (ex. the amount of fuel the NPC has);
- If the map is huge, it is not going to improve performances, especially if the NPC starts mid-way from the border and the destination;
- We must do a round of computation for every possible destination (for a free-roaming map that is going to be a big CPU and memory hog).

What we can do, if the heuristic is computationally intensive, is keeping a cache of the last N estimations. Locality will help you a lot, unless your NPC is teleporting around.



[Insert "how to get there" code]



## Let's Try Different Estimators

In the following slides, we will test some basic heuristics. You can reproduce the same results in unity setting the `StarSquare` component as follows:

- X = 20;
- Y = 20;
- Edge Probability = 0.5;
- Random Seed = 18238.

The heuristic we will present are neither *standard* nor *universal*, they are just easily implementable given the context.

Every heuristic must be modelled based on the intended behavior of the NPC.

- The way you perform the estimation must be in line with the way the NPC should "think about reaching the destination".
- This is will add a lot of realism to your NPCs.



## Euclidean Estimation

The geometric distance to the destination

``` c#
protected float EuclideanEstimator(Node from, Node to) {
    return (from.sceneObject.transform.position -
            to.sceneObject.transform.position).magnitude;
}
```

![](img/euclid_est.png)

Not only we explore less nodes, but the resulting path is different (less optimal)

![](img/astar_euclid.png)



## Manhattan Estimation

It is the distance along the axes

```c#
protected float ManhattanEstimator(Node from, Node to) {
    return(
    	Mathf.Abs(from.sceneObject.transform.position.x - 
                  to.sceneObject.transform.position.x) +
    	Mathf.Abs(from.sceneObject.transform.position.z - 
                  to.sceneObject.transform.position.z)
        );
}
```

![](img/manhattan_est.png)

In this case, we have a slightly better solution than using the Euclidean estimator, but we visited more nodes

![](img/astar_manh.png)



## Bisector Estimator

It's the distance to the line connecting the origin and the goal

```c#
private float BisectorEstimator(Node from, Node to) {
    Ray r = new Ray(Vector3.zero, to.sceneObject.transform.position);
    return Vector3.Cross(r.direction, from.sceneObject.transform.position -
                         r.origin).magnitude;
}
```

![](img/bisector_est.png)

As we can see, compared to the Manhattan estimation, we push more toward the center of the field

![](img/astar_bisec.png)



## Full Bisector Estimator

It's the distance to reach the line connecting the origin and the goal and then the goal.

```c#
private float FullBisectorEstimator(Node from, Node to) {
    Ray r = new Ray(Vector3.zero, to.sceneObject.transform.position);
    Vector3 toBisector = Vector3.Cross(r.direction, 
                                       from.sceneObject.transform.position - r.origin);
    return toBisector.magnitude + (to.sceneObject.transform.position -
                                   (from.sceneObject.transform.position + toBisector)
                                   ).magnitude;
}
```

![](img/full_bisec_est.png)

We get the same result as when using the bisector estimator, but surprisingly we optimise a lot the exploration. It looks like a more complex model might be of help (at least in this case).

![](img/astar_full_bi.png)



## Zero Estimator

Always returns zero.

```c#
protected static float ZeroEstimator(Node from, Node to) {
    return 0f;
}
```

This is not really useful (as a heuristic) but can be used as a benchmark to compare the performances of other estimators with Dijkstra without changing application.



## Some Considerations

Someone might ask: "How comes when i do not stop at the first hit, i always end up exploring all the graph?"

That is a problem of our setup:

1. The nodes are usually well connected. Default edge probability is 0.75 and many nodes have 4 edges;
2. The map is small and regular, so it is difficult to have a sensible variation between the estimation (any estimation) and the actual minimum path.

**Exercise**: Recreate the "Mountain" environment for A* (hint: you may want to define another heuristic).



## Clustering Heuristics

In complex maps heuristics can be difficult to compute. A possible alternative is to divide nodes in groups (clusters) and create a table reporting the proximity between each group (lookup table). THis approach is similar to the BGP protocol used in networking for inner-AS routing.

![](img/clust_heur.png)

In a graph, we usually cluster nodes belonging to the same well-connected region.

Two ways to define clusters:

1. By hand (old school);
2. Using graph algorithms (easier, but must be supervised, it will create an output that an operator must check).

Then, we compute the lookup table, a table indicating the minimum distance between each pair of clusters. This activity is usually performed offline.

Clusters are used as part of a heuristic strategy:

- Same cluster --> Use euclidean distance;
- Different cluster --> Use the lookup table.

**N.B.** They are both underestimations!

Keeping small clusters will dramatically improve performances, especially in indoor settings.

**N.B.** This is not the same as hierarchical pathfinding!



# Hierarchical Pathfinding

Goal: go to the office

1. Go to the parking lot;
2. Drive car;
3. Park;
4. Go to your desk.

Goal: go to the parking lot

1. Exit home;
2. Cross the street;
3. Find car;
4. Unlock car;
5. Open door;
6. Sit in the car.

Goal: Drive car

1. Start engine;
2. Put drive gear;
3. Press gas;
4. Go to the exit;
5. Check lanes;
6. Enter road;
7. **Follow way to office**.

We can group nodes and manage them as a single location for the purpose of a higher level pathfinding.

**N.B.** This is not clustering, because we have more than one graph.

![](img/hierarch_path.png)

![](img/hierarch_path2.png)



## Connecting Groups

Groups (as nodes in a higher-level graph) must be connected if there is at least a couple of lower-level nodes with an edge linking them.

Problem: Calculating edges cost may be a pain. It can be done:

1. Manually;
2. Using costs from lower level graph.

![](img/cost_calc.png)



## Algorithms for Cost Calculation

Minimum distance:

- The minimum weight of all edges linking the two groups;
- This is what our A* algorithm will likely use;
- An underestimation (and we like it).

Maximin distance:

- We calculate the shortest path inside the area between each incoming node and the outgoing node, and then we take the maximum. This will be representative of a "maximum traversal cost" of the area to go toward the outgoing node.
- We set the weight of the outgoing edge as the traversal cost plus the link cost toward the outgoing node.

Average minimum distance:

- Same calculations as in the maxmin distance, but values are averaged instead of taking the maximum.

![](img/heurs.png)



## Performing Pathfinding

Basically, we apply A* (or Dijkstra) starting from the highest-level graph.

The result from each run are used to trim down nodes in the next level.

To avoid solving trivial problem, we should start from the highest level where start and goal are not in the same node (level 2).



## Progressive Calculation

Hierarchical pathfinding is suitable for progressive calculation:

1. We calculate the path on level N;
2. For each level M < N we calculate the path only for the first edge of level M+1;
3. We can start navigation and then calculate other edges only when needed.

**N.B.** Errors in cost evaluation play a major role here. A negligible error on a large scale may force the NPC to create a very unrealistic path at some lower level.

On the other hand, we can avoid progressive calculation and run a higher-level pathfinding in full. THe result will be used to exclude nodes in the lower-level computation.

While this can greatly boos performances, excluding nodes may prevent us from finding optimal or more realistic paths

![](img/trimming.png)

Since we have heuristics, we must live with the fact hierarchical pathfinding is giving us an approximated solution. There is no "golden heuristic", working in every single case.

![](img/errors.png)

![](img/errors2.png)

