# Designing AI for Games

In this lesson if will be explained how different techniques used from classic AI can be exploited to obtain specific behaviour for videogames.

This lesson goal is to understand how different techniques and algorithms in the field of Artificial Intelligence can be profitably exploited to obtain specific gameplays.

# Designing AI for a Video Game

Designing AI for a videogame is all about convincing players that they are dealing with a "real intelligence", we trick the player.

Differently from classic AI, here we are **not interested about optimization** (having a car that have always a perfect drive won't be fun). In games, AI should challenge the player and then lose. Also we cannot wait much time to let the agent take a decision, we need instant behaviour and we have a limited computational resource.

AI for videogames should emulate a human behaviour... which is defensively not perfect nor optimized.

AI in games fulfills two main purposes:

- Assisting gameplay (helpful NPCs);
- Enhancing immersion (enemies), include psychology of the agent(s).

There are many types and uses of AI, several bey become **crucial for the success of the game**. When using agents, the Game Designer defines:

- The psychology of the creature(s);
- Where to place them;
- How to make their behaviour readable/scary/etc.;
- How to avoid predictability;
- ...

There is a list of behaviors given from the game designers that has to be implemented by the developer (included in the game design document). The AI design process (ideally) consists in:

1. Work out **desired behaviours** from the Game Design Document;
2. Select the **simplest set of techniques** supporting them:
   1. Plan how to integrate them;
   2. Plan how to integrate AI and the game engine;
   3. Put placeholders for character behaviours.
3. Start work.

This is the ideal procedure, but in real time this process is heavily influenced by publisher milestones and quick and dirty code packed in the final version of the game.



## 1. Evaluating Characters Behaviour

Every character's behaviour follows a description made by the game designer, so its important to read and understand that description and work out those behaviours.

Behaviours are not set in stone, they evolve during the game development, they can change during testing, so flexibility is very important. This is the reason why is also important do to code optimization only at the end (when possible).

There are three main groups of question to answer in order to develop a good AI:

1. **Movement**:
   - Individuals or groups?
   - Degree of realism?
   - Any physical simulation needed?
   - Any pathfinding needed?
   - Characters' motion influenced by other characters?
2. **Decision Making**:
   - Full range of different possible actions?
   - How many distinct states for each character (how actions will be grouped together)?
   - When and why will the character change its behaviour?
   - Characters need to look ahead to make decisions?
   - Character decision are influenced by players actions?
3. **Tactical and strategic AI**:
   - Characters need to understand the game on a large-scale?
   - Characters work together?
   - Characters think for themselves and still display a certain pre-defined group behaviour or not?
   - Do you need decisions for specific groups of characters?



## 2. Selecting Techniques

Once the behaviours are set, we have to select candidate techniques to implement them.

Critical point: **decision making system**. There are many different approaches, but try first with the simplest one (behaviour trees, state machines), and avoid complex exotic unless it's really necessary.

It exists a large zoo of AI techniques... better avoiding to get lost. Try to **understand how the bits fit together** by trying to fit each technique into a general structure for making intelligent game characters (it is often needed to implement different techniques together).

![](img/gen_purpose.png)

![](img/gen_purpose2.png)

### AI Model for Videogames: Strategy

Strategy is what **coordinates a whole team**. 

Each character has its own decision and movement algorithm, but their decision making will be influenced and guided by a group strategy

*Example*: Half-Life and Tom Clancy's Ghost Recon are among the first examples of enemies which coordinates in order to surround effectively the player.

### AI Model for Videogames: Decision Making

Decision making is when a character **works out what to do next**.

Decision making algorithms can be:

- Simple, like an animal that moves away when the player gets near;
- Complex, obtained by chaining multiple simple actions.

Each character has a range of possible behaviours. Once the behaviour has been chosen it can be executed by animation and movement techniques (ex. a melee attack) or even have no visual effect (ex. Civilization).

### AI Model for Videogames: Movement

Movement is processed by algorithms that **turn decisions into some kind of motion**.

Movement algorithm can be:

- Simple, like homing in;
- Complex, like sentinel detecting the player raises an alarm in Splinter Cell.

**N.B** A lot of actions are carried and described out using animation directly.

### AI Model for Videogames: Infrastructure

In order to build AI for a game, algorithms alone are not enough:

- Movement requires animation and physics simulation;
- AI needs information about the game world (perception) and current game state;
- AI must be managed to optimize CPU and memory usage, this implies taking into consideration different set of algorithms according to the situation.

# AI and Shooter Games

AI in shooters is quite complex, you need the character to perform:

- Movement (control enemies);
- Firing (accurate fire control);
- Decision making (generally state machines);
- Perception (who to shoot and where they are);
- Pathfinding (NPCs plan their route);
- Tactical AI (NPCs find safe positions or ambush).

AI Architecture for shooter games:

![](img/shooter_ai.png)



## Movement

Movement is the most visible part of character behaviour.

Shooters have the most complex sets of animations (tens of hundreds of sequences combined, plus inverse kinematics and ragdoll physics).

Moving around the level is a challenge, AI needs to first work out the route and then break motion into animation.

Possible approaches (more diffused):

1. AI split in two parts:
   1. Pathfinding;
   2. Animation Management.
2. Games using scripting languages use the same controls as the players: AI needs to specify only direction, velocity, weapon changes, etc.

In a **indoor** dynamic setting, characters have to find a way in constrained settings while reacting to other characters/player moves (ex. pathfinding + repulsion force between all characters similar to collision systems).

In **open areas** (ex. space) there is pathfinding + collision system + formation motion system.

**N.B.** The "floor" can be the ceiling as well.



## Firing

It's important to guarantee a good trade-off between accuracy and fun:

- DOOM - Criticized for excessively accurate aiming, developers had to slow down projectiles to assure players a possibility to survive;
- Medal of Honor: Airborne: Uses firing models allowing characters to miss in exciting ways.



## Decision Making

Commonly achieved through:

- **Finite state machines**: Not every action is reachable, only the ones connected to the current state;
- **Behaviour trees**: Any decision can be reached through the tree.

Common approach: bot scripting system. Script (in game-specific language) that polls the current game state and asks actions to be executed (animations, pathfinding, movement). Scripting language may be available to players (Unreal).

![](img/ai_ex.png)

**Emergent behaviour** different in each play through in "Sniper Èlite":

- Range of finite state machines operating on waypoints of the level;
- Behaviours dependent on actions of other characters and tactical situation at nearby waypoints;
- A bit of randomness in the decision making process added "credibility" and an apparent cooperation (but no squad-based AI was there).

Autonomous AI in "No one lives forever 2" was achieved by blending FSMs and goal-oriented behaviours:

- Each character has a pre-determined set of goals which influence its behaviour;
- Periodically, the character evaluates goals to select the most relevant in that moment, which takes control of its behaviour;
- Inside each goal there is a FSM;
- Waypoints are used to check whether the character is in the correct position for a behaviour (firing, using a PC, etc.);
- Waypoints nearby are used by the character to evaluate which actions are available.



## Perception

May be faked by putting a radius around each enemy: it'll "come to life" whenever player enters the circle (ex. Doom).

More sophisticated approaches may require a "real" sense management system.

Let's see several examples.

### Ghost Recon

In "Ghost Recon" the sense management system informs the AI characters about:

- Amount of broken cover provided by bushes and similar;
- Amount of camouflage by testing character against its background. The line of sight goes straight to the character and then beyond it, till the next collision: the id of the two materials are compared to determine the level of concealment.

### Splinter Cell

In "Splinter Cell":

- Each AI checks if the player is visible, taking into account dynamic mist, shadows, etc., but the background is not taken into account;
- When the concealment level drops under a certain threshold the player is spotted;
- AI also uses a cone-of-sight and a sound model to spot players.



## Pathfinding and Tactical AI

Developers use a wide range of representations for pathfinding, like:

- Waypoints;
- Navigation meshes (internal spaces), also known as "navmeshes";
- Hierarchical pathfinding.

Let's see several examples.

### Navigation Mesh

![](img/navmesh.png)

The graph is based on polygons: each polygon is a node, that may be interconnected to neighboring ones. Generally polygons are triangles/quadrilaterals, with 3 or 4 interconnections at most.

### Soldier of Fortune

In "Soldier of Fortune" links in the pathfinding graph are marked with the type of action needed to traverse them. Gives the illusion the agent reacts to the type of terrain. This is an example of "embedded navigation".

### Half-Life

In "Half-Life" the AI uses waypoints to figure out how to surround the player



# Shooter-Like Games

A number of genres (not shooters) share similar characteristics with shooters:

- Single player;
- First/third person view;
- Presence of enemies.

They use very similar AI techniques.



## Platforms and Adventures

Some example of shooter-like game genres are:

- **Platform**:
  - (Usually) younger audience;
  - Make enemies interesting but fairly predictable.
- **Adventure**:
  - Enemies are puzzle to solve (ex. discover when an enemies lowers its shield, etc.).

Let's confront characteristics:

- **Movement**: Is similar to FPS, but:
  - In platform also flying enemies (2D or 3D movement algorithms);
  - In adventure lots of animations to communicate character behaviour.
- **Pathfinding**: Generally not necessary.
- **Decision Making**: *Normal* vs *Spotted the player*
  - Normal = Patrolling, random movements, set of animations;
  - Spotted = Seed/pursue.

### MMOs and MOBAs

Server is always running on a dedicated hardware (it must be protected from the players).

Marginal need for AI (the challenge are other players), but in very large environment and for very large quantities of agents. Beware of **scalability issues** for pathfinding and perception.

- Pathfinding: Pool planners, hierarchical pathfinding, instanced geometry (A* simply won't work with many mobs).



# AI and Driving Games

One of the most specialized, genre-specific AI, with crucial tasks focused on movement.

Player will judge the AI on the basis of how well it drives the car. Generally realistic goal-seeking behaviour, clever tactical reasoning/route finding are not necessary.

Two main sub-genres:

- Race driving;
- Urban driving.

![](img/drive_ai.png)

So, how does racing games movement works? There are two main options (+ many variations):

- **Racing lines** - Those are *splines* with data attached, and there is no steering behaviour needed, level designer defines the spline and data (speed, steering to get back to the line if needed, etc.). This happens for examples in games like:
  - *Formula 1*;
  - *Grand Theft Auto 3*.
- **AI drives the car** - More recent approach, may be coupled with splines:
  - Movement can be +/- realistic, according to the player expectations;
  - Physics should be the same for AI and player (critical issue);
  - May be coupled with splines, where AI tries to follow the line (always manually designed), but it is not "on a rail";
  - Special steering behaviour for overtaking, on a long straight (slower cars) vs on corners (F1);
  - Approach "chase the rabbit".

![](img/spline.png)

In maths, a spline is a function that is piecewise-defined by polynomial functions, and which possesses a high degree of smoothness at the places where the polynomial pieces connect (which are known as **knots**).



## Special Approaches to Movement

"*Manic Karts"*:

- Fuzzy decision making instead of racing lines.

"*Forza Motorsport*":

- Neutral networks to learn how to drive by observing humans;
- The shipped game was the results of hundreds of hours of training.

"*Driver*" introduced a new genre:

- Set in city, goal is to avoid/catch other cars;
- No predefined fixed racetracks;
- Enemies catching the player.
- Solutions:
  - AI follows path for escaping/performs homing-in algorithm for catching player;
  - Cars created only in the surrounding block (more added when needed);
  - Pathfinding for catching the player;
  - Tactical analysis for escaping routes or to surround the player.

![](img/urban_drive.png)



## Driving-like Games

The same approaches used for driving games can be applied to:

- Extreme sports game:
  - Racing line + tricks sub-game (ex. a combo to be performed during jumps).
- Futuristic racers:
  - Racing + weapons;
  - Aiming and firing system;
  - Decision system (slow down to be overtaken and target the player, etc.).

# AI and Real Time Strategy Games

"*Dune II*" - Not the first RTS, but created a new genre (ore diffused on PC), whose key needs for AI are:

- Pathfinding;
- Group movement;
- Tactical and strategic AI;
- Decision making.

![](img/rts_arch.png)



## RTS Pathfinding

Early RTS ("*Warcraft*", "*Command and Conquer*", ...) were based on huge tiled grids, with a focus on efficient and quick pathfinding.

Recent RTS use arrays of heights (*heightfield* or *heightmaps*) to render the landscape, that are used also for pathfinding (in some case pre-processed).

Several RTS use also deformable terrain ("*Company of Heroes*") increasing pathfinding difficulties (no pre-computation possible).

### Heightfield (or Heightmaps)

![](img/heightf.png)

Heightfield are raster images used to store values:

- Surface elevation data, for display in 3D;
- Bump mapping to calculate where 3D data would create shadows;
- Displacement mapping the heightmap is converted into a 3D mesh.

Widely used in terrain rendering software and videogames.

Ideal way to store digital terrain elevations: Require substantially less memory than a regular polygonal mesh.



## RTS Group Movement

Main issue: group movement.

Older RTS:

- "*Kohan: Ahriman's Gift*" and "*Warhammer: Dark Homen*" units are collected into formations (with size limit) and moved as a whole. Motion-system has pre-defined patterns.
- "*Homeworld*" formation extended in 3D, no size limit (need for scalability), solved with scalable formation with different slots according to group numerosity.

Main issue: group movement management.

More recent RTS:

- "*Full Spectrum Warrior*" formation depended on the features of the level (single linear near a wall, double behind a cover, etc.). Player has only indirect control on the group: formation pattern decided by the AI and units move independently and cover each other if needed.



## RTS Tactical and Strategic AI

RTSs introduced **influence mapping**

Output of tactic/strategic AI used for:

- Guiding pathfinding: units in "*Total Annihilation*" took into consideration terrain (ex. rocky formation) when moving.
- Selection of location for construction, but city walls are an open issue (often pre-designed by a level designer): In "*Empire Earth*" AI used influence mapping + spatial reasoning to put walls between valuable areas and enemies.
- Large scale troop maneuvers: in "*Empire: Total War*" AI directs units towards the enemty and tries to avoid cannons and attacks.

![](img/caesar.png)



## RTS Decision Making

Multiple AI levels for decision making:

1. Individual unit;
2. Group;
3. Whole side.

For each level an AI component is created, Complexity varies from game to game.

AI modules can be or not independent (ex. in "*Warcraft III*" the central AI may decide to play in an offensive style)

Decision making technology: FMSs, decision trees, Markov and probabilistic methods, set of rules, ...

### Example: Zeus Master of Olympus

Multiple AI levels for decision making:

1. Individual unit (heroes, gods, monsters);
2. Groups (groups of citizens, merchants);
3. Whole side (enemy's armies).



# AI and Turn-Based Strategy Games

Older turn-based strategy games:

- Variant of existing board games ("*3D Tic-Tac-Toe*");
- Minimax techniques were often enough.

More recent, highly complex turn based strategy games ("*Cid Meier's Civilization*", "*3D Worms*") have almost unlimited number of possible moves, there is a similarity with RTSs games:

- Pathfinding, decision making, tactical and strategic AI;
- Quite simple movement algorithms.

![](img/turnb_games.png)



## TBSGs Timing

Both player and computer have longer time to think than in RTS, but AI has to compete with a higher level of human thinking;

- Game/level designers must try to simplify as much as possible the AI job by, for example, creating levels easy to tactically analyze, research trees easily searchable, and so on;
- Beware of the full range of decisions to take: economic system, research tree, movement, resource management, etc.



## TBSGs Helping the Player

TBSGs should also help the player to automate boring tasks:

- Players of "*Master of Orion 3*" can assign a number of decisions to the AI;

An assistance AI implies:

- Having decision tools which have no (or little) strategic input from higher levels;
- Understanding the player strategy.



# AI and Sport Games

The main AI needs for sport games are distributed among a huge range of different games:

- "*Madden NFL 2009*";
- "*World Championship Pool 2004*";
- "*FIFA*" series.

Huge body of knowledge ready to use for generating good strategies but:

- Not always easy to encode;
- Players expect human-like competencies;
- Team sports require appropriate team coordination.

Multi-level AI is requested:

- Higher level: strategic decision;
- Middle level: coordinated motion system (play patterns);
- Lowers level: single unit.

![](img/sport_arch.png)



## Sport Physic Prediction

Physic prediction is how balls movement must be predicted in order to allow AI to make decision (ex intercepting the ball):

- Complex dynamics integral to the game (pool, golf), where physics should predict the result;
- Simpler dynamics (soccer, baseball), where predicting the trajectory could be enough.

Same solutions used for firing weapons can be used to predict ball movement.



## Playbooks and Content Creation

Playbooks is a set of movement patterns used by teams in specific circumstance. May refer to:

- The whole team;
- Smaller groups of players.

Algorithms for making sure that characters move at the correct time.

Formation motion system.